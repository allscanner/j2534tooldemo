@echo ==================================
@echo SET VisualStudio PATH
@echo ==================================
call "%VS110COMNTOOLS%\vsvars32.bat

@echo ==================================
@echo Clean VisualStudio Solution
@echo ==================================
devenv J2534ToolDemo.sln /Clean Debug
devenv J2534ToolDemo.sln /Clean Release
rmdir Data\*.* /s /q
rmdir Debug /s /q
rmdir DATA /s /q
rmdir Release /s /q
rmdir J2534ToolDemo\Debug /s /q
rmdir J2534ToolDemo\Release /s /q
del J2534ToolDemo\*.aps
del *.sdf
del *.log
rmdir ipch /s /q

PAUSE