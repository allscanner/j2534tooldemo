rem ************************************************
rem * Script to compile the solutions of .sln     *
rem * Created by: Yuanmin                         *
rem * Created 2016.07.16                          *
rem ************************************************


rem Set environment variables**********************************************************
set _devenv="%VS110COMNTOOLS%..\..\Common7\IDE\devenv.com" 

rem Set compile log********************************************************************
set _log="%~dp0compileResults.log"

echo [%DATE% %Time%] Start compile sequence >%_log%
echo Used compile configuration is %buildAnyCPU% >>%_log%

rem Start compile**********************************************************************

@set str1=%cd%
@cd..
@set str2=%cd%

@:next  
@if not "%str2%"=="" (  
    set /a num+=1  
    set "str2=%str2:~1%"  
    set "str1=%str1:~1%"
    goto next  
)

set _solution_file="%~dp0%str1:~1%.sln"

%_devenv% %_solution_file% /build "Debug|Win32" /Out %_log%
%_devenv% %_solution_file% /build "Release|Win32" /Out %_log%
if not %errorlevel% == 0 echo %_solution_file% failed!   Error: %errorlevel% >>%_log%
if %errorlevel% == 0 echo %_solution_file% compiled successful >>%_log%

rem If compile failed stop processing:
if not %errorlevel% == 0 pause

rem Execute result*********************************************************************
echo [%DATE% %Time%] Finished compile sequence >>%_log%

"%~dp0Debug\%str1:~1%.exe"

rem pause     
