#include "stdafx.h"
#include "Channel.h"



CChannel::CChannel(void)
{

}

CChannel::~CChannel(void)
{

}

void CChannel::SetChannelId(UINT32_T uichannelId)
{
	this->ChannelId = uichannelId;
}

void CChannel::SetProtocolId(UINT32_T uiprotocolId)
{
	this->ProtocolId = uiprotocolId;
}

void CChannel::SetBaudRate(UINT32_T uibaudrate)
{
	this->BaudRate = uibaudrate;
}

void CChannel::SetConectFlag(UINT32_T uiconectflag)
{
	this->ConectFlag = uiconectflag;
}

void CChannel::SetChannelExist(BOOL bStatus)
{
	this->bExist = bStatus;
}


UINT32_T CChannel::GetChannelId() const
{
	return this->ChannelId;
}

UINT32_T CChannel::GetProtocolId() const
{
	return this->ProtocolId;
}

UINT32_T CChannel::GetBaudRate() const
{
	return this->BaudRate;
}

UINT32_T CChannel::GetConectFlag() const
{
	return this->ConectFlag;
}

BOOL CChannel::GetChannelExist() const
{
	return this->bExist;
}
