

#pragma once

#include "../J2534ToolDemo/Common/applications.h"

typedef struct tagConnectInfo
{
	UINT32_T ulProtocolIndex;
	UINT32_T ulBaudRateIndex;
	UINT32_T ulConnectFlags;
	BOOL bExist;
}ConnectInfo;

class CChannel
{
public:
	CChannel(void);
	~CChannel(void);

	void SetProtocolId(UINT32_T uiprotocolId);
	void SetBaudRate(UINT32_T uibaudrate);
	void SetConectFlag(UINT32_T uiconectflag);
	void SetChannelId(UINT32_T uichannelId);
	void SetChannelExist(BOOL bStatus);

	UINT32_T GetProtocolId() const;
	UINT32_T GetBaudRate() const;
	UINT32_T GetConectFlag() const;
	UINT32_T GetChannelId() const;
	BOOL GetChannelExist() const;

protected:
	UINT32_T ProtocolIdIndex;
	UINT32_T ProtocolId;
	UINT32_T BaudRateIndex;
	UINT32_T BaudRate;
	UINT32_T ConectFlag;
	UINT32_T ChannelId;
	BOOL bExist;			// ��ʶͨ��״̬
};

