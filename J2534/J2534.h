﻿/*******************************************************************************
 * FILE: J2534.h
 * This file is part of ALLScanner VCX software.
 * COPYRIGHT (C) ALLScanner 2008-2014. 
 *	
 * DESC: VCX J2534-2 API Defines
 * 
 * HISTORY:
 * Date			Author		Notes	
 * 2014-5-2		RTT			Create
 *	
*******************************************************************************/

#ifndef __J2534_H__
#define __J2534_H__


#ifdef __cplusplus
extern "C" {
#endif 
	
	
// J2534 API Document Version String.
#define STR_DLL_VERSION				"03.02.150713"
#define STR_API_VERSION				"04.04"
#define STR_FW_VERSION			    "01.34"


/*******************************************************************************
 * J2534 Protocol ID
 */
typedef enum
{
    /* ProtocolID J2534-1 */
	J1850VPW		= 0x01,
	J1850PWM		= 0x02,
	ISO9141			= 0x03,
	ISO14230		= 0x04,
	CAN				= 0x05,
	ISO15765		= 0x06,
	SCI_A_ENGINE	= 0x07,
	SCI_A_TRANS		= 0x08,
	SCI_B_ENGINE	= 0x09,
	SCI_B_TRANS		= 0x0a,

	/* ProtocolID J2534-2 : Pin Selected */
	J1850VPW_PS     = 0x8000,	// GM / DC CLASS2
	J1850PWM_PS     = 0x8001,	// FORD SCP
	ISO9141_PS      = 0x8002,	// ISO9141 & ISO9141-2
	ISO14230_PS     = 0x8003,	// ISO14230-4
	CAN_PS          = 0x8004,	// Raw CAN
	ISO15765_PS     = 0x8005,	// ISO15765-2
	J2610_PS        = 0x8006,	// DC SCI
	SW_ISO15765_PS 	= 0x8007,	// Single Wire ISO15765-2
	SW_CAN_PS      	= 0x8008,	// Single Wire Raw CAN
	GM_UART_PS      = 0x8009,	// GM UART
	UART_ECHO_BYTE_PS = 0x800A,	// J2818(VAG KW1281) / J2809(HONDA)
	HONDA_DIAGH_PS  = 0x800B,	// Honda Old Protocol 92Hm
	J1939_PS  	    = 0x800C,	// 
	J1708_PS  	    = 0x800D,	// 
	TP2_0_PS  	    = 0x800E,	// 
	FT_CAN_PS  	    = 0x800F,	// 
    FT_ISO15765_PS  = 0x8010,	// 
    CAN_XON_XOFF_PS = 0x8011,	// Volvo CAN (SPX I-VIEW)

    /* ProtocolID J2534-2 : Additional Channel */
	CAN_CH1         = 0x9000,   // 1st additional CAN channel
	CAN_CH2         = 0x9001, 	// 2nd additional CAN channel
	CAN_CH3 		= 0x9002, 
	J1850VPW_CH1 	= 0x9080, 
	J1850PWM_CH1 	= 0x9100, 
	ISO9141_CH1 	= 0x9180, 
	ISO14230_CH1 	= 0x9200, 
	ISO15765_CH1 	= 0x9280,   // 1st additional ISO15765 (TMVCI)
	ISO15765_CH2 	= 0x9281, 
	ISO15765_CH3 	= 0x9282, 
	SW_CAN_CH1 		= 0x9300, 
	SW_ISO15765_CH1 = 0x9380,
    J2610_CH1       = 0x9400,
    FT_CAN_CH1      = 0x9480,
    FT_ISO15765_CH1 = 0x9500,
    GM_UART_CH1     = 0x9580,
    ECHO_BYTE_CH1   = 0x9600,
    HONDA_DIAGH_CH1 = 0x9680,
    J1939_CH1       = 0x9700,
    J1708_CH1       = 0x9780,
    TP2_0_CH1       = 0x9800,
	ANALOG_IN_CH1	= 0xC000, 

    /* Tool Manufacturer Specific */
	AISIN_ISO9141   = 0x10000,  // DG
	HONDA_DIAGH     = 0x10003, 	// Honda DIAG-H (SPX I-VIEW)
	HONDA_ECHO_BYTE = 0x10004,	// Honda ECHO BYTE (SPX I-VIEW)
	VOLVO_CAN2      = 0x10000001,   // RTT 2009.07.31CAN2

	J2534_PROTOCOL_NUM
} J2534_PROTOCOL;

/*******************************************************************************
 * J2534 Error codes
 */
typedef enum
{
	J2534_STATUS_NOERROR,				// Function call successful.
	J2534_ERR_NOT_SUPPORTED,			// Function not supported.
	J2534_ERR_INVALID_CHANNEL_ID,		// Invalid ChannelID value.
	J2534_ERR_INVALID_PROTOCOL_ID,		// InvalRid ProtocolID value.
	J2534_ERR_NULL_PARAMETER,			// NULL pointer supplied where a valid 
										// pointer
										// is required.
	J2534_ERR_INVALID_IOCTL_VALUE,		// Invalid value for Ioctl parameter 
	J2534_ERR_INVALID_FLAGS,			// Invalid flag values.
	J2534_ERR_FAILED,					// Undefined error, use 
										// PassThruGetLastError for description
										// of error.
	J2534_ERR_DEVICE_NOT_CONNECTED,		// Device not connected to PC
	J2534_ERR_TIMEOUT,					// Timeout. No message available to 
										// read or 
										// could not read the specified no. of 
										// msgs.
	J2534_ERR_INVALID_MSG,				// Invalid message structure pointed 
										// to by pMsg.
	J2534_ERR_INVALID_TIME_INTERVAL,	// Invalid TimeInterval value.
	J2534_ERR_EXCEEDED_LIMIT,			// ALL periodic message IDs have been 
										// used.
	J2534_ERR_INVALID_MSG_ID,			// Invalid MsgID value.
	J2534_ERR_DEVICE_IN_USE,			// Device is currently open.
	J2534_ERR_INVALID_IOCTL_ID,			// Invalid IoctlID value.
	J2534_ERR_BUFFER_EMPTY,				// Protocol message buffer empty.
	J2534_ERR_BUFFER_FULL,				// Protocol message buffer full.
	J2534_ERR_BUFFER_OVERFLOW,			// Protocol message buffer overflow.
	J2534_ERR_PIN_INVALID,				// Invalid pin number.
	J2534_ERR_CHANNEL_IN_USE,			// Channel already in use.
	J2534_ERR_MSG_PROTOCOL_ID,			// Protocol type does not match the
										// protocol associated with the 
										// Channel ID
	J2534_ERR_INVALID_FILTER_ID,		// Invalid FilterID value
	J2534_ERR_NO_FLOW_CONTROL,			// No flow control filter set (for 
										// protocolID ISO15765 only).
	J2534_ERR_NOT_UNIQUE,				// No filter has been set (all
										// protocolIDs except ISO15765)
	J2534_ERR_INVALID_BAUDRATE,
	J2534_ERR_INVALID_DEVICE_ID,
    J2534_ERR_PIN_IN_USE,
	J2534_ERR_ADDRESS_NOT_CLAIMED	= 0x10000
} J2534_ERROR;
#define J2534_ERR_NULLPARAMETER			J2534_ERR_NULL_PARAMETER	/*v0202*/

/*******************************************************************************
 * 	Ioctl ID Values
 */
typedef enum
{
	GET_CONFIG					= 0x01,
	SET_CONFIG					= 0x02,
	READ_VBATT					= 0x03,
	FIVE_BAUD_INIT 				= 0x04,
	FAST_INIT 					= 0x05,
	SET_PIN_USE 				= 0x06,	//
	CLEAR_TX_BUFFER 			= 0x07,
	CLEAR_RX_BUFFER 			= 0x08,
	CLEAR_PERIODIC_MSGS 		= 0x09,
	CLEAR_MSG_FILTERS 			= 0x0a,
	CLEAR_FUNCT_MSG_LOOKUP_TABLE = 0x0b,
	ADD_TO_FUNCT_MSG_LOOKUP_TABLE = 0x0c,
	DELETE_FROM_FUNCT_MSG_LOOKUP_TABLE = 0x0d,
	READ_PROG_VOLTAGE 			= 0x0e,
	// J2534-2
	SW_CAN_HS					= 0x8000,
	SW_CAN_NS					= 0x8001,
	SET_POLL_RESPONSE 			= 0x8002,
	BECOME_MASTER 				= 0x8003,
    START_REPEAT_MESSAGE        = 0x8004,
    QUERY_REPEAT_MESSAGE        = 0x8005,
    STOP_REPEAT_MESSAGE         = 0x8006,
	// J2534-2 Device Config Parameters
	GET_DEVICE_CONFIG 			= 0x8007,
	SET_DEVICE_CONFIG 			= 0x8008,
	// J2534-2 J1939 
	PROTECT_J1939_ADDR 			= 0x8009,
	// J2534-2 TP2.0 
	REQUEST_CONNECTION 			= 0x800A,
	TEARDOWN_CONNECTION 		= 0x800B,
	// J2534-2 Discovery Mechanism 
	GET_DEVICE_INFO 			= 0x800C,
	GET_PROTOCOL_INFO 			= 0x800D,
	READ_V_J1962PIN1 			= 0x800E,
	// DG Define
	CAN_SET_BTR					= 0x10000, 
	FIVE_BAUD_INIT_NO_INV_KB2 	= 0x10001,
	// Teradyne Define:
	//READ_PIN_VOLTAGE = 0x10001,
	//CHECK_CABLE_TYPE = 0x10002, /* Teradyne Defined Value to enable a Cable Check to be performed */
	SET_HONDA_WEN 				= 0x10003,
    SET_HONDA_SCS 				= 0x10004,

    START_PERIODIC_MSG_SSM      = 0x10021,
    CHECK_VERSION_SSM           = 0x20071,
    OPEN_CHANNEL_SSM            = 0x2007d,

    HONDA_ISO9141_UNKNOW        = 0x00100000,   // CRV2011 RTT.2014.7.25.

	VOLVO_CAN_PS 				= 0x10001004,	// VOLVO CAN pin select RTT.2009.07.31.
} J2534_IOCTLID;

/*******************************************************************************
 * 	IOCTL SET_CONFIG Param ID
 */
typedef enum
{
	DATA_RATE 					= 0x01,
	NOT_USED_02 				= 0x02,
	LOOPBACK 					= 0x03,
	NODE_ADDRESS 				= 0x04,
	NETWORK_LINE 				= 0x05,
	P1_MIN 						= 0x06,
	P1_MAX 						= 0x07,
	P2_MIN 						= 0x08,
	P2_MAX 						= 0x09,
	P3_MIN 						= 0x0A,
	P3_MAX 						= 0x0B,
	P4_MIN 						= 0x0C,
	P4_MAX 						= 0x0D,
	W1 							= 0x0E,
	W2 							= 0x0F,
	W3 							= 0x10,
	W4 							= 0x11,
	W5 							= 0x12,
	TIDLE 						= 0x13,
	TINIL 						= 0x14,
	TWUP 						= 0x15,
	PARITY 						= 0x16,
	BIT_SAMPLE_POINT 			= 0x17,
	SYNC_JUMP_WIDTH 			= 0x18,
	W0 							= 0x19,
	T1_MAX 						= 0x1A,
	T2_MAX 						= 0x1B,
	T4_MAX 						= 0x1C,
	T5_MAX 						= 0x1D,
	ISO15765_BS 				= 0x1E,
	ISO15765_STMIN 				= 0x1F,
	DATA_BITS 					= 0x20,
	FIVE_BAUD_MOD 				= 0x21,
	BS_TX 						= 0x22,
	STMIN_TX 					= 0x23,
	T3_MAX 						= 0x24,
	ISO15765_WFT_MAX 			= 0x25,
	
	// J2534-2
	CAN_MIXED_FORMAT			= 0x8000,
	J1962_PINS 					= 0x8001,
	SW_CAN_HS_DATA_RATE 		= 0x8010,
	SW_CAN_SPEEDCHANGE_ENABLE 	= 0x8011,
	SW_CAN_RES_SWITCH 			= 0x8012,

	// Analogs
	ACTIVE_CHANNELS 			= 0x8020,
	SAMPLE_RATE 				= 0x8021,
	SAMPLES_PER_READING 		= 0x8022,
	READINGS_PER_MSG 			= 0x8023,
	AVERAGING_METHOD 			= 0x8024,
	SAMPLE_RESOLUTION 			= 0x8025,
	INPUT_RANGE_LOW 			= 0x8026,
	INPUT_RANGE_HIGH 			= 0x8027,

	// J2534-2 UART Echo Byte protocol parameters
	UEB_T0_MIN             		= 0x8028,
	UEB_T1_MAX             		= 0x8029,
	UEB_T2_MAX             		= 0x802A,
	UEB_T3_MAX             		= 0x802B,
	UEB_T4_MIN             		= 0x802C,
	UEB_T5_MAX             		= 0x802D,
	UEB_T6_MAX             		= 0x802E,
	UEB_T7_MIN             		= 0x802F,
	UEB_T7_MAX             		= 0x8030,
	UEB_T9_MIN             		= 0x8031,
 
	// Pin selection
	J1939_PINS             		= 0x803D,
	J1708_PINS             		= 0x803E,

	// J2534-2 J1939 config parameters
	J1939_T1             		= 0x803F,
	J1939_T2             		= 0x8040,
	J1939_T3             		= 0x8041,
	J1939_T4             		= 0x8042,
	J1939_BRDCST_MIN_DELAY 		= 0x8043,

	// J2534-2 TP2.0
	TP2_0_T_BR_INT      		= 0x8044,
	TP2_0_T_E            		= 0x8045,
	TP2_0_MNTC          		= 0x8046,
	TP2_0_T_CTA      	    	= 0x8047,
	TP2_0_MNCT       	    	= 0x8048,
	TP2_0_MNTB          		= 0x8049,
	TP2_0_MNT      	        	= 0x804A,
	TP2_0_T_WAIT      	    	= 0x804B,
	TP2_0_T1      	        	= 0x804C,
	TP2_0_T3      	        	= 0x804D,
	TP2_0_IDENTIFER      		= 0x804E,
	TP2_0_RXIDPASSIVE      		= 0x804F,

	// Man specify
	ISO15765_PAD_BYTE 			= 0x00010000,
	// rtt.2009.07.31.CAN pin select
	VOLVO_PARA1 				= 0x10000001,
    
	// my.2016.01.07
	_ISO15765_SIMULTANEOUS	= 0x26,
	DT_ISO15765_PAD_BYTE		= 0x00010001,
	SW_CAN_SPDCHG_ENABLE		= 0x8013,

	RESERVED_CONFIG_PARAMID
} J2534_CONFIG_PARAMID;

/*******************************************************************************
 * 	IOCTL SET_DEVICE_CONFIG Param ID
 */
typedef enum
{
    NON_VOLATILE_STORE_1        = 0x0000C001,
    NON_VOLATILE_STORE_2        = 0x0000C002,
    NON_VOLATILE_STORE_3        = 0x0000C003,
    NON_VOLATILE_STORE_4        = 0x0000C004,
    NON_VOLATILE_STORE_5        = 0x0000C005,
    NON_VOLATILE_STORE_6        = 0x0000C006,
    NON_VOLATILE_STORE_7        = 0x0000C007,
    NON_VOLATILE_STORE_8        = 0x0000C008,
    NON_VOLATILE_STORE_9        = 0x0000C009,
    NON_VOLATILE_STORE_10       = 0x0000C00A,
    RESERVED_DEVICECONFIG_PARAMID
} J2534_2_DEVICECONFIG_PARAMID;

/*******************************************************************************
 * 	Miscellaneous define
 */

// Values of Voltage passed to PassThruSetProgrammingVoltage
#define SHORT_TO_GROUND					0xFFFFFFFE
#define VOLTAGE_OFF						0xFFFFFFFF

#define BUS_NORMAL						0
#define BUS_PLUS						1
#define BUS_MINUS						2

#define NO_PARITY						0
#define ODD_PARITY						1
#define EVEN_PARITY						2

//SWCAN
#define DISBLE_SPDCHANGE		/*-2*/	0
#define ENABLE_SPDCHANGE		/*-2*/	1
#define DISCONNECT_RESISTOR		/*-2*/	0
#define CONNECT_RESISTOR		/*-2*/	1
#define AUTO_RESISTOR			/*-2*/	2

//Mixed Mode
#define CAN_MIXED_FORMAT_OFF	/*-2*/	0
#define CAN_MIXED_FORMAT_ON		/*-2*/	1
#define CAN_MIXED_FORMAT_ALL_FRAMES	/*-2*/	2 // Not supported

// -2 Discovery
#define NOT_SUPPORTED       0
#define SUPPORTED           1

// -2 Analog averaging
#define SIMPLE_AVERAGE		0x00000000 // Simple arithmetic mean
#define MAX_LIMIT_AVERAGE	0x00000001 // Choose the biggest value
#define MIN_LIMIT_AVERAGE	0x00000002 // Choose the lowest value
#define MEDIAN_AVERAGE		0x00000003


/*******************************************************************************
 * 	PassthruConnect Flag define
 */
#define CONNFLAG_CAN_29BIT_ID   		0x00000100
#define CONNFLAG_ISO9141_NO_CHECKSUM   	0x00000200
#define CONNFLAG_NO_CHECKSUM   			0x00000200	// J2534-2002
#define CONNFLAG_CAN_ID_BOTH   			0x00000800
#define CONNFLAG_ISO9141_K_LINE_ONLY   	0x00001000

/*******************************************************************************
 * 	Passthru MSG RxStatus define
 */
#define RXSTATUS_TX_MSG_TYPE   			0x00000001
#define RXSTATUS_START_OF_MESSAGE   	0x00000002
#define RXSTATUS_ISO15765_FIRST_FRAME	0x00000002	// J2534-2002
#define RXSTATUS_RX_BREAK   			0x00000004
#define RXSTATUS_TX_INDICATION   		0x00000008
#define RXSTATUS_ISO15765_PADDING_ERROR 0x00000010
#define RXSTATUS_ISO15765_ADDR_TYPE   	0x00000080
#define RXSTATUS_CAN_29BIT_ID   		0x00000100
#define RXSTATUS_SW_CAN_HV_RX   		0x00010000	// J2534-2
#define RXSTATUS_SW_CAN_HS_RX   		0x00020000	// J2534-2
#define RXSTATUS_SW_CAN_NS_RX   		0x00040000	// J2534-2
#define RXSTATUS_J1939_ADDRESS_CLAIMED	0x00010000	// J2534-2
#define RXSTATUS_J1939_ADDRESS_LOST		0x00020000	// J2534-2

/*******************************************************************************
 * 	Passthru MSG TxFlags define
 */
#define TXFLAG_ISO15765_FRAME_PAD   	0x00000040
#define TXFLAG_ISO15765_ADDR_TYPE   	0x00000080
#define TXFLAG_CAN_29BIT_ID   			0x00000100
#define TXFLAG_WAIT_P3_MIN_ONLY   		0x00000200
#define TXFLAG_SW_CAN_HV_TX   			0x00000400	// J2534-2
#define TXFLAG_SCI_MODE   				0x00400000
#define TXFLAG_SCI_TX_VOLTAGE   		0x00800000

/*******************************************************************************
 * 	J2534 Message Structure
 */
#define PASSTHRU_MSG_DATA_SIZE		4128
typedef struct
{
	unsigned long ulProtocolID;
	unsigned long ulRxStatus;
	unsigned long ulTxFlags;
	unsigned long ulTimeStamp;
	unsigned long ulDataSize;
	unsigned long ulExtraDataIndex;
	unsigned char ucData[PASSTHRU_MSG_DATA_SIZE];
} PASSTHRU_MSG;
#define J2534_TX_MSG_SIZE	sizeof(PASSTHRU_MSG)
#define J2534_RX_MSG_SIZE	sizeof(PASSTHRU_MSG)

/*******************************************************************************
 * 	Filter Types
 */
typedef enum
{
	J2534_FILTER_NONE,
	J2534_FILTER_PASS,
	J2534_FILTER_BLOCK,
	J2534_FILTER_FLOW_CONTROL
} J2534_FILTER;

/*******************************************************************************
 * 	SCONFIG Structure
 */
typedef struct
{
//	char strName[32];				// name string(rtt/20090304) 
	J2534_CONFIG_PARAMID Parameter;	// name of parameter
	unsigned long ulValue;			// value of the parameter
} SCONFIG;

/*******************************************************************************
 * 	SCONFIG_LIST Structure
 */
typedef struct
{
	unsigned long ulNumOfParams;	// number of SCONFIG elements
	SCONFIG *pConfigPtr;			// array of SCONFIG
} SCONFIG_LIST;

/*******************************************************************************
 * 	SBYTE_ARRAY Structure
 */
typedef struct
{
	unsigned long ulNumOfBytes;		// number of bytes in the array
	unsigned char *pucBytePtr;		// array of bytes
} SBYTE_ARRAY;

/*******************************************************************************
 * 	J2534-2 
 */
typedef struct
{
	unsigned long Parameter;
	unsigned long Value;
    unsigned long Supported;
} SPARAM;

typedef struct
{
	unsigned long NumOfParams;
	SPARAM *ParamPtr;
} SPARAM_LIST;


/*******************************************************************************
 * J2534 DLL FUNCTION Exports
 */
#ifdef VCXPT32_EXPORTS				// defined by VCXPT32.vcxproj
    #define J2534_DLL_EXPORT
#endif

#ifdef PASSPT32_EXPORTS				// defined by PassPT32.vcxproj
    #define J2534_DLL_EXPORT
#endif

#ifdef J2534_DLL_EXPORT
    #define DLLFUNCTION     		__declspec(dllexport)
    #define J2534API                __stdcall
#else
	#define DLLFUNCTION
    #define J2534API
#endif


J2534_ERROR DLLFUNCTION J2534API PassThruOpen(
								void *pName, 
								unsigned long *pulDeviceID);

J2534_ERROR DLLFUNCTION J2534API PassThruClose(
								unsigned long ulDeviceID);

J2534_ERROR DLLFUNCTION J2534API PassThruConnect(
								unsigned long ulDeviceID,
								J2534_PROTOCOL enumProtocolID,
								unsigned long ulFlags,
								unsigned long ulBaudRate,
								unsigned long *pulChannelID);

J2534_ERROR DLLFUNCTION J2534API PassThruDisconnect(
								unsigned long ulChannelID);

J2534_ERROR DLLFUNCTION J2534API PassThruReadMsgs(
								unsigned long ulChannelID,
								PASSTHRU_MSG *pstructJ2534Msg,
								unsigned long *pulNumMsgs,
								unsigned long ulTimeout);

J2534_ERROR DLLFUNCTION J2534API PassThruWriteMsgs(
								unsigned long ulChannelID,
								PASSTHRU_MSG *pstructJ2534Msg,
								unsigned long *pulNumMsgs,
								unsigned long ulTimeout);

J2534_ERROR DLLFUNCTION J2534API PassThruStartPeriodicMsg(
								unsigned long ulChannelID, 
								PASSTHRU_MSG *pstructJ2534Msg, 
								unsigned long *pulMsgID, 
								unsigned long ulTimeInterval);

J2534_ERROR DLLFUNCTION J2534API	PassThruStopPeriodicMsg(
								unsigned long ulChannelID,
								unsigned long ulMsgID);

J2534_ERROR DLLFUNCTION J2534API PassThruStartMsgFilter(
								unsigned long ulChannelID,
								J2534_FILTER enumFilterType,
								PASSTHRU_MSG *pstructJ2534Mask,
								PASSTHRU_MSG *pstructJ2534Pattern,
								PASSTHRU_MSG *pstructJ2534FlowControl,
								unsigned long *pulFilterID);

J2534_ERROR DLLFUNCTION J2534API PassThruStopMsgFilter(
								unsigned long ulChannelID,
								unsigned long ulFilterID);

J2534_ERROR DLLFUNCTION J2534API PassThruSetProgrammingVoltage(
								unsigned long ulDeviceID,
								unsigned long ulPin,
								unsigned long ulVoltage);

J2534_ERROR DLLFUNCTION J2534API PassThruReadVersion(
								unsigned long ulDeviceID,
								char *pFirmwareVersion,
								char *pDllVersion,
								char *pApiVersion);

J2534_ERROR DLLFUNCTION J2534API PassThruGetLastError(
								//unsigned long ulErrorID,
								char *pErrorDescription);

J2534_ERROR DLLFUNCTION J2534API PassThruIoctl(
								unsigned long ulChannelID,
								J2534_IOCTLID enumIoctlID,
								void *pInput,
								void *pOutput);

/** 
 *	extend API
 */
J2534_ERROR DLLFUNCTION J2534API PassThruReset(
								unsigned long ulDeviceID);



#ifdef __cplusplus
}
#endif 

#endif	/* __J2534_H__ */

