﻿/*******************************************************************************
 * FILE: J2534Dll.cpp
 * This file is part of ALLScanner VCX software.
 * COPYRIGHT (C) ALLScanner 2008-2014. 
 *		
 * DESC: J2534 DLL Import function
 * 
 * HISTORY:
 * Date			Author		Notes	
 * 2014-7-10		RTT			Create
 *	
*******************************************************************************/
#include "stdafx.h"
#include "J2534Dll.h"

#include "common.h"
#include "J2534Log.h"


PTOPEN                  _PassThruOpen                   = NULL; 
PTCLOSE                 _PassThruClose                  = NULL; 
PTCONNECT               _PassThruConnect                = NULL; 
PTDISCONNECT            _PassThruDisconnect             = NULL; 
PTREADMSGS              _PassThruReadMsgs               = NULL; 
PTWRITEMSGS             _PassThruWriteMsgs              = NULL; 
PTSTARTPERIODICMSG      _PassThruStartPeriodicMsg       = NULL; 
PTSTOPPERIODICMSG       _PassThruStopPeriodicMsg        = NULL; 
PTSTARTMSGFILTER        _PassThruStartMsgFilter         = NULL; 
PTSTOPMSGFILTER         _PassThruStopMsgFilter          = NULL; 
PTSETPROGRAMMINGVOLTAGE _PassThruSetProgrammingVoltage  = NULL; 
PTREADVERSION           _PassThruReadVersion            = NULL; 
PTGETLASTERROR          _PassThruGetLastError           = NULL; 
PTIOCTL                 _PassThruIoctl                  = NULL; 


static HINSTANCE hDLL = NULL;
static DWORD fLibLoaded = FALSE;

#define PRINT_ERROR_AND_EXIT \
{ \
    DWORD err = GetLastError(); \
    PrintLastError(err); \
    return FALSE;\
}


#define LOAD_FUNCTION( name, type ) {\
    name = type(GetProcAddress(hDLL, #name )); \
    if (name==NULL)	\
    PRINT_ERROR_AND_EXIT\
}				


unsigned long J2534DllLoad(LPCTSTR szDLL)
{
    // Can't load a library if the string is NULL
    if (szDLL == NULL)
    {
        J2534LogLine("ERROR: szDLL str NULL");
        return FALSE;
    }

    // Can't load a library if there's one currently loaded
    if (fLibLoaded)
    {
        J2534LogLine("ERROR: DLL already loaded");
        return FALSE;
    }

    hDLL = LoadLibrary(szDLL);
    if (hDLL == NULL)
    {
        // Try to get the error text
        // Set the internal error text based on the win32 message
        J2534LogLine("ERROR: Loadlibrary ErrorCode = %d", GetLastError());
        return FALSE;
    }

    fLibLoaded = TRUE;

    _PassThruOpen = (PTOPEN)GetProcAddress(hDLL, "PassThruOpen");
    _PassThruClose = (PTCLOSE)GetProcAddress(hDLL, "PassThruClose");
    _PassThruConnect = (PTCONNECT)GetProcAddress(hDLL, "PassThruConnect");
    _PassThruDisconnect = (PTDISCONNECT)GetProcAddress(hDLL, "PassThruDisconnect");
    _PassThruReadMsgs = (PTREADMSGS)GetProcAddress(hDLL, "PassThruReadMsgs");
    _PassThruWriteMsgs = (PTWRITEMSGS)GetProcAddress(hDLL, "PassThruWriteMsgs");
    _PassThruStartPeriodicMsg = (PTSTARTPERIODICMSG)GetProcAddress(hDLL, "PassThruStartPeriodicMsg");
    _PassThruStopPeriodicMsg = (PTSTOPPERIODICMSG)GetProcAddress(hDLL, "PassThruStopPeriodicMsg");
    _PassThruStartMsgFilter = (PTSTARTMSGFILTER)GetProcAddress(hDLL, "PassThruStartMsgFilter");
    _PassThruStopMsgFilter = (PTSTOPMSGFILTER)GetProcAddress(hDLL, "PassThruStopMsgFilter");
    _PassThruSetProgrammingVoltage = (PTSETPROGRAMMINGVOLTAGE)GetProcAddress(hDLL, "PassThruSetProgrammingVoltage");
    _PassThruReadVersion = (PTREADVERSION)GetProcAddress(hDLL, "PassThruReadVersion");
    _PassThruGetLastError = (PTGETLASTERROR)GetProcAddress(hDLL, "PassThruGetLastError");
    _PassThruIoctl = (PTIOCTL)GetProcAddress(hDLL, "PassThruIoctl");

    J2534LogLine("J2534 DLL Load Success.");
    return TRUE;
}

void J2534DllUnload(void)
{
    // Can't unload a library if there's nothing loaded
    if (! fLibLoaded)
        return;

    fLibLoaded = FALSE;

    // Invalidate the function pointers
    _PassThruOpen = NULL;
    _PassThruClose = NULL;
    _PassThruConnect = NULL;
    _PassThruDisconnect = NULL;
    _PassThruReadMsgs = NULL;
    _PassThruWriteMsgs = NULL;
    _PassThruStartPeriodicMsg = NULL;
    _PassThruStopPeriodicMsg = NULL;
    _PassThruStartMsgFilter = NULL;
    _PassThruStopMsgFilter = NULL;
    _PassThruSetProgrammingVoltage = NULL;
    _PassThruReadVersion = NULL;
    _PassThruGetLastError = NULL;
    _PassThruIoctl = NULL;

    BOOL fSuccess;
    fSuccess = FreeLibrary(hDLL);
    if (! fSuccess)
    {
        // Try to get the error text
        // Set the internal error text based on the win32 message
    }
}

unsigned long J2534DllLoaded(void)
{
    return fLibLoaded;
}