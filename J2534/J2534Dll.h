﻿/*******************************************************************************
 * FILE: J2534DLL.h
 * This file is part of ALLScanner VCX software.
 * COPYRIGHT (C) ALLScanner 2008-2014. 
 *		
 * DESC: J2534 DLL Import Defines
 * 
 * HISTORY:
 * Date			Author		Notes	
 * 2014-5-2		RTT			Create
 *	
*******************************************************************************/

#ifndef __J2534DLL_H__
#define __J2534DLL_H__

#include "Windows.h"

#ifdef __cplusplus
extern "C" {
#endif 
	

/*******************************************************************************
 * J2534 DLL FUNCTION Imports
 */

#ifndef J2534API
#define J2534API		__stdcall
#endif

/* Typedefs and function pointers for J2534 API */
typedef unsigned long (J2534API *PTOPEN)(void *, unsigned long *);
typedef unsigned long (J2534API *PTCLOSE)(unsigned long);
typedef unsigned long (J2534API *PTCONNECT)(unsigned long, unsigned long, unsigned long, unsigned long, unsigned long *);
typedef unsigned long (J2534API *PTDISCONNECT)(unsigned long);
typedef unsigned long (J2534API *PTREADMSGS)(unsigned long, void *, unsigned long *, unsigned long);
typedef unsigned long (J2534API *PTWRITEMSGS)(unsigned long, void *, unsigned long *, unsigned long);
typedef unsigned long (J2534API *PTSTARTPERIODICMSG)(unsigned long, void *, unsigned long *, unsigned long);
typedef unsigned long (J2534API *PTSTOPPERIODICMSG)(unsigned long, unsigned long);
typedef unsigned long (J2534API *PTSTARTMSGFILTER)(unsigned long, unsigned long, void *, void *, void *, unsigned long *);
typedef unsigned long (J2534API *PTSTOPMSGFILTER)(unsigned long, unsigned long);
typedef unsigned long (J2534API *PTSETPROGRAMMINGVOLTAGE)(unsigned long, unsigned long, unsigned long);
typedef unsigned long (J2534API *PTREADVERSION)(unsigned long, char *, char *, char *);
typedef unsigned long (J2534API *PTGETLASTERROR)(char *);
typedef unsigned long (J2534API *PTIOCTL)(unsigned long, unsigned long, void *, void *);


extern  PTOPEN                  _PassThruOpen;
extern  PTCLOSE                 _PassThruClose;
extern  PTCONNECT               _PassThruConnect;
extern  PTDISCONNECT            _PassThruDisconnect;
extern  PTREADMSGS              _PassThruReadMsgs;
extern  PTWRITEMSGS             _PassThruWriteMsgs;
extern  PTSTARTPERIODICMSG      _PassThruStartPeriodicMsg;
extern  PTSTOPPERIODICMSG       _PassThruStopPeriodicMsg;
extern  PTSTARTMSGFILTER        _PassThruStartMsgFilter;
extern  PTSTOPMSGFILTER         _PassThruStopMsgFilter;
extern  PTSETPROGRAMMINGVOLTAGE _PassThruSetProgrammingVoltage;
extern  PTREADVERSION           _PassThruReadVersion;
extern  PTGETLASTERROR          _PassThruGetLastError;
extern  PTIOCTL                 _PassThruIoctl;


unsigned long J2534DllLoad(LPCTSTR szDLL);
void J2534DllUnload(void);
unsigned long J2534DllLoaded(void);

#ifdef __cplusplus
}
#endif 

#endif	/* __J2534DLL_H__ */

