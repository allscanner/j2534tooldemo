﻿/*******************************************************************************
 * FILE: J2534Log.cpp
 * This file is part of ALLScanner VCX software.
 * COPYRIGHT (C) ALLScanner 2008-2014. 
 *		
 * DESC: VCX J2534 Log helper
 * 
 * HISTORY:
 * Date			Author		Notes	
 * 2014-7-2		RTT			Create
 *	
*******************************************************************************/

#include "stdafx.h"
#include "common.h"
#include "J2534Log.h"

static const struct _J2534_ERR_MSG { 
	int err_id;
	char * msg;
} J2534ErrMsg[] = {
    {J2534_STATUS_NOERROR,				"J2534_STATUS_OK"},
    {J2534_ERR_NOT_SUPPORTED,			"J2534_ERR_NOT_SUPPORTED"},
    {J2534_ERR_INVALID_CHANNEL_ID,		"J2534_ERR_INVALID_CHANNEL_ID"},
    {J2534_ERR_INVALID_PROTOCOL_ID,		"J2534_ERR_INVALID_PROTOCOL_ID"},
    {J2534_ERR_NULL_PARAMETER,			"J2534_ERR_NULL_PARAMETER"},
    {J2534_ERR_INVALID_IOCTL_VALUE,		"J2534_ERR_INVALID_IOCTL_VALUE"},
    {J2534_ERR_INVALID_FLAGS,			"J2534_ERR_INVALID_FLAGS"},
    {J2534_ERR_FAILED,					"J2534_ERR_FAILED"},
    {J2534_ERR_DEVICE_NOT_CONNECTED,	"J2534_ERR_DEVICE_NOT_CONNECTED"},
    {J2534_ERR_TIMEOUT,					"J2534_ERR_TIMEOUT"},
    {J2534_ERR_INVALID_MSG,				"J2534_ERR_INVALID_MSG"},
    {J2534_ERR_INVALID_TIME_INTERVAL,	"J2534_ERR_INVALID_TIME_INTERVAL"},
    {J2534_ERR_EXCEEDED_LIMIT,			"J2534_ERR_EXCEEDED_LIMIT"},
    {J2534_ERR_INVALID_MSG_ID,			"J2534_ERR_INVALID_MSG_ID"},
    {J2534_ERR_DEVICE_IN_USE,			"J2534_ERR_DEVICE_IN_USE"},
    {J2534_ERR_INVALID_IOCTL_ID,		"J2534_ERR_INVALID_IOCTL_ID"},
    {J2534_ERR_BUFFER_EMPTY,			"J2534_ERR_BUFFER_EMPTY"},
    {J2534_ERR_BUFFER_FULL,				"J2534_ERR_BUFFER_FULL"},
    {J2534_ERR_BUFFER_OVERFLOW,			"J2534_ERR_BUFFER_OVERFLOW"},
    {J2534_ERR_PIN_INVALID,				"J2534_ERR_PIN_INVALID"},
    {J2534_ERR_CHANNEL_IN_USE,			"J2534_ERR_CHANNEL_IN_USE"},
    {J2534_ERR_MSG_PROTOCOL_ID,			"J2534_ERR_MSG_PROTOCOL_ID"},
    {J2534_ERR_INVALID_FILTER_ID,		"J2534_ERR_INVALID_FILTER_ID"},
    {J2534_ERR_NO_FLOW_CONTROL,			"J2534_ERR_NO_FLOW_CONTROL"},
    {J2534_ERR_NOT_UNIQUE,				"J2534_ERR_NOT_UNIQUE"},
    {J2534_ERR_INVALID_BAUDRATE,		"J2534_ERR_INVALID_BAUDRATE"},
    {J2534_ERR_INVALID_DEVICE_ID,		"J2534_ERR_INVALID_DEVICE_ID"},
    {J2534_ERR_PIN_IN_USE,              "J2534_ERR_PIN_IN_USE"},

    {-1,                                NULL}
};


/**
 *	print Windows GetLastError info
 */
void PrintLastError( int error )
{
	LPTSTR errorText = NULL;
	FormatMessage(
	   // use system message tables to retrieve error text
	   FORMAT_MESSAGE_FROM_SYSTEM
	   // allocate buffer on local heap for error text
	   |FORMAT_MESSAGE_ALLOCATE_BUFFER
	   // Important! will fail otherwise, since we're not 
	   // (and CANNOT) pass insertion parameters
	   |FORMAT_MESSAGE_IGNORE_INSERTS,  
	   NULL,    // unused with FORMAT_MESSAGE_FROM_SYSTEM
	   error,
	   MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT),
	   (LPTSTR)&errorText,  // output 
	   0, // minimum size for output buffer
	   NULL);   // arguments - see note
	if (errorText != NULL)
	{
		J2534LogLine("LastError: [%d] %s", error, errorText);
	}
}

#define STR_SZ  256
void J2534LogProcess(void *hModule)
{
    char str[STR_SZ]={0};

    GetModuleFileName(NULL, (LPWSTR)str, STR_SZ);   // 句柄为空获取父进程名称
    J2534LogLine("APP Name: %s", str);
    GetModuleFileName((HMODULE)hModule, (LPWSTR)str, STR_SZ);// hModule是本DLL的句柄
    J2534LogLine("DLL Name: %s", str);
}

void J2534LogTimeStmap(void)
{
    char str[STR_SZ];
    SYSTEMTIME stm;
    //GetSystemTime(&stm);   // 获取UTC时间
    GetLocalTime(&stm);      // 获取本地时间
    sprintf_s(str, STR_SZ, "%02d:%02d:%02d.%03d",stm.wHour,stm.wMinute,stm.wSecond,stm.wMilliseconds);
    J2534LogLine("[%s]", str);
}

void J2534LogBuffer(unsigned char *pBuf, unsigned long ulLen)
{
    if (!pBuf)
        return;
    for (int i = 0; i < ulLen; i++)
    {
        if ((i % 16) == 0)
            J2534LogPrint("\r\n0x%08x:", pBuf + i);
        J2534LogPrint(" %02x", pBuf[i]);
    }
    J2534LogPrint("\r\n");
}

void J2534LogByteArray(SBYTE_ARRAY *pMsg)
{
    if (!pMsg)
    {
        J2534LogLine("Msg NULL!");
        return;
    }

    J2534LogLine("Msg->ulNumOfBytes: %d", pMsg->ulNumOfBytes);
    J2534LogPrint("Msg->pucBytePtr:");
    for(unsigned int i = 0; i < pMsg->ulNumOfBytes; i++) 
    {
        J2534LogPrint(" %02x", pMsg->pucBytePtr[i]);
    }
    J2534LogPrint("\r\n");
}

void J2534LogMsgs(PASSTHRU_MSG *pMsg, unsigned long ulNumMsg)
{
    if (ulNumMsg > 1)
        J2534LogLine("Msg Num = %d", ulNumMsg);
    for (unsigned int i = 0; i < ulNumMsg; i++)
    {
        J2534LogLine("Msg #%d : ", i);
        if (!&pMsg[i])
        {
            J2534LogLine("Msg NULL!");
            return;
        }
        J2534LogLine("Msg->ulProtocolID: 0x%08x", pMsg[i].ulProtocolID);
        J2534LogLine("Msg->ulRxStatus: 0x%08x", pMsg[i].ulRxStatus);
        J2534LogRxStatus(pMsg[i].ulRxStatus);
        J2534LogLine("Msg->ulTxFlags: 0x%08x", pMsg[i].ulTxFlags);
        J2534LogTxFlag(pMsg[i].ulTxFlags);
        J2534LogLine("Msg->ulTimeStamp: %10u", pMsg[i].ulTimeStamp);
        J2534LogLine("Msg->ulDataSize: %d", pMsg[i].ulDataSize);
        J2534LogLine("Msg->ulExtraDataIndex: %d", pMsg[i].ulExtraDataIndex);
        J2534LogPrint("Msg->ucData:");
        if (pMsg[i].ulDataSize > PASSTHRU_MSG_DATA_SIZE)
        {
            J2534LogLine("Msg->ulDataSize Error");
            continue;
        }
        for(unsigned int j = 0; j < pMsg[i].ulDataSize; j++)
        {
            J2534LogPrint(" %02x", pMsg[i].ucData[j]);
        }
        J2534LogPrint("\r\n");
    }

}

void J2534LogError(J2534_ERROR errorCode)
{
	int i=0;

	while (J2534ErrMsg[i].msg != NULL) 
	{
		if (errorCode == J2534ErrMsg[i].err_id)
		{
			J2534LogLine("[0x%08x] %s", errorCode, J2534ErrMsg[i].msg);
			return;
		}
		i++;
	}
	J2534LogLine("[0x%08x] Invalid errorCode.", errorCode);
}

void J2534GetErrorMsg(J2534_ERROR errorCode, char *pMsg)
{
    int i=0;

    if (pMsg == NULL) return;

    while (J2534ErrMsg[i].msg != NULL) 
    {
        if (errorCode == J2534ErrMsg[i].err_id)
        {
            sprintf_s(pMsg, 256, "[0x%08x] %s", errorCode, J2534ErrMsg[i].msg);
            return;
        }
        i++;
    }
}

void J2534LogProtocol(J2534_PROTOCOL ulProtocolID)
{
    J2534LogPrint("J2534 Protocol: [0x%08x] ", ulProtocolID);
    switch (ulProtocolID)
    {
    case J1850VPW:			J2534LogLine("J1850VPW");       	break;
    case J1850PWM:			J2534LogLine("J1850PWM");       	break;
    case ISO9141:			J2534LogLine("ISO9141");        	break;
    case ISO14230:			J2534LogLine("ISO14230");       	break;
    case CAN:				J2534LogLine("CAN");        		break;
    case ISO15765:			J2534LogLine("ISO15765");       	break;
    case SCI_A_ENGINE:		J2534LogLine("SCI_A_ENGINE");   	break;
    case SCI_A_TRANS:		J2534LogLine("SCI_A_TRANS");    	break;
    case SCI_B_ENGINE:		J2534LogLine("SCI_B_ENGINE");   	break;
    case SCI_B_TRANS:		J2534LogLine("SCI_B_TRANS");    	break;
	case J1850VPW_PS:       J2534LogLine("*J1850VPW_PS");       break;
	case J1850PWM_PS:       J2534LogLine("*J1850PWM_PS");       break;
	case ISO9141_PS:        J2534LogLine("*ISO9141_PS");        break;
	case ISO14230_PS:       J2534LogLine("*ISO14230_PS");       break;
	case CAN_PS:            J2534LogLine("*CAN_PS");            break;
	case ISO15765_PS:       J2534LogLine("*ISO15765_PS");       break;
	case J2610_PS:          J2534LogLine("*J2610_PS");          break;
	case SW_ISO15765_PS:    J2534LogLine("*SW_ISO15765_PS");    break;
	case SW_CAN_PS:         J2534LogLine("*SW_CAN_PS");         break;
	case GM_UART_PS:        J2534LogLine("*GM_UART_PS");        break;
	case UART_ECHO_BYTE_PS: J2534LogLine("*UART_ECHO_BYTE_PS"); break;
	case HONDA_DIAGH_PS:    J2534LogLine("*HONDA_DIAGH_PS");    break;
	case J1939_PS:          J2534LogLine("*J1939_PS");          break;
	case J1708_PS:          J2534LogLine("*J1708_PS");          break;
	case TP2_0_PS:          J2534LogLine("*TP2_0_PS");          break;
	case FT_CAN_PS:         J2534LogLine("*FT_CAN_PS");         break;
	case FT_ISO15765_PS:    J2534LogLine("*FT_ISO15765_PS");    break;
    case CAN_XON_XOFF_PS:   J2534LogLine("*CAN_XON_XOFF_PS");   break;
	case CAN_CH1:           J2534LogLine("*CAN_CH1");           break;
	case CAN_CH2:           J2534LogLine("*CAN_CH2");           break;
	case CAN_CH3:           J2534LogLine("*CAN_CH3");           break;
	case J1850VPW_CH1:      J2534LogLine("*J1850VPW_CH1");      break;
	case J1850PWM_CH1:      J2534LogLine("*J1850PWM_CH1");      break;
	case ISO9141_CH1:       J2534LogLine("*ISO9141_CH1");       break;
	case ISO14230_CH1:      J2534LogLine("*ISO14230_CH1");      break;
	case ISO15765_CH1:      J2534LogLine("*ISO15765_CH1");      break;
	case ISO15765_CH2:      J2534LogLine("*ISO15765_CH2");      break;
	case ISO15765_CH3:      J2534LogLine("*ISO15765_CH3");      break;
	case SW_CAN_CH1:        J2534LogLine("*SW_CAN_CH1");        break;
	case SW_ISO15765_CH1:   J2534LogLine("*SW_ISO15765_CH1");   break;
	case J2610_CH1:         J2534LogLine("*J2610_CH1");         break;
	case FT_CAN_CH1:        J2534LogLine("*FT_CAN_CH1");        break;
	case FT_ISO15765_CH1:   J2534LogLine("*FT_ISO15765_CH1");   break;
	case GM_UART_CH1:       J2534LogLine("*GM_UART_CH1");       break;
	case ECHO_BYTE_CH1:     J2534LogLine("*ECHO_BYTE_CH1");     break;
	case HONDA_DIAGH_CH1:   J2534LogLine("*HONDA_DIAGH_CH1");   break;
	case J1939_CH1:         J2534LogLine("*J1939_CH1");         break;
	case J1708_CH1:         J2534LogLine("*J1708_CH1");         break;
	case TP2_0_CH1:         J2534LogLine("*TP2_0_CH1");         break;
	case ANALOG_IN_CH1:     J2534LogLine("*ANALOG_IN_CH1");     break;
	case AISIN_ISO9141:     J2534LogLine("*AISIN_ISO9141");     break;
	case HONDA_DIAGH:       J2534LogLine("*HONDA_DIAGH1");      break;
	case HONDA_ECHO_BYTE:   J2534LogLine("*HONDA_DIAGH2");      break;
	case VOLVO_CAN2:        J2534LogLine("*VOLVO_CAN2");        break;
    default:	        	J2534LogLine("Unknown"); 			break;
    }
}

void J2534LogConfigList(SCONFIG_LIST *pConfigList)
{
    SCONFIG *pConfig;

    J2534LogLine("ulNumOfParams = %d", pConfigList->ulNumOfParams);
    for (unsigned int i = 0; i < pConfigList->ulNumOfParams; i++)
    {
        pConfig = &pConfigList->pConfigPtr[i];
        J2534LogPrint("Config #%d \t", i);
        switch (pConfig->Parameter)
        {
        case DATA_RATE: 		        J2534LogLine("DATA_RATE");				    break;
        case LOOPBACK: 			        J2534LogLine("LOOPBACK");				    break;
        case NODE_ADDRESS: 		        J2534LogLine("NODE_ADDRESS");			    break;
        case NETWORK_LINE: 		        J2534LogLine("NETWORK_LINE");			    break;
        case P1_MIN: 			        J2534LogLine("P1_MIN");					    break;
        case P1_MAX: 			        J2534LogLine("P1_MAX");					    break;
        case P2_MIN: 			        J2534LogLine("P2_MIN");					    break;
        case P2_MAX: 			        J2534LogLine("P2_MAX");					    break;
        case P3_MIN: 			        J2534LogLine("P3_MIN");					    break;
        case P3_MAX: 			        J2534LogLine("P3_MAX");					    break;
        case P4_MIN: 			        J2534LogLine("P4_MIN");					    break;
        case P4_MAX: 			        J2534LogLine("P4_MAX");					    break;
        case W1: 				        J2534LogLine("W1");						    break;
        case W2: 				        J2534LogLine("W2");						    break;
        case W3: 				        J2534LogLine("W3");						    break;
        case W4: 				        J2534LogLine("W4");						    break;
        case W5: 				        J2534LogLine("W5");						    break;
        case TIDLE: 			        J2534LogLine("TIDLE");					    break;
        case TINIL: 			        J2534LogLine("TINIL");					    break;
        case TWUP: 				        J2534LogLine("TWUP");					    break;
        case PARITY: 			        J2534LogLine("PARITY");					    break;
        case BIT_SAMPLE_POINT: 	        J2534LogLine("BIT_SAMPLE_POINT");		    break;
        case SYNC_JUMP_WIDTH: 	        J2534LogLine("SYNC_JUMP_WIDTH");		    break;
        case W0: 				        J2534LogLine("W0");						    break;
        case T1_MAX: 			        J2534LogLine("T1_MAX");					    break;
        case T2_MAX: 			        J2534LogLine("T2_MAX");					    break;
        case T4_MAX: 			        J2534LogLine("T4_MAX");					    break;
        case T5_MAX: 			        J2534LogLine("T5_MAX");					    break;
        case ISO15765_BS: 		        J2534LogLine("ISO15765_BS");			    break;
        case ISO15765_STMIN:	        J2534LogLine("ISO15765_STMIN");			    break;
        case DATA_BITS: 		        J2534LogLine("DATA_BITS");				    break;
        case FIVE_BAUD_MOD: 	        J2534LogLine("FIVE_BAUD_MOD");			    break;
        case BS_TX: 			        J2534LogLine("BS_TX");					    break;
        case STMIN_TX: 			        J2534LogLine("STMIN_TX");				    break;
        case T3_MAX: 			        J2534LogLine("T3_MAX");					    break;
        case ISO15765_WFT_MAX: 	        J2534LogLine("ISO15765_WFT_MAX");		    break;
		case CAN_MIXED_FORMAT:          J2534LogLine("*CAN_MIXED_FORMAT");          break;
		case J1962_PINS:                J2534LogLine("*J1962_PINS");                break;
		case SW_CAN_HS_DATA_RATE:       J2534LogLine("*SW_CAN_HS_DATA_RATE");       break;
		case SW_CAN_SPEEDCHANGE_ENABLE: J2534LogLine("*SW_CAN_SPEEDCHANGE_ENABLE"); break;
		case SW_CAN_RES_SWITCH:         J2534LogLine("*SW_CAN_RES_SWITCH");         break;
		case ACTIVE_CHANNELS:           J2534LogLine("*ACTIVE_CHANNELS");           break;
		case SAMPLE_RATE:               J2534LogLine("*SAMPLE_RATE");               break;
		case SAMPLES_PER_READING:       J2534LogLine("*SAMPLES_PER_READING");       break;
		case READINGS_PER_MSG:          J2534LogLine("*READINGS_PER_MSG");          break;
		case AVERAGING_METHOD:          J2534LogLine("*AVERAGING_METHOD");          break;
		case SAMPLE_RESOLUTION:         J2534LogLine("*SAMPLE_RESOLUTION");         break;
		case INPUT_RANGE_LOW:           J2534LogLine("*INPUT_RANGE_LOW");           break;
		case INPUT_RANGE_HIGH:          J2534LogLine("*INPUT_RANGE_HIGH");          break;
		case UEB_T0_MIN:                J2534LogLine("*UEB_T0_MIN");                break;
		case UEB_T1_MAX:                J2534LogLine("*UEB_T1_MAX");                break;
		case UEB_T2_MAX:                J2534LogLine("*UEB_T2_MAX");                break;
		case UEB_T3_MAX:                J2534LogLine("*UEB_T3_MAX");                break;
		case UEB_T4_MIN:                J2534LogLine("*UEB_T4_MIN");                break;
		case UEB_T5_MAX:                J2534LogLine("*UEB_T5_MAX");                break;
		case UEB_T6_MAX:                J2534LogLine("*UEB_T6_MAX");                break;
		case UEB_T7_MIN:                J2534LogLine("*UEB_T7_MIN");                break;
		case UEB_T7_MAX:                J2534LogLine("*UEB_T7_MAX");                break;
		case UEB_T9_MIN:                J2534LogLine("*UEB_T9_MIN");                break;
		case J1939_PINS:                J2534LogLine("*J1939_PINS");                break;
		case J1708_PINS:                J2534LogLine("*J1708_PINS");                break;
		case J1939_T1:                  J2534LogLine("*J1939_T1");                  break;
		case J1939_T2:                  J2534LogLine("*J1939_T2");                  break;
		case J1939_T3:                  J2534LogLine("*J1939_T3");                  break;
		case J1939_T4:                  J2534LogLine("*J1939_T4");                  break;
		case J1939_BRDCST_MIN_DELAY:    J2534LogLine("*J1939_BRDCST_MIN_DELAY");    break;
		case TP2_0_T_BR_INT:            J2534LogLine("*TP2_0_T_BR_INT");            break;
		case TP2_0_T_E:                 J2534LogLine("*TP2_0_T_E");                 break;
		case TP2_0_MNTC:                J2534LogLine("*TP2_0_MNTC");                break;
		case TP2_0_T_CTA:               J2534LogLine("*TP2_0_T_CTA");               break;
		case TP2_0_MNCT:                J2534LogLine("*TP2_0_MNCT");                break;
		case TP2_0_MNTB:                J2534LogLine("*TP2_0_MNTB");                break;
		case TP2_0_MNT:                 J2534LogLine("*TP2_0_MNT");                 break;
		case TP2_0_T_WAIT:              J2534LogLine("*TP2_0_T_WAIT");              break;
		case TP2_0_T1:                  J2534LogLine("*TP2_0_T1");                  break;
		case TP2_0_T3:                  J2534LogLine("*TP2_0_T3");                  break;
		case TP2_0_IDENTIFER:           J2534LogLine("*TP2_0_IDENTIFER");           break;
		case TP2_0_RXIDPASSIVE:         J2534LogLine("*TP2_0_RXIDPASSIVE");         break;
		case ISO15765_PAD_BYTE:         J2534LogLine("*ISO15765_PAD_BYTE");         break;
		case VOLVO_PARA1:               J2534LogLine("*VOLVO_PARA1");               break;
        default:				        J2534LogLine("Unknown Parameter");		    break;
        }
        J2534LogLine("Parameter:\t0x%08x", pConfig->Parameter);
        J2534LogLine("ulValue:\t0x%08x(%d)", pConfig->ulValue, pConfig->ulValue);
    }
    
}

void J2534LogDeviceConfigList(SCONFIG_LIST *pConfigList)
{
    SCONFIG *pConfig;

    J2534LogLine("ulNumOfParams = %d", pConfigList->ulNumOfParams);
    for (unsigned int i = 0; i < pConfigList->ulNumOfParams; i++)
    {
        pConfig = &pConfigList->pConfigPtr[i];
        J2534LogPrint("Config #%d \t", i);
        switch (pConfig->Parameter)
        {
        case NON_VOLATILE_STORE_1: 		J2534LogLine("NON_VOLATILE_STORE_1");	    break;
        case NON_VOLATILE_STORE_2: 		J2534LogLine("NON_VOLATILE_STORE_2");	    break;
        case NON_VOLATILE_STORE_3: 		J2534LogLine("NON_VOLATILE_STORE_3");	    break;
        case NON_VOLATILE_STORE_4: 		J2534LogLine("NON_VOLATILE_STORE_4");	    break;
        case NON_VOLATILE_STORE_5: 		J2534LogLine("NON_VOLATILE_STORE_5");	    break;
        case NON_VOLATILE_STORE_6: 		J2534LogLine("NON_VOLATILE_STORE_6");	    break;
        case NON_VOLATILE_STORE_7: 		J2534LogLine("NON_VOLATILE_STORE_7");	    break;
        case NON_VOLATILE_STORE_8: 		J2534LogLine("NON_VOLATILE_STORE_8");	    break;
        case NON_VOLATILE_STORE_9: 		J2534LogLine("NON_VOLATILE_STORE_9");	    break;
        case NON_VOLATILE_STORE_10:		J2534LogLine("NON_VOLATILE_STORE_10");	    break;
        default:				        J2534LogLine("Unknown Parameter");		    break;
        }
        // TODO:
        if (pConfig->Parameter == NON_VOLATILE_STORE_1)
            pConfig->ulValue = 0;
        if (pConfig->Parameter == NON_VOLATILE_STORE_3)
            pConfig->ulValue = 1;
        J2534LogLine("Parameter:\t0x%08x", pConfig->Parameter);
        J2534LogLine("ulValue:\t0x%08x(%d)", pConfig->ulValue, pConfig->ulValue);
    }

}

void J2534LogConnectFlag(unsigned long ulConnectFlag)
{
    if (ulConnectFlag & CONNFLAG_CAN_29BIT_ID)
        J2534LogLine("CONNFLAG_CAN_29BIT_ID");
    if (ulConnectFlag & CONNFLAG_ISO9141_NO_CHECKSUM)
        J2534LogLine("CONNFLAG_ISO9141_NO_CHECKSUM");
    //if (ulConnectFlag & CONNFLAG_NO_CHECKSUM)     // J2534-2002
    //    J2534LogLine("CONNFLAG_NO_CHECKSUM");
    if (ulConnectFlag & CONNFLAG_CAN_ID_BOTH)
        J2534LogLine("CONNFLAG_CAN_ID_BOTH");
    if (ulConnectFlag & CONNFLAG_ISO9141_K_LINE_ONLY)
        J2534LogLine("CONNFLAG_ISO9141_K_LINE_ONLY");
}

void J2534LogTxFlag(unsigned long ulTxFlag)
{
	if (ulTxFlag & TXFLAG_ISO15765_FRAME_PAD)
		J2534LogLine("TXFLAG_ISO15765_FRAME_PAD");
	if (ulTxFlag & TXFLAG_ISO15765_ADDR_TYPE)
		J2534LogLine("TXFLAG_ISO15765_ADDR_TYPE");
	if (ulTxFlag & TXFLAG_CAN_29BIT_ID)
		J2534LogLine("TXFLAG_CAN_29BIT_ID");
	if (ulTxFlag & TXFLAG_WAIT_P3_MIN_ONLY)
		J2534LogLine("TXFLAG_WAIT_P3_MIN_ONLY");
	if (ulTxFlag & TXFLAG_SW_CAN_HV_TX)
		J2534LogLine("TXFLAG_SW_CAN_HV_TX");
	if (ulTxFlag & TXFLAG_SCI_MODE)
		J2534LogLine("TXFLAG_SCI_MODE");
	if (ulTxFlag & TXFLAG_SCI_TX_VOLTAGE)
		J2534LogLine("TXFLAG_SCI_TX_VOLTAGE");
}

void J2534LogRxStatus(unsigned long ulRxStatus)
{
	if (ulRxStatus & RXSTATUS_TX_MSG_TYPE)
		J2534LogLine("RXSTATUS_TX_MSG_TYPE");
	if (ulRxStatus & RXSTATUS_START_OF_MESSAGE)
		J2534LogLine("RXSTATUS_START_OF_MESSAGE");
// 	if (ulRxStatus & RXSTATUS_ISO15765_FIRST_FRAME)
// 		J2534LogLine("RXSTATUS_ISO15765_FIRST_FRAME");
	if (ulRxStatus & RXSTATUS_RX_BREAK)
		J2534LogLine("RXSTATUS_RX_BREAK");
	if (ulRxStatus & RXSTATUS_TX_INDICATION)
		J2534LogLine("RXSTATUS_TX_INDICATION");
	if (ulRxStatus & RXSTATUS_ISO15765_PADDING_ERROR)
		J2534LogLine("RXSTATUS_ISO15765_PADDING_ERROR");
	if (ulRxStatus & RXSTATUS_ISO15765_ADDR_TYPE)
		J2534LogLine("RXSTATUS_ISO15765_ADDR_TYPE");
	if (ulRxStatus & RXSTATUS_CAN_29BIT_ID)
		J2534LogLine("RXSTATUS_CAN_29BIT_ID");
	if (ulRxStatus & RXSTATUS_SW_CAN_HV_RX)
		J2534LogLine("RXSTATUS_SW_CAN_HV_RX");
	if (ulRxStatus & RXSTATUS_SW_CAN_HS_RX)
		J2534LogLine("RXSTATUS_SW_CAN_HS_RX");
	if (ulRxStatus & RXSTATUS_SW_CAN_NS_RX)
		J2534LogLine("RXSTATUS_SW_CAN_NS_RX");
	if (ulRxStatus & RXSTATUS_J1939_ADDRESS_CLAIMED)
		J2534LogLine("RXSTATUS_J1939_ADDRESS_CLAIMED");
	if (ulRxStatus & RXSTATUS_J1939_ADDRESS_LOST)
		J2534LogLine("RXSTATUS_J1939_ADDRESS_LOST");
}