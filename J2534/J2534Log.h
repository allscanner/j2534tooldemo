﻿/*******************************************************************************
 * FILE: J2534Channel.h
 * This file is part of ALLScanner VCX software.
 * COPYRIGHT (C) ALLScanner 2008-2014. All Rights Reserved.
 *		
 * DESC: VCX J2534 Channel Operation
 * 
 * HISTORY:
 * Date			Author		Notes	
 * 2014-5-2		RTT			Create
 *	
*******************************************************************************/

#ifndef __J2534LOG_H__
#define __J2534LOG_H__

#include "j2534.h"
#include "log.h"

#ifdef __cplusplus
extern "C" {
#endif 
	

/*******************************************************************************
 * J2534Log FUNCTION
 */

#define J2534LogPrint(fmt, ...)		log_write(LOG_LEVEL_API, fmt, __VA_ARGS__)

#define J2534LogLine(fmt, ...)      log_write(LOG_LEVEL_API, fmt"\r\n", __VA_ARGS__)

void J2534LogInit(void);

void J2534LogProcess(void *hModule);
void J2534LogTimeStmap(void);
void J2534LogBuffer(unsigned char *pBuf, unsigned long ulLen);
void J2534LogByteArray(SBYTE_ARRAY *pMsg);
void J2534LogMsgs(PASSTHRU_MSG *pMsg, unsigned long ulNumMsg);
void J2534LogError(J2534_ERROR errorCode);
void J2534GetErrorMsg(J2534_ERROR errorCode, char *pMsg);

void J2534LogProtocol(J2534_PROTOCOL ulProtocolID);
void J2534LogConfigList(SCONFIG_LIST *pConfigList);
void J2534LogDeviceConfigList(SCONFIG_LIST *pConfigList);
void J2534LogConnectFlag(unsigned long ulConnectFlag);
void J2534LogTxFlag(unsigned long ulTxFlag);
void J2534LogRxStatus(unsigned long ulRxStatus);


#ifdef __cplusplus
}
#endif 

#endif	/* __J2534LOG_H__ */

