﻿/*******************************************************************************
 * FILE: PassThruSdk.cpp
 * This file is part of ALLScanner VCX software.
 * COPYRIGHT (C) ALLScanner 2008-2014. 
 *		
 * DESC: J2534 DLL Import declaration 
 * 
 * HISTORY:
 * Date			Author		Notes	
 * 2016-07-01		my		Create
*******************************************************************************/

#include "stdafx.h"
#include "PassThruSdk.h"

PassThruSdk::PassThruSdk(void)
{
	this->deviceId = 0;

	for(UINT8_T ucLoop = 0; ucLoop < CONNECT_MAX_COUNT; ucLoop++)
	{
		this->m_Channel[ucLoop].SetProtocolId(0);
		this->m_Channel[ucLoop].SetBaudRate(0);
		this->m_Channel[ucLoop].SetConectFlag(0);
		this->m_Channel[ucLoop].SetChannelId(0);
		this->m_Channel[ucLoop].SetChannelExist(FALSE);
	}
}

PassThruSdk::~PassThruSdk(void)
{
}

UINT32_T PassThruSdk::LoadDll(LPCTSTR lpAddr)
{
	return J2534DllLoad(lpAddr);			// 加载设备驱动  GetRegeditVCXPassthruPath()
}

void PassThruSdk::UnLoadDll()
{
	J2534DllUnload();
}

UINT32_T PassThruSdk::OpenDevice(void* pName)
{
	return _PassThruOpen(pName, &this->deviceId);
}

UINT32_T PassThruSdk::CloseDevice()
{
	UINT32_T RetVal = 0;
	ASSERT(this->GetDeviceId() != 0);
	RetVal = _PassThruClose(this->GetDeviceId());

	for(UINT8_T ucLoop = 0; ucLoop < CONNECT_MAX_COUNT; ucLoop++)
	{
		if(this->m_Channel[ucLoop].GetChannelExist() == TRUE)
		{
			this->m_Channel[ucLoop].SetProtocolId(0);
			this->m_Channel[ucLoop].SetBaudRate(0);
			this->m_Channel[ucLoop].SetConectFlag(0);
			this->m_Channel[ucLoop].SetChannelId(0);
			this->m_Channel[ucLoop].SetChannelExist(FALSE);
		}
	}
	this->deviceId = 0;

	return RetVal;
}

UINT32_T PassThruSdk::Connect(UINT32_T UProtocolId, UINT32_T UBaudRate, UINT32_T UConnectFlag)
{
	UINT32_T RetVal = 0, OchannelId = 0;

	for(UINT8_T ucLoop = 0; ucLoop < CONNECT_MAX_COUNT; ucLoop++)
	{
		if(this->m_Channel[ucLoop].GetChannelExist() == FALSE)
		{
			RetVal = _PassThruConnect(this->deviceId,
				UProtocolId,
				UConnectFlag,
				UBaudRate,
				&OchannelId);

			if(RetVal != 0)	break;

			this->m_Channel[ucLoop].SetChannelId(OchannelId);
			this->m_Channel[ucLoop].SetProtocolId(UProtocolId);
			this->m_Channel[ucLoop].SetBaudRate(UBaudRate);
			this->m_Channel[ucLoop].SetConectFlag(UConnectFlag);
			this->m_Channel[ucLoop].SetChannelExist(TRUE);
			this->CurrChannelId = OchannelId;
			break;
		}
	}
	return RetVal; 
}

UINT32_T PassThruSdk::DisConnect(UINT32_T uiChannelNum)
{
	UINT32_T RetVal = 0;
	RetVal = _PassThruDisconnect(this->m_Channel[uiChannelNum-1].GetChannelId());

	this->m_Channel[uiChannelNum-1].SetProtocolId(0);
	this->m_Channel[uiChannelNum-1].SetBaudRate(0);
	this->m_Channel[uiChannelNum-1].SetConectFlag(0);
	this->m_Channel[uiChannelNum-1].SetChannelId(0);
	this->m_Channel[uiChannelNum-1].SetChannelExist(FALSE);

	return RetVal;
}

UINT32_T PassThruSdk::ReadVersion(INT8_T* FwVersion, INT8_T* DllVersion, INT8_T* ApiVersion)
{
	return _PassThruReadVersion(this->deviceId, FwVersion, DllVersion, ApiVersion);
}

UINT32_T PassThruSdk::ReadMessages(UINT32_T uiChannelNum, PASSTHRU_MSG *RxMsgs, UINT32_T* RevMsgsNum, UINT32_T Timeout)
{
	PASSTHRU_MSG RecieveMsgs;
	UINT32_T RetVal = 0, RxNums = 1;

	memset(&RecieveMsgs, 0, 4128);

	RetVal = _PassThruReadMsgs(this->m_Channel[uiChannelNum-1].GetChannelId(), &RecieveMsgs, &RxNums, Timeout);

	*RevMsgsNum = RxNums;
	*RxMsgs = RecieveMsgs;
	return RetVal;
}

UINT32_T PassThruSdk::WriteMessages(UINT32_T uiChannelNum, BinaryData bData, UINT32_T* pRxStatus, UINT32_T TxFlags, UINT32_T Timeout)
{
	PASSTHRU_MSG TxMsgs;
	UINT32_T RetVal = 0, TxNums = 1;

	_PassThruIoctl(this->m_Channel[uiChannelNum-1].GetChannelId(), 8, NULL, NULL);

	TxMsgs.ulProtocolID = this->m_Channel[uiChannelNum-1].GetProtocolId();
	TxMsgs.ulRxStatus = 0;
	TxMsgs.ulTxFlags = TxFlags;
	//TxMsgs.ulTimeStamp = 0;
	TxMsgs.ulDataSize = bData.ulResLen;//4090;
	TxMsgs.ulExtraDataIndex = 0;//bData.ulResLen;//4090;
	memcpy(TxMsgs.ucData, bData.ucConvertData, bData.ulResLen);

	RetVal = _PassThruWriteMsgs(this->m_Channel[uiChannelNum-1].GetChannelId(), &TxMsgs, &TxNums, Timeout);

	*pRxStatus = TxMsgs.ulRxStatus;
	return RetVal;
}

UINT32_T PassThruSdk::StartFilterMessage(UINT32_T uiChannelNum, BinaryData pMask, BinaryData pPattern, BinaryData pFlowctl, UINT32_T uTxFlag, UINT32_T uFilterType, UINT32_T *uOutFilterId)
{
	PASSTHRU_MSG pFilterMask, pFilterPattern, pFilterFlowControl;
	UINT32_T pFilterID = 0, RetVal = 0;

	memset(&pFilterMask, 0, sizeof(pFilterMask));
	memset(&pFilterPattern, 0, sizeof(pFilterPattern));
	memset(&pFilterFlowControl, 0, sizeof(pFilterFlowControl));

	/* 获取过滤器Mask的长度及消息内容 */
	pFilterMask.ulProtocolID = this->m_Channel[uiChannelNum-1].GetProtocolId();
	pFilterMask.ulTxFlags = uTxFlag;
	pFilterMask.ulRxStatus = 0;
	pFilterMask.ulTimeStamp = 0;
	pFilterMask.ulDataSize = pMask.ulResLen;
	pFilterMask.ulExtraDataIndex = 0;//pMask.ulResLen;
	memcpy(pFilterMask.ucData, pMask.ucConvertData, pMask.ulResLen);

	/* 获取过滤器Pattern的长度及消息内容 */
	pFilterPattern.ulProtocolID = this->m_Channel[uiChannelNum-1].GetProtocolId();
	pFilterPattern.ulTxFlags = uTxFlag;
	pFilterPattern.ulRxStatus = 0;
	pFilterPattern.ulTimeStamp = 0;
	pFilterPattern.ulDataSize =  pPattern.ulResLen;
	pFilterPattern.ulExtraDataIndex = 0;//pPattern.ulResLen;
	memcpy(pFilterPattern.ucData, pPattern.ucConvertData, pPattern.ulResLen);

	/* 获取过滤器FlowControl的长度及消息内容 */
	pFilterFlowControl.ulProtocolID = this->m_Channel[uiChannelNum-1].GetProtocolId();
	pFilterFlowControl.ulTxFlags = uTxFlag;
	pFilterFlowControl.ulRxStatus = 0;
	pFilterFlowControl.ulTimeStamp = 0;
	pFilterFlowControl.ulDataSize = pFlowctl.ulResLen;
	pFilterFlowControl.ulExtraDataIndex = 0;//pFlowctl.ulResLen;
	memcpy(pFilterFlowControl.ucData, pFlowctl.ucConvertData, pFlowctl.ulResLen);

	if((this->m_Channel[uiChannelNum-1].GetProtocolId() == ISO15765)||(this->m_Channel[uiChannelNum-1].GetProtocolId() == ISO15765_PS)||(this->m_Channel[uiChannelNum-1].GetProtocolId() == SW_ISO15765_PS))
		RetVal = _PassThruStartMsgFilter(this->m_Channel[uiChannelNum-1].GetChannelId(), uFilterType, &pFilterMask, &pFilterPattern, &pFilterFlowControl, &pFilterID);
	else	/* 非ISO15765等类型协议，不需要设置流控制过滤器 */
		RetVal = _PassThruStartMsgFilter(this->m_Channel[uiChannelNum-1].GetChannelId(), uFilterType, &pFilterMask, &pFilterPattern, NULL, &pFilterID);

	*uOutFilterId = pFilterID;
	return RetVal;
}

UINT32_T PassThruSdk::StopFilterMessage(UINT32_T uiChannelNum, UINT32_T pFilterId)
{
	UINT32_T RetVal = _PassThruStopMsgFilter(this->m_Channel[uiChannelNum-1].GetChannelId(), pFilterId);
	pFilterId = 0;
	return RetVal;
}

UINT32_T PassThruSdk::StartPeriodicMessage(UINT32_T uiChannelNum, BinaryData pMsgs, UINT32_T uTxFlag, UINT32_T uInterval, UINT32_T *uOutPeriodicrId)
{
	PASSTHRU_MSG pPeriodicMsg;
	UINT32_T pPeriodicId = 0, RetVal = 0;

	pPeriodicMsg.ulProtocolID = this->m_Channel[uiChannelNum-1].GetProtocolId();
	pPeriodicMsg.ulRxStatus = 0;
	pPeriodicMsg.ulTxFlags = uTxFlag;
	pPeriodicMsg.ulTimeStamp = 0;
	pPeriodicMsg.ulDataSize = pMsgs.ulResLen;
	pPeriodicMsg.ulExtraDataIndex = 0;
	memcpy(pPeriodicMsg.ucData, pMsgs.ucConvertData, pMsgs.ulResLen);

	RetVal = _PassThruStartPeriodicMsg(this->m_Channel[uiChannelNum-1].GetChannelId(), &pPeriodicMsg, &pPeriodicId, uInterval);

	*uOutPeriodicrId = pPeriodicId;
	return RetVal;
}

UINT32_T PassThruSdk::StopPeriodicMessage(UINT32_T uiChannelNum, UINT32_T pPeriodicId)
{
	return _PassThruStopPeriodicMsg(this->m_Channel[uiChannelNum-1].GetChannelId(), pPeriodicId);
}

//void PassThruSdk::Clear()
//{
	//this->ChannelId = 0;
	//this->ProtocolId = 0;
	//this->BaudRate = 0;
	//this->ConnectFlag = 0;
//}

UINT32_T PassThruSdk::GetDeviceId() const
{
	return this->deviceId;
}

UINT32_T PassThruSdk::GetCurrChannelId() const
{
	return this->CurrChannelId;
}

#if 0
UINT32_T PassThruSdk::GetChannelId(UINT32_T ChannelNums) const
{
	return this->m_Channel[ChannelNums-1].GetChannelId();
}

UINT32_T PassThruSdk::GetProtocolId(UINT32_T ChannelNums) const
{
	return this->m_Channel[ChannelNums-1].GetProtocolId();
}

BOOL PassThruSdk::GetChannelStatus(UINT32_T ChannelNums) const
{
	return this->m_Channel[ChannelNums-1].GetChannelExist();
}
#endif

UINT32_T ProtocolStrToValue(CString strProtocol)
{
	UINT32_T ulprotocolId = 0;
	// 获取协议名称控件的当前值
	if("J1850VPW" == strProtocol)
		ulprotocolId =  J1850VPW;
	else if("J1850PWM" == strProtocol)
		ulprotocolId =  J1850PWM;
	else if("ISO9141" == strProtocol)
		ulprotocolId =  ISO9141;
	else if("ISO14230" == strProtocol)
		ulprotocolId =  ISO14230;
	else if("CAN" == strProtocol)
		ulprotocolId =  CAN;
	else if("ISO15765" == strProtocol)
		ulprotocolId =  ISO15765;
	else if("SCI_A_ENGINE" == strProtocol)
		ulprotocolId =  SCI_A_ENGINE;
	else if("SCI_A_TRANS" == strProtocol)
		ulprotocolId =  SCI_A_TRANS;
	else if("SCI_B_ENGINE" == strProtocol)
		ulprotocolId =  SCI_B_ENGINE;
	else if("SCI_B_TRANS" == strProtocol)
		ulprotocolId =  SCI_B_TRANS;
	else if("J1850VPW_PS" == strProtocol)
		ulprotocolId =  J1850VPW_PS;
	else if("J1850PWM_PS" == strProtocol)
		ulprotocolId =  J1850PWM_PS;
	else if("ISO9141_PS" == strProtocol)
		ulprotocolId =  ISO9141_PS;
	else if("ISO14230_PS" == strProtocol)
		ulprotocolId =  ISO14230_PS;
	else if("CAN_PS" == strProtocol)
		ulprotocolId =  CAN_PS;
	else if("ISO15765_PS" == strProtocol)
		ulprotocolId =  ISO15765_PS;
	else if("SW_CAN_PS" == strProtocol)
		ulprotocolId =  SW_CAN_PS;
	else if("SW_ISO15765_PS" == strProtocol)
		ulprotocolId =  SW_ISO15765_PS;
	else if("J1939_PS" == strProtocol)
		ulprotocolId = J1939_PS;
	else if("VOLVO_CAN2" == strProtocol)
		ulprotocolId = VOLVO_CAN2;
	return ulprotocolId;
}

CString ProtocolValueToStr(unsigned long ulProtocol)
{
	CString strProtocol;
	if(ulProtocol == J1850VPW)
		strProtocol = "J1850VPW";
	else if(ulProtocol == J1850PWM)
		strProtocol = "J1850PWM";
	else if(ulProtocol == ISO9141)
		strProtocol = "ISO9141";
	else if(ulProtocol == ISO14230)
		strProtocol = "ISO14230";
	else if(ulProtocol == CAN)
		strProtocol = "CAN";
	else if(ulProtocol == ISO15765)
		strProtocol = "ISO15765";
	else if(ulProtocol == SCI_A_ENGINE)
		strProtocol = "SCI_A_ENGINE";
	else if(ulProtocol == SCI_A_TRANS)
		strProtocol = "SCI_A_TRANS";
	else if(ulProtocol == SCI_B_ENGINE)
		strProtocol = "SCI_B_ENGINE";
	else if(ulProtocol == SCI_B_TRANS)
		strProtocol = "SCI_B_TRANS";
	else if(ulProtocol == J1850VPW_PS)
		strProtocol = "J1850VPW_PS";
	else if(ulProtocol == J1850PWM_PS)
		strProtocol = "J1850PWM_PS";
	else if(ulProtocol == ISO9141_PS)
		strProtocol = "ISO9141_PS";
	else if(ulProtocol == ISO14230_PS)
		strProtocol = "ISO14230_PS";
	else if(ulProtocol == CAN_PS)
		strProtocol = "CAN_PS";
	else if(ulProtocol == ISO15765_PS)
		strProtocol = "ISO15765_PS";
	else if(ulProtocol == SW_CAN_PS)
		strProtocol = "SW_CAN_PS";
	else if(ulProtocol == SW_ISO15765_PS)
		strProtocol = "SW_ISO15765_PS";
	else if(strProtocol == J1939_PS)
		strProtocol = "J1939_PS";
	else if(strProtocol == VOLVO_CAN2)
		strProtocol = "VOLVO_CAN2";
	return strProtocol;
}

UINT32_T IoctlStrToValue(CString cIoctlName)
{
	UINT32_T ulIoctlValue;
	if (cIoctlName == "GET_CONFIG")
		ulIoctlValue = GET_CONFIG;
	else if (cIoctlName == "SET_CONFIG")
		ulIoctlValue = SET_CONFIG;
	else if (cIoctlName == "READ_VBATT")
		ulIoctlValue = READ_VBATT;
	else if (cIoctlName == "FIVE_BAUD_INIT")
		ulIoctlValue = FIVE_BAUD_INIT;
	else if (cIoctlName == "FAST_INIT")
		ulIoctlValue = FAST_INIT;
	else if (cIoctlName == "SET_PIN_USE")
		ulIoctlValue = SET_PIN_USE;
	else if (cIoctlName == "CLEAR_TX_BUFFER")
		ulIoctlValue = CLEAR_TX_BUFFER;
	else if (cIoctlName == "CLEAR_RX_BUFFER")
		ulIoctlValue = CLEAR_RX_BUFFER;
	else if (cIoctlName == "CLEAR_PERIODIC_MSGS")
		ulIoctlValue = CLEAR_PERIODIC_MSGS;
	else if (cIoctlName == "CLEAR_MSG_FILTERS")
		ulIoctlValue = CLEAR_MSG_FILTERS;
	else if (cIoctlName == "CLEAR_FUNCT_MSG_LOOKUP_TABLE")
		ulIoctlValue = CLEAR_FUNCT_MSG_LOOKUP_TABLE;
	else if (cIoctlName == "ADD_TO_FUNCT_MSG_LOOKUP_TABLE")
		ulIoctlValue = ADD_TO_FUNCT_MSG_LOOKUP_TABLE;
	else if (cIoctlName == "DELETE_FROM_FUNCT_MSG_LOOKUP_TABLE")
		ulIoctlValue = DELETE_FROM_FUNCT_MSG_LOOKUP_TABLE;
	else if (cIoctlName == "READ_PROG_VOLTAGE")
		ulIoctlValue = READ_PROG_VOLTAGE;
	return ulIoctlValue;
}

UINT32_T ChannelStrToValue(CString cChannelName)
{
	UINT32_T ulChannelId = 0;
	if("Channel 1" == cChannelName)
		ulChannelId = 1;
	else if("Channel 2" == cChannelName)
		ulChannelId = 2;
	else if("Channel 3" == cChannelName)
		ulChannelId = 3;
	else if("Channel 4" == cChannelName)
		ulChannelId = 4;
	else if("Channel 5" == cChannelName)
		ulChannelId = 5;
	else
		ulChannelId = 0;
	return ulChannelId;
}

CString IoctlValueToStr(unsigned long ulIoctlName)
{
	CString strIoctl;
	if (ulIoctlName == GET_CONFIG)
		strIoctl = "GET_CONFIG";
	else if (ulIoctlName == SET_CONFIG)
		strIoctl = "SET_CONFIG";
	else if (ulIoctlName == READ_VBATT)
		strIoctl = "READ_VBATT";
	else if (ulIoctlName == FIVE_BAUD_INIT)
		strIoctl = "FIVE_BAUD_INIT";
	else if (ulIoctlName == FAST_INIT)
		strIoctl = "FAST_INIT";
	else if (ulIoctlName == SET_PIN_USE)
		strIoctl = "SET_PIN_USE";
	else if (ulIoctlName == CLEAR_TX_BUFFER)
		strIoctl = "CLEAR_TX_BUFFER";
	else if (ulIoctlName == CLEAR_RX_BUFFER)
		strIoctl = "CLEAR_RX_BUFFER";
	else if (ulIoctlName == CLEAR_PERIODIC_MSGS)
		strIoctl = "CLEAR_PERIODIC_MSGS";
	else if (ulIoctlName == CLEAR_MSG_FILTERS)
		strIoctl = "CLEAR_MSG_FILTERS";
	else if (ulIoctlName == CLEAR_FUNCT_MSG_LOOKUP_TABLE)
		strIoctl = "CLEAR_FUNCT_MSG_LOOKUP_TABLE";
	else if (ulIoctlName == ADD_TO_FUNCT_MSG_LOOKUP_TABLE)
		strIoctl = "ADD_TO_FUNCT_MSG_LOOKUP_TABLE";
	else if (ulIoctlName == DELETE_FROM_FUNCT_MSG_LOOKUP_TABLE)
		strIoctl = "DELETE_FROM_FUNCT_MSG_LOOKUP_TABLE";
	else if (ulIoctlName == READ_PROG_VOLTAGE)
		strIoctl = "READ_PROG_VOLTAGE";
	return strIoctl;
}

UINT32_T FilterStrToValue(CString strFilterType)
{
	UINT32_T ulFilterTypeValue;
	if(strFilterType == "Pass")
		ulFilterTypeValue = J2534_FILTER_PASS;
	else if(strFilterType == "Block")
		ulFilterTypeValue = J2534_FILTER_BLOCK;
	else if(strFilterType == "Flow")
		ulFilterTypeValue = J2534_FILTER_FLOW_CONTROL;
	else if(strFilterType == "None")
		ulFilterTypeValue = J2534_FILTER_NONE;
	return ulFilterTypeValue;
}

CString FilterValueToStr(UINT32_T ulFilterType)
{
	CString strFilterType;
	if(ulFilterType == J2534_FILTER_PASS)
		strFilterType = "Pass";
	if(ulFilterType == J2534_FILTER_BLOCK)
		strFilterType = "Block";
	if(ulFilterType == J2534_FILTER_FLOW_CONTROL)
		strFilterType = "Flow";
	if(ulFilterType == J2534_FILTER_NONE)
		strFilterType = "None";
	return strFilterType;
}
