﻿/*******************************************************************************
 * FILE: PassThruSdk.h
 * This file is part of ALLScanner VCX software.
 * COPYRIGHT (C) ALLScanner 2008-2014. 
 *		
 * DESC: J2534 DLL Import Defines 
 * 
 * HISTORY:
 * Date			Author		Notes	
 * 2016-01-01		my		Create
*******************************************************************************/

#pragma once

#include "../J2534/J2534Dll.h"
#include "../J2534/J2534.h"
#include "../J2534/J2534Log.h"
#include "../J2534ToolDemo/Common/applications.h"
#include "../J2534ToolDemo/Common/CString.h"
#include "Channel.h"
#include <map>

#define FILTER_MAX_COUNT	10
#define PERIODIC_MAX_COUNT	10
#define CONNECT_MAX_COUNT	10

// Error codes
typedef enum
{
	PRO_OK,							// 成功.
	PRO_ECU_BUSY,					// ECU忙碌
	PRO_ERR_DEFAULT,				// 缺省错误
	PRO_ERR_LOADDLL,				// DLL加载失败
	PRO_ERR_PARAMETER,				// 参数错误
	PRO_ERR_DEVICE,					// VCI设备未连接.
	PRO_ERR_COMM,					// 通信错误
	PRO_ERR_ECU_NACK,				// ECU否定应答
	PRO_ERR_ECU_BREAK,				// ECU断开连接.
	PRO_BUFF_EMPTY,					// ECU无回复
	PRO_ERROR = 1000,
} PRO_ERR_T;

/************************************************************
	*功		 能：过滤器配置参数结构体，存放数据
	*参 数 说 明：无
	*返  回	 值：无
	*说	     明：无
************************************************************/
typedef struct tagFilters
{
	BinaryData	pMask;				// 屏蔽消息内容
	BinaryData	pPattern;			// 比较消息内容
	BinaryData	pFlowControl;		// 流控制消息内容
	UINT32_T	ulTxFlags;
	UINT32_T	ulFilterType;
	UINT32_T	ulFilterId;
	BOOL		bCheckStatus;
}FilterParam;

/************************************************************
	*功		 能：特定通道的过滤器配置参数结构体，存放指定通道的过滤器数据
	*参 数 说 明：无
	*返  回	 值：无
	*说	     明：无
************************************************************/
typedef struct tagChannelFilters
{
	UINT32_T	ChannelNumber;
	FilterParam	_filterParam[FILTER_MAX_COUNT];
}ChannelFilters;

/************************************************************
	*功		 能：定时器配置参数结构体，存放数据
	*参 数 说 明：无
	*返  回	 值：无
	*说	     明：无
************************************************************/
typedef struct  tagPeriodics
{
	BinaryData	pMessage;			// 定时发送的消息内容
	UINT32_T	ulTxFlags;
	UINT32_T	ulIntervel;
	UINT32_T	ulMsgId;
	BOOL		bCheckStatus;
}PeriodicParam;

/************************************************************
	*功		 能：特定通道的定时器配置参数结构体，存放指定通道的定时器数据
	*参 数 说 明：无
	*返  回	 值：无
	*说	     明：无
************************************************************/
typedef struct tagChannelPeriodics
{
	UINT32_T		ChannelNumber;
	PeriodicParam	_periodicParam[PERIODIC_MAX_COUNT];
}ChannelPeriodics;

//map<UINT32_T, PeriodicParam> ChannelPeriodics;

/************************************************************
	*功		 能：Ioctl配置参数结构体，存放数据
	*参 数 说 明：无
	*返  回	 值：无
	*说	     明：无
************************************************************/
typedef struct tagIoctls
{
	UINT32_T	ulDataRate;
	BOOL		bLookBack;
	UINT8_T		ucNodeAddress;
	UINT8_T		ucNetworkLine;
	UINT16_T	uiP1Max;
	UINT16_T	uiP3Min;
	UINT16_T	uiP4Min;
	UINT16_T	uiW0;
	UINT16_T	uiW1;
	UINT16_T	uiW2;
	UINT16_T	uiW3;
	UINT16_T	uiW4;
	UINT16_T	uiW5;
	UINT16_T	uiTidle;
	UINT16_T	uiTinil;
	UINT16_T	uiTwup;
	UINT8_T		ucParity;
	BOOL		bDataBits;	// 0-表示8个数据位； 1-表示7个数据位
	UINT8_T		ucFiveBaudMod;
	UINT8_T		ucBitSamplePoint;
	UINT8_T		ucSyncJumpWidth;
	UINT8_T		ucIso15765BS;
	UINT8_T		ucIso15765Stmin;
	UINT16_T	uiBsTx;
	UINT16_T	uiStminTx;
	UINT8_T		ucIso15765WftMax;
	UINT32_T	ulIso15765Simultaneous;
	UINT32_T	ulDtIso15765PadByte;
	UINT32_T	ulCanMixedFormat;
	UINT32_T	ulSwCanHsDataRate;
	UINT32_T	ulSwCanSpdchgEnable;
	UINT32_T	ulSwCanResSwitch;
	UINT16_T	uiJ1962Pins;
	BOOL		bIsSetStatus;		// 判断配置参数是否被设置
	BOOL		bIsSetInit;			// 连接通道后，判断通道是否初始化
}IoctlParam;

/************************************************************
	*功		 能：特定通道的Ioctl配置参数结构体，存放指定通道的Ioctl数据
	*参 数 说 明：无
	*返  回	 值：无
	*说	     明：无
************************************************************/
typedef struct tagChannelIoctlParams
{
	UINT32_T	ChannelNumber;
	IoctlParam	_ioctlParam;
}ChannelIoctlParams;

/************************************************************
	*功		 能：初始化进入参数的结构体，存放初始化进入数据
	*参 数 说 明：无
	*返  回	 值：无
	*说	     明：无
************************************************************/
typedef struct tagInits
{
	INT8_T	cECUAddr;
	INT8_T	cKeyWord1;
	INT8_T	cKeyWord2;
	BinaryData	bSendMsgs;
	UINT32_T	ulTxFlag;
	BinaryData	bRevMegs;
	UINT32_T	ulRxFlag;
	BOOL		bInitStatus;
	BOOL		bFastSetStatus;
	BOOL		bFiveSetStatus;
}InitParams;

/************************************************************
	*功		 能：特定通道的初始化进入参数结构体，存放指定通道的初始化进入参数数据
	*参 数 说 明：无
	*返  回	 值：无
	*说	     明：无
************************************************************/
typedef struct tagChannelInitConfig
{
	UINT32_T	ChannelNumber;
	InitParams	_initConfig;
}ChannelInits;

/************************************************************
	*功		 能：SendMessage参数的结构体，存放消息内容
	*参 数 说 明：无
	*返  回	 值：无
	*说	     明：无
************************************************************/
typedef struct tagSendMessages
{
	UINT32_T	ulProtocolId;
	UINT32_T	ulTxFlag;
	UINT32_T	ulTimeOut;
	BinaryData	bSendMegs;
}SendMessages;

/************************************************************
	*功		 能：特定通道的SendMessage参数结构体，存放指定通道的消息内容
	*参 数 说 明：无
	*返  回	 值：无
	*说	     明：无
************************************************************/
typedef struct tagChannelSendMessages
{
	UINT32_T		ChannelNumber;
	SendMessages	_sendMessage;
}ChannelSendMessages;

/************************************************************
	*功		 能：RecieveMessage参数的结构体，存放消息内容
	*参 数 说 明：无
	*返  回	 值：无
	*说	     明：无
************************************************************/
typedef struct tagRevMessages
{
	PASSTHRU_MSG	pJ2534ToolSdk;
	UINT32_T		uMsgsNum;
}RevMessages;

/************************************************************
	*功		 能：特定通道的RecieveMessage参数结构体，存放指定通道的消息内容
	*参 数 说 明：无
	*返  回	 值：无
	*说	     明：无
************************************************************/
typedef struct tagChannelRecieveInfo
{
	UINT32_T		ChannelNumber;
	UINT32_T		uProtocolValue;
	BOOL			bStatus;
}ChannelRecieveInfo;

class PassThruSdk
{
public:
	PassThruSdk(void);
	virtual ~PassThruSdk(void);
		
	UINT32_T LoadDll(LPCTSTR lpAddr);
	void UnLoadDll();
	UINT32_T OpenDevice(void* pName);
	UINT32_T CloseDevice();
	UINT32_T Connect(UINT32_T uProtocolId, UINT32_T uBaudRate, UINT32_T uConnectFlag);
	UINT32_T DisConnect(UINT32_T uichannelnum);
	UINT32_T ReadVersion(INT8_T* FwVersion, INT8_T* DllVersion, INT8_T* ApiVersion);
	UINT32_T StartFilterMessage(UINT32_T uichannelnum, BinaryData pMask, BinaryData pPattern, BinaryData pFlowctl, UINT32_T uTxFlag, UINT32_T uFilterType, UINT32_T *uOutFilterId);
	UINT32_T StopFilterMessage(UINT32_T uichannelnum, UINT32_T pFilterId);
	UINT32_T StartPeriodicMessage(UINT32_T uichannelnum, BinaryData pMsgs, UINT32_T uTxFlag, UINT32_T uInterval, UINT32_T *uOutPeriodicrId);
	UINT32_T StopPeriodicMessage(UINT32_T uichannelnum, UINT32_T pPeriodicId);
	UINT32_T ReadMessages(UINT32_T uichannelnum, PASSTHRU_MSG *RxMsgs, UINT32_T* RevMsgsNum, UINT32_T Timeout);
	UINT32_T WriteMessages(UINT32_T uichannelnum, BinaryData bData, UINT32_T* pRxStatus, UINT32_T TxFlags, UINT32_T Timeout);
	
	UINT32_T GetDeviceId() const;

	UINT32_T GetCurrChannelId() const;
	//UINT32_T GetChannelId(UINT32_T uichannelnum) const;
	//UINT32_T GetProtocolId(UINT32_T uichannelnum) const;
	//BOOL GetChannelStatus(UINT32_T uichannelnum) const;

protected:
	UINT32_T deviceId;
	//UINT32_T ChannelId;
	//UINT32_T ProtocolId;
	//UINT32_T BaudRate;
	//UINT32_T ConnectFlag;

	UINT32_T CurrChannelId;

public:
	CChannel m_Channel[CONNECT_MAX_COUNT];	// 实现多通道
};

UINT32_T ProtocolStrToValue(CString cProtocolName);
UINT32_T IoctlStrToValue(CString cIoctlName);
UINT32_T FilterStrToValue(CString strFilterType);
UINT32_T ChannelStrToValue(CString cChannelName);

CString ProtocolValueToStr(UINT32_T ulProtocolName);
CString IoctlValueToStr(UINT32_T ulIoctlName);
CString FilterValueToStr(UINT32_T ulFilterType);

