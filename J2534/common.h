/*******************************************************************************
 * FILE: common.h
 * This file is part of ALLScanner VCX software.
 * COPYRIGHT (C) ALLScanner 2008-2012.
 * 
 * DESC: common header.
 * 
 * HISTORY:
 * Data         Author      Notes 
 * 2012-8-6     RTT         Create
 *
*******************************************************************************/
#ifndef __COMMON_H__
#define __COMMON_H__


#ifdef _WIN32

#include <stdlib.h>
#include <stdio.h>
#include <assert.h>
#include <string.h>
//#include "log.h"

#define _malloc				    malloc
#define _free				    free
#define _ASSERT				    assert

#ifndef _CONSOLE
#define LOG_FILE
#endif

#ifdef LOG_FILE
#define _printf				    LOG_API
#else
#define _printf				    printf
#endif

#endif


/*******************************************************************************
 * 	inline
 */
#undef _INLINE
#undef _ASM
#if defined ( _WIN32 )
	#define _INLINE				static inline
#elif defined ( __CC_ARM )
	#define _ASM				__asm
	#define _INLINE				static __inline
#elif defined ( __ICCARM__ )
	#define _ASM				__asm
	#define _INLINE				static inline
#else
	#define _INLINE				
#endif


/*******************************************************************************
 * 	bool
 */

#ifndef	TRUE
	#define TRUE				1
#endif

#ifndef	FALSE 
	#define FALSE				0
#endif

#ifndef	NULL
	#define	NULL				0
#endif


/*******************************************************************************
 * 	data type
 */

typedef	unsigned char			uint8_t;
typedef	unsigned short			uint16_t;
typedef	unsigned int			uint32_t;
typedef unsigned long long		uint64_t;

#ifndef __IO
#define	__IO    				volatile
#endif



/*******************************************************************************
 * 	GET_TIME_MS
 */

#ifdef _WIN32

#include <windows.h>	//windows.h include mmsystem.h

// 返回当前时间 单位：微秒
_INLINE unsigned long long HighGetTimeUs(void) 
{
    static LARGE_INTEGER liFreq = {0};
    static LARGE_INTEGER liStart = {0};
    LARGE_INTEGER liCount = {0};

    if (0 == liFreq.QuadPart)
        QueryPerformanceFrequency(&liFreq);
#ifdef GET_TIME_START_FROM_0
    if (0 == liStart.QuadPart)
        QueryPerformanceCounter(&liStart);
	QueryPerformanceCounter(&liCount);
	return (unsigned long long)((liCount.QuadPart - liStart.QuadPart) * 1000000 / liFreq.QuadPart);
#else
    QueryPerformanceCounter(&liCount);
    return (unsigned long long)((liCount.QuadPart) * 1000000 / liFreq.QuadPart);
#endif
}

#define GET_TIME_MS		        ( (unsigned long)(HighGetTimeUs() / 1000) )
#define GET_TIME_US		        ( (unsigned long)HighGetTimeUs() )

#endif


/*******************************************************************************
 * 	common used macro
 */

#ifndef ABS
	#define ABS(x)				( ((x) > 0) ? (x) : -(x) )
#endif

#ifndef	MAX
	#define	MAX(a, b)			( ((a) > (b)) ? (a) : (b) )
#endif

#ifndef	MIN
	#define	MIN(a, b)			( ((a) < (b)) ? (a) : (b) )
#endif


#define UP_CASE(c)				( ((c) >= 'a' && (c) <= 'z') ? ((c) - 0x20) : (c) ) 

#define IS_DEC(c)				( (c) >= '0' && (c) <= '9' ) 

#define IS_AF(c)  				( (c >= 'A') && (c <= 'F') || ((c) >= 'a' && (c) <= 'f') )

#define IS_HEX(c)				( IS_DEC(c) || IS_AF(c) )

#define CONVERT_DEC(c)  		( c - '0' )

#define CONVERT_AF(c)			( IS_AF(c) ? (UP_CASE(c) - 'A' + 10) : 0 )

#define CONVERT_HEX(c)   		( IS_DEC(c) ? (c - '0') : CONVERT_AF(c) )

#define BYTE2CHAR(b)			( (b > 9) ? (b + 0x37) : (b + '0') )


// 判断是否在取值范围内
#define ISINRANGE(data, ma, mi)	( (data >= mi) && (data <= ma) )


// 得到一个field在结构体(struct)中的偏移量
#define FPOS(type, field)		( (uint32)&(( type *)0)->field )	


// 得到一个结构体中field所占用的字节数
#define FSIZE(type, field)		sizeof( ((type *) 0)->field )	


// 防止溢出的一个方法 
#define INC_SAT(val)			( val = ((val)+1 > (val)) ? (val)+1 : (val))


// 返回数组元素的个数
#define ARR_SIZE(a)				( sizeof( (a) ) / sizeof( (a[0]) ) )


// 交换四字节顺序
#define SWAP_4BYTES(ptr)		do { uint8_t _byte;			\
	_byte = (ptr)[0]; (ptr)[0] = (ptr)[3]; (ptr)[3] = _byte;	\
	_byte = (ptr)[1]; (ptr)[1] = (ptr)[2]; (ptr)[2] = _byte; } while (0)

// 交换二字节顺序
#define SWAP_2BYTES(ptr)		do { uint8_t _byte;			\
	_byte = (ptr)[0]; (ptr)[0] = (ptr)[1]; (ptr)[1] = _byte; } while (0)

#define SWAP_U16(a)			  	(((uint16_t)(a) & 0xff00) >> 8 |		\
								 ((uint16_t)(a) & 0x00ff) << 8 )

#define SWAP_U32(a)			  	(((uint32_t)(a) & 0xff000000) >> 24 |	\
								 ((uint32_t)(a) & 0x00ff0000) >> 8 |		\
								 ((uint32_t)(a) & 0x0000ff00) << 8 |		\
								 ((uint32_t)(a) & 0x000000ff) << 24 )

#define READ_U32_LSB(ptr)		( *(uint32_t *)(ptr) )
#define READ_U16_LSB(ptr)		( *(uint16_t *)(ptr) )

#define READ_U32_MSB(ptr)		((ptr)[0] << 24 |	\
								 (ptr)[1] << 16 |	\
								 (ptr)[2] << 8  |	\
								 (ptr)[3] )

#define READ_U16_MSB(ptr)		((ptr)[0] << 8  |	\
								 (ptr)[1] )

#define SET_BITX(a, b)    		( a |= 1U << b )
#define CLR_BITX(a, b)  		( a &= ~(1U << b) )
#define GET_BITX(a, b)	   		( (a >> b) & 0x01 )

// NOTE: must 0 < b < 63
#define SET_BITX64(a, b)    	( ((uint8_t *)&a)[b/8] |= (1 << (b%8)) )
#define CLR_BITX64(a, b)    	( ((uint8_t *)&a)[b/8] &= ~(1 << (b%8)) )
// NOTE: when bit is TRUE, return could >= 1, NOT SURE == 1
#define GET_BITX64(a, b)    	( ((uint8_t *)&a)[b/8] & (1 << (b%8)) )


#define DATE2U32(y, m, d)		( ((y-1970) << 24) + (m << 16) + (d << 8) )
#define DATE2U16(y, m, d)		( ((y-1970) << 9) + (m << 5) + (d) )

/******************************************************************************/


#endif /* __COMMON_H__ */
