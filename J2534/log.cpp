﻿#include "stdafx.h"
#include <Windows.h>
#include <stdio.h>
#include <share.h>
#include <time.h>
#include <tchar.h>

#include "log.h"


typedef struct _log
{
	char path[MAX_PATH];
	int level;
    int flag;
    int initted;
    int console;
    HANDLE handle;
} LOG_T;

LOG_T gLog =
{
    "console",//"C:\\VCX\\VCX.log",	
    LOG_LEVEL_API,	
    LOG_FLAG_ADD,
	FALSE,
    FALSE,
    NULL,
};

#define STR_SZ  256
void log_get_timestamp(char *str)
{
    SYSTEMTIME stm;
    if (str == NULL) return;
    //GetSystemTime(&stm);   // 获取UTC时间
    GetLocalTime(&stm);      // 获取本地时间
    sprintf_s(str, STR_SZ, "%02d:%02d:%02d.%03d", stm.wHour, stm.wMinute, stm.wSecond, stm.wMilliseconds);
}
void log_get_time(char *str)
{
    SYSTEMTIME stm;
    if (str == NULL) return;
    //GetSystemTime(&stm);   // 获取UTC时间
    GetLocalTime(&stm);      // 获取本地时间
    sprintf_s(str, STR_SZ, "%02d%02d%02d", stm.wHour, stm.wMinute, stm.wSecond);
}
void log_get_date(char *str)
{
    SYSTEMTIME stm;
    if (str == NULL) return;
    //GetSystemTime(&stm);   // 获取UTC时间
    GetLocalTime(&stm);      // 获取本地时间
	sprintf_s(str, STR_SZ, "%04d%02d%02d", stm.wYear, stm.wMonth, stm.wDay);
}


int log_init(char *path, unsigned int level, unsigned int flag)
{
	DWORD createFlag = CREATE_ALWAYS;
	char path_str[MAX_PATH] = "";
	char date_str[STR_SZ] = "";
	char time_str[STR_SZ] = "";
    
	//if (gLog[level].initted == TRUE)
	//{
	//	return TRUE;
	//}
	gLog.handle = INVALID_HANDLE_VALUE;
	gLog.initted = FALSE;
    gLog.console = FALSE;
    strcpy_s(path_str, MAX_PATH, path);

    if (strcmp(path_str, "CONOUT$") == 0)
    {
        gLog.handle = GetStdHandle(STD_OUTPUT_HANDLE);
        if (gLog.handle == INVALID_HANDLE_VALUE)
        {
            // TODO: 非终端应用程序调用DLL， 以下方法不能创建出终端窗口
            gLog.handle = CreateFile(	(TCHAR *)path_str, 
                GENERIC_WRITE, 
                FILE_SHARE_WRITE, 
                NULL, 
                OPEN_EXISTING ,	        
                FILE_ATTRIBUTE_NORMAL,  // Ignored for Console
                NULL);
            if (gLog.handle == INVALID_HANDLE_VALUE){
                return false ;
            }
        }
        gLog.console = TRUE;
	}
	else 
    {
        gLog.console = FALSE;
		if (flag == LOG_FLAG_NEW)
		{
            // 去除扩展名 vcx.log
            for (int i = strlen(path_str) - 1; i >= 0 ; i--)
			{
				if(path_str[i] == '.')
				{
					path_str[i] = '\0';
					break;
				}
				path_str[i] = '\0';
			}

            // 添加日期时间: vcx_2014-01-01_08-08-08
			log_get_date(date_str);
			log_get_time(time_str);
			strcat_s(path_str, MAX_PATH, "_");
			strcat_s(path_str, MAX_PATH, date_str);
			strcat_s(path_str, MAX_PATH, "_");
			strcat_s(path_str, MAX_PATH, time_str);
			strcat_s(path_str, MAX_PATH, ".log");

            createFlag = OPEN_ALWAYS;	// 如果文件存在，则打开文件；如果不存在，则创建一个新文件
		}
		else if (flag == LOG_FLAG_ADD)
		{
			createFlag = OPEN_ALWAYS;	// 如果文件存在，则打开文件；如果不存在，则创建一个新文件
		}
		else
		{
			createFlag = CREATE_ALWAYS;	// 如果文件存在则清除并重建该文件；如果文件不存在，则创建一个新文件
		}

		gLog.handle = CreateFile(	(TCHAR *)path_str, 
			GENERIC_WRITE, 
			FILE_SHARE_READ | FILE_SHARE_WRITE, 
			NULL, 
			createFlag,	        
			FILE_ATTRIBUTE_NORMAL,  // | FILE_FLAG_NO_BUFFERING,// 直接对磁盘文件进行中间缓存写操作(慢)
			NULL);

		if (gLog.handle == INVALID_HANDLE_VALUE){
			return false ;
		}

		// 移动到文件末尾
		SetFilePointer(gLog.handle, 0, NULL, FILE_END) ;
	}
	
    strcpy_s(gLog.path, MAX_PATH, path_str);
	gLog.initted = TRUE;
    gLog.level = level;
    gLog.flag = flag;

    log_get_date(date_str);
    log_get_time(time_str);
    log_write(LOG_LEVEL_ALL, "\r\nLOG TIME  = %s %s \r\n", date_str, time_str);
    log_write(LOG_LEVEL_ALL, "LOG PATH  = %s \r\n", gLog.path);
    log_write(LOG_LEVEL_ALL, "LOG LEVEL = 0x%02x \r\n", gLog.level);
    log_write(LOG_LEVEL_ALL, "LOG FLAG  = 0x%02x \r\n", gLog.flag);

	return TRUE;
}

int log_deinit(void)
{
    if (gLog.initted == FALSE)
        return FALSE;

	//if (gLog.console == TRUE)
		CloseHandle(gLog.handle);

	gLog.handle = INVALID_HANDLE_VALUE;
	gLog.initted = FALSE;
	gLog.console = FALSE;
    return TRUE;
}

#define LOG_BUFFER_SIZE     1024
int log_write(unsigned int level, const char *format, ...)
{
    va_list args;
    char printBuffer[LOG_BUFFER_SIZE];
	DWORD printLen;
	DWORD dwLen;

	if (gLog.initted == FALSE)
		return FALSE;
	if ((gLog.level & level) == 0)
		return TRUE;

	va_start (args, format);
	printLen = vsprintf_s(printBuffer, LOG_BUFFER_SIZE, format, args);
	va_end (args);

	return WriteFile(gLog.handle, printBuffer, printLen, &dwLen, NULL);
}


int log_test(void)
{
	char str[STR_SZ] = "\0";

	printf("log_test \r\n") ;
	
	log_init("C:\\VCX\\VCX.LOG", LOG_LEVEL_API, LOG_FLAG_NEW);
	log_get_time(str);
    LOG_ERR("LOG_ERR test: %s \r\n", str);
    LOG_DBG("LOG_DBG test: %s \r\n", str);
    LOG_COM("LOG_COM test: %s \r\n", str);
    LOG_API("LOG_MSG test: %s \r\n", str);
	Sleep(1000);
	log_deinit();
	
	printf("log_test speed test 100000 times: \r\n");
	log_get_time(str);
	printf("start time = %s \r\n", str);
	log_init("C:\\VCX\\VCX.LOG", LOG_LEVEL_API, LOG_FLAG_CLR);
	for (int i = 0; i < 100000; i++)
	{
		log_get_time(str);
		LOG_COM("\nLOG_LEVEL_COMM test [%05d]: %s \r\n", i, str);
	}
    log_get_time(str);
    printf("stop time = %s \r\n", str);
    log_deinit();

	printf("log test ok. \r\n");
	//getchar() ;
	return 0;
}

