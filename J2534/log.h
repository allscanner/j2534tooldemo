﻿#ifndef __LOG_H__
#define __LOG_H__

#ifdef	__cplusplus
extern "C"{
#endif



/* 日志生成方式 */
typedef enum _LOG_FLAG
{
    LOG_FLAG_CLR	        = 0x00,	// 清除
	LOG_FLAG_ADD		    = 0x01,	// 追加
	LOG_FLAG_NEW		    = 0x02	// 新建
} LOG_FLAG_T;

/* 日志级别 */
typedef enum _LOG_LEVEL
{
    LOG_LEVEL_OFF           = 0x00,
	LOG_LEVEL_ERR		    = 0x01,	// 错误
	LOG_LEVEL_DBG		    = 0x02, // 调试
	LOG_LEVEL_COM		    = 0x04, // 通信
	LOG_LEVEL_API		    = 0x08, // 消息
    LOG_LEVEL_ALL           = 0xFF
} LOG_LEVEL_T;


#define LOG_ERR(x, ...)		log_write(LOG_LEVEL_ERR, x, __VA_ARGS__)
#define LOG_DBG(x, ...)		log_write(LOG_LEVEL_DBG, x, __VA_ARGS__)
#define LOG_COM(x, ...)		log_write(LOG_LEVEL_COM, x, __VA_ARGS__)
#define LOG_API(x, ...)		log_write(LOG_LEVEL_API, x, __VA_ARGS__)

#define LogPrint(fmt, ...)	log_write(LOG_LEVEL_API, fmt, __VA_ARGS__)
#define LogLine(fmt, ...)   log_write(LOG_LEVEL_API, fmt"\r\n", __VA_ARGS__)


int log_init(char *path, unsigned int level, unsigned int flag);
int log_deinit(void);
int log_write(unsigned int level, const char *format, ...);

int log_test(void);


#ifdef	__cplusplus
}
#endif

#endif
