#include "StdAfx.h"
#include "CString.h"
#include "assert.h"
#include <stdlib.h>
#include <stdio.h>
//#include "stdlibstm.h"

CCString::CCString()
{
	m_pContain = NULL;
}

CCString::~CCString()
{
	Empty();
}

CCString::CCString(const char *str)
{
	unsigned long usLth = 0;
	m_pContain = NULL;

	if(str){
		usLth = strlen(str);
		if (0 < usLth)
		{
			SetSize(usLth);
			memcpy(m_pContain->pucData, str, usLth);
		}
	}
}


CCString::CCString(const CCString& str)
{
	m_pContain = NULL;
	if(0 < str.GetLength())
	{
		SetSize(str.GetLength());
		memcpy(m_pContain->pucData, str.GetBuffer(), str.GetLength());
	}
}

const CCString& CCString::operator =(const CCString& str)
{
	if (m_pContain == str.m_pContain) return *this;
	
	Empty();
	Append(str.GetBuffer());
	return *this;
}

const CCString& CCString::operator =(char ch)
{
	Empty();
	SetSize(1);	
	m_pContain->pucData[0] = ch;

	return *this;
}
const CCString& CCString::operator =(const char *str)
{
	Empty();
	Append(str);
	return *this;
}
#if 1
const CCString& CCString::operator +=(const CCString& str)
{
	Append(str.GetBuffer());
	return *this;
}
const CCString& CCString::operator +=(char ch)
{
	char str[2]={ch,0};
	Append(str);
	return *this;
}
const CCString& CCString::operator +=(const char *str)
{
	Append(str);
	return *this;
}
#endif

CCString operator +(const CCString& lhs,const CCString& rhs)
{
	CCString s=lhs;	s+=rhs;
	return s;
}
CCString operator +(const char *lhs,const CCString& rhs)
{
	CCString s=lhs;
	s+=rhs;
	return s;
}
CCString operator +(char lhs,const CCString& rhs)
{
	CCString s;
	s=lhs;	s+=rhs;
	return s;
}
CCString operator +(const CCString& lhs,const char *rhs)
{
	CCString s=lhs;	s+=rhs;
	return s;
}
CCString operator +(const CCString& lhs,char rhs)
{
	CCString s=lhs;	s+=rhs;
	return s;
}

CCString CCString::Left(unsigned long ulLength)
{
	unsigned long iLoop=0;
	CCString str = "";
	assert(ulLength<=GetLength());

			
	for (iLoop=0; iLoop<ulLength; iLoop++)
	{
		str += m_pContain->pucData[iLoop];		
	}
	
	return str;
}

CCString CCString::Right(unsigned long ulLength)
{
	unsigned long iLoop=0;
	CCString str = "";
	assert(ulLength<=GetLength());

	for (iLoop=GetLength()-ulLength; iLoop<GetLength(); iLoop++)
	{
		str += m_pContain->pucData[iLoop];
	}
	return str;
}


long CCString::Insert(unsigned long nIndex, char ch)
{
	if (nIndex > GetLength()) nIndex = GetLength();
	SetSize(1+GetLength());
	memmove(&m_pContain->pucData[nIndex+1], &m_pContain->pucData[nIndex], GetLength()-nIndex-1);
	m_pContain->pucData[nIndex] = ch;
	m_pContain->pucData[m_pContain->usLthData] = '\0';
	return nIndex;
}

long CCString::Insert(unsigned long nIndex, const char *str)
{
	unsigned long uLth = strlen(str);
	if (nIndex > GetLength()) nIndex = GetLength();
	if (0 == uLth) return nIndex;

	SetSize(uLth+GetLength());
	memmove(&m_pContain->pucData[nIndex+uLth], &m_pContain->pucData[nIndex], GetLength()-uLth-nIndex);
	memcpy(&m_pContain->pucData[nIndex], str, uLth);
	m_pContain->pucData[m_pContain->usLthData] = '\0';

	return nIndex;	
}


long CCString::Insert(unsigned long nIndex, const CCString &str)
{
	switch (str.GetLength())
	{
	case 0: return nIndex;
	case 1:	return Insert(nIndex, str[0]);
	default:return Insert(nIndex, str.GetBuffer());	
	}		
}

#if 1 //debug my
void CCString::Append(const char *str)
{
	if (NULL == str) return;

	unsigned long iLength = strlen(str);
	SetSize(GetLength()+iLength);
	strcat_s(m_pContain->pucData, (GetLength()+iLength), str);	
}
#endif

const char *CCString::GetBuffer(unsigned long ulPos /*= 0*/) const
{
	if (NULL == m_pContain) return NULL;
	return m_pContain->pucData;
}
unsigned long CCString::GetLength() const
{
	if (NULL == m_pContain) return 0;
	
	return m_pContain->usLthData;
}
void CCString::Empty()
{
	if (NULL == m_pContain) return;

	delete[] m_pContain->pucData;
	m_pContain->pucData = NULL;
	delete m_pContain;
	m_pContain = NULL;
}

bool CCString::IsEmpty()
{
	if ((NULL == m_pContain) || (0 == m_pContain->usLthData)) return TRUE;

	return FALSE;
}

CCString CCString::Mid(unsigned long pos, unsigned long n)const
{
	unsigned long usLoop = 0;
	CCString strResult;
	
	if (pos < GetLength())
	{
		if (n > m_pContain->usLthData-pos)
		{
			n = m_pContain->usLthData-pos;
		}
		strResult.SetSize(n);
		for (usLoop=0; usLoop<n; usLoop++,pos++)
		{
			strResult.SetAt(usLoop, m_pContain->pucData[pos]);
		}
	}

	return strResult;
}

char CCString::GetAt(unsigned long nIndex)
{
	assert(m_pContain);
	assert(nIndex<m_pContain->usLthData);
	
	return m_pContain->pucData[nIndex];	
}

void CCString::SetAt(unsigned long nIndex, char ch)
{
	assert(m_pContain);
	assert(nIndex<m_pContain->usLthData);
	
	m_pContain->pucData[nIndex] = ch;	
}

void CCString::Delete(unsigned long nIndex, unsigned long nCount/* =1 */)
{
	unsigned long nMoveCount;
	if (0 == nCount) return;
	assert(m_pContain);
	assert(nIndex<m_pContain->usLthData);

	nMoveCount = m_pContain->usLthData - (nIndex + nCount);

	if (nMoveCount)
		memmove(&m_pContain->pucData[nIndex], &m_pContain->pucData[nIndex + nCount], nMoveCount);

	m_pContain->usLthData -= nCount;
	m_pContain->pucData[m_pContain->usLthData] = '\0';	
}

char CCString::operator [] (unsigned long nIndex) const
{
	assert(m_pContain);
	assert(nIndex<m_pContain->usLthData);
	return m_pContain->pucData[nIndex];
}
char& CCString::operator [] (unsigned long nIndex)
{
	assert(m_pContain);
	assert(nIndex<m_pContain->usLthData);
	return m_pContain->pucData[nIndex];
}

long CCString::Find(char ch, unsigned long uPosStart /* = 0 */)
{
	if (NULL == m_pContain) return -1;
	for(long i=uPosStart;i<m_pContain->usLthData;i++){
		if(m_pContain->pucData[i]==ch)return i;
	}
	return -1;
}

long CCString::Find(const char *str, unsigned long uPosStart /* = 0 */)
{
	char *pstr = NULL;
	if (NULL == m_pContain) return -1;
	if(m_pContain->usLthData==0)return -1;
	if (uPosStart >= GetLength()) return -1;
	
	pstr = m_pContain->pucData;
	while (uPosStart)
	{
		uPosStart--;
		pstr++;
	}

	pstr=strstr(pstr, str);
	if(pstr)return pstr-m_pContain->pucData;
	return -1;
}

long CCString::Find(const CCString &str, unsigned long uPosStart /* = 0 */)
{
	return Find(((CCString)str).GetBuffer(0), uPosStart);
}


long CCString::ReverseFind(char ch)
{
	if (NULL == m_pContain) return -1;
	for(long i=m_pContain->usLthData-1;i>=0;i--){
		if(m_pContain->pucData[i]==ch)return i;
	}
	return -1;
}

long CCString::Compare (const CCString& str) const
{
	return Compare(str.GetBuffer());
}
long CCString::Compare (const char *str) const
{
	unsigned long nLength=strlen(str);
	if((NULL == m_pContain) || (0 == m_pContain->usLthData)) 
	{
		if (0 == nLength) return 0;
		return -str[0];
	}
		
	if(str==NULL) return m_pContain->pucData[0];

	int res=strncmp(m_pContain->pucData,str,min(nLength,GetLength()));
	if(res==0){
    	if(GetLength()<nLength)return -1;
		res=(GetLength()!=nLength?1:0);
	}
    return res;
}

bool operator== ( const CCString& lhs, const CCString& rhs )
{
    return (lhs.GetLength () == rhs.GetLength () && 0 == lhs.Compare (rhs));
}
bool operator== ( const char* lhs, const CCString& rhs )
{
    return 0 == rhs.Compare (lhs);
}
bool operator== ( const CCString& lhs, const char* rhs )
{
    return 0 == lhs.Compare (rhs);
}

#if 0 //debug my
void CCString::MakeUpper()
{
	if (NULL == m_pContain) return;
	if(m_pContain->pucData)_strupr(m_pContain->pucData);
}
#endif

#if 0 //debug my
void CCString::MakeLower()
{
	if (NULL == m_pContain) return;
	if(m_pContain->pucData)_strlwr(m_pContain->pucData);
}
#endif

void CCString::TrimLeft()
{
	TrimLeft(' ');
	TrimLeft('\t');
}
void CCString::TrimRight()
{
	TrimRight(' ');
	TrimRight('\t');
}

void CCString::TrimLeft(char ch)
{
	int i=0;
	if (NULL == m_pContain) return;
	while(i<m_pContain->usLthData&&m_pContain->pucData[i]==ch)i++;
	if(i>0){
		m_pContain->usLthData-=i;
		memcpy(m_pContain->pucData,m_pContain->pucData+i,m_pContain->usLthData);
	}
}
void CCString::TrimRight(char ch)
{
	int i=GetLength()-1;
	while(i>=0&&m_pContain->pucData[i]==ch){
		m_pContain->pucData[i--]=0;
		m_pContain->usLthData--;
	}
}

void CCString::Trim()
{
	TrimRight();
	TrimLeft();
}
unsigned long CCString::Replace(const char *src,const char *dest)
{
	if (0 == GetLength()) return 0;

	unsigned long count=0, ulPos=0, ulLth;
	unsigned long srclen=strlen(src);
	unsigned long destlen=strlen(dest);
	char *pstr = NULL;
	while (NULL != (pstr=strstr(&m_pContain->pucData[ulPos], src)))
	{
		count++;
		ulPos = pstr - m_pContain->pucData;
		if (destlen == srclen)
		{
			memcpy(&m_pContain->pucData[ulPos], dest, destlen);
		}
		else
		{
			ulLth = GetLength();
			SetSize(ulLth+destlen-srclen);
			memmove(&m_pContain->pucData[ulPos+destlen], &m_pContain->pucData[ulPos+srclen], ulLth-ulPos-srclen);
			memcpy(&m_pContain->pucData[ulPos], dest, destlen);
		}

		ulPos += destlen;
	}

	return count;
}

long CCString::CompareNoCase (const CCString& str) const
{
	return CompareNoCase(str.GetBuffer());
}

long CCString::CompareNoCase (const char *str) const
{
	unsigned long nLength=strlen(str);
	if (0 == GetLength())
	{
		if (0 == nLength) return 0;
		
		return -str[0];
	}

	if(str==NULL) return m_pContain->pucData[0];
	
#ifdef _STM32	
	int res=strncasecmp(m_pContain->pucData,str, min(nLength,GetLength()));
#else
	int res=_strnicmp(m_pContain->pucData,str,min(nLength,GetLength()));
#endif
	if(res==0){
		if(GetLength()<nLength)return -1;
		res=(GetLength()!=nLength?1:0);
	}
    return res;
}


bool operator!= ( const CCString& lhs, const CCString& rhs )
{
    return !(lhs == rhs);
}
bool operator!= ( const char* lhs, const CCString& rhs )
{
    return !(lhs == rhs);
}
bool operator!= ( const CCString& lhs, const char* rhs )
{
    return !(lhs == rhs);
}
bool operator< ( const CCString& lhs, const CCString& rhs )
{
    return 0 > lhs.Compare (rhs);
}
bool operator< ( const char* lhs, const CCString& rhs )
{
    return 0 < rhs.Compare (lhs);
}
bool operator< ( const CCString& lhs, const char* rhs )
{
    return 0 > lhs.Compare (rhs);
}
bool operator> ( const CCString& lhs, const CCString& rhs )
{
    return rhs < lhs;
}
bool operator> ( const char* lhs, const CCString& rhs )
{
    return rhs < lhs;
}
bool operator> ( const CCString& lhs, const char* rhs )
{
    return rhs < lhs;
}
bool operator<= ( const CCString& lhs, const CCString& rhs )
{
    return !(rhs < lhs);
}
bool operator<= ( const char* lhs, const CCString& rhs )
{
    return !(rhs < lhs);
}
bool operator<= ( const CCString& lhs, const char* rhs )
{
    return !(rhs < lhs);
}
bool operator>= ( const CCString& lhs, const CCString& rhs )
{
    return !(lhs < rhs);
}
bool operator>= ( const char* lhs, const CCString& rhs )
{
    return !(lhs < rhs);
}
bool operator>= ( const CCString& lhs, const char* rhs )
{
    return !(lhs < rhs);
}



long CCString::NewInstance(unsigned long iNewSize)
{
	char *pucNew = NULL;
	if (NULL == m_pContain)	return SetSize(iNewSize);	
	
	if (m_pContain->usLthAlloc < iNewSize)
	{
		m_pContain->usLthAlloc = iNewSize;
		iNewSize %= m_pContain->usLthGrowBy;
		if (0 != iNewSize)	
		{			
			m_pContain->usLthAlloc -= iNewSize;
			m_pContain->usLthAlloc += m_pContain->usLthGrowBy;		
		}
		if (NULL == m_pContain->pucData)
		{			
			m_pContain->pucData = new char[m_pContain->usLthAlloc];			
			assert(m_pContain->pucData);	
			memset(m_pContain->pucData, 0, m_pContain->usLthAlloc);
		}
		else
		{			
			pucNew = new char[m_pContain->usLthAlloc];
			assert(pucNew);		
			
			memcpy(pucNew, m_pContain->pucData, m_pContain->usLthData);
			delete[] m_pContain->pucData;
			m_pContain->pucData = pucNew;
			memset(&m_pContain->pucData[m_pContain->usLthData], 0, m_pContain->usLthAlloc-m_pContain->usLthData);
			pucNew = NULL;
		}
	}
	else if (NULL == m_pContain->pucData)
	{		
		m_pContain->pucData = new char[m_pContain->usLthAlloc];
		memset(m_pContain->pucData, 0, m_pContain->usLthAlloc);
	}	

	return 0;
}


long CCString::SetSize(unsigned long nNewSize, unsigned long nGrowBy/* =DEFAULT_GROW_BY */)
{
	if (NULL == m_pContain)
	{
		m_pContain = new tagContain;
		m_pContain->pucData		= NULL;
		m_pContain->usLthData		= 0;
		m_pContain->usLthGrowBy	= nGrowBy;
		m_pContain->usLthAlloc	= m_pContain->usLthGrowBy;
	}
	else
		m_pContain->usLthGrowBy	= nGrowBy;
	
	NewInstance(nNewSize+1);
	m_pContain->usLthData = nNewSize;
	return nNewSize+1;
}


void CCString::FreeExtra(void)
{
	unsigned long usLth = 0;
	//BYTE *pucNew = NULL;
	if ((NULL == m_pContain) || (NULL == m_pContain->pucData))	return;
	
	usLth = m_pContain->usLthAlloc - m_pContain->usLthData - 1;
	if (usLth > m_pContain->usLthGrowBy)
	{
		usLth -= (usLth%m_pContain->usLthGrowBy);
		
		m_pContain->pucData = (char*) realloc(m_pContain->pucData, m_pContain->usLthAlloc-usLth);	
		assert(m_pContain->pucData);			
		m_pContain->usLthAlloc -= usLth;
	}
}
/*
#define TCHAR_ARG   char 
#define WCHAR_ARG   short
#define CHAR_ARG    char

#ifdef _X86_
	#define DOUBLE_ARG  _AFX_DOUBLE
#else
	#define DOUBLE_ARG  double
#endif

#define FORCE_ANSI      0x10000
#define FORCE_UNICODE   0x20000
#define FORCE_INT64     0x40000

#define	_alloca		malloc

#ifdef _STM32
typedef char* LPCTSTR;
typedef wchar_t *LPWSTR, *PWSTR, *LPCWSTR;
//typedef unsigned short wchar_t;
typedef  char  *LPCSTR, *LPTSTR;

typedef unsigned int size_t;

#define __cdecl     
#define _T(x)	x


#define _stprintf   sprintf
#define _ttoi       atoi
#define _tcsncmp    strncmp
#define _tcslen     strlen
#define	lstrlen		strlen
#define	lstrlenA	strlen

#define _istdigit(c) ((c) >= '0' && (c) <= '9') 

char *  _tcsinc(const char * _pc) { return (char *)(_pc+1); }
unsigned long _tclen(const char *_cpc) { return (_cpc,1); }

size_t __cdecl wcslen (
        const wchar_t * wcs
        )
{
        const wchar_t *eos =  wcs;

        while( *eos++ ) ;

        return( (size_t)(eos - wcs - 1) );
}


#endif

void CCString::FormatV(char* lpszFormat, va_list argList)
{
	ASSERT(lpszFormat);

	va_list argListSave = argList;

	// make a guess at the maximum length of the resulting string
	int nMaxLen = 0;
	for (LPCTSTR lpsz = lpszFormat; *lpsz != '\0'; lpsz = _tcsinc(lpsz))
	{
		// handle '%' character, but watch out for '%%'
		if (*lpsz != '%' || *(lpsz = _tcsinc(lpsz)) == '%')
		{
			nMaxLen += _tclen(lpsz);
			continue;
		}

		int nItemLen = 0;

		// handle '%' character with format
		int nWidth = 0;
		for (; *lpsz != '\0'; lpsz = _tcsinc(lpsz))
		{
			// check for valid flags
			if (*lpsz == '#')
				nMaxLen += 2;   // for '0x'
			else if (*lpsz == '*')
				nWidth = va_arg(argList, int);
			else if (*lpsz == '-' || *lpsz == '+' || *lpsz == '0' ||
				*lpsz == ' ')
				;
			else // hit non-flag character
				break;
		}
		// get width and skip it
		if (nWidth == 0)
		{
			// width indicated by
			nWidth = _ttoi(lpsz);
			for (; *lpsz != '\0' && _istdigit(*lpsz); lpsz = _tcsinc(lpsz))
				;
		}
		ASSERT(nWidth >= 0);

		int nPrecision = 0;
		if (*lpsz == '.')
		{
			// skip past '.' separator (width.precision)
			lpsz = _tcsinc(lpsz);

			// get precision and skip it
			if (*lpsz == '*')
			{
				nPrecision = va_arg(argList, int);
				lpsz = _tcsinc(lpsz);
			}
			else
			{
				nPrecision = _ttoi(lpsz);
				for (; *lpsz != '\0' && _istdigit(*lpsz); lpsz = _tcsinc(lpsz))
					;
			}
			ASSERT(nPrecision >= 0);
		}

		// should be on type modifier or specifier
		int nModifier = 0;
		if (_tcsncmp(lpsz, _T("I64"), 3) == 0)
		{
			lpsz += 3;
			nModifier = FORCE_INT64;
#if !defined(_X86_) && !defined(_ALPHA_)
			// __int64 is only available on X86 and ALPHA platforms
			ASSERT(FALSE);
#endif
		}
		else
		{
			switch (*lpsz)
			{
			// modifiers that affect size
			case 'h':
				nModifier = FORCE_ANSI;
				lpsz = _tcsinc(lpsz);
				break;
			case 'l':
				nModifier = FORCE_UNICODE;
				lpsz = _tcsinc(lpsz);
				break;

			// modifiers that do not affect size
			case 'F':
			case 'N':
			case 'L':
				lpsz = _tcsinc(lpsz);
				break;
			}
		}

		// now should be on specifier
		switch (*lpsz | nModifier)
		{
		// single characters
		case 'c':
		case 'C':
			nItemLen = 2;
			va_arg(argList, TCHAR_ARG);
			break;
		case 'c'|FORCE_ANSI:
		case 'C'|FORCE_ANSI:
			nItemLen = 2;
			va_arg(argList, CHAR_ARG);
			break;
		case 'c'|FORCE_UNICODE:
		case 'C'|FORCE_UNICODE:
			nItemLen = 2;
			va_arg(argList, WCHAR_ARG);
			break;

		// strings
		case 's':
			{
				LPCTSTR pstrNextArg = va_arg(argList, LPCTSTR);
				if (pstrNextArg == NULL)
				   nItemLen = 6;  // "(null)"
				else
				{
				   nItemLen = lstrlen(pstrNextArg);
				   nItemLen = max(1, nItemLen);
				}
			}
			break;

		case 'S':
			{
#ifndef _UNICODE
				LPWSTR pstrNextArg = va_arg(argList, LPWSTR);
				if (pstrNextArg == NULL)
				   nItemLen = 6;  // "(null)"
				else
				{
				   nItemLen = wcslen(pstrNextArg);
				   nItemLen = max(1, nItemLen);
				}
#else
				LPCSTR pstrNextArg = va_arg(argList, LPCSTR);
				if (pstrNextArg == NULL)
				   nItemLen = 6; // "(null)"
				else
				{
				   nItemLen = lstrlenA(pstrNextArg);
				   nItemLen = max(1, nItemLen);
				}
#endif
			}
			break;

		case 's'|FORCE_ANSI:
		case 'S'|FORCE_ANSI:
			{
				LPCSTR pstrNextArg = va_arg(argList, LPCSTR);
				if (pstrNextArg == NULL)
				   nItemLen = 6; // "(null)"
				else
				{
				   nItemLen = lstrlenA(pstrNextArg);
				   nItemLen = max(1, nItemLen);
				}
			}
			break;

		case 's'|FORCE_UNICODE:
		case 'S'|FORCE_UNICODE:
			{
				LPWSTR pstrNextArg = va_arg(argList, LPWSTR);
				if (pstrNextArg == NULL)
				   nItemLen = 6; // "(null)"
				else
				{
				   nItemLen = wcslen(pstrNextArg);
				   nItemLen = max(1, nItemLen);
				}
			}
			break;
		}

		// adjust nItemLen for strings
		if (nItemLen != 0)
		{
			if (nPrecision != 0)
				nItemLen = min(nItemLen, nPrecision);
			nItemLen = max(nItemLen, nWidth);
		}
		else
		{
			switch (*lpsz)
			{
			// integers
			case 'd':
			case 'i':
			case 'u':
			case 'x':
			case 'X':
			case 'o':
				if (nModifier & FORCE_INT64)
					va_arg(argList, __int64);
				else
					va_arg(argList, int);
				nItemLen = 32;
				nItemLen = max(nItemLen, nWidth+nPrecision);
				break;

			case 'e':
			case 'g':
			case 'G':
				va_arg(argList, DOUBLE_ARG);
				nItemLen = 128;
				nItemLen = max(nItemLen, nWidth+nPrecision);
				break;

			case 'f':
				{
					double f;
					LPTSTR pszTemp;

					// 312 == strlen("-1+(309 zeroes).")
					// 309 zeroes == max precision of a double
					// 6 == adjustment in case precision is not specified,
					//   which means that the precision defaults to 6
					pszTemp = (LPTSTR)_alloca(max(nWidth, 312+nPrecision+6));

					f = va_arg(argList, double);
					_stprintf( pszTemp, _T( "%*.*f" ), nWidth, nPrecision+6, f );
					nItemLen = _tcslen(pszTemp);
				}
				break;

			case 'p':
				va_arg(argList, void*);
				nItemLen = 32;
				nItemLen = max(nItemLen, nWidth+nPrecision);
				break;

			// no output
			case 'n':
				va_arg(argList, int*);
				break;

			default:
				ASSERT(FALSE);  // unknown formatting option
			}
		}

		// adjust nMaxLen for output nItemLen
		nMaxLen += nItemLen;
	}


	SetSize(nMaxLen);
	vsprintf(m_pContain->pucData, lpszFormat, argListSave);
	//_CRTIMP int __cdecl vsprintf(char *, const char *, va_list);

	FreeExtra();

	va_end(argListSave);
}

// formatting (using wsprintf style formatting)
void CCString::Format(CCString strFormat, ...)
{
	ASSERT(strFormat.GetLength());

	va_list argList;
	va_start(argList, strFormat);
	FormatV((char *)strFormat.GetBuffer(), argList);
	va_end(argList);
}

*/

