#ifndef __CCString_H
#define __CCString_H

//#include "adsStd.h"
#include "stdarg.h"

#ifdef _STM32
#define CCString CString
#else
//#define CString CCString
#endif
 
class CCString
{
public:
	CCString();
	CCString(const char *str);	
	CCString(const CCString& str);
	~CCString();

	const CCString& operator =(const CCString& str);
	const CCString& operator =(char ch);
	const CCString& operator =(const char *str);
#if 1
	const CCString& operator +=(const CCString& str);
	const CCString& operator +=(char ch);
	const CCString& operator +=(const char *str);
#endif

	friend CCString operator +(const CCString& lhs,const CCString& rhs);
	friend CCString operator +(const char *lhs,const CCString& rhs);
	friend CCString operator +(char lhs,const CCString& rhs);
	friend CCString operator +(const CCString& lhs,const char *rhs);
	friend CCString operator +(const CCString& lhs,char rhs);

	char operator [] (unsigned long nIndex) const;
	char& operator [] (unsigned long nIndex);

	long Compare (const CCString& str) const;
	long Compare (const char *str) const;
	long CompareNoCase (const CCString& str) const;
	long CompareNoCase (const char *str) const;

	friend bool operator== ( const CCString& lhs, const CCString& rhs );
	friend bool operator== ( const char* lhs, const CCString& rhs );
	friend bool operator== ( const CCString& lhs, const char* rhs );
	
	friend bool operator!= ( const CCString& lhs, const CCString& rhs );
	friend bool operator!= ( const char* lhs, const CCString& rhs );
	friend bool operator!= ( const CCString& lhs, const char* rhs );
	
	friend bool operator< ( const CCString& lhs, const CCString& rhs );
	friend bool operator< ( const char* lhs, const CCString& rhs );
	friend bool operator< ( const CCString& lhs, const char* rhs );
	
	friend bool operator> ( const CCString& lhs, const CCString& rhs );
	friend bool operator> ( const char* lhs, const CCString& rhs );
	friend bool operator> ( const CCString& lhs, const char* rhs );
	
	friend bool operator<= ( const CCString& lhs, const CCString& rhs );
	friend bool operator<= ( const char* lhs, const CCString& rhs );
	friend bool operator<= ( const CCString& lhs, const char* rhs );
	
	friend bool operator>= ( const CCString& lhs, const CCString& rhs );
	friend bool operator>= ( const char* lhs, const CCString& rhs );
	friend bool operator>= ( const CCString& lhs, const char* rhs );
    const char *GetBuffer(unsigned long ulPos = 0) const;
	unsigned long GetLength() const;
	bool IsEmpty();
	void Empty();
	CCString Mid( unsigned long pos = 0, unsigned long n = 0xffff ) const;
	void Append(const char *str);
	void SetAt(unsigned long nIndex, char ch);
	char GetAt(unsigned long nIndex);	
	void Delete(unsigned long nIndex, unsigned long nCount=1);
	long Find(char ch, unsigned long uPosStart = 0);
	long Find(const char *str, unsigned long uPosStart = 0);
	long Find(const CCString &str, unsigned long uPosStart = 0);
	long ReverseFind(char ch);
	unsigned long Replace(const char *src,const char *dest);
	void TrimLeft();
	void TrimRight();
	void TrimLeft(char ch);
	void TrimRight(char ch);

	void Trim();
	void MakeUpper();
	void MakeLower();

	CCString Left(unsigned long ulLength);
	CCString Right(unsigned long ulLength);
	
	long Insert(unsigned long nIndex, char ch);
	long Insert(unsigned long nIndex, const char *str);
	long Insert(unsigned long nIndex, const CCString &str);

	//void Format(CCString strFormat, ...);
	//void FormatV(char* strFormat, va_list argList);


private:
	struct tagContain {
		char *pucData;
		unsigned long usLthData;
        unsigned long usLthAlloc;
        unsigned long usLthGrowBy;
        //unsigned long m_iCount;
    }* m_pContain;

    long NewInstance(unsigned long iNewSize);
	long SetSize(unsigned long nNewSize, unsigned long nGrowBy = 8);
    void FreeExtra(void);
};

#endif 

