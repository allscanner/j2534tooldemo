#include "stdafx.h"
#include "DataConvert.h"
#include "stdio.h"
#include <afxstr.h>

#if 0 //debug my
unsigned long CDataConvert::Split(CGroup < CString > &vctStrDataSet, CString strContent, const CString &strSplitSign, D_BOOL bIgnoreMultSign /* = TRUE */)
{
	int iPos;
	int iLthSrc		= strContent.GetLength();
	int iLthSplit	= strSplitSign.GetLength();

	vctStrDataSet.clear();

	if ("" == strContent)	return 0;

	if (0 == iLthSplit)
	{
		vctStrDataSet.push_back(strContent);
	}
	else
	{
		for (iPos=0; iPos+iLthSplit-1<iLthSrc; iPos++)
		{
			//if ((iPos+iLthSplit) <= iLthSrc)
			{
				if (strSplitSign == strContent.Mid(iPos, iLthSplit))
				{
					vctStrDataSet.push_back(strContent.Left(iPos));

					if (bIgnoreMultSign)  //??????????
					{
						while (strSplitSign == strContent.Mid(iPos+iLthSplit, iLthSplit))
						{
							strContent.Delete(iPos+iLthSplit, iLthSplit);
						}
					}

					strContent.Delete(0, iPos+iLthSplit);
					iPos = -1;
					iLthSrc = strContent.GetLength();
				}
			}
		}

		vctStrDataSet.push_back(strContent);
	}

	return vctStrDataSet.size();
}

CString CDataConvert::Assem(const CGroup < CString > &vctStrSplitSet, const CString &strSplitSign)
{
	CString strResult;
	for (int iLoop=0; iLoop<vctStrSplitSet.size(); iLoop++)
	{
		strResult += strSplitSign + vctStrSplitSet[iLoop];
	}

	strResult.Delete(0, strSplitSign.GetLength());

	return strResult;
}
#endif

#if 0 //debug my
CString CDataConvert::UIntToString(unsigned long ulValue, unsigned char ucType, unsigned char ulLth /* = 0 */)
{
	CString strResult;
	char buf[30];
	strResult += (_ultoa_s(ulValue, buf, ucType));

#ifdef WIN32
	strResult.MakeUpper();
#endif

	if (ulLth > strResult.GetLength())
	{
		while (ulLth>strResult.GetLength())
			strResult.Insert(0, '0');
	}
	else if ((16 == ucType) && (1 == (strResult.GetLength()%2)))
	{
		strResult.Insert(0, '0');
	}

	return strResult;
}
#endif

CBinary CDataConvert::UIntToBinary(unsigned long ulValue, unsigned char ulLth /*= 0*/)
{
	CBinary bin;

	while (ulValue)
	{
		bin.InsertAt(0, 0xFF&ulValue);
		ulValue >>= 8;
		if (0 < ulLth) ulLth--;
	}

	while (ulLth)
	{
		bin.InsertAt(0, 0);
		ulLth--;
	}

	return bin;
}

CString CDataConvert::IntToDecString(long iValue)
{
	CString strResult;
	char ch[30];
	sprintf_s(ch, "%d", iValue);
	strResult += ch;

	return strResult;
}

#if 0 //debug my
long CDataConvert::StrToInt(CString strNum, unsigned char ucType)
{
	if ((10 == ucType) && (2 < strNum.GetLength()) && ('0' == strNum[0]) && (('x' == strNum[1]) || ('X' == strNum[1])))
		ucType = 16;

	return  strtol(strNum.GetBuffer(0), NULL, ucType);
}
#endif

unsigned long CDataConvert::StrToUInt(CString strNum, unsigned char ucType)
{
	if ((10 == ucType) && (2 < strNum.GetLength()) && ('0' == strNum[0]) && (('x' == strNum[1]) || ('X' == strNum[1])))
		ucType = 16;

	return  strtoul((char*)strNum.GetBuffer(0), NULL, ucType);
}

float CDataConvert::StrToFloat(char *strFloat)
{
	float flResult = 0, flOffset=1;
	unsigned char ucLoop, ucLth = strlen(strFloat);

	for (ucLoop=0; ucLoop<ucLth; ucLoop++)
	{
		if ('.' == strFloat[ucLoop]) break;
		if (('0' > strFloat[ucLoop]) || ('9' < strFloat[ucLoop])) return flResult;
		flResult *= 10;
		flResult += (strFloat[ucLoop] - '0');
	}

	for (ucLoop++; ucLoop<ucLth; ucLoop++)
	{
		if (('0' > strFloat[ucLoop]) || ('9' < strFloat[ucLoop])) break;
		flOffset = flOffset * 0.1;
		flResult += flOffset*(strFloat[ucLoop] - '0');
	}

	return flResult;
}

#if 0
void CDataConvert::GetIDFromStrID(char *hexin, unsigned char *outbuf)
{
	unsigned char ucLoop = 0;
	unsigned char ulLth = 0;
	char *strStop = NULL;

	while (NULL != hexin)
	{
		ulLth = strlen(hexin);
		if ((2 < ulLth) && ('0' == hexin[0]) && (('x' == hexin[1]) || ('X' == hexin[1])))
		{
			outbuf[ucLoop] = strtoul(hexin, &strStop, 16);
		}
		else
		{
			outbuf[ucLoop] = strtoul(hexin, &strStop, 10);
		}

		if ((NULL == strStop) || (0 == strlen(strStop))) break;
		hexin = &strStop[1];
		ucLoop++;
	}
}


CString CDataConvert::BinaryToString(CBinary binData)
{
	return HexDataToString((unsigned char *) binData.GetBuffer(), binData.GetSize());
}

CString CDataConvert::HexDataToString(unsigned char *pData, unsigned long ulLth)
{
	unsigned long uLoop=0;
	char strTemp[2] = {0};
	CString strResult;

	if (0 == ulLth) return "";

	for (uLoop=0; uLoop<ulLth; uLoop++)
	{
		sprintf(strTemp, "%02X", pData[uLoop]);
		strResult += strTemp;
	}
	return strResult;
}

CBinary CDataConvert::HexStrToBinary(CString &strHexStr)
{
	unsigned char ucValue = 0;
	unsigned long usLoop;
	CBinary binResult;
	char ch;

	//binResult.SetSize(usLth/2);

	for (usLoop=0,ucValue=0; usLoop<strHexStr.GetLength(); usLoop++)
	{
		ch = strHexStr.GetAt(usLoop);
		if (('0' <= ch) && ('9' >= ch))
		{
			ucValue += (ch-'0');
		}
		else if (('a' <= ch) && ('f' >= ch))
		{
			ucValue += (10+ch-'a');
		}
		else if (('A' <= ch) && ('F' >= ch))
		{
			ucValue += (10+ch-'A');
		}

		if (0 == (usLoop%2))
		{
			ucValue <<= 4;
		}
		else
		{
			binResult += ucValue;
			ucValue = 0;
		}
	}

	return binResult;
}


void CDataConvert::GetStrVerValue(char *strResult, char *strFormat, unsigned char *pucData, unsigned char arrucPos[])
{
	unsigned long uiLoop = 0, ujLoop = 0, ulPos = 0;
	unsigned char ucLthFor = strlen(strFormat);
	//u8 ucLthPos = strlen(arrucPos);
	unsigned char ucLthPos = sizeof(arrucPos);
	char *strFormatSub = strFormat;
	char strTemp[20] = {0};
	memset(strResult, 0, strlen(strResult));

	for (uiLoop=0; uiLoop<ucLthPos; uiLoop++)
	{
		for (ujLoop=ulPos; ujLoop<ucLthFor; ujLoop++)
		{
			if ((0 != ulPos) && ('%' == strFormat[ujLoop]))
			{
				strFormat[ujLoop] = '\0';
				sprintf(strTemp, strFormatSub, pucData[arrucPos[uiLoop]]);
				strcat(strResult, strTemp);
				strFormat[ujLoop] = '%';
				ulPos = ujLoop;
				strFormatSub = &strFormat[ujLoop];
				break;
			}
		}
		if (ujLoop < ucLthFor) continue;
		ulPos = ucLthFor;
		sprintf(strTemp, strFormatSub, pucData[arrucPos[uiLoop]]);
		strcat(strResult, strTemp);
	}
}
#endif
