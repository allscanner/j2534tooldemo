﻿#include "StdAfx.h"
#include "ErrorCode.h"

INT32_T CErrorCode::m_iErrorCode = EC_SUCCESS;

#if 0
CErrorCode::CErrorCode(void)
{
	//long CErrorCode::m_iErrorCode = EC_SUCCESS;
}

CErrorCode::~CErrorCode(void)
{

}
#endif

BOOL CErrorCode::SetLastError(INT32_T iErrorCode) {
    m_iErrorCode=iErrorCode;
    return false;
}

INT32_T CErrorCode::GetLastError()
{
    return m_iErrorCode;
}

void CErrorCode::Clear() {
    m_iErrorCode = EC_SUCCESS;
}

BOOL CErrorCode::IsType(INT32_T iErrorCode) {
    return (iErrorCode==m_iErrorCode);
}

BOOL CErrorCode::Unsuccess()
{
    return (m_iErrorCode!=EC_SUCCESS);
}

CString CErrorCode::GetErrInfo(INT32_T iErrorCode)
{
	CString str;
	switch (iErrorCode)
	{
	case J2534_STATUS_NOERROR:				str = "J2534_STATUS_OK";						break;
	case J2534_ERR_NOT_SUPPORTED:			str = "J2534_ERR_NOT_SUPPORTED";				break;
	case J2534_ERR_INVALID_CHANNEL_ID:		str = "J2534_ERR_INVALID_CHANNEL_ID";			break;
	case J2534_ERR_INVALID_PROTOCOL_ID:		str = "J2534_ERR_INVALID_PROTOCOL_ID";			break;
	case J2534_ERR_NULL_PARAMETER:			str = "J2534_ERR_NULL_PARAMETER";				break;
	case J2534_ERR_INVALID_IOCTL_VALUE:		str = "J2534_ERR_INVALID_IOCTL_VALUE";			break;
	case J2534_ERR_INVALID_FLAGS:			str = "J2534_ERR_INVALID_FLAGS";				break;
	case J2534_ERR_FAILED:					str = "J2534_ERR_FAILED";						break;
	case J2534_ERR_DEVICE_NOT_CONNECTED:	str = "J2534_ERR_DEVICE_NOT_CONNECTED";			break;
	case J2534_ERR_TIMEOUT:					str = "J2534_ERR_TIMEOUT";						break;
	case J2534_ERR_INVALID_MSG:				str = "J2534_ERR_INVALID_MSG";					break;
	case J2534_ERR_INVALID_TIME_INTERVAL:	str = "J2534_ERR_INVALID_TIME_INTERVAL";		break;
	case J2534_ERR_EXCEEDED_LIMIT:			str = "J2534_ERR_EXCEEDED_LIMIT";				break;
	case J2534_ERR_INVALID_MSG_ID:			str = "J2534_ERR_INVALID_MSG_ID";				break;
	case J2534_ERR_DEVICE_IN_USE:			str = "J2534_ERR_DEVICE_IN_USE";				break;
	case J2534_ERR_INVALID_IOCTL_ID:		str = "J2534_ERR_INVALID_IOCTL_ID";				break;
	case J2534_ERR_BUFFER_EMPTY:			str = "J2534_ERR_BUFFER_EMPTY";					break;
	case J2534_ERR_BUFFER_FULL:				str = "J2534_ERR_BUFFER_FULL";					break;
	case J2534_ERR_BUFFER_OVERFLOW:			str = "J2534_ERR_BUFFER_OVERFLOW";				break;
	case J2534_ERR_PIN_INVALID:				str = "J2534_ERR_PIN_INVALID";					break;
	case J2534_ERR_CHANNEL_IN_USE:			str = "J2534_ERR_CHANNEL_IN_USE";				break;
	case J2534_ERR_MSG_PROTOCOL_ID:			str = "J2534_ERR_MSG_PROTOCOL_ID";				break;
	case J2534_ERR_INVALID_FILTER_ID:		str = "J2534_ERR_INVALID_FILTER_ID";			break;
	case J2534_ERR_NO_FLOW_CONTROL:			str = "J2534_ERR_NO_FLOW_CONTROL";				break;
	case J2534_ERR_NOT_UNIQUE:				str = "J2534_ERR_NOT_UNIQUE";					break;
	case J2534_ERR_INVALID_BAUDRATE:		str = "J2534_ERR_INVALID_BAUDRATE";				break;
	case J2534_ERR_INVALID_DEVICE_ID:		str = "J2534_ERR_INVALID_DEVICE_ID";			break;
	case J2534_ERR_PIN_IN_USE:				str = "J2534_ERR_PIN_IN_USE";					break;
	case J2534_ERR_ADDRESS_NOT_CLAIMED:		str = "J2534_ERR_ADDRESS_NOT_CLAIMED";			break;		
	default:
		break;
	}
	return str;
}