﻿#ifndef ERRORCODE_H_HEADER_INCLUDED_B980E8A1
#define ERRORCODE_H_HEADER_INCLUDED_B980E8A1

#include "../../J2534/J2534.h"
#include "applications.h"

extern CString g_PassthruStatus;

#define PASSTHRUMESSAGEVIEW(ret, strContent)\
{\
	if (ret != J2534_STATUS_NOERROR)\
	{\
		g_PassthruStatus = _err.GetErrInfo(ret);\
		return;\
	}\
	else\
	{\
		g_PassthruStatus = strContent;\
	}\
}

class CErrorCode
{
public:
    enum tagERROR_CODE {
        EC_SUCCESS			= 0,	// 成功
		EC_TIME_OVER		= -1,	// 超时
		EC_ALLOCATE_MEMORY	= -2,	// 内存分配不成功
		EC_PARAMETER		= -3,	// 函数调用参数非法
		EC_NONE_SYSTEM		= -4,	// 扫描系统不存在
		EC_RUN_SET			= -5,	// 系统执行设定错，即不存在执行当前操作的前提条件
		EC_DATA				= -6,	// 数据错误
		EC_SLOP_OVER		= -7,	// 下标越界
		EC_SYS_RESOURCE		= -8,	// 系统资源错误(除内存外的所有系统资源)
		EC_COMMUNICATION	= -9,	// 通讯错误
		EC_ECU_REFUSE		= -10,	// ECU否定回答
		EC_RECEIVE_LEN		= -11,	// 接收长度错误
		EC_PACKET_TIME_OVER = -12,	// 包超时
		EC_INT_COMMUNICATION = -13	// 内部通信错误
    };

#if 0
	CErrorCode(void);
	~CErrorCode(void);
#endif

    /*-----------------------------------------------------------------------------
	功    能：是否存在错误
	参数说明：无
	返 回 值：存在返回true, 不存在错误--false
	说    明：无
    -----------------------------------------------------------------------------*/
    static BOOL Unsuccess();

    /*-----------------------------------------------------------------------------
	功    能：设置错误代码
	参数说明：D_ErrorCode  iErrorCode -- 错误代码，必须使用本类中的枚举值
	返 回 值：出错返回true, 成功--false
	说    明：无
    -----------------------------------------------------------------------------*/
    static BOOL SetLastError(INT32_T iErrorCode);

    /*-----------------------------------------------------------------------------
	功    能：取得错误代码
	参数说明：无
	返 回 值：错误代码
	说    明：无
    -----------------------------------------------------------------------------*/
    static INT32_T GetLastError();

    /*-----------------------------------------------------------------------------
	功    能：清除错误代码
	参数说明：无
	返 回 值：无
	说    明：无
    -----------------------------------------------------------------------------*/
    static void Clear();

    /*-----------------------------------------------------------------------------
	功    能：判断是否为某种错误类型
	参数说明：D_ErrorCode  iErrorCode -- 错误代码
	返 回 值：是 -- true,  否 -- false
	说    明：无
    -----------------------------------------------------------------------------*/
    static BOOL IsType(INT32_T iErrorCode);

	CString GetErrInfo(INT32_T iErrorCode);

public:
    // 错误代码值
    static INT32_T m_iErrorCode;

};


#endif /* ERRORCODE_H_HEADER_INCLUDED_B980E8A1 */
