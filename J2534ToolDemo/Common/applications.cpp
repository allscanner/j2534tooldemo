﻿/*******************************************************************************
 * FILE: applications.cpp
 * This file is part of ALLScanner VCX software.
 * COPYRIGHT (C) ALLScanner 2008-2014. 
 *		
 * DESC: J2534Tool's declaration of uxiliary function 
 * 
 * HISTORY:
 * Date			Author		Notes	
 * 2016-01-01		my		Create
*******************************************************************************/

#include "stdafx.h"
#include "applications.h"


extern PassthruRegeditDevice regDeviceInfo;

/*获取指定路径下的驱动*/
CString GetRegeditVCXPassthruPath()
{
	CRegKey m_hKey;
	DWORD ValueLen = MAX_PATH;
	TCHAR Value[MAX_PATH];
	INT32_T iRet;
	memset(Value,0,MAX_PATH);

	iRet = m_hKey.Open(HKEY_LOCAL_MACHINE, _T("SOFTWARE\\PassThruSupport.04.04\\VXDIAG - J2534"), KEY_READ);//SOFTWARE\\Wow6432Node\\PassThruSupport.04.04\\VCX-Passthru
	if (iRet != ERROR_SUCCESS)
	{
		MessageBoxW(NULL, _T("注册表加载，打开失败..."), _T("Warnning"), MB_ICONSTOP);
		return Value;
	}

	if (m_hKey.QueryStringValue(_T("FunctionLibrary"), Value, &ValueLen) != ERROR_SUCCESS)
	{
		MessageBoxW(NULL, _T("注册表加载，查询失败..."), _T("Warnning"), MB_ICONSTOP);
		return Value;
	}
	return Value;
}

/*获取计算机系统类型*/
BOOL IsWow64() 
{ 
	typedef BOOL (WINAPI *LPFN_ISWOW64PROCESS) (HANDLE, PBOOL); 
	LPFN_ISWOW64PROCESS fnIsWow64Process; 
	BOOL bIsWow64 = FALSE; 
	fnIsWow64Process = (LPFN_ISWOW64PROCESS)GetProcAddress( GetModuleHandle(L"kernel32"),"IsWow64Process"); 
	if (NULL != fnIsWow64Process) 
	{ 
		fnIsWow64Process(GetCurrentProcess(),&bIsWow64);
	} 
	return bIsWow64; 
} 

#if 0
/*动态加载PassThruSupport.04.04注册表下的所有驱动*/
_RegeditInfo GetRegeditPassthruSupport_04_04_Path()
{
	_RegeditInfo _regInfo;
	HKEY		hPrimaryKey = NULL, hSubKey = NULL;
	DWORD	szDllPathLen = MAX_PATH;
	TCHAR	szPathValue[MAX_PATH];
	TCHAR	tcPrimaryKeyPath[MAX_PATH];// = _T("SOFTWARE\\PassThruSupport.04.04");
	UINT16_T uiRet, subKeyCount;

	memset(tcPrimaryKeyPath, 0, MAX_PATH);

	if (IsWow64())		// 判断系统类型
		_tcscpy_s(tcPrimaryKeyPath, L"SOFTWARE\\Wow6432Node\\PassThruSupport.04.04");
	else
		_tcscpy_s(tcPrimaryKeyPath, L"SOFTWARE\\PassThruSupport.04.04");

	// Init Struct
	UINT16_T uiLoop=0,ujLoop=0;
	for(uiLoop=0;uiLoop<MAX_DEVICE_NUM;uiLoop++)
	{
		for(ujLoop=0;ujLoop<MAX_PATH;ujLoop++)
		{
			_regInfo.szDriveName[uiLoop][ujLoop] = L'';
			_regInfo.szDrivePath[uiLoop][ujLoop] = L'';
		}
	}
	// 获取注册表子键数目
	//uiRet = RegOpenKeyEx(HKEY_LOCAL_MACHINE, tcPrimaryKeyPath, 0, KEY_READ, &hPrimaryKey);

	//TCHAR    achKey[MAX_PATH];   // buffer for subkey name    
	//DWORD    cbName;                   // size of name string     
	//TCHAR    achClass[MAX_PATH] = TEXT("");  // buffer for class name     
	//DWORD    cchClassName = MAX_PATH;  // size of class string     
	//DWORD    cSubKeys = 0;               // number of subkeys     
	//DWORD    cbMaxSubKey;              // longest subkey size     
	//DWORD    cchMaxClass;              // longest class string     
	//DWORD    cValues;              // number of values for key     
	//DWORD    cchMaxValue;          // longest value name     
	//DWORD    cbMaxValueData;       // longest value data     
	//DWORD    cbSecurityDescriptor; // size of security descriptor     
	//FILETIME ftLastWriteTime;      // last write time     

	//DWORD i, retCode; 
	//retCode = RegQueryInfoKey(  
	//	hPrimaryKey,                    // key handle     
	//	achClass,                // buffer for class name     
	//	&cchClassName,           // size of class string     
	//	NULL,                    // reserved     
	//	&cSubKeys,               // number of subkeys     
	//	&cbMaxSubKey,            // longest subkey size     
	//	&cchMaxClass,            // longest class string     
	//	&cValues,                // number of values for this key     
	//	&cchMaxValue,            // longest value name     
	//	&cbMaxValueData,         // longest value data     
	//	&cbSecurityDescriptor,   // security descriptor     
	//	&ftLastWriteTime);       // last write time  

	
	if(uiRet==ERROR_SUCCESS)
	{
		for(subKeyCount = 0; ; subKeyCount++)
		{
			_regInfo.ucDriveCount = subKeyCount;
			TCHAR szNameValue[MAX_PATH] = _T("");
			DWORD dwNameValue = sizeof(szNameValue);

			uiRet = RegEnumKeyEx(hPrimaryKey, subKeyCount, szNameValue, &dwNameValue, NULL, NULL, NULL, NULL);
			if(uiRet == ERROR_NO_MORE_ITEMS)
			{
				RegCloseKey(hPrimaryKey);
				return _regInfo;
			}
			_tcscpy_s((_regInfo.szDriveName[subKeyCount]), szNameValue);

			
			UINT16_T szPrimaryKeyPathLen = lstrlen(tcPrimaryKeyPath);
			UINT16_T szNameValueLen = lstrlen(szNameValue);
			UINT16_T iKeyNameLen = lstrlen(L"FunctionLibrary");
			TCHAR* tTemp = new TCHAR[szPrimaryKeyPathLen + szNameValueLen + 3];//+ iKeyNameLen
			tTemp[0] = L'\0';
			lstrcat(tTemp, tcPrimaryKeyPath);
			lstrcat(tTemp, L"\\");
			lstrcat(tTemp, szNameValue);

			uiRet = RegOpenKeyEx(HKEY_LOCAL_MACHINE, tTemp, 0, KEY_READ, &hSubKey);
			if(uiRet!=ERROR_SUCCESS)
			{  
				break;
			}
			DWORD szPathValueLen = sizeof(szPathValue)/sizeof(TCHAR);
			RegQueryValueEx(hSubKey, L"FunctionLibrary", 0, NULL, (BYTE*)szPathValue, &szPathValueLen);
			
			_tcscpy_s((_regInfo.szDrivePath[subKeyCount]), szPathValue);

			delete []tTemp;
			RegCloseKey(hSubKey);
		}

		if(uiRet == 0)
		{
			MessageBoxW(NULL, _T("未找到注册表信息..."), _T("Warnning"), MB_ICONSTOP);	
		}
	}
	else{
		MessageBoxW(NULL, _T("注册表获取失败..."), _T("Warnning"), MB_ICONSTOP);
		return _regInfo;
	}
}
#endif

/*动态加载PassThruSupport.04.04注册表下的所有驱动*/
void GetRegeditPassthruSupport_04_04_Path()
{
	UINT16_T uiRet = 0, subKeyCount = 0;
	HKEY	hPrimaryKey = NULL, hSubKey = NULL;
	//DWORD	szDllPathLen = MAX_PATH;
	TCHAR	szPathValue[MAX_PATH];
	TCHAR	tcPrimaryKeyPath[MAX_PATH];

	memset(tcPrimaryKeyPath, 0, MAX_PATH);

	if(IsWow64())		// 判断系统类型
		_tcscpy_s(tcPrimaryKeyPath, L"SOFTWARE\\Wow6432Node\\PassThruSupport.04.04");
	else
		_tcscpy_s(tcPrimaryKeyPath, L"SOFTWARE\\PassThruSupport.04.04");
	
	uiRet = RegOpenKeyEx(HKEY_LOCAL_MACHINE, tcPrimaryKeyPath, 0, KEY_READ, &hPrimaryKey);

	if(uiRet==ERROR_SUCCESS)
	{
		for(subKeyCount = 0; ; subKeyCount++)
		{
			regDeviceInfo.ulDeviceCounts = subKeyCount;
			TCHAR szNameValue[MAX_PATH] = _T("");
			DWORD dwNameValue = sizeof(szNameValue);

			uiRet = RegEnumKeyEx(hPrimaryKey, subKeyCount, szNameValue, &dwNameValue, NULL, NULL, NULL, NULL);
			if(uiRet == ERROR_NO_MORE_ITEMS)
			{
				RegCloseKey(hPrimaryKey);
				return ;
			}
			_tcscpy_s(regDeviceInfo.regDevice[subKeyCount].szRegeditName, szNameValue);

			UINT16_T szPrimaryKeyPathLen = lstrlen(tcPrimaryKeyPath);
			UINT16_T szNameValueLen = lstrlen(szNameValue);
			UINT16_T iKeyNameLen = lstrlen(L"FunctionLibrary");
			TCHAR* tTemp = new TCHAR[szPrimaryKeyPathLen + szNameValueLen + 3];//+ iKeyNameLen
			tTemp[0] = L'\0';
			lstrcat(tTemp, tcPrimaryKeyPath);
			lstrcat(tTemp, L"\\");
			lstrcat(tTemp, szNameValue);

			uiRet = RegOpenKeyEx(HKEY_LOCAL_MACHINE, tTemp, 0, KEY_READ, &hSubKey);
			if(uiRet!=ERROR_SUCCESS)
			{  
				break;
			}
			DWORD szPathValueLen = sizeof(szPathValue)/sizeof(TCHAR);
			RegQueryValueEx(hSubKey, L"FunctionLibrary", 0, NULL, (BYTE*)szPathValue, &szPathValueLen);

			_tcscpy_s(regDeviceInfo.regDevice[subKeyCount].szRegeditPath, szPathValue);
			delete []tTemp;
			RegCloseKey(hSubKey);
		}

		if(uiRet == 0)
			MessageBoxW(NULL, _T("未找到注册表信息..."), _T("Warnning"), MB_ICONSTOP);	
	}
	else{
		MessageBoxW(NULL, _T("注册表获取失败..."), _T("Warnning"), MB_ICONSTOP);
		return ;
	}
}

//////////////////////////////////////////////////////////////////////
// Function  : XSleepThread()
// Purpose   : The thread which will sleep for the given duration
// Returns   : DWORD WINAPI
// Parameters:       
//  1. pWaitTime -
//////////////////////////////////////////////////////////////////////
DWORD WINAPI XSleepThread(LPVOID pWaitTime)
{
	XSleep_Structure *sleep = (XSleep_Structure *)pWaitTime;
	Sleep(sleep->duration);
	SetEvent(sleep->eventHandle);
	return 0;

}

//////////////////////////////////////////////////////////////////////
// Function  : XSleep()
// Purpose   : To make the application sleep for the specified time
//             duration.
//             Duration the entire time duration XSleep sleeps, it
//             keeps processing the message pump, to ensure that all
//             messages are posted and that the calling thread does
//             not appear to block all threads!
// Returns   : none
// Parameters:       
//  1. nWaitInMSecs - Duration to sleep specified in miliseconds.
//////////////////////////////////////////////////////////////////////
void XSleep(int nWaitInMSecs)
{
	XSleep_Structure sleep;
	sleep.duration      = nWaitInMSecs;
	sleep.eventHandle   = CreateEvent(NULL, TRUE, FALSE, NULL);
	DWORD threadId;
	CreateThread(NULL, 0, &XSleepThread, &sleep, 0, &threadId);
	MSG msg;
	while(::WaitForSingleObject(sleep.eventHandle, 0) == WAIT_TIMEOUT)
	{
		//get and dispatch messages
		if(::PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
		{
			::TranslateMessage(&msg);
			::DispatchMessage(&msg);
		}
	}
	CloseHandle(sleep.eventHandle);
}


void KillExe(CString szExeName)
{
	if(szExeName.IsEmpty())
		return ;

	PROCESSENTRY32 pe; 
	HANDLE hSnapshot = CreateToolhelp32Snapshot(TH32CS_SNAPPROCESS, 0); 
	if (((UINT16_T)hSnapshot) != -1) 
	{ 
		pe.dwSize = sizeof(pe); 
		if (Process32First(hSnapshot, &pe)) 
		{
			do 
			{ 
				//if (lstrcmpi((LPCWSTR)szExeName, pe.szExeFile + lstrlen(pe.szExeFile) - lsr) == 0) 
				if(szExeName.CompareNoCase(pe.szExeFile)==0)
				{ 
					HANDLE hProcessHandle; 
					hProcessHandle = OpenProcess(0x1F0FFF, true, pe.th32ProcessID); 
					if (hProcessHandle != NULL) 
					{ 
						if (TerminateProcess(hProcessHandle, 0) != NULL) 
						{ 
							CloseHandle(hProcessHandle); 
						} 
					} 
					CloseHandle(hSnapshot); 
					return; 
				} 
			}while (Process32Next(hSnapshot, &pe));
		} 
		CloseHandle(hSnapshot); 
	} 
}

CString HexToString(unsigned char ucData[],unsigned long len)
{
	CString str = L"", strTemp = L"" ;

	for(UINT16_T uiLoop = 0; uiLoop< len; uiLoop++)
	{
		strTemp.Format(_T(" %02X") ,ucData[uiLoop]) ;
		str += strTemp ;
	}
	str.Delete(0 ,1);
	return str;	
}

CString UInitToString(UINT32_T uValue, UINT8_T radix, UINT8_T uDecLen /*= 0*/)
{
	CString srtRes;
	if (radix == 10)
		srtRes.Format(L"%d", uValue);
	else if (radix == 16)
	{	
		if(uDecLen == 0)
			srtRes.Format(L"0x%x", uValue);
		else if(uDecLen == 2)
			srtRes.Format(L"0x%.2x", uValue);
		else if(uDecLen == 4)
			srtRes.Format(L"0x%.4x", uValue);
	}
	return srtRes;
}

CString UINT32ToHexString(UINT32_T uValue, UINT8_T uHexLen /*= 0*/)
{
	CString srtRes = L"";
	if(uHexLen == 0)
		srtRes.Format(L"0x%x", uValue);
	else if(uHexLen == 2)
		srtRes.Format(L"0x%.2x", uValue);
	else if(uHexLen == 4)
		srtRes.Format(L"0x%.4x", uValue);
	else if(uHexLen == 8)
		srtRes.Format(L"0x%.8x", uValue);
	return srtRes;
}
CString UINT32ToDecString(UINT32_T uValue, UINT8_T uDecLen /*= 0*/)
{
	CString srtRes = L"";
	if(uDecLen == 0)
		srtRes.Format(L"%d", uValue);
	else if(uDecLen == 2)
		srtRes.Format(L"%.2d", uValue);
	else if(uDecLen == 4)
		srtRes.Format(L"%.4d", uValue);
	else if(uDecLen == 4)
		srtRes.Format(L"%.8d", uValue);
	return srtRes;
}
CString UINT16ToHexString(UINT16_T uValue, UINT8_T uHexLen /*= 0*/)
{
	CString srtRes = L"";
	if(uHexLen == 0)
		srtRes.Format(L"0x%x", uValue);
	else if(uHexLen == 2)
		srtRes.Format(L"0x%.2x", uValue);
	else if(uHexLen == 4)
		srtRes.Format(L"0x%.4x", uValue);
	return srtRes;
}
CString UINT16ToDecString(UINT16_T uValue, UINT8_T uDecLen /*= 0*/)
{
	CString srtRes = L"";
	if(uDecLen == 0)
		srtRes.Format(L"%d", uValue);
	else if(uDecLen == 2)
		srtRes.Format(L"%.2d", uValue);
	else if(uDecLen == 4)
		srtRes.Format(L"%.4d", uValue);
	return srtRes;
}
CString UINT8ToHexStirng(UINT8 uValue, UINT8_T uHexLen /*= 0*/)
{
	CString srtRes = L"";
	if(uHexLen == 0)
		srtRes.Format(L"0x%x", uValue);
	else if(uHexLen == 2)
		srtRes.Format(L"0x%.2x", uValue);
	return srtRes;
}
CString UINT8ToDecStirng(UINT8 uValue, UINT8_T uDecLen /*= 0*/)
{
	CString srtRes = L"";
	if(uDecLen == 0)
		srtRes.Format(L"%d", uValue);
	else if(uDecLen == 2)
		srtRes.Format(L"%.2d", uValue);
	return srtRes;
}


#if 0
CString UintToDecString(unsigned long iValue)
{
	CString strResult;
	char ch[30];
	sprintf_s(ch, "%d", iValue);
	strResult += ch;

	return strResult;
}
#endif

#if 0
SBYTE_ARRAY StringToHex(CString str)
{
	SBYTE_ARRAY res;
	UINT32_T DataSize = 0;
	CString strTemp;
	memset(&res, 0, sizeof(res));

	res.pucBytePtr  = new UINT8_T[sizeof(UINT8_T)];		// 为指针分配内存

	str.Remove(_TCHAR(' '));
	DataSize = lstrlen(str);
	if((DataSize%2) != 0)
	{
		str += '0';
		DataSize = lstrlen(str);
	}

	res.ulNumOfBytes = DataSize/2;
	for (int i = 0;i<res.ulNumOfBytes;i++)
	{
		strTemp=str.Mid(2*i,2);
		res.pucBytePtr[i] = _tcstol(strTemp, 0, 16);
	}
	return res;
}
#endif
BinaryData StringToHex(CString str)
{
	BinaryData	binData;
	UINT32_T	DataSize = 0;
	CString		strTemp;
	memset(&binData, 0, sizeof(binData));

	str.Remove(_TCHAR(' '));
	DataSize = lstrlen(str);
	if((DataSize%2) != 0)
	{
		str += '0';
		DataSize = lstrlen(str);
	}

	binData.ulResLen = DataSize/2;
	for (UINT8_T uiLoop = 0;uiLoop<binData.ulResLen;uiLoop++)
	{
		strTemp=str.Mid(2*uiLoop,2);
		binData.ucConvertData[uiLoop] = _tcstol(strTemp, 0, 16);
	}
	return binData;
}

void StringToHex(CString str, UINT8_T *pData, UINT32_T len)
{
	UINT8_T pBuf[MAX_PATH];
	UINT32_T DataSize = 0, sLen=0;
	CString strTemp;
	//memset(&res, 0, sizeof(res));

	//pBuf = new UINT8_T[sizeof(UINT8_T)];		// 为指针分配内存

	str.Remove(_TCHAR(' '));
	DataSize = lstrlen(str);
	if((DataSize%2) != 0)
	{
		str += '0';
		DataSize = lstrlen(str);
	}

	sLen = DataSize/2;
	for (UINT16_T uiLoop = 0;uiLoop<sLen;uiLoop++)
	{
		strTemp=str.Mid(2*uiLoop,2);
		pBuf[uiLoop] = _tcstol(strTemp, 0, 16);
	}
	pData = pBuf;
	len = sLen;
}

/*------------------------------------------------------------------------
*功    能：将输入不规范的字符串，修改为规范的Binary类型的字符串
*参数说明：str - 需要修改的字符串
*返 回 值：修改规范的字符串
*说    明：①："999" -> "99 90"
		  ②："9999" -> "99 99"
		  ③：……
------------------------------------------------------------------------*/
CString HandleDataByteString(CString str)
{
	UINT32_T DataSize = 0, sLen=0;

	if(str.GetLength() == 0)
		return str;

	str.Remove(_TCHAR(' '));
	DataSize = lstrlen(str);
	if((DataSize%2) != 0)
	{
		str += '0';
		DataSize = lstrlen(str);
	}

	sLen = DataSize/2;
	for (UINT16_T uiLoop = 1; uiLoop < sLen; uiLoop++)
		str.Insert(3*uiLoop-1, ' ');
	return str;
}

/*------------------------------------------------------------------------
*功    能：比较2个Binary结构体的内容
*参数说明：binCur - 获取到的当前Binary结构
           binLast - 获取到的上次Binary结构
*返 回 值：比较2个结构体内容相同，则返回TRUE; 否则返回FALSE
*说    明：无
------------------------------------------------------------------------*/
BOOL CompareBinaryData(BinaryData binCur, BinaryData binLast)
{
	if(binCur.ulResLen != binLast.ulResLen)
		return FALSE;
	if(memcmp(binCur.ucConvertData, binLast.ucConvertData, binCur.ulResLen))
		return FALSE;
	return TRUE;
}

BinaryData CopyBinaryData(BinaryData binStr)
{
	BinaryData bin;
	memset(&bin, 0, sizeof(bin));
	bin.ulResLen = binStr.ulResLen;
	memcpy(bin.ucConvertData, binStr.ucConvertData, binStr.ulResLen);
	return bin;
}

void SetBit(UINT32_T& ulInputData, const UINT8_T n)
{
	ulInputData |=(UINT32_T)(1<<n);
}

void ClearBit(UINT32_T& ulInputData, const UINT8_T n)
{
	ulInputData &= ~(UINT16_T)(1<<n);
}

void CopyDataToClipboard(INT8_T* pData, UINT32_T Len)
{
	if (OpenClipboard(NULL))
	{
		HGLOBAL clipbuffer;
		INT8_T *buffer;
		EmptyClipboard();   
		clipbuffer=GlobalAlloc(GMEM_DDESHARE,Len);
		buffer=(INT8_T*)GlobalLock(clipbuffer);   
		strcpy_s(buffer, Len+1, pData);   
		GlobalUnlock(clipbuffer);
		SetClipboardData(CF_TEXT,clipbuffer);   
		CloseClipboard();
	}
}

//void UnicodeToAnsi(CString strMsgData, INT8_T* pData)
//{
//	INT8_T WideCharLen = WideCharToMultiByte(CP_ACP, 0, strMsgData, -1, NULL, 0, NULL, NULL);// 先取得转换后的ANSI字符串所需的长度
//	//INT8_T* pData = (INT8_T*)calloc(WideCharLen, sizeof(INT8_T));
//	INT8_T cData[MAX_PATH];// 分配缓冲区
//	memset(cData, 0, MAX_PATH);
//	WideCharToMultiByte(CP_ACP, 0, strMsgData, -1, cData, WideCharLen, NULL, NULL);// 开始转换                                                
//	*pData = cData[MAX_PATH];
//}

UINT8_T GetSystemByte()
{
	SYSTEM_INFO sys_Info;
	memset(&sys_Info, 0, sizeof(SYSTEM_INFO));

	//用这种方式是为了兼容64位的程序  
	typedef VOID (WINAPI *LPFN_GetNativeSystemInfo)(LPSYSTEM_INFO lpSystemInfo); 
	LPFN_GetNativeSystemInfo nsInfo;
	nsInfo=(LPFN_GetNativeSystemInfo)GetProcAddress(GetModuleHandle(_T("kernel32")),                                                                          "GetNativeSystemInfo");    
	if(NULL != nsInfo)
	{    
		nsInfo(&sys_Info);    
	}    
	else
	{    
		GetSystemInfo(&sys_Info);    
	} 

	if(sys_Info.wProcessorArchitecture == PROCESSOR_ARCHITECTURE_AMD64 ||     
		sys_Info.wProcessorArchitecture == PROCESSOR_ARCHITECTURE_IA64 ){        
		return WINDOWS_SYSTEM_64BIT; 
	}else
	{
		return WINDOWS_SYSTEM_32BIT;
	}
	return WINDOWS_SYSTEM_ERROR;
}



CString GetSystemTime()
{
	SYSTEMTIME time;
	GetLocalTime(&time);
	CString months, days, hours, minutes, seconds, CurrentTime;
	months.Format(_T("%.2d月"), time.wMonth);
	days.Format(_T("%.2d日"), time.wDay);
	hours.Format(_T("%.2d时"), time.wHour);
	minutes.Format(_T("%.2d分"), time.wMinute);
	seconds.Format(_T("%.2d秒"), time.wSecond);
	CurrentTime = months + days + hours + minutes + seconds;
	return CurrentTime;
}

void Trimstring(std::string & str )
{
	int s = str.find_first_not_of(" ");
	int e = str.find_last_not_of(" ");
	str = str.substr(s,e-s+1);
	return;
}


// 获取系统的当前时间，单位微秒(us)
unsigned long long GetSysTimeMicros()
{
#ifdef _WIN32
	// 从1601年1月1日0:0:0:000到1970年1月1日0:0:0:000的时间(单位100ns)
#define EPOCHFILETIME   (116444736000000000UL)
	FILETIME ft;
	LARGE_INTEGER li;
	unsigned long long tt = 0;
	GetSystemTimeAsFileTime(&ft);
	li.LowPart = ft.dwLowDateTime;
	li.HighPart = ft.dwHighDateTime;
	// 从1970年1月1日0:0:0:000到现在的微秒数(UTC时间)
	tt = (li.QuadPart - EPOCHFILETIME) /10;
	return tt;
#else
	timeval tv;
	gettimeofday(&tv, 0);
	return (int64_t)tv.tv_sec * 1000000 + (int64_t)tv.tv_usec;
#endif // _WIN32
	return 0;
}