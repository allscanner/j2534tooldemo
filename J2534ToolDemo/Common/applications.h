﻿/*******************************************************************************
 * FILE: applications.h
 * This file is part of ALLScanner VCX software.
 * COPYRIGHT (C) ALLScanner 2008-2014. 
 *		
 * DESC: J2534Tool's defines of uxiliary function 
 * 
 * HISTORY:
 * Date			Author		Notes	
 * 2016-01-01		my		Create
*******************************************************************************/

#ifndef _APPLICATIONS_H_
#define _APPLICATIONS_H_

#include <iostream>
#include <string>
#include "tlhelp32.h"
#include "CString.h"
#include <windows.h>
#include <time.h>
#include <afxstr.h>

using namespace std;

typedef char					INT8_T;
typedef unsigned char	UINT8_T;
typedef int						INT16_T;
typedef unsigned int		UINT16_T;
//typedef unsigned short		UINT16_T;
typedef long					INT32_T;
typedef unsigned long	UINT32_T;


#define MAX_DEVICE_NUM	40

#define MAX_BAUDRATE_COUNT 20
#define MAX_PROTOCOL_COUNT 20
#define MAX_CONNECTFLAG_COUNT 20

#define WINDOWS_SYSTEM_32BIT 32
#define WINDOWS_SYSTEM_64BIT 64
#define WINDOWS_SYSTEM_ERROR 0

/************************************************************
	*功		 能：used internally by the XSleep function 
	*参 数 说 明：无
	*返  回	 值：无
	*说	     明：无
************************************************************/
struct XSleep_Structure
{
	UINT16_T duration;
	HANDLE eventHandle;
};

/************************************************************
	*功		 能：用于保存驱动注册表信息 
	*参 数 说 明：无
	*返  回	 值：无
	*说	     明：无
************************************************************/
typedef struct _tagPassthruRegedit
{
	TCHAR szRegeditName[MAX_PATH];
	TCHAR szRegeditPath[MAX_PATH];
}PassthruRegedit;

/************************************************************
	*功		 能：用于保存指定组驱动注册表信息 
	*参 数 说 明：无
	*返  回	 值：无
	*说	     明：无
************************************************************/
typedef struct _tagPassthruDevice
{
	UINT32_T		ulDeviceCounts;
	PassthruRegedit regDevice[MAX_DEVICE_NUM];
}PassthruRegeditDevice;

/************************************************************
	*功		 能：自定义用于保存数据
	*参 数 说 明：无
	*返  回	 值：无
	*说	     明：无
************************************************************/
typedef struct _tagContain
{
	UINT8_T		ucConvertData[MAX_PATH];  // the string from UI's is to the data of UINI8_T type
	UINT32_T	ulResLen;
}BinaryData;

/************************************************************
	*功		 能：用于Kill进程
	*参 数 说 明：szExeName-表示将要Kill的进程的名字
	*返  回	 值：无
	*说	     明：无
************************************************************/
void KillExe(CString szExeName);

/************************************************************
	*功		 能：应用程序休眠
	*参 数 说 明：nWaitInMSecs-表示将要休眠的时间(单位ms)
	*返  回	 值：无
	*说	     明：无
************************************************************/
void XSleep(INT16_T nWaitInMSecs);

/************************************************************
	*功		 能：动态加载PassThruSupport.04.04注册表下的所有驱动
	*参 数 说 明：无
	*返  回	 值：无
	*说	     明：无
************************************************************/
void GetRegeditPassthruSupport_04_04_Path();

/************************************************************
	*功		 能：获取指定路径下的驱动
	*参 数 说 明：无
	*返  回	 值：无
	*说	     明：无
************************************************************/
CString GetRegeditVCXPassthruPath();

/************************************************************
	*功		 能：获取系统是32位 or 64位
	*参 数 说 明：无
	*返  回	 值：操作系统位数
	*说	     明：无
************************************************************/
UINT8_T GetSystemByte();

/************************************************************
	*功		 能：16进制整形转为字符串
	*参 数 说 明：ucData[] - 将要转换的整形数据
				 len - 整形数据的长度
	*返  回	 值：无
	*说	     明：无
************************************************************/
CString HexToString(UINT8_T ucData[],UINT32_T len);

/************************************************************
	*功		 能：无符号整形转为字符串
	*参 数 说 明：uValue - 将要转换的整形数据
				 radix - 将要转换成的进制
				 uHexLen - 转换后的字符串数据位数
	*返  回	 值：无
	*说	     明：无
************************************************************/
CString UInitToString(UINT32_T uValue, UINT8_T radix, UINT8_T uHexLen = 0);

/************************************************************
	*功		 能：无符号整形转为10进制或16进制字符串
	*参 数 说 明：uValue - 将要转换的整形数据
				 uHexLen - 转换为16进制后的字符串数据位数
				 uDecLen - 转换为10进制后的字符串数据位数
	*返  回	 值：无
	*说	     明：无
************************************************************/
CString UINT32ToHexString(UINT32_T uValue, UINT8_T uHexLen = 0);
CString UINT32ToDecString(UINT32_T uValue, UINT8_T uDecLen = 0);
CString UINT16ToHexString(UINT16_T uValue, UINT8_T uHexLen = 0);
CString UINT16ToDecString(UINT16_T uValue, UINT8_T uDecLen = 0);
CString UINT8ToHexStirng(UINT8 uValue, UINT8_T uHexLen = 0);
CString UINT8ToDecStirng(UINT8 uValue, UINT8_T uDecLen = 0);

void SetBit(UINT32_T& ulInputData, const UINT8_T n);
void ClearBit(UINT32_T& ulInputData, const UINT8_T n);

void CopyDataToClipboard(INT8_T* pData, UINT32_T Len);
//void UnicodeToAnsi(CString strMsgData, INT8_T* pData);
CString GetSystemTime();
void StringToHex(CString str, UINT8_T* pData, UINT32_T len);
BinaryData StringToHex(CString str);
BinaryData CopyBinaryData(BinaryData binStr);
BOOL CompareBinaryData(BinaryData binCur, BinaryData binLast);


CString HandleDataByteString(CString str);

unsigned long long GetSysTimeMicros();

// 将std::string字符串去除空格
void Trimstring(std::string & str );


#endif // _APPLICATIONS_H_