﻿/*******************************************************************************
 * FILE: FilterConfig.cpp
 * This file is part of ALLScanner VCX software.
 * COPYRIGHT (C) ALLScanner 2008-2014. 
 *		
 * DESC: J2534Tool Filter Config Parameters's declaration 
 * 
 * HISTORY:
 * Date			Author		Notes	
 * 2016-01-01		my			Create
*******************************************************************************/

#include "stdafx.h"
#include "J2534ToolDemo.h"
#include "FilterConfig.h"
#include "afxdialogex.h"

// FilterConfig 对话框

IMPLEMENT_DYNAMIC(FilterConfig, CDialogEx)

FilterConfig::FilterConfig(CWnd* pParent /*=NULL*/)
	: CDialogEx(FilterConfig::IDD, pParent)
{

}

FilterConfig::~FilterConfig()
{

}

void FilterConfig::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
}


BEGIN_MESSAGE_MAP(FilterConfig, CDialogEx)
	ON_BN_CLICKED(IDC_BTN_FILTERCONFIG_CLEARALL, &FilterConfig::OnBnClickedBtnFilterconfigClearall)
	ON_BN_CLICKED(IDC_BTN_FILTERCONFIG_CANCEL, &FilterConfig::OnBnClickedBtnFilterconfigCancel)
	ON_BN_CLICKED(IDC_BTN_FILTERCONFIG_CREATEPASSALLFILTER, &FilterConfig::OnBnClickedBtnFilterconfigCreatepassallfilter)
	ON_BN_CLICKED(IDC_BTN_FILTERCONFIG_APPLY, &FilterConfig::OnBnClickedBtnFilterconfigApply)
	ON_WM_SYSCOMMAND()
	ON_CBN_SELCHANGE(IDC_CB_FILTERCONFIG_PROTOCOL, &FilterConfig::OnCbnSelchangeCbFilterconfigProtocol)
END_MESSAGE_MAP()


#define ARR_MESSAGE_COUNT 10

// Global Variables
UINT32_T uFilterCurrChannelNum = 0;
BinaryData bMask[FILTER_MAX_COUNT], bPattern[FILTER_MAX_COUNT], bFlowctl[FILTER_MAX_COUNT];
UINT32_T uTxFlag[FILTER_MAX_COUNT], uFilterType[FILTER_MAX_COUNT];
UINT32_T uOutFilterId[FILTER_MAX_COUNT];
BOOL bCheckBox[FILTER_MAX_COUNT];

extern CErrorCode _err;
extern CString g_PassthruStatus;
extern UINT32_T g_GlobalStatus;
extern UINT32_T g_DevicesCount;
extern ChannelFilters saveChannelfilterdata[CONNECT_MAX_COUNT];
extern ConnectInfo g_ConnectInfo[CONNECT_MAX_COUNT];
extern Mutex g_ThreadLock;

// FilterConfig 消息处理程序
static int _arryMaskdata[FILTER_MAX_COUNT] = {IDC_ED_FILTERCONFIG_MASKDATA0, IDC_ED_FILTERCONFIG_MASKDATA1, IDC_ED_FILTERCONFIG_MASKDATA2,
	IDC_ED_FILTERCONFIG_MASKDATA3, IDC_ED_FILTERCONFIG_MASKDATA4, IDC_ED_FILTERCONFIG_MASKDATA5, IDC_ED_FILTERCONFIG_MASKDATA6,
	IDC_ED_FILTERCONFIG_MASKDATA7,  IDC_ED_FILTERCONFIG_MASKDATA8, IDC_ED_FILTERCONFIG_MASKDATA9};
static int _arryPatterndata[FILTER_MAX_COUNT] = {IDC_ED_FILTERCONFIG_PATTERNDATA0, IDC_ED_FILTERCONFIG_PATTERNDATA1, IDC_ED_FILTERCONFIG_PATTERNDATA2,
	IDC_ED_FILTERCONFIG_PATTERNDATA3, IDC_ED_FILTERCONFIG_PATTERNDATA4, IDC_ED_FILTERCONFIG_PATTERNDATA5, IDC_ED_FILTERCONFIG_PATTERNDATA6,
	IDC_ED_FILTERCONFIG_PATTERNDATA7, IDC_ED_FILTERCONFIG_PATTERNDATA8, IDC_ED_FILTERCONFIG_PATTERNDATA9};
static int _arryFlowcontroldata[FILTER_MAX_COUNT] = {IDC_ED_FILTERCONFIG_FLOWCONTROL0, IDC_ED_FILTERCONFIG_FLOWCONTROL1, IDC_ED_FILTERCONFIG_FLOWCONTROL2,
	IDC_ED_FILTERCONFIG_FLOWCONTROL3, IDC_ED_FILTERCONFIG_FLOWCONTROL4, IDC_ED_FILTERCONFIG_FLOWCONTROL5, IDC_ED_FILTERCONFIG_FLOWCONTROL6,
	IDC_ED_FILTERCONFIG_FLOWCONTROL7, IDC_ED_FILTERCONFIG_FLOWCONTROL8, IDC_ED_FILTERCONFIG_FLOWCONTROL9};
static int _arryTxflags[FILTER_MAX_COUNT] = {IDC_ED_FILTERCONFIG_TXFLAGS0, IDC_ED_FILTERCONFIG_TXFLAGS1, IDC_ED_FILTERCONFIG_TXFLAGS2, IDC_ED_FILTERCONFIG_TXFLAGS3,
	IDC_ED_FILTERCONFIG_TXFLAGS4, IDC_ED_FILTERCONFIG_TXFLAGS5, IDC_ED_FILTERCONFIG_TXFLAGS6, IDC_ED_FILTERCONFIG_TXFLAGS7, 
	IDC_ED_FILTERCONFIG_TXFLAGS8, IDC_ED_FILTERCONFIG_TXFLAGS9};
static int _arryFilters[FILTER_MAX_COUNT] = {IDC_CB_FILTERCONFIG_FILTER0, IDC_CB_FILTERCONFIG_FILTER1, IDC_CB_FILTERCONFIG_FILTER2, IDC_CB_FILTERCONFIG_FILTER3,
	IDC_CB_FILTERCONFIG_FILTER4, IDC_CB_FILTERCONFIG_FILTER5, IDC_CB_FILTERCONFIG_FILTER6, IDC_CB_FILTERCONFIG_FILTER7,
	IDC_CB_FILTERCONFIG_FILTER8, IDC_CB_FILTERCONFIG_FILTER9};
static int _arryEnabled[FILTER_MAX_COUNT] = {IDC_CK_FILTERCONFIG_ENABLE0, IDC_CK_FILTERCONFIG_ENABLE1, IDC_CK_FILTERCONFIG_ENABLE2, IDC_CK_FILTERCONFIG_ENABLE3,
	IDC_CK_FILTERCONFIG_ENABLE4, IDC_CK_FILTERCONFIG_ENABLE5, IDC_CK_FILTERCONFIG_ENABLE6, IDC_CK_FILTERCONFIG_ENABLE7,
	IDC_CK_FILTERCONFIG_ENABLE8, IDC_CK_FILTERCONFIG_ENABLE9};
static int _arryNumber[FILTER_MAX_COUNT] = {IDC_ST_FILTERCONFIG_NUM0, IDC_ST_FILTERCONFIG_NUM1, IDC_ST_FILTERCONFIG_NUM2, IDC_ST_FILTERCONFIG_NUM3, IDC_ST_FILTERCONFIG_NUM4,
	IDC_ST_FILTERCONFIG_NUM5, IDC_ST_FILTERCONFIG_NUM6, IDC_ST_FILTERCONFIG_NUM7, IDC_ST_FILTERCONFIG_NUM8, IDC_ST_FILTERCONFIG_NUM9};

BOOL FilterConfig::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	/* 界面显示 */
	if(m_FilterSdk.GetDeviceId() == 0)
		UIDisplayFilterConfig(FALSE);
	else
		UIDisplayFilterConfig(TRUE);

	return TRUE;  // return TRUE unless you set the focus to a control
	// 异常: OCX 属性页应返回 FALSE
}

void FilterConfig::InitGlobalFilterParam()
{
	for(UINT8_T uiLoop = 0; uiLoop < FILTER_MAX_COUNT; uiLoop++)
	{
		memset(&bMask[uiLoop], 0, sizeof(bMask[uiLoop]));
		memset(&bPattern[uiLoop], 0, sizeof(bPattern[uiLoop]));
		memset(&bFlowctl[uiLoop], 0, sizeof(bFlowctl[uiLoop]));
		uTxFlag[uiLoop] = 0;
		uFilterType[uiLoop] = 0;
		bCheckBox[uiLoop] = FALSE;
	}
	memset(uOutFilterId, 0, FILTER_MAX_COUNT);
}

void FilterConfig::UIDisplayFilterConfig(BOOL res)
{
	UILoadFilterSdkParam();

	InitGlobalFilterParam();
	InitFilterParamMaskdata();
	InitFilterParamPatterndata();
	InitFilterParamFlowcontrol();	
	InitFilterParamTxflags();
	InitFilterParamFilterType();
	InitFilterParamIsChecked(FALSE);
		
	UILoadFilterDefaultParam();

	SetFilterAllCtlsIsEnable(res);
	SetFilterBtnStatus();
}


/*------------------------------------------------------------------------
*功		 能：初始化加载Device，Protocol控件的显示
*参 数 说 明：无
*返  回	 值：无
*说	     明：无
------------------------------------------------------------------------*/
void FilterConfig::UILoadFilterSdkParam()
{
	/* DeviceId 显示 */
	CString strFilterDevice = L"J2534 Tool ", strProtocolName;
	INT8_T ucDeviceId[5],ucTemp = 0;
	//_itoa_s(m_FilterSdk.GetDeviceId(), ucDeviceId, 16);
	strFilterDevice += UINT32ToDecString(g_DevicesCount);
	GetDlgItem(IDC_ED_FILTERCONFIG_DEVICE)->SetWindowTextW(strFilterDevice);

	/* ProtocolId 显示 */
	((CComboBox*)GetDlgItem(IDC_CB_FILTERCONFIG_PROTOCOL))->ResetContent();
	for(UINT8_T ucLoop = 0; ucLoop < CONNECT_MAX_COUNT; ucLoop++)
	{
		if(m_FilterSdk.m_Channel[ucLoop].GetChannelExist() != FALSE)
		{
			((CComboBox*)GetDlgItem(IDC_CB_FILTERCONFIG_PROTOCOL))->InsertString(ucTemp,ProtocolValueToStr(m_FilterSdk.m_Channel[ucLoop].GetProtocolId()));
			ucTemp++;
		}
	}
	((CComboBox*)GetDlgItem(IDC_CB_FILTERCONFIG_PROTOCOL))->SetCurSel(0);

	/* 获取当前通道号 */
	((CComboBox*)GetDlgItem(IDC_CB_FILTERCONFIG_PROTOCOL))->GetLBText(((CComboBox*)GetDlgItem(IDC_CB_FILTERCONFIG_PROTOCOL))->GetCurSel(), strProtocolName);
	for(UINT8_T uiLoop = 0; uiLoop < CONNECT_MAX_COUNT; uiLoop++)
	{
		if(ProtocolStrToValue(strProtocolName) == m_FilterSdk.m_Channel[uiLoop].GetProtocolId())
		{	
			uFilterCurrChannelNum = m_FilterSdk.m_Channel[uiLoop].GetChannelId();
			break;
		}
	}
}

void FilterConfig::SetFilterAllCtlsIsEnable(BOOL res)
{
	UINT8_T uiLoop;
	((CStatic*)GetDlgItem(IDC_ST_FILTERCONFIG_MASKDATA))->EnableWindow(res);
	for(uiLoop=0; uiLoop<ARR_MESSAGE_COUNT; uiLoop++)
		((CEdit*)GetDlgItem(_arryMaskdata[uiLoop]))->EnableWindow(res);

	((CStatic*)GetDlgItem(IDC_ST_FILTERCONFIG_PATTERNDATA))->EnableWindow(res);
	for(uiLoop=0; uiLoop<ARR_MESSAGE_COUNT; uiLoop++)
		((CEdit*)GetDlgItem(_arryPatterndata[uiLoop]))->EnableWindow(res);

	switch (m_FilterSdk.m_Channel[uFilterCurrChannelNum-1].GetProtocolId())
	{
	case ISO15765:
	case ISO15765_PS:
	case SW_ISO15765_PS:
		SetFilterFlowCtlIsEnable(TRUE);
		break;
	default:
		SetFilterFlowCtlIsEnable(FALSE);
		break;
	}

	((CStatic*)GetDlgItem(IDC_ST_FILTERCONFIG_TXFLAGS))->EnableWindow(res);
	for(uiLoop=0; uiLoop<ARR_MESSAGE_COUNT; uiLoop++)
		((CEdit*)GetDlgItem(_arryTxflags[uiLoop]))->EnableWindow(res);

	((CStatic*)GetDlgItem(IDC_ST_FILTERCONFIG_FILTERS))->EnableWindow(res);
	for(uiLoop=0; uiLoop<ARR_MESSAGE_COUNT; uiLoop++)
		((CComboBox*)GetDlgItem(_arryFilters[uiLoop]))->EnableWindow(res);

	((CStatic*)GetDlgItem(IDC_ST_FILTERCONFIG_ENABLE))->EnableWindow(res);
	for(uiLoop=0; uiLoop<ARR_MESSAGE_COUNT; uiLoop++)
		((CEdit*)GetDlgItem(_arryEnabled[uiLoop]))->EnableWindow(res);

	for(uiLoop=0; uiLoop<ARR_MESSAGE_COUNT; uiLoop++)
		GetDlgItem(_arryNumber[uiLoop])->EnableWindow(res);
}

void FilterConfig::InitFilterParamMaskdata()
{
	for(UINT8_T uiLoop=0; uiLoop<ARR_MESSAGE_COUNT; uiLoop++)
		GetDlgItem(_arryMaskdata[uiLoop])->SetWindowTextW(L"");
}
void FilterConfig::InitFilterParamPatterndata()
{
	for(UINT8_T uiLoop=0; uiLoop<ARR_MESSAGE_COUNT; uiLoop++)
		GetDlgItem(_arryPatterndata[uiLoop])->SetWindowTextW(L"");
}
void FilterConfig::InitFilterParamFlowcontrol()
{
	for(UINT8_T uiLoop=0; uiLoop<ARR_MESSAGE_COUNT; uiLoop++)
		GetDlgItem(_arryFlowcontroldata[uiLoop])->SetWindowTextW(L"");
}

void FilterConfig::InitFilterParamFilterType()
{
	CString strFilterType1[3] = {L"None", L"Pass", L"Block"};
	CString strFilterType2[2] = {L"None", L"Flow"};
	// 初始化Filter参数
	for(UINT8_T uiLoop=0; uiLoop<ARR_MESSAGE_COUNT; uiLoop++)
	{
		((CComboBox*)GetDlgItem(_arryFilters[uiLoop]))->ResetContent();
		switch(m_FilterSdk.m_Channel[uFilterCurrChannelNum-1].GetProtocolId())
		{
		case ISO15765:
		case ISO15765_PS:
		case SW_ISO15765_PS:
			for (UINT8_T ujLoop=0; ujLoop<2; ujLoop++)
				((CComboBox*)GetDlgItem(_arryFilters[uiLoop]))->InsertString(ujLoop, strFilterType2[ujLoop]);
			break;
		default:
			for (UINT8_T ujLoop=0; ujLoop<3; ujLoop++)
				((CComboBox*)GetDlgItem(_arryFilters[uiLoop]))->InsertString(ujLoop, strFilterType1[ujLoop]);
			break;
		}

		((CComboBox*)GetDlgItem(_arryFilters[uiLoop]))->SetCurSel(0);
	}
}

void FilterConfig::InitFilterParamTxflags()
{
	// 初始化TxFlags参数
	for(UINT8_T uiLoop=0; uiLoop<ARR_MESSAGE_COUNT; uiLoop++)
		GetDlgItem(_arryTxflags[uiLoop])->SetWindowTextW(L"0x00000000");
}

void FilterConfig::InitFilterParamIsChecked(BOOL res)
{
	for(UINT8_T uiLoop=0; uiLoop<ARR_MESSAGE_COUNT; uiLoop++)
		((CButton*)GetDlgItem(_arryEnabled[uiLoop]))->SetCheck(res);

	UpdateData(FALSE);
}

void FilterConfig::SetFilterFlowCtlIsEnable(BOOL bTemp)
{
	((CStatic*)GetDlgItem(IDC_ST_FILTERCONFIG_FLOWCONTROL))->EnableWindow(bTemp);
	for(UINT8_T uiLoop=0; uiLoop<ARR_MESSAGE_COUNT; uiLoop++)
		((CEdit*)GetDlgItem(_arryFlowcontroldata[uiLoop]))->EnableWindow(bTemp);
}

void FilterConfig::SetFilterBtnIsEnable(BOOL bBtn1, BOOL bBtn2, BOOL bBtn3, BOOL bBtn4)
{
	GetDlgItem(IDC_BTN_FILTERCONFIG_CREATEPASSALLFILTER)->EnableWindow(bBtn1);
	GetDlgItem(IDC_BTN_FILTERCONFIG_CLEARALL)->EnableWindow(bBtn2);
	GetDlgItem(IDC_BTN_FILTERCONFIG_APPLY)->EnableWindow(bBtn3);
	GetDlgItem(IDC_BTN_FILTERCONFIG_CANCEL)->EnableWindow(bBtn4);
}

void FilterConfig::SetFilterBtnIsVisible(BOOL bBtn1, BOOL bBtn2, BOOL bBtn3, BOOL bBtn4)
{
	GetDlgItem(IDC_BTN_FILTERCONFIG_CREATEPASSALLFILTER)->ShowWindow(bBtn1);
	GetDlgItem(IDC_BTN_FILTERCONFIG_CLEARALL)->ShowWindow(bBtn2);
	GetDlgItem(IDC_BTN_FILTERCONFIG_APPLY)->ShowWindow(bBtn3);
	GetDlgItem(IDC_BTN_FILTERCONFIG_CANCEL)->ShowWindow(bBtn4);
}

void FilterConfig::SetFilterBtnStatus()
{
	switch(m_FilterSdk.m_Channel[uFilterCurrChannelNum-1].GetProtocolId())
	{
	case ISO15765:
	case ISO15765_PS:
	case SW_ISO15765_PS:
		SetFilterBtnIsVisible(FALSE, TRUE, TRUE, TRUE);
		SetFilterBtnIsEnable(FALSE, TRUE, TRUE, TRUE);
		break;
	default:
		SetFilterBtnIsVisible(TRUE, TRUE, TRUE, TRUE);
		SetFilterBtnIsEnable(TRUE, TRUE, TRUE, TRUE);
		break;
	}
}

void FilterConfig::OnBnClickedBtnFilterconfigClearall()
{
	UINT32_T RetVal = 0;
	InitFilterParamMaskdata();
	InitFilterParamPatterndata();
	InitFilterParamFilterType();
	InitFilterParamFlowcontrol();
	InitFilterParamTxflags();
	InitFilterParamIsChecked(FALSE);
	SetFilterAllCtlsIsEnable(TRUE);

	g_ThreadLock.Lock();
	RetVal = _PassThruIoctl(m_FilterSdk.m_Channel[uFilterCurrChannelNum-1].GetChannelId(), CLEAR_MSG_FILTERS, NULL, NULL);
	g_ThreadLock.Unlock();
	if (RetVal != J2534_STATUS_NOERROR)
		::MessageBoxW(NULL, _err.GetErrInfo(RetVal), L"Warnning!", MB_ICONINFORMATION);
	//PASSTHRUMESSAGEVIEW(RetVal, L"Clear ALL-OK");

	for(UINT8_T ujLoop = 0; ujLoop < CONNECT_MAX_COUNT; ujLoop++)
	{
		memset(&saveChannelfilterdata[uFilterCurrChannelNum-1]._filterParam[ujLoop].pMask, 0, sizeof(saveChannelfilterdata[uFilterCurrChannelNum-1]._filterParam[ujLoop].pMask));
		memset(&saveChannelfilterdata[uFilterCurrChannelNum-1]._filterParam[ujLoop].pPattern, 0, sizeof(saveChannelfilterdata[uFilterCurrChannelNum-1]._filterParam[ujLoop].pPattern));
		memset(&saveChannelfilterdata[uFilterCurrChannelNum-1]._filterParam[ujLoop].pFlowControl, 0, sizeof(saveChannelfilterdata[uFilterCurrChannelNum-1]._filterParam[ujLoop].pFlowControl));
		saveChannelfilterdata[uFilterCurrChannelNum-1]._filterParam[ujLoop].ulTxFlags = 0;
		saveChannelfilterdata[uFilterCurrChannelNum-1]._filterParam[ujLoop].ulFilterType = 0;
		saveChannelfilterdata[uFilterCurrChannelNum-1]._filterParam[ujLoop].ulFilterId = 0;
		saveChannelfilterdata[uFilterCurrChannelNum-1]._filterParam[ujLoop].bCheckStatus = FALSE;
	}

	InitGlobalFilterParam();
	SetFilterBtnIsEnable(TRUE, TRUE, FALSE, FALSE);
}


void FilterConfig::OnBnClickedBtnFilterconfigCancel()
{
	SetFilterBtnIsEnable(TRUE, TRUE, FALSE, FALSE);
}


void FilterConfig::OnBnClickedBtnFilterconfigCreatepassallfilter()
{
	switch (m_FilterSdk.m_Channel[uFilterCurrChannelNum-1].GetProtocolId())
	{
	case J1850VPW:
	case ISO9141:	
	case ISO9141_PS:
	case ISO14230:
		SetFilterAllCtlsIsEnable(TRUE);
		((CEdit*)GetDlgItem(IDC_ED_FILTERCONFIG_MASKDATA0))->SetWindowTextW(L"00");
		((CEdit*)GetDlgItem(IDC_ED_FILTERCONFIG_PATTERNDATA0))->SetWindowTextW(L"00");
		((CEdit*)GetDlgItem(IDC_ED_FILTERCONFIG_FLOWCONTROL0))->EnableWindow(FALSE);
		((CComboBox*)GetDlgItem(IDC_CB_FILTERCONFIG_FILTER0))->SetCurSel(1);
		((CButton*)GetDlgItem(IDC_CK_FILTERCONFIG_ENABLE0))->SetCheck(TRUE);
		break;
	case J1850PWM:
		SetFilterAllCtlsIsEnable(TRUE);
		((CEdit*)GetDlgItem(IDC_ED_FILTERCONFIG_MASKDATA0))->SetWindowTextW(L"00 00 00");
		((CEdit*)GetDlgItem(IDC_ED_FILTERCONFIG_PATTERNDATA0))->SetWindowTextW(L"00 00 00");
		((CEdit*)GetDlgItem(IDC_ED_FILTERCONFIG_FLOWCONTROL0))->EnableWindow(FALSE);
		((CComboBox*)GetDlgItem(IDC_CB_FILTERCONFIG_FILTER0))->SetCurSel(1);
		((CButton*)GetDlgItem(IDC_CK_FILTERCONFIG_ENABLE0))->SetCheck(TRUE);
		break;
	case CAN:
	case CAN_PS:
	case SW_CAN_PS:
		SetFilterAllCtlsIsEnable(TRUE);
		((CEdit*)GetDlgItem(IDC_ED_FILTERCONFIG_MASKDATA0))->SetWindowTextW(L"00 00 00 00");
		((CEdit*)GetDlgItem(IDC_ED_FILTERCONFIG_PATTERNDATA0))->SetWindowTextW(L"00 00 00 00");
		((CEdit*)GetDlgItem(IDC_ED_FILTERCONFIG_FLOWCONTROL0))->EnableWindow(FALSE);
		((CComboBox*)GetDlgItem(IDC_CB_FILTERCONFIG_FILTER0))->SetCurSel(1);
		((CButton*)GetDlgItem(IDC_CK_FILTERCONFIG_ENABLE0))->SetCheck(TRUE);
		break;
	case ISO15765:
	case ISO15765_PS:
	case SW_ISO15765_PS:
		//SetFilterAllCtlsIsEnable(TRUE);
		//((CEdit*)GetDlgItem(IDC_ED_FILTERCONFIG_MASKDATA0))->SetWindowTextW(L"");
		//((CEdit*)GetDlgItem(IDC_ED_FILTERCONFIG_PATTERNDATA0))->SetWindowTextW(L"");
		//((CEdit*)GetDlgItem(IDC_ED_FILTERCONFIG_FLOWCONTROL0))->SetWindowTextW(L"");
		//((CEdit*)GetDlgItem(IDC_ED_FILTERCONFIG_FLOWCONTROL0))->EnableWindow(TRUE);
		//((CComboBox*)GetDlgItem(IDC_CB_FILTERCONFIG_FILTER0))->SetCurSel(1);
		//((CButton*)GetDlgItem(IDC_CK_FILTERCONFIG_ENABLE0))->SetCheck(TRUE);
		break;
	//case J1939_PS:	// 卡车
	//	break;
	//case VOLVO_CAN2:
	//	break;
	default:
		break;
	}
	SetFilterBtnIsEnable(TRUE, TRUE, TRUE, TRUE);
}

/*------------------------------------------------------------------------
*功		 能：获取过滤器内容
*参 数 说 明：uiCount-表示保存过滤器的组号
*返  回	 值：返回获取的过滤器内容
*说	     明：无
------------------------------------------------------------------------*/
FilterParam FilterConfig::GetFilterValue(UINT8_T uiCount)
{
	FilterParam filter;
	
	memset(&filter, 0, sizeof(filter));

	filter.pMask.ulResLen = bMask[uiCount].ulResLen;
	memcpy(filter.pMask.ucConvertData, bMask[uiCount].ucConvertData, bMask[uiCount].ulResLen);

	filter.pPattern.ulResLen = bPattern[uiCount].ulResLen;
	memcpy(filter.pPattern.ucConvertData, bPattern[uiCount].ucConvertData, bPattern[uiCount].ulResLen);

	filter.pFlowControl.ulResLen = bFlowctl[uiCount].ulResLen;
	memcpy(filter.pFlowControl.ucConvertData, bFlowctl[uiCount].ucConvertData, bFlowctl[uiCount].ulResLen);

	filter.ulTxFlags = uTxFlag[uiCount];
	filter.ulFilterType = uFilterType[uiCount];
	//filter.bCheckStatus = bCheckBox[uiCount];

	return filter;
}

/*------------------------------------------------------------------------
*功		 能：判断当前过滤器与之前过滤器，是否相同
*参 数 说 明：filterCur - 当前过滤器内容
			filterLast - 之前过滤器内容
*返  回	 值：如果比较结果相同，则返回TRUE; 否则返回FALSE
*说	     明：无
------------------------------------------------------------------------*/
BOOL FilterConfig::FilterParamCurIsEqualLast(FilterParam filterCur, FilterParam filterLast)
{
	if(!CompareBinaryData(filterCur.pMask, filterLast.pMask))	//比较2个结构体内容相同，则返回TRUE;
		return FALSE;

	if(!CompareBinaryData(filterCur.pPattern, filterLast.pPattern))
		return FALSE;

	if(!CompareBinaryData(filterCur.pFlowControl, filterLast.pFlowControl))
		return FALSE;

	if(filterCur.ulTxFlags != filterLast.ulTxFlags)
		return FALSE;

	if(filterCur.ulFilterType != filterLast.ulFilterType)
		return FALSE;
	return TRUE;
}

/*------------------------------------------------------------------------
*功		 能：判断当前过滤器是否被使用
*参 数 说 明：pFilterId表示当前过滤器id
*返  回	 值：如果过滤器被使用，则返回TRUE; 否则返回FALSE
*说	     明：无
------------------------------------------------------------------------*/
BOOL FilterConfig::GetFilterUseStatus(UINT32_T pFilterId)
{
	if(pFilterId != 0)	// 此过滤器未被设置
		return TRUE;
	return FALSE;
}

/*------------------------------------------------------------------------
*功		 能：设置过滤器pFilterId的当前值
*参 数 说 明：pFilterId表示当前过滤器id
      		pSetValue表示将要设置的值
*返  回	 值：无
*说	     明：无
------------------------------------------------------------------------*/
void FilterConfig::SetFilterUseStatus(UINT32_T *pFilterId, UINT32_T pSetValue)
{
	*pFilterId = pSetValue;
}


void FilterConfig::OnBnClickedBtnFilterconfigApply()
{
	FilterParam pFilterCurrent[FILTER_MAX_COUNT];
	CString		strProtocolName;
	UINT32_T	RetVal = 0;
	CString		strMask, strPattern, strFlowControl, strTxFlags, strFilterType;
	BOOL		bEqualStatus[FILTER_MAX_COUNT];		// 标记当前过滤器是否发生修改：0-表示修改，1-表示未修改

	memset(bEqualStatus, 1, FILTER_MAX_COUNT);
	for(UINT8_T ucLoop = 0; ucLoop < FILTER_MAX_COUNT; ucLoop++)
		memset(&pFilterCurrent[ucLoop], 0, sizeof(pFilterCurrent[ucLoop]));

	for(UINT8_T uiLoop = 0; uiLoop < FILTER_MAX_COUNT; uiLoop++)
	{
		memset(&pFilterCurrent[uiLoop], 0, sizeof(pFilterCurrent[uiLoop]));

		if(BST_CHECKED == ((CButton*)GetDlgItem(_arryEnabled[uiLoop]))->GetCheck())
		{
			GetDlgItem(_arryMaskdata[uiLoop])->GetWindowTextW(strMask);
			bMask[uiLoop] = StringToHex(strMask);

			GetDlgItem(_arryPatterndata[uiLoop])->GetWindowTextW(strPattern);
			bPattern[uiLoop] = StringToHex(strPattern);

			if((m_FilterSdk.m_Channel[uFilterCurrChannelNum-1].GetProtocolId()==ISO15765)||(m_FilterSdk.m_Channel[uFilterCurrChannelNum-1].GetProtocolId()==ISO15765_PS)||(m_FilterSdk.m_Channel[uFilterCurrChannelNum-1].GetProtocolId()==SW_ISO15765_PS))
				GetDlgItem(_arryFlowcontroldata[uiLoop])->GetWindowTextW(strFlowControl);
			bFlowctl[uiLoop] = StringToHex(strFlowControl);

			GetDlgItem(_arryTxflags[uiLoop])->GetWindowTextW(strTxFlags);
			uTxFlag[uiLoop] = _tcstol(strTxFlags, 0, 16);

			GetDlgItem(_arryFilters[uiLoop])->GetWindowTextW(strFilterType);
			uFilterType[uiLoop] = FilterStrToValue(strFilterType);

			pFilterCurrent[uiLoop] = GetFilterValue(uiLoop);	// 获取当前的Filter配置

			if(FilterParamCurIsEqualLast(pFilterCurrent[uiLoop], saveChannelfilterdata[uFilterCurrChannelNum-1]._filterParam[uiLoop]))//pFilterLast
				bEqualStatus[uiLoop] = 1;						// 表示数据未被修改
			else
				bEqualStatus[uiLoop] = 0;

			if (uFilterType[uiLoop] == J2534_FILTER_NONE)
			{
				if(saveChannelfilterdata[uFilterCurrChannelNum-1]._filterParam[uiLoop].ulFilterId != 0)//if(GetFilterUseStatus(uOutFilterId[uiLoop])!=0)
				{
					g_ThreadLock.Lock();
					RetVal = m_FilterSdk.StopFilterMessage(uFilterCurrChannelNum, uOutFilterId[uiLoop]);
					g_ThreadLock.Unlock();
					PASSTHRUMESSAGEVIEW(RetVal, L"PassThruStopFilter()-OK");
					bCheckBox[uiLoop] = FALSE;
				}
				SetFilterUseStatus(&uOutFilterId[uiLoop], 0);
				((CEdit*)GetDlgItem(_arryMaskdata[uiLoop]))->SetWindowTextW(L"");
				((CEdit*)GetDlgItem(_arryPatterndata[uiLoop]))->SetWindowTextW(L"");
				((CEdit*)GetDlgItem(_arryFlowcontroldata[uiLoop]))->SetWindowTextW(L"");
				((CButton*)GetDlgItem(_arryEnabled[uiLoop]))->SetCheck(FALSE);
				bCheckBox[uiLoop] = FALSE;

				/* 进行过滤器数据备份 */
				GetDlgItem(_arryMaskdata[uiLoop])->GetWindowTextW(strMask);
				bMask[uiLoop] = StringToHex(strMask);

				GetDlgItem(_arryPatterndata[uiLoop])->GetWindowTextW(strPattern);
				bPattern[uiLoop] = StringToHex(strPattern);

				if((m_FilterSdk.m_Channel[uFilterCurrChannelNum-1].GetProtocolId()==ISO15765)||(m_FilterSdk.m_Channel[uFilterCurrChannelNum-1].GetProtocolId()==ISO15765_PS)||(m_FilterSdk.m_Channel[uFilterCurrChannelNum-1].GetProtocolId()==SW_ISO15765_PS))
					GetDlgItem(_arryFlowcontroldata[uiLoop])->GetWindowTextW(strFlowControl);
				bFlowctl[uiLoop] = StringToHex(strFlowControl);

				saveChannelfilterdata[uFilterCurrChannelNum-1].ChannelNumber = uFilterCurrChannelNum;
				saveChannelfilterdata[uFilterCurrChannelNum-1]._filterParam[uiLoop].pMask = CopyBinaryData(bMask[uiLoop]);
				saveChannelfilterdata[uFilterCurrChannelNum-1]._filterParam[uiLoop].pPattern = CopyBinaryData(bPattern[uiLoop]);
				saveChannelfilterdata[uFilterCurrChannelNum-1]._filterParam[uiLoop].pFlowControl = CopyBinaryData(bFlowctl[uiLoop]);
				saveChannelfilterdata[uFilterCurrChannelNum-1]._filterParam[uiLoop].ulTxFlags = uTxFlag[uiLoop];
				saveChannelfilterdata[uFilterCurrChannelNum-1]._filterParam[uiLoop].ulFilterType = uFilterType[uiLoop];
				saveChannelfilterdata[uFilterCurrChannelNum-1]._filterParam[uiLoop].ulFilterId = 0;
				saveChannelfilterdata[uFilterCurrChannelNum-1]._filterParam[uiLoop].bCheckStatus = bCheckBox[uiLoop];
			}
			else
			{
				if(bEqualStatus[uiLoop] == 0)					// 修改过滤器之前，关闭当前过滤器
				{
					if(GetFilterUseStatus(saveChannelfilterdata[uFilterCurrChannelNum-1]._filterParam[uiLoop].ulFilterId))
					{
						g_ThreadLock.Lock();
						RetVal = m_FilterSdk.StopFilterMessage(uFilterCurrChannelNum, uOutFilterId[uiLoop]);
						g_ThreadLock.Unlock();
						PASSTHRUMESSAGEVIEW(RetVal, L"PassThruStopFilter()-OK");
					}
					g_ThreadLock.Lock();
					RetVal = m_FilterSdk.StartFilterMessage(uFilterCurrChannelNum, bMask[uiLoop], bPattern[uiLoop], bFlowctl[uiLoop], uTxFlag[uiLoop], uFilterType[uiLoop], &uOutFilterId[uiLoop]);
					g_ThreadLock.Unlock();
					PASSTHRUMESSAGEVIEW(RetVal, L"PassThruStartFilter()-OK");
					bCheckBox[uiLoop] = TRUE;
				}

				/* 进行过滤器数据备份 */
				saveChannelfilterdata[uFilterCurrChannelNum-1].ChannelNumber = uFilterCurrChannelNum;
				saveChannelfilterdata[uFilterCurrChannelNum-1]._filterParam[uiLoop].pMask = CopyBinaryData(bMask[uiLoop]);
				saveChannelfilterdata[uFilterCurrChannelNum-1]._filterParam[uiLoop].pPattern = CopyBinaryData(bPattern[uiLoop]);
				saveChannelfilterdata[uFilterCurrChannelNum-1]._filterParam[uiLoop].pFlowControl = CopyBinaryData(bFlowctl[uiLoop]);
				saveChannelfilterdata[uFilterCurrChannelNum-1]._filterParam[uiLoop].ulTxFlags = uTxFlag[uiLoop];
				saveChannelfilterdata[uFilterCurrChannelNum-1]._filterParam[uiLoop].ulFilterType = uFilterType[uiLoop];
				saveChannelfilterdata[uFilterCurrChannelNum-1]._filterParam[uiLoop].ulFilterId = uOutFilterId[uiLoop];
				saveChannelfilterdata[uFilterCurrChannelNum-1]._filterParam[uiLoop].bCheckStatus = bCheckBox[uiLoop];
			}
		}
		else
		{
			saveChannelfilterdata[uFilterCurrChannelNum-1]._filterParam[uiLoop].bCheckStatus = FALSE;
		}
	}
	
	SetFilterBtnIsEnable(TRUE, TRUE, TRUE, TRUE); //debug m.y
}

void FilterConfig::UILoadFilterDefaultParam()
{
	CString strMask, strPattern, strFlowctl, strFilterType, strTxFlag;

	for(UINT8_T uiLoop = 0; uiLoop < FILTER_MAX_COUNT; uiLoop++)
	{		
		strMask = HexToString(saveChannelfilterdata[uFilterCurrChannelNum-1]._filterParam[uiLoop].pMask.ucConvertData, saveChannelfilterdata[uFilterCurrChannelNum-1]._filterParam[uiLoop].pMask.ulResLen);
		GetDlgItem(_arryMaskdata[uiLoop])->SetWindowTextW(strMask);

		strPattern = HexToString(saveChannelfilterdata[uFilterCurrChannelNum-1]._filterParam[uiLoop].pPattern.ucConvertData, saveChannelfilterdata[uFilterCurrChannelNum-1]._filterParam[uiLoop].pPattern.ulResLen);
		GetDlgItem(_arryPatterndata[uiLoop])->SetWindowTextW(strPattern);

		if((m_FilterSdk.m_Channel[uFilterCurrChannelNum-1].GetProtocolId()==ISO15765)||(m_FilterSdk.m_Channel[uFilterCurrChannelNum-1].GetProtocolId()==ISO15765_PS)||(m_FilterSdk.m_Channel[uFilterCurrChannelNum-1].GetProtocolId()==SW_ISO15765_PS))
		{
			strFlowctl = HexToString(saveChannelfilterdata[uFilterCurrChannelNum-1]._filterParam[uiLoop].pFlowControl.ucConvertData, saveChannelfilterdata[uFilterCurrChannelNum-1]._filterParam[uiLoop].pFlowControl.ulResLen);
			GetDlgItem(_arryFlowcontroldata[uiLoop])->SetWindowTextW(strFlowctl);
		}
		else
		{
			GetDlgItem(_arryFlowcontroldata[uiLoop])->SetWindowTextW(L"");
			GetDlgItem(_arryFlowcontroldata[uiLoop])->EnableWindow(FALSE);
		}

		strTxFlag = L"";
		strTxFlag.Format(L"0x%.8x", (saveChannelfilterdata[uFilterCurrChannelNum-1]._filterParam[uiLoop].ulTxFlags));
		GetDlgItem(_arryTxflags[uiLoop])->SetWindowTextW(strTxFlag);

		strFilterType = FilterValueToStr(saveChannelfilterdata[uFilterCurrChannelNum-1]._filterParam[uiLoop].ulFilterType);
		int nIndex = ((CComboBox*)GetDlgItem(_arryFilters[uiLoop]))->SelectString(-1, strFilterType);
		//ASSERT(nIndex != CB_ERR);
		((CComboBox*)GetDlgItem(_arryTxflags[uiLoop]))->SetCurSel(nIndex);

		((CButton*)GetDlgItem(_arryEnabled[uiLoop]))->SetCheck(saveChannelfilterdata[uFilterCurrChannelNum-1]._filterParam[uiLoop].bCheckStatus);
	}
}

void FilterConfig::OnSysCommand(UINT nID, LPARAM lParam)
{
	// TODO: 在此添加消息处理程序代码和/或调用默认值
	if ((nID & 0xFFF0) == SC_CLOSE) 
	{
		ClearBit(g_GlobalStatus, 3);
		for(UINT8_T ujLoop = 1; ujLoop <= CONNECT_MAX_COUNT; ujLoop++)
		{
			for(UINT8_T uiLoop = 0; uiLoop < FILTER_MAX_COUNT; uiLoop++)
			{
				if (saveChannelfilterdata[ujLoop-1]._filterParam[uiLoop].bCheckStatus == TRUE)
				{
					SetBit(g_GlobalStatus,3);
					break;
				}
			}
		}
		SendMessage(WM_CLOSE, 0, 0);
	} 
	else 
	{ 
		CDialog::OnSysCommand(nID, lParam); 
	} 
}


BOOL FilterConfig::PreTranslateMessage(MSG* pMsg)
{
	if (pMsg->message == WM_CHAR)
	{
		if(!((IS_HEX(pMsg->wParam))||(pMsg->wParam == 32)||(pMsg->wParam == 8)))
			return TRUE;
	}

	if((pMsg->message == WM_KEYDOWN) && (pMsg->wParam == VK_ESCAPE))
		return TRUE;  
	else if((pMsg->message == WM_KEYDOWN) && (pMsg->wParam == VK_RETURN))
		return TRUE;
	else
		return CDialogEx::PreTranslateMessage(pMsg);
}


void FilterConfig::OnCbnSelchangeCbFilterconfigProtocol()
{
	CString strProtocolName;
	/* 获取当前通道号 */
	((CComboBox*)GetDlgItem(IDC_CB_FILTERCONFIG_PROTOCOL))->GetLBText(((CComboBox*)GetDlgItem(IDC_CB_FILTERCONFIG_PROTOCOL))->GetCurSel(), strProtocolName);
	for(UINT8_T uiLoop = 0; uiLoop < CONNECT_MAX_COUNT; uiLoop++)
	{
		if(ProtocolStrToValue(strProtocolName) == m_FilterSdk.m_Channel[uiLoop].GetProtocolId())
		{	
			uFilterCurrChannelNum = m_FilterSdk.m_Channel[uiLoop].GetChannelId();
			break;
		}
	}

	InitFilterParamMaskdata();
	InitFilterParamPatterndata();
	InitFilterParamFlowcontrol();	
	InitFilterParamTxflags();
	InitFilterParamFilterType();
	InitFilterParamIsChecked(FALSE);
	
	UILoadFilterDefaultParam();
	SetFilterAllCtlsIsEnable(TRUE);
	SetFilterBtnStatus();
}


