﻿/*******************************************************************************
 * FILE: FilterConfig.h
 * This file is part of ALLScanner VCX software.
 * COPYRIGHT (C) ALLScanner 2008-2014. 
 *		
 * DESC: J2534Tool Filter Config Parameters's Defines 
 * 
 * HISTORY:
 * Date			Author		Notes	
 * 2016-01-01		my		Create
*******************************************************************************/

#pragma once
#include "afxwin.h"
#include <vector>
#include "../J2534/J2534Dll.h"
#include "../J2534/J2534.h"
#include "../J2534/J2534Log.h"
#include "../J2534/PassThruSdk.h"
#include "../J2534/MakeLock.h"
#include "../J2534/common.h"
#include "Common/ErrorCode.h"
#include <afxstr.h>
//#include "Common/CString.h"

class FilterConfig : public CDialogEx
{
	DECLARE_DYNAMIC(FilterConfig)

public:
	FilterConfig(CWnd* pParent = NULL);   // 标准构造函数
	virtual ~FilterConfig();

	PassThruSdk m_FilterSdk;

// 对话框数据
	enum { IDD = IDD_FILTERCONFIG_DIALOG };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 支持

	DECLARE_MESSAGE_MAP()
public:
	virtual BOOL OnInitDialog();
	afx_msg void OnBnClickedBtnFilterconfigClearall();
	afx_msg void OnBnClickedBtnFilterconfigCancel();
	afx_msg void OnBnClickedBtnFilterconfigCreatepassallfilter();
	afx_msg void OnBnClickedBtnFilterconfigApply();
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);

	void InitGlobalFilterParam();
	void InitFilterParamMaskdata();
	void InitFilterParamPatterndata();
	void InitFilterParamFlowcontrol();
	void InitFilterParamFilterType();
	void InitFilterParamTxflags();
	void InitFilterParamIsChecked(BOOL res);
	
	void UILoadFilterSdkParam();			// 界面加载设备、协议信息
	void UILoadFilterDefaultParam();		// 界面加载初始化或者之前已经配置的过滤器
	void UIDisplayFilterConfig(BOOL res);
	void SetFilterAllCtlsIsEnable(BOOL res);
	void SetFilterFlowCtlIsEnable(BOOL bTemp);
	void SetFilterBtnIsEnable(BOOL bBtn1, BOOL bBtn2, BOOL bBtn3, BOOL bBtn4);
	void SetFilterBtnIsVisible(BOOL bBtn1, BOOL bBtn2, BOOL bBtn3, BOOL bBtn4);
	void SetFilterBtnStatus();
	void SetFilterUseStatus(UINT32_T *pFilterId, UINT32_T pSetValue);
	

	BOOL GetFilterUseStatus(UINT32_T pFilterId);
	BOOL FilterParamCurIsEqualLast(FilterParam filterCur, FilterParam filterLast);
	FilterParam GetFilterValue(UINT8_T uiLoop);
	virtual BOOL PreTranslateMessage(MSG* pMsg);
	afx_msg void OnCbnSelchangeCbFilterconfigProtocol();
};
