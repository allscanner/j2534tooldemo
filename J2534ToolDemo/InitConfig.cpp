// InitConfig.cpp : 实现文件
//

#include "stdafx.h"
#include "J2534ToolDemo.h"
#include "InitConfig.h"
#include "../J2534/MakeLock.h"
#include "afxdialogex.h"


// InitConfig 对话框

IMPLEMENT_DYNAMIC(InitConfig, CDialogEx)

InitConfig::InitConfig(CWnd* pParent /*=NULL*/)
	: CDialogEx(InitConfig::IDD, pParent)
{

}

InitConfig::~InitConfig()
{
}

void InitConfig::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
}


BEGIN_MESSAGE_MAP(InitConfig, CDialogEx)
	ON_CBN_SELCHANGE(IDC_CB_INIT_PROTOCOL, &InitConfig::OnCbnSelchangeCbInitProtocol)
	ON_BN_CLICKED(IDC_BTN_FIVEBAUD_EXECUTE, &InitConfig::OnBnClickedBtnFivebaudExecute)
	ON_BN_CLICKED(IDC_BTN_FASTCMD_EXECUTE, &InitConfig::OnBnClickedBtnFastcmdExecute)
	ON_WM_SYSCOMMAND()
END_MESSAGE_MAP()


// InitConfig 消息处理程序




// Global Variables
UINT32_T uInitCurrChannelNum = 0;

extern CErrorCode _err;
extern CString g_PassthruStatus;
extern ChannelInits saveChannelinitdata[CONNECT_MAX_COUNT];
extern UINT32_T g_GlobalStatus;
extern UINT32_T g_DevicesCount;
extern Mutex g_ThreadLock;

BOOL InitConfig::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	// TODO:  在此添加额外的初始化
	UILoadInitSdkParam();
	UILoadInitDefaultParam();
	SetInitFiveAndFastEnable(uInitCurrChannelNum);
	return TRUE;  // return TRUE unless you set the focus to a control
	// 异常: OCX 属性页应返回 FALSE
}

void InitConfig::UILoadInitSdkParam()
{
	/* DeviceId 显示 */
	CString	strFilterDevice = L"J2534 Tool ", strProtocolName;
	INT8_T	ucTemp = 0;
	strFilterDevice += UINT32ToDecString(g_DevicesCount);
	GetDlgItem(IDC_ED_INIT_DEVICE)->SetWindowTextW(strFilterDevice);

	/* ProtocolId 显示 */
	((CComboBox*)GetDlgItem(IDC_CB_INIT_PROTOCOL))->ResetContent();
	for(UINT8_T ucLoop = 0; ucLoop < CONNECT_MAX_COUNT; ucLoop++)
	{
		if(m_InitSdk.m_Channel[ucLoop].GetChannelExist() != FALSE)
		{
			((CComboBox*)GetDlgItem(IDC_CB_INIT_PROTOCOL))->InsertString(ucTemp,ProtocolValueToStr(m_InitSdk.m_Channel[ucLoop].GetProtocolId()));
			ucTemp++;
		}
	}
	((CComboBox*)GetDlgItem(IDC_CB_INIT_PROTOCOL))->SetCurSel(0);

	/* 获取当前通道号 */
	((CComboBox*)GetDlgItem(IDC_CB_INIT_PROTOCOL))->GetLBText(((CComboBox*)GetDlgItem(IDC_CB_INIT_PROTOCOL))->GetCurSel(), strProtocolName);
	for(UINT8_T uiLoop = 0; uiLoop < CONNECT_MAX_COUNT; uiLoop++)
	{
		if(ProtocolStrToValue(strProtocolName) == m_InitSdk.m_Channel[uiLoop].GetProtocolId())
		{	
			uInitCurrChannelNum = m_InitSdk.m_Channel[uiLoop].GetChannelId();
			break;
		}
	}
}

void InitConfig::UILoadInitDefaultParam()
{
	CString strEcuAddr, strKeyWord1, strKeyWord2, strMsgsData, strTxFlags, strRxFlags, strMsgsRev;
	strEcuAddr = UINT8ToHexStirng(saveChannelinitdata[uInitCurrChannelNum-1]._initConfig.cECUAddr, 2);
	GetDlgItem(IDC_ED_INITCONFIG_ECUADDR)->SetWindowText(strEcuAddr);
	
	if(saveChannelinitdata[uInitCurrChannelNum-1]._initConfig.bFastSetStatus != FALSE)
	{
		strKeyWord1 = UINT8ToHexStirng(saveChannelinitdata[uInitCurrChannelNum-1]._initConfig.cKeyWord1, 2);
		GetDlgItem(IDC_ED_INITCONFIG_KEYWORD1)->SetWindowText(strKeyWord1);
	}

	if(saveChannelinitdata[uInitCurrChannelNum-1]._initConfig.bFastSetStatus != FALSE)
	{
		strKeyWord2 = UINT8ToHexStirng(saveChannelinitdata[uInitCurrChannelNum-1]._initConfig.cKeyWord2, 2);
		GetDlgItem(IDC_ED_INITCONFIG_KEYWORD2)->SetWindowText(strKeyWord2);
	}

	strMsgsData = HexToString(saveChannelinitdata[uInitCurrChannelNum-1]._initConfig.bSendMsgs.ucConvertData, saveChannelinitdata[uInitCurrChannelNum-1]._initConfig.bSendMsgs.ulResLen);
	GetDlgItem(IDC_ED_INITCONFIG_MSGDATA)->SetWindowText(strMsgsData);

	strTxFlags = L"";
	strTxFlags.Format(L"0x%.8d", (saveChannelinitdata[uInitCurrChannelNum-1]._initConfig.ulTxFlag));
	GetDlgItem(IDC_ED_INITCONFIG_TXFLAGS)->SetWindowTextW(strTxFlags);

	if(saveChannelinitdata[uInitCurrChannelNum-1]._initConfig.bFastSetStatus != FALSE)
	{
		strRxFlags = L"";
		strRxFlags.Format(L"0x%.8d", (saveChannelinitdata[uInitCurrChannelNum-1]._initConfig.ulRxFlag));
		GetDlgItem(IDC_ED_INITCONFIG_RXFLAGS)->SetWindowTextW(strRxFlags);
	}

	strMsgsRev = HexToString(saveChannelinitdata[uInitCurrChannelNum-1]._initConfig.bRevMegs.ucConvertData, saveChannelinitdata[uInitCurrChannelNum-1]._initConfig.bRevMegs.ulResLen);
	GetDlgItem(IDC_ED_INITCONFIG_REVMSGDATA)->SetWindowText(strMsgsRev);
}

void InitConfig::SetInitFiveBaudEnable(BOOL bVal)
{
	UINT16_T uiAryFiveBaud[8] = {IDC_ST_INITCONFIG_FIVEBAUDINIT, IDC_ST_INITCONFIG_ECUADDR, 
		IDC_ED_INITCONFIG_ECUADDR, IDC_ST_INITCONFIG_KEYWORD1, IDC_ED_INITCONFIG_KEYWORD1, 
		IDC_ST_INITCONFIG_KEYWORD2, IDC_ED_INITCONFIG_KEYWORD2, IDC_BTN_FIVEBAUD_EXECUTE};
	for (UINT16_T uiLoop = 0; uiLoop < 8; uiLoop++)
	{
		if ((bVal==TRUE)&&(uiLoop==2))
			GetDlgItem(IDC_ED_INITCONFIG_ECUADDR)->SetWindowTextW(L"0x33");
		if ((bVal==TRUE)&&((uiLoop==4)||(uiLoop==6)))
			continue;
		GetDlgItem(uiAryFiveBaud[uiLoop])->EnableWindow(bVal);
	}
	if(bVal == FALSE)
		GetDlgItem(IDC_ED_INITCONFIG_ECUADDR)->SetWindowTextW(L"");
}


void InitConfig::SetInitFastCmdEnable(BOOL bVal)
{
	UINT16_T uiAryFastCmd[10] = {IDC_ST_INITCONFIG_FASTINIT, 
		IDC_ST_INITCONFIG_MSGDATA, IDC_ED_INITCONFIG_MSGDATA, 
		IDC_ST_INITCONFIG_TXFLAGS, IDC_ED_INITCONFIG_TXFLAGS, 
		IDC_BTN_FASTCMD_EXECUTE, 
		IDC_ST_INITCONFIG_REVMSGDATA, IDC_ED_INITCONFIG_REVMSGDATA,
		IDC_ST_INITCONFIG_RXFLAGS, IDC_ED_INITCONFIG_RXFLAGS};
	CString strTxFlags = L"", strMsgsData;

	for (UINT16_T uiLoop = 0; uiLoop < 10; uiLoop++)
	{
		if(!((uiLoop==7)||(uiLoop==9)))
			GetDlgItem(uiAryFastCmd[uiLoop])->EnableWindow(bVal);
		if ((bVal==TRUE)&&(uiLoop==2))
		{
			//GetDlgItem(IDC_ED_INITCONFIG_MSGDATA)->SetWindowTextW(L"c1 33 f1 81");	
			//GetDlgItem(IDC_ED_INITCONFIG_TXFLAGS)->SetWindowTextW(L"0x00000000");

			//strMsgsData = HexToString(saveChannelinitdata[uInitCurrChannelNum-1]._initConfig.bSendMsgs.ucConvertData, saveChannelinitdata[uInitCurrChannelNum-1]._initConfig.bSendMsgs.ulResLen);
			//GetDlgItem(IDC_ED_INITCONFIG_MSGDATA)->SetWindowText(strMsgsData);

			//strTxFlags.Format(L"0x%.8d", (saveChannelinitdata[uInitCurrChannelNum-1]._initConfig.ulTxFlag));
			//GetDlgItem(IDC_ED_INITCONFIG_TXFLAGS)->SetWindowTextW(strTxFlags);
		}
	}
	if(bVal == FALSE)
	{
		GetDlgItem(IDC_ED_INITCONFIG_MSGDATA)->SetWindowTextW(L"");
		GetDlgItem(IDC_ED_INITCONFIG_TXFLAGS)->SetWindowTextW(L"");	
	}
}

void InitConfig::SetInitFiveAndFastEnable(UINT32_T ChannelId)
{
	//UINT32_T 
	switch (m_InitSdk.m_Channel[ChannelId-1].GetProtocolId())
	{
	case J1850VPW:
	case J1850PWM:
	case ISO9141_PS:
	case CAN:
	case CAN_PS:
	case ISO15765:
	case ISO15765_PS:
	case SW_CAN_PS:
	case SW_ISO15765_PS:
	case VOLVO_CAN2:
		SetInitFiveBaudEnable(FALSE);
		SetInitFastCmdEnable(FALSE);
		break;
	case ISO9141:		
	case ISO14230:
		SetInitFiveBaudEnable(TRUE);
		SetInitFastCmdEnable(TRUE);
		break;
	//case J1939_PS:
	//	break;
	default:
		break;
	}
}

void InitConfig::OnCbnSelchangeCbInitProtocol()
{
	CString strProtocolName;
	/* 获取当前通道号 */
	((CComboBox*)GetDlgItem(IDC_CB_INIT_PROTOCOL))->GetLBText(((CComboBox*)GetDlgItem(IDC_CB_INIT_PROTOCOL))->GetCurSel(), strProtocolName);
	for(UINT8_T uiLoop = 0; uiLoop < CONNECT_MAX_COUNT; uiLoop++)
	{
		if(ProtocolStrToValue(strProtocolName) == m_InitSdk.m_Channel[uiLoop].GetProtocolId())
		{	
			uInitCurrChannelNum = m_InitSdk.m_Channel[uiLoop].GetChannelId();
			break;
		}
	}

	UILoadInitDefaultParam();
	SetInitFiveAndFastEnable(uInitCurrChannelNum);
}

void InitConfig::OnBnClickedBtnFivebaudExecute()
{
	SBYTE_ARRAY sFiveBaudInit, sFiveBaudRev;
	CString strECUAddr;
	UINT8_T uch[6];
	UINT32_T RetVal = 0;

	GetDlgItem(IDC_ED_INITCONFIG_ECUADDR)->GetWindowTextW(strECUAddr);
	sFiveBaudInit.ulNumOfBytes = 1;
	*uch = static_cast<UINT8_T>(_tcstol(strECUAddr, 0, 16));
	sFiveBaudInit.pucBytePtr = uch;

	sFiveBaudRev.pucBytePtr = (UINT8_T*)malloc(sizeof(sFiveBaudRev.pucBytePtr));
	g_ThreadLock.Lock();
	RetVal = _PassThruIoctl(m_InitSdk.m_Channel[uInitCurrChannelNum-1].GetChannelId(), FIVE_BAUD_INIT, &sFiveBaudInit, &sFiveBaudRev);
	g_ThreadLock.Unlock();
	PASSTHRUMESSAGEVIEW(RetVal, L"PassThruFiveBaudInit()-OK");

	if (sFiveBaudRev.ulNumOfBytes != 0)
	{
		GetDlgItem(IDC_ED_INITCONFIG_KEYWORD1)->SetWindowTextW(L"0x"+HexToString(&sFiveBaudRev.pucBytePtr[0],1));
		GetDlgItem(IDC_ED_INITCONFIG_KEYWORD2)->SetWindowTextW(L"0x"+HexToString(&sFiveBaudRev.pucBytePtr[1],1));
	}

	//保存init配置数据
	saveChannelinitdata[uInitCurrChannelNum-1].ChannelNumber = uInitCurrChannelNum;
	saveChannelinitdata[uInitCurrChannelNum-1]._initConfig.cECUAddr = _tcstol(strECUAddr, 0, 16);
	saveChannelinitdata[uInitCurrChannelNum-1]._initConfig.cKeyWord1 = sFiveBaudRev.pucBytePtr[0];
	saveChannelinitdata[uInitCurrChannelNum-1]._initConfig.cKeyWord2 = sFiveBaudRev.pucBytePtr[1];
	saveChannelinitdata[uInitCurrChannelNum-1]._initConfig.bFiveSetStatus = TRUE;

	free(sFiveBaudRev.pucBytePtr);
	sFiveBaudRev.pucBytePtr = NULL;
}


void InitConfig::OnBnClickedBtnFastcmdExecute()
{
	PASSTHRU_MSG pFastSend, pFastRev;
	BinaryData bFastSend;
	CString strFastSend, strTxFlags, strRxFlags, strRevMsgs;
	UINT32_T RetVal = 0;

	memset(pFastRev.ucData, 0, PASSTHRU_MSG_DATA_SIZE);

	GetDlgItem(IDC_ED_INITCONFIG_MSGDATA)->GetWindowTextW(strFastSend);
	GetDlgItem(IDC_ED_INITCONFIG_TXFLAGS)->GetWindowTextW(strTxFlags);

	bFastSend = StringToHex(strFastSend);
	pFastSend.ulProtocolID = m_InitSdk.m_Channel[uInitCurrChannelNum].GetProtocolId();
	pFastSend.ulRxStatus = 0;
	pFastSend.ulTxFlags = _tcstol(strTxFlags, 0, 16);
	//pFastSend.ulTimeStamp = 0;
	pFastSend.ulDataSize = bFastSend.ulResLen;
	pFastSend.ulExtraDataIndex = bFastSend.ulResLen;
	memcpy(pFastSend.ucData, bFastSend.ucConvertData, bFastSend.ulResLen);

	g_ThreadLock.Lock();
	RetVal = _PassThruIoctl(m_InitSdk.m_Channel[uInitCurrChannelNum-1].GetChannelId(), FAST_INIT, &pFastSend, &pFastRev);
	g_ThreadLock.Unlock();
	PASSTHRUMESSAGEVIEW(RetVal, L"PassThruFastInit()-OK");

	strRxFlags = L"";
	strRxFlags.Format(L"0x%.8d", pFastRev.ulRxStatus);
	GetDlgItem(IDC_ED_INITCONFIG_RXFLAGS)->SetWindowText(strRxFlags);

	strRevMsgs = HexToString(pFastRev.ucData, pFastRev.ulDataSize);
	GetDlgItem(IDC_ED_INITCONFIG_REVMSGDATA)->SetWindowText(strRevMsgs);

	saveChannelinitdata[uInitCurrChannelNum-1].ChannelNumber = uInitCurrChannelNum;

	saveChannelinitdata[uInitCurrChannelNum-1]._initConfig.bSendMsgs.ulResLen = bFastSend.ulResLen;
	memcpy(saveChannelinitdata[uInitCurrChannelNum-1]._initConfig.bSendMsgs.ucConvertData, &bFastSend.ucConvertData, bFastSend.ulResLen);

	saveChannelinitdata[uInitCurrChannelNum-1]._initConfig.ulTxFlag = _tcstol(strTxFlags, 0, 16);

	saveChannelinitdata[uInitCurrChannelNum-1]._initConfig.bRevMegs.ulResLen = pFastRev.ulDataSize;
	memcpy(saveChannelinitdata[uInitCurrChannelNum-1]._initConfig.bRevMegs.ucConvertData, pFastRev.ucData, pFastRev.ulDataSize);
	
	saveChannelinitdata[uInitCurrChannelNum-1]._initConfig.ulRxFlag = pFastRev.ulRxStatus;
	saveChannelinitdata[uInitCurrChannelNum-1]._initConfig.bFastSetStatus = TRUE;
}


void InitConfig::OnSysCommand(UINT nID, LPARAM lParam)
{
	// TODO: 在此添加消息处理程序代码和/或调用默认值
	if ((nID & 0xFFF0) == SC_CLOSE) 
	{
		for(UINT8_T ucLoop = 0; ucLoop < CONNECT_MAX_COUNT; ucLoop++)
		{
			if((saveChannelinitdata[ucLoop]._initConfig.bFiveSetStatus == TRUE)||
				(saveChannelinitdata[ucLoop]._initConfig.bFastSetStatus == TRUE))
			{
				SetBit(g_GlobalStatus, 8);
				break;
			}
		}
		SendMessage(WM_CLOSE, 0, 0);
	} 
	else 
	{ 
		CDialog::OnSysCommand(nID, lParam); 
	} 
}


BOOL InitConfig::PreTranslateMessage(MSG* pMsg)
{
	// TODO: 在此添加专用代码和/或调用基类
	if((pMsg->message == WM_KEYDOWN) && (pMsg->wParam == VK_ESCAPE))
		return TRUE;  
	else if((pMsg->message == WM_KEYDOWN) && (pMsg->wParam == VK_RETURN))
		return TRUE;
	else
		return CDialogEx::PreTranslateMessage(pMsg);
}
