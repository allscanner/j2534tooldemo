#pragma once
#include "../J2534/J2534Dll.h"
#include "../J2534/J2534.h"
#include "../J2534/J2534Log.h"
#include "../J2534/PassThruSdk.h"
#include "Common/ErrorCode.h"

// InitConfig 对话框

class InitConfig : public CDialogEx
{
	DECLARE_DYNAMIC(InitConfig)

public:
	InitConfig(CWnd* pParent = NULL);   // 标准构造函数
	virtual ~InitConfig();

	PassThruSdk m_InitSdk;

// 对话框数据
	enum { IDD = IDD_INITCONFIG_DIALOG };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 支持

	DECLARE_MESSAGE_MAP()

	
public:
	virtual BOOL OnInitDialog();

	void UILoadInitSdkParam();
	void UILoadInitDefaultParam();
	void SetInitFiveBaudEnable(BOOL bVal);
	void SetInitFastCmdEnable(BOOL bVal);
	void SetInitFiveAndFastEnable(UINT32_T ChannelId);
	afx_msg void OnCbnSelchangeCbInitProtocol();
	afx_msg void OnBnClickedBtnFivebaudExecute();
	afx_msg void OnBnClickedBtnFastcmdExecute();
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	virtual BOOL PreTranslateMessage(MSG* pMsg);
};
