
// J2534ToolDemo.h : PROJECT_NAME 应用程序的主头文件
//

#pragma once

#ifndef __AFXWIN_H__
	#error "在包含此文件之前包含“stdafx.h”以生成 PCH 文件"
#endif

#include "resource.h"		// 主符号


// CJ2534ToolDemoApp:
// 有关此类的实现，请参阅 J2534ToolDemo.cpp
//

class CJ2534ToolDemoApp : public CWinApp
{
public:
	CJ2534ToolDemoApp();

// 重写
public:
	virtual BOOL InitInstance();

// 实现

	DECLARE_MESSAGE_MAP()
};

extern CJ2534ToolDemoApp theApp;