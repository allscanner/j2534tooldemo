/*******************************************************************************
 * FILE: J2534ToolDemoDlg.cpp
 * This file is part of ALLScanner VCX software.
 * COPYRIGHT (C) ALLScanner 2008-2014. 
 *		
 * DESC: J2534ToolDemoDlg declaration 
 * 
 * HISTORY:
 * Date			Author		Notes	
 * 2016-07-01		my		Create
*******************************************************************************/
// J2534ToolDemoDlg.cpp : 实现文件
//

#include "stdafx.h"
#include "J2534ToolDemo.h"
#include "J2534ToolDemoDlg.h"
#include "afxdialogex.h"


#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// 用于应用程序“关于”菜单项的 CAboutDlg 对话框

class CAboutDlg : public CDialogEx
{
public:
	CAboutDlg();

// 对话框数据
	enum { IDD = IDD_ABOUTBOX };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 支持

// 实现
protected:
	DECLARE_MESSAGE_MAP()
};

CAboutDlg::CAboutDlg() : CDialogEx(CAboutDlg::IDD)
{
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialogEx)
END_MESSAGE_MAP()


// CJ2534ToolDemoDlg 对话框

CJ2534ToolDemoDlg::CJ2534ToolDemoDlg(CWnd* pParent /*=NULL*/)
	: CDialogEx(CJ2534ToolDemoDlg::IDD, pParent)
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
}

void CJ2534ToolDemoDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_LIST_MESSAGE_VIEW, m_ListMessage);
	DDX_Control(pDX, IDC_BTN_LOADDLL, m_BtnLoadDll);
	DDX_Control(pDX, IDC_BTN_RECVMESSAGE, m_BtnRecvMsgs);
	DDX_Control(pDX, IDC_BTN_STOPRECVMESSAGE, m_BtnStopRecvMsgs);
	DDX_Control(pDX, IDC_BTN_CLEARMESSAGE, m_BtnClear);
	DDX_Control(pDX, IDC_BTN_OPENDEVICE, m_BtnOpenDevice);
	DDX_Control(pDX, IDC_BTN_CLOSEDEVICE, m_BtnCloseDevice);
}

BEGIN_MESSAGE_MAP(CJ2534ToolDemoDlg, CDialogEx)
	ON_WM_SYSCOMMAND()
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_WM_TIMER()
	ON_WM_CTLCOLOR()
	ON_NOTIFY(LVN_ITEMCHANGED, IDC_LIST_MESSAGE_VIEW, &CJ2534ToolDemoDlg::OnItemchangedListMessageView)
	ON_NOTIFY(NM_RCLICK, IDC_LIST_MESSAGE_VIEW, &CJ2534ToolDemoDlg::OnRclickListMessageView)
	ON_CBN_SELCHANGE(IDC_CB_PROTOCOLNAME, &CJ2534ToolDemoDlg::OnCbnSelchangeCbProtocolname)
	ON_CBN_SELCHANGE(IDC_CB_INTERFACENAME, &CJ2534ToolDemoDlg::OnCbnSelchangeCbInterfacename)
	ON_CBN_SELCHANGE(IDC_CB_CHANNELIDVALUE, &CJ2534ToolDemoDlg::OnCbnSelchangeCbChannelidvalue)
	ON_CBN_SELCHANGE(IDC_CB_SENDPROTOCOLNAME, &CJ2534ToolDemoDlg::OnCbnSelchangeCbSendprotocolname)

	ON_BN_CLICKED(IDC_BTN_LOADDLL, &CJ2534ToolDemoDlg::OnBnClickedBtnLoaddll)
	ON_BN_CLICKED(IDC_BTN_UNLOADDLL, &CJ2534ToolDemoDlg::OnBnClickedBtnUnloaddll)
	ON_BN_CLICKED(IDC_BTN_OPENDEVICE, &CJ2534ToolDemoDlg::OnBnClickedBtnOpendevice)
	ON_BN_CLICKED(IDC_BTN_CLOSEDEVICE, &CJ2534ToolDemoDlg::OnBnClickedBtnClosedevice)
	ON_BN_CLICKED(IDC_BTN_CONNECTPROTOCOL, &CJ2534ToolDemoDlg::OnBnClickedBtnConnectprotocol)
	ON_BN_CLICKED(IDC_BTN_DISCONNECTPROTOCOL, &CJ2534ToolDemoDlg::OnBnClickedBtnDisconnectprotocol)
	ON_BN_CLICKED(IDC_BTN_SENDMESSAGE, &CJ2534ToolDemoDlg::OnBnClickedBtnSendmessage)
	ON_BN_CLICKED(IDC_BTN_RECVMESSAGE, &CJ2534ToolDemoDlg::OnBnClickedBtnRecvmessage)
	ON_BN_CLICKED(IDC_BTN_STOPRECVMESSAGE, &CJ2534ToolDemoDlg::OnBnClickedBtnStoprecvmessage)
	ON_BN_CLICKED(IDC_BTN_CLEARMESSAGE, &CJ2534ToolDemoDlg::OnBnClickedBtnClearmessage)
	ON_WM_SIZE()
	ON_BN_CLICKED(IDC_BTN_SETINIT, &CJ2534ToolDemoDlg::OnBnClickedBtnSetinit)
	ON_BN_CLICKED(IDC_BTN_SETFILTER, &CJ2534ToolDemoDlg::OnBnClickedBtnSetfilter)
	ON_BN_CLICKED(IDC_BTN_SETPERIODIC, &CJ2534ToolDemoDlg::OnBnClickedBtnSetperiodic)
	ON_BN_CLICKED(IDC_BTN_SETPARAMCONFIG, &CJ2534ToolDemoDlg::OnBnClickedBtnSetparamconfig)
END_MESSAGE_MAP()


// CJ2534ToolDemoDlg 消息处理程序

/* Global Variables */

/************************************************************
	*功		 能：发送、接收数据线程句柄
	*参 数 说 明：无
	*返  回	 值：无
	*说	     明：无
************************************************************/
HANDLE	hRevMsgThread = NULL;				// 声明接收数据线程句柄
HANDLE	hSendMsgThread = NULL;

/************************************************************
	*功		 能：Passthru状态显示（错误提示）
	*参 数 说 明：无
	*返  回	 值：无
	*说	     明：无
************************************************************/
CErrorCode _err;

//UINT32_T g_CurrentTimeStamp = 0, g_LastTimeStamp = 0, g_TimeStamp = 0;

/************************************************************
	*功		 能：Passthru操作，状态内容
	*参 数 说 明：无
	*返  回	 值：无
	*说	     明：无
************************************************************/
CString g_PassthruStatus = L"";

/************************************************************
	*功		 能：保存各通道过滤器配置项内容
	*参 数 说 明：无
	*返  回	 值：无
	*说	     明：无
************************************************************/
//FilterParam savefilterdata[FILTER_MAX_COUNT];
ChannelFilters saveChannelfilterdata[CONNECT_MAX_COUNT];

/*------------------------------------------------------------------------
	*功		 能：保存各通道定时器配置项内容
	*参 数 说 明：无
	*返  回	 值：无
	*说	     明：无
------------------------------------------------------------------------*/
//PeriodicParam saveperiodicdata[PERIODIC_MAX_COUNT];
ChannelPeriodics saveChannelperiodicdata[CONNECT_MAX_COUNT];

/************************************************************
	*功		 能：保存各通道Ioctl配置项内容
	*参 数 说 明：无
	*返  回	 值：无
	*说	     明：无
************************************************************/
//IoctlParam saveioctldata;
ChannelIoctlParams saveChannelioctldata[CONNECT_MAX_COUNT];

/************************************************************
	*功		 能：保存各通道FastCmd和FiveBaud配置项内容
	*参 数 说 明：无
	*返  回	 值：无
	*说	     明：无
************************************************************/
ChannelInits saveChannelinitdata[CONNECT_MAX_COUNT];

/************************************************************
	*功		 能：保存各通道SendMessage内容
	*参 数 说 明：无
	*返  回	 值：无
	*说	     明：无
************************************************************/
ChannelSendMessages	saveChannelSendMsgdata[CONNECT_MAX_COUNT];

/************************************************************
	*功		 能：设备总数记录
	*参 数 说 明：无
	*返  回	 值：无
	*说	     明：无
************************************************************/
UINT32_T	g_DevicesCount = 0;

/************************************************************
	*功		 能：通道总数记录
	*参 数 说 明：无
	*返  回	 值：无
	*说	     明：无
************************************************************/
UINT32_T	g_ChannelsCount = 0;

/************************************************************
	*功		 能：通道最大总数记录
	*参 数 说 明：无
	*返  回	 值：无
	*说	     明：无
************************************************************/
UINT32_T	g_ChannelsMax = 0;

/************************************************************
	*功		 能：List Control控件中显示行的数量
	*参 数 说 明：无
	*返  回	 值：无
	*说	     明：无
************************************************************/
UINT32_T	uListMessageCount = 0;

/************************************************************
	*功		 能：保存通道中消息内容
	*参 数 说 明：无
	*返  回	 值：无
	*说	     明：无
************************************************************/
ConnectInfo g_ConnectInfo[CONNECT_MAX_COUNT];

/************************************************************
	*功		 能：保存通道中消息内容
	*参 数 说 明：无
	*返  回	 值：无
	*说	     明：无
************************************************************/
ChannelRecieveInfo saveChannelRecvInfo[CONNECT_MAX_COUNT];

/************************************************************
	*功		 能：保存注册表信息
	*参 数 说 明：无
	*返  回	 值：无
	*说	     明：无
************************************************************/
PassthruRegeditDevice regDeviceInfo;

/************************************************************
	*global flag of every status	
	*0000 0000 0000 0000 
	*第0位：0-表示Dll驱动加载失败；1-表示Dll驱动加载成功
	*第1位：0-表示打开设备失败；1-表示打开设备成功
	*第2位：0-表示设备连接失败；1-表示设备连接成功
	*第3位：0-表示过滤器未设置；1-表示过滤器已设置
	*第4位：0-表示定时器未设置；1-表示定时器已设置
	*第5位：0-表示Ioctl配置参数未设置；1-表示Ioctl配置参数已设置
	*第6位：0-表示SendMessage之前；1-表示已经开始发送消息
	*第7位：0-表示RecvMessage之前；1-表示已经开始接收中
	*第8位：0-表示Five Baud和Fast Cmd不需要设置；1-表示已经设置
	*其 他：预留
************************************************************/
UINT32_T g_GlobalStatus = 0;

/************************************************************
	*功		 能：发送、接收消息的互斥锁
	*参 数 说 明：无
	*返  回	 值：无
	*说	     明：无
************************************************************/
Mutex g_ThreadLock;

deque<ListctrlContent> stuContentDeque;


void CJ2534ToolDemoDlg::InitListMessageView()
{
	CString arrListTerm[6] = {L"TimeStamp", L"Sender", L"ChannelId", L"NetWork ID", L"RxStatus", L"Data Bytes"};
	UINT16_T arrListWidth[6] = {90, 85, 92, 90, 92, 500};

	m_ListMessageRow = 0;
	m_ListMessageColumn = 0;

	for (UINT8_T iLoop = 0; iLoop < 6; iLoop++)
	{
		m_ListMessage.InsertColumn(iLoop, arrListTerm[iLoop]);
		m_ListMessage.SetColumnWidth(iLoop, arrListWidth[iLoop]);
	}
	m_ListMessage.SetExtendedStyle(LVS_EX_FULLROWSELECT|LVS_EX_GRIDLINES);//设置扩展格
}

void CJ2534ToolDemoDlg::InitJ2345Interface()
{
	// Init Struct
	regDeviceInfo.ulDeviceCounts = 0;
	for(UINT8_T uiLoop = 0; uiLoop < MAX_DEVICE_NUM; uiLoop++)
		memset(&regDeviceInfo.regDevice[uiLoop], L'', sizeof(regDeviceInfo.regDevice[uiLoop]));

	GetRegeditPassthruSupport_04_04_Path();

	for(UINT8_T iLoop = 0; iLoop < regDeviceInfo.ulDeviceCounts; iLoop++)
		((CComboBox*)GetDlgItem(IDC_CB_INTERFACENAME))->InsertString(iLoop, regDeviceInfo.regDevice[iLoop].szRegeditName);	

	((CEdit*)GetDlgItem(IDC_EDIT_PATHOFDLL))->SetWindowTextW(regDeviceInfo.regDevice[0].szRegeditPath);
	((CComboBox*)GetDlgItem(IDC_CB_INTERFACENAME))->SetCurSel(0);//设置默认显示的是第一个"1"，自己设置
}

void CJ2534ToolDemoDlg::InitUIStatusBar()
{
	// Status Bar加载到对话框
	m_Statusbar.Create(this);	// CMFCStatusBar类，不能实现Create（）方法

	UINT16_T parts[6], widths[6];
	for(UINT8_T uiLoop=0; uiLoop<6; uiLoop++)
		parts[uiLoop]=1000+uiLoop;

	m_Statusbar.SetIndicators(parts, 6);

	m_Statusbar.SetPaneText(0, L"提示：");
	m_Statusbar.SetPaneText(2, L"Passthru操作状态：");
	m_Statusbar.SetPaneText(4, L"当前时间：");

	//m_Statusbar.SetPaneTextColor(3, RGB(0,64,64));	// CMFCStatusBar类才具有该方法

	widths[0] = 40;
	widths[1] = 240;
	widths[2] = 140;
	widths[3] = 400;
	widths[4] = 70;
	widths[5] = 32765;

	for(UINT8_T uiLoop=0; uiLoop<6; uiLoop++)
		m_Statusbar.SetPaneInfo(uiLoop, 1000+uiLoop, SBPS_NORMAL, widths[uiLoop]);

	RepositionBars(AFX_IDW_CONTROLBAR_FIRST, AFX_IDW_CONTROLBAR_LAST, 0);
}

void CJ2534ToolDemoDlg::InitUIConfigView()
{
	SetTextColor(RGB(255,0,0), IDC_ED_INITSTATUS, L"Status None");
	SetTextFont(15, FW_BOLD, FALSE, IDC_ED_INITSTATUS);

	SetTextColor(RGB(255,0,0), IDC_ED_FILTERSTATUS, L"Status None");
	SetTextFont(15, FW_BOLD, FALSE, IDC_ED_FILTERSTATUS);

	SetTextColor(RGB(255,0,0), IDC_ED_PERIODICSTATUS, L"Status None");
	SetTextFont(15, FW_BOLD, FALSE, IDC_ED_PERIODICSTATUS);

	SetTextColor(RGB(255,0,0), IDC_ED_PARAMCONFIGSTATUS, L"Status None");
	SetTextFont(15, FW_BOLD, FALSE, IDC_ED_PARAMCONFIGSTATUS);
}

void CJ2534ToolDemoDlg::InitUIControls()
{
	/* 加载菜单栏 */
	m_Menu.LoadMenuW(IDR_MENU_FILE);
	SetMenu(&m_Menu);

	/* 状态栏加载 */
	InitUIStatusBar();

	/* 初始化配置参数状态显示 */
	InitUIConfigView();

	/* 控件使能 */
	SetJ2534ToolDemoIsLoadDllBtnEnable(TRUE,FALSE);
	SetJ2534ToolDemoOpenDeviceEnable(FALSE);
	SetJ2534ToolDemoConnectDeviceEnable(FALSE);
	SetJ2534ToolDemoConfigParamEnable(FALSE);
	SetJ2534ToolDemoSendMsgDataEnable(FALSE);
	SetJ2534ToolDemoRecvProtocolCkEnable(FALSE);
}

void CJ2534ToolDemoDlg::ClearCurrChannelConfigParam(UINT32_T ChannelId)
{
	/* 初始化Filter配置参数 */
	saveChannelfilterdata[ChannelId-1].ChannelNumber = 0;
	for(UINT8_T ujLoop = 0; ujLoop < FILTER_MAX_COUNT; ujLoop++)
	{
		memset(&saveChannelfilterdata[ChannelId-1]._filterParam[ujLoop].pMask, 0, sizeof(saveChannelfilterdata[ChannelId-1]._filterParam[ujLoop].pMask));
		memset(&saveChannelfilterdata[ChannelId-1]._filterParam[ujLoop].pPattern, 0, sizeof(saveChannelfilterdata[ChannelId-1]._filterParam[ujLoop].pPattern));
		memset(&saveChannelfilterdata[ChannelId-1]._filterParam[ujLoop].pFlowControl, 0, sizeof(saveChannelfilterdata[ChannelId-1]._filterParam[ujLoop].pFlowControl));
		saveChannelfilterdata[ChannelId-1]._filterParam[ujLoop].ulTxFlags = 0;
		saveChannelfilterdata[ChannelId-1]._filterParam[ujLoop].ulFilterType = 0;
		saveChannelfilterdata[ChannelId-1]._filterParam[ujLoop].ulFilterId = 0;
		saveChannelfilterdata[ChannelId-1]._filterParam[ujLoop].bCheckStatus = FALSE;
	}

	/* 初始化Periodic配置参数 */
	saveChannelperiodicdata[ChannelId-1].ChannelNumber = 0;
	for(UINT8_T ujLoop = 0; ujLoop < PERIODIC_MAX_COUNT; ujLoop++)
	{
		memset(&saveChannelperiodicdata[ChannelId-1]._periodicParam[ujLoop].pMessage, 0, sizeof(saveChannelperiodicdata[ChannelId-1]._periodicParam[ujLoop].pMessage));
		saveChannelperiodicdata[ChannelId-1]._periodicParam[ujLoop].ulTxFlags = 0;
		saveChannelperiodicdata[ChannelId-1]._periodicParam[ujLoop].ulIntervel = 100;
		saveChannelperiodicdata[ChannelId-1]._periodicParam[ujLoop].bCheckStatus = FALSE;
	}


	/* 初始化Ioctl配置参数 */
	saveChannelioctldata[ChannelId-1].ChannelNumber = 0;
	saveChannelioctldata[ChannelId-1]._ioctlParam.ulDataRate = 0;
	saveChannelioctldata[ChannelId-1]._ioctlParam.bLookBack = FALSE;
	saveChannelioctldata[ChannelId-1]._ioctlParam.ucNodeAddress = 0;
	saveChannelioctldata[ChannelId-1]._ioctlParam.ucNetworkLine = 0;
	saveChannelioctldata[ChannelId-1]._ioctlParam.uiP1Max = 0;
	saveChannelioctldata[ChannelId-1]._ioctlParam.uiP3Min = 0;
	saveChannelioctldata[ChannelId-1]._ioctlParam.uiP4Min = 0;
	saveChannelioctldata[ChannelId-1]._ioctlParam.uiW0 = 0;
	saveChannelioctldata[ChannelId-1]._ioctlParam.uiW1 = 0;
	saveChannelioctldata[ChannelId-1]._ioctlParam.uiW2 = 0;
	saveChannelioctldata[ChannelId-1]._ioctlParam.uiW3 = 0;
	saveChannelioctldata[ChannelId-1]._ioctlParam.uiW4 = 0;
	saveChannelioctldata[ChannelId-1]._ioctlParam.uiW5 = 0;
	saveChannelioctldata[ChannelId-1]._ioctlParam.uiTidle = 0;
	saveChannelioctldata[ChannelId-1]._ioctlParam.uiTinil = 0;
	saveChannelioctldata[ChannelId-1]._ioctlParam.uiTwup = 0;
	saveChannelioctldata[ChannelId-1]._ioctlParam.ucParity = 0;
	saveChannelioctldata[ChannelId-1]._ioctlParam.bDataBits = FALSE;	// 0-8个数据位； 1-7个数据位
	saveChannelioctldata[ChannelId-1]._ioctlParam.ucFiveBaudMod = 0;
	saveChannelioctldata[ChannelId-1]._ioctlParam.ucBitSamplePoint = 0;
	saveChannelioctldata[ChannelId-1]._ioctlParam.ucSyncJumpWidth = 0;
	saveChannelioctldata[ChannelId-1]._ioctlParam.ucIso15765BS = 0;
	saveChannelioctldata[ChannelId-1]._ioctlParam.ucIso15765Stmin = 0;
	saveChannelioctldata[ChannelId-1]._ioctlParam.uiBsTx = 0;
	saveChannelioctldata[ChannelId-1]._ioctlParam.uiStminTx = 0;
	saveChannelioctldata[ChannelId-1]._ioctlParam.ucIso15765WftMax = 0;
	saveChannelioctldata[ChannelId-1]._ioctlParam.ulIso15765Simultaneous = 0;
	saveChannelioctldata[ChannelId-1]._ioctlParam.ulDtIso15765PadByte = 0;
	saveChannelioctldata[ChannelId-1]._ioctlParam.ulCanMixedFormat = 0;
	saveChannelioctldata[ChannelId-1]._ioctlParam.ulSwCanHsDataRate = 0;
	saveChannelioctldata[ChannelId-1]._ioctlParam.ulSwCanSpdchgEnable = 0;
	saveChannelioctldata[ChannelId-1]._ioctlParam.ulSwCanResSwitch = 0;
	saveChannelioctldata[ChannelId-1]._ioctlParam.uiJ1962Pins = 0;
	saveChannelioctldata[ChannelId-1]._ioctlParam.bIsSetStatus = FALSE;
	saveChannelioctldata[ChannelId-1]._ioctlParam.bIsSetInit = FALSE;

	/* 初始化FastCmd和FiveBaud配置参数 */
	UINT8_T uSend[MAX_PATH] = {0xc1, 0x33, 0xf1, 0x81};
	saveChannelinitdata[ChannelId-1].ChannelNumber = 0;
	saveChannelinitdata[ChannelId-1]._initConfig.cECUAddr = 0;
	saveChannelinitdata[ChannelId-1]._initConfig.cKeyWord1 = 0;
	saveChannelinitdata[ChannelId-1]._initConfig.cKeyWord2 = 0;
	saveChannelinitdata[ChannelId-1]._initConfig.bSendMsgs.ulResLen = 4;
	memcpy(&(saveChannelinitdata[ChannelId-1]._initConfig.bSendMsgs.ucConvertData), &uSend, MAX_PATH);
	saveChannelinitdata[ChannelId-1]._initConfig.ulTxFlag = 0;
	saveChannelinitdata[ChannelId-1]._initConfig.bRevMegs.ulResLen = 0;
	memset(&(saveChannelinitdata[ChannelId-1]._initConfig.bRevMegs.ucConvertData), 0, MAX_PATH);
	saveChannelinitdata[ChannelId-1]._initConfig.ulRxFlag = 0;
	saveChannelinitdata[ChannelId-1]._initConfig.bInitStatus = TRUE;
	saveChannelinitdata[ChannelId-1]._initConfig.bFiveSetStatus = FALSE;
	saveChannelinitdata[ChannelId-1]._initConfig.bFastSetStatus = FALSE;

	/* 初始化SendMessage配置参数 */
	saveChannelSendMsgdata[ChannelId-1].ChannelNumber = 0;
	saveChannelSendMsgdata[ChannelId-1]._sendMessage.ulProtocolId = 0;
	saveChannelSendMsgdata[ChannelId-1]._sendMessage.ulTxFlag = 0;
	saveChannelSendMsgdata[ChannelId-1]._sendMessage.ulTimeOut = 100;
	memset(&saveChannelSendMsgdata[ChannelId-1]._sendMessage.bSendMegs, 0, MAX_PATH);

	/* 初始化RecvMessage配置参数 */
	saveChannelRecvInfo[ChannelId-1].ChannelNumber = 0;
	saveChannelRecvInfo[ChannelId-1].uProtocolValue = 0;
	saveChannelRecvInfo[ChannelId-1].bStatus = FALSE;
}

void CJ2534ToolDemoDlg::SetDefaultConfigParam()
{
	for(UINT8_T uiLoop = 1; uiLoop <= CONNECT_MAX_COUNT; uiLoop++)
		ClearCurrChannelConfigParam(uiLoop);
}

void CJ2534ToolDemoDlg::LoadButtonPicture()
{
	m_BtnLoadDll.SetImage(IDB_BTN1_32, IDB_BTN1_HOT_32);
	m_BtnRecvMsgs.SetImage(IDB_PLAY_HOT);
	m_BtnStopRecvMsgs.SetImage(IDB_STOP_HOT);
	m_BtnClear.SetImage(IDB_CLEAR);
	m_BtnOpenDevice.SetImage(IDB_OPEN, IDB_UNLOCK);
	m_BtnCloseDevice.SetImage(IDB_UNLOCK, IDB_LOCKED);
}

BOOL CJ2534ToolDemoDlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	// 将“关于...”菜单项添加到系统菜单中。

	// IDM_ABOUTBOX 必须在系统命令范围内。
	ASSERT((IDM_ABOUTBOX & 0xFFF0) == IDM_ABOUTBOX);
	ASSERT(IDM_ABOUTBOX < 0xF000);

	CMenu* pSysMenu = GetSystemMenu(FALSE);
	if (pSysMenu != NULL)
	{
		BOOL bNameValid;
		CString strAboutMenu;
		bNameValid = strAboutMenu.LoadString(IDS_ABOUTBOX);
		ASSERT(bNameValid);
		if (!strAboutMenu.IsEmpty())
		{
			pSysMenu->AppendMenu(MF_SEPARATOR);
			pSysMenu->AppendMenu(MF_STRING, IDM_ABOUTBOX, strAboutMenu);
		}
	}

	// 设置此对话框的图标。当应用程序主窗口不是对话框时，框架将自动
	//  执行此操作
	SetIcon(m_hIcon, TRUE);			// 设置大图标
	SetIcon(m_hIcon, FALSE);		// 设置小图标

	// TODO: 在此添加额外的初始化代码
	GetClientRect(&m_rect);

	LoadButtonPicture();

	/* 设置定时器 */
	SetTimer(9, 500, NULL);
	SetTimer(1, 20, NULL);

	/*初始化界面控件*/
	InitUIControls();

	/*初始化ListControl控件*/
	InitListMessageView();

	/*动态获取驱动列表*/	
	InitJ2345Interface();

	return TRUE;  // 除非将焦点设置到控件，否则返回 TRUE
}

void CJ2534ToolDemoDlg::OnSysCommand(UINT nID, LPARAM lParam)
{
	if ((nID & 0xFFF0) == IDM_ABOUTBOX)
	{
		CAboutDlg dlgAbout;
		dlgAbout.DoModal();
	}
	else
	{
		CDialogEx::OnSysCommand(nID, lParam);
	}
}

// 如果向对话框添加最小化按钮，则需要下面的代码
//  来绘制该图标。对于使用文档/视图模型的 MFC 应用程序，
//  这将由框架自动完成。

void CJ2534ToolDemoDlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // 用于绘制的设备上下文

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// 使图标在工作区矩形中居中
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// 绘制图标
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialogEx::OnPaint();
	}
}

//当用户拖动最小化窗口时系统调用此函数取得光标
//显示。
HCURSOR CJ2534ToolDemoDlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}



DWORD __stdcall ThreadSendMsgFun(LPVOID IParams)
{
	CJ2534ToolDemoDlg	*pDlg = (CJ2534ToolDemoDlg *)IParams;

	ListctrlContent sendlistContent;
	CString		strSendCmd, strSendTimeout, strSendTxflags, strSendRxflags, strSendRate;
	CString		strSendInterval, strSendFrequency;
	UINT32_T	ulTimeout = 0, ulTxflags = 0, uRxMsgStatus = 0, ulRate = 0;
	UINT32_T	ulInterval, ulFrequency; 
	UINT32_T	RetVal = 0;
	CString		minutes, seconds, milliseconds, TimeStamp;
	SYSTEMTIME	time;

	if (pDlg->J2534ToolDemoSdk.GetDeviceId()==0)
		return 1;

	UINT32_T uCurrSendChannelId = 0;
	CString strProtocolName;
	/* 获取当前通道号 */
	((CComboBox*)(pDlg->GetDlgItem(IDC_CB_SENDPROTOCOLNAME)))->GetLBText(((CComboBox*)(pDlg->GetDlgItem(IDC_CB_SENDPROTOCOLNAME)))->GetCurSel(), strProtocolName);
	for(UINT8_T uiLoop = 0; uiLoop < CONNECT_MAX_COUNT; uiLoop++)
	{
		if(ProtocolStrToValue(strProtocolName) == pDlg->J2534ToolDemoSdk.m_Channel[uiLoop].GetProtocolId())
		{	
			uCurrSendChannelId = pDlg->J2534ToolDemoSdk.m_Channel[uiLoop].GetChannelId();
			break;
		}
	}

	pDlg->GetDlgItem(IDC_ED_SENDDATATIMEOUT)->GetWindowTextW(strSendTimeout);
	ulTimeout = _tcstol(strSendTimeout, 0, 10);

	pDlg->GetDlgItem(IDC_ED_SENDMESSAGES)->GetWindowTextW(strSendCmd);

	pDlg->GetDlgItem(IDC_ED_SENDTXFLAGS)->GetWindowTextW(strSendTxflags);
	ulTxflags = _tcstol(strSendTxflags, 0, 16);

	pDlg->GetDlgItem(IDC_ED_SENDDATARATE)->GetWindowTextW(strSendRate);
	ulRate = _tcstol(strSendRate, 0, 10);

	pDlg->GetDlgItem(IDC_ED_SENDDATAINTERVAL)->GetWindowTextW(strSendInterval);
	ulInterval = _tcstol(strSendInterval, 0, 10);

	pDlg->GetDlgItem(IDC_ED_SENDDATAFREQUENCY)->GetWindowTextW(strSendFrequency);
	ulFrequency = _tcstol(strSendFrequency, 0, 10);


	for(UINT32_T uiLoop = 0; uiLoop < ulFrequency; uiLoop++)
	{
		g_ThreadLock.Lock();
		RetVal = pDlg->J2534ToolDemoSdk.WriteMessages(uCurrSendChannelId, StringToHex(strSendCmd), &uRxMsgStatus, ulTxflags, ulTimeout);
		g_PassthruStatus = _err.GetErrInfo(RetVal);

		strSendRxflags.Format(_T("0x%.8x"), uRxMsgStatus);

		GetLocalTime(&time);

		minutes.Format(_T("%.2d:"), time.wMinute);
		seconds.Format(_T("%.2d:"), time.wSecond);
		milliseconds.Format(_T("%.3d"), time.wMilliseconds);
		TimeStamp = minutes + seconds + milliseconds;

		sendlistContent.ListRow = pDlg->m_ListMessageRow;
		sendlistContent.sTimestamp = TimeStamp;
		sendlistContent.sSender = _T("Tool>>>");
		sendlistContent.sChannelId = UINT32ToHexString(uCurrSendChannelId,8);
		sendlistContent.sNetWorkId = ProtocolValueToStr(pDlg->J2534ToolDemoSdk.m_Channel[uCurrSendChannelId-1].GetProtocolId());
		sendlistContent.sRxStatus = strSendRxflags;
		sendlistContent.sDataBytes = HandleDataByteString(strSendCmd).MakeUpper();//strSendCmd.MakeUpper();

		stuContentDeque.push_back(sendlistContent);

		//pDlg->SendMessage(WM_MyMessage, 0, (LPARAM)&sendlistContent);

		// 保存各通道SendMessage消息
		saveChannelSendMsgdata[uCurrSendChannelId-1].ChannelNumber = uCurrSendChannelId;
		saveChannelSendMsgdata[uCurrSendChannelId-1]._sendMessage.ulProtocolId = pDlg->J2534ToolDemoSdk.m_Channel[uCurrSendChannelId-1].GetProtocolId();
		saveChannelSendMsgdata[uCurrSendChannelId-1]._sendMessage.ulTxFlag = ulTxflags;
		saveChannelSendMsgdata[uCurrSendChannelId-1]._sendMessage.ulTimeOut = ulTimeout;
		memcpy(saveChannelSendMsgdata[uCurrSendChannelId-1]._sendMessage.bSendMegs.ucConvertData, StringToHex(strSendCmd).ucConvertData, StringToHex(strSendCmd).ulResLen);

		//SetEvent(hRevEvent);
		if(ulInterval>10)
			XSleep(ulInterval-7);
		else
			XSleep(ulInterval);
		g_ThreadLock.Unlock();
		TRACE2("%.8x, %s\r\n", hSendMsgThread, L" SendThread");
	}
	CloseHandle(hSendMsgThread);
	//ReleaseMutex(hSendMsgThread);
	hSendMsgThread = NULL;
	return 0;
}


DWORD __stdcall ThreadRevMsgFun(LPVOID IParams)
{
	CJ2534ToolDemoDlg	*pDlg = (CJ2534ToolDemoDlg *)IParams;
	PASSTHRU_MSG	RevMsg;
	UINT32_T	RxMsgsNum = 1, uTimeout = 0, RetVal = 0;
	UINT32_T	CurrChannelNum = 0;
	CString		strRevTimeout, strRxStatus, strRevMessage, strProtocolName;
	CString		minutes, seconds, milliseconds, TimeStamp;
	ListctrlContent recvlistContent;
	SYSTEMTIME	time;
	unsigned long long llTimeUs = 0;

	pDlg->GetDlgItem(IDC_ED_RECVDATATIMEOUT)->GetWindowTextW(strRevTimeout);
	if(strRevTimeout != "")
		uTimeout = _tcstol(strRevTimeout, 0, 10);
	else
		uTimeout = 0;

	while(1)
	{			
		g_ThreadLock.Lock();		// 互斥
		for(UINT8_T ucLoop = 0; ucLoop < g_ChannelsMax; ucLoop++)
		{	
			strRxStatus = "", strRevMessage = "";

			if(saveChannelRecvInfo[ucLoop].bStatus == TRUE)//if(pDlg->J2534ToolSdk.GetChannelStatus(ucLoop) == TRUE)
			{ // 根据接收通道需求，接收数据
				for(UINT8_T uiLoop = 0; uiLoop < 5; uiLoop++)
				{	
					RetVal = pDlg->J2534ToolDemoSdk.ReadMessages(pDlg->J2534ToolDemoSdk.m_Channel[ucLoop].GetChannelId(), &RevMsg, &RxMsgsNum, uTimeout);

					if (RetVal != J2534_STATUS_NOERROR)
					{	
						if(RetVal == J2534_ERR_BUFFER_EMPTY)
							g_PassthruStatus = _err.GetErrInfo(RetVal);
					}
					else
						g_PassthruStatus = L"PassThruReadMessages()-OK";

					strRxStatus.Format(_T("0x%.8x"), RevMsg.ulRxStatus);					//整形转换为CString类型字符串
					strRevMessage = HexToString(RevMsg.ucData, RevMsg.ulDataSize);

					if((RxMsgsNum != 0) && (strRevMessage != _T("")))
					{	
						GetLocalTime(&time);

						minutes.Format(_T("%.2d:"), time.wMinute);
						seconds.Format(_T("%.2d:"), time.wSecond);
						milliseconds.Format(_T("%.3d"), time.wMilliseconds);
						TimeStamp = minutes + seconds + milliseconds;

						recvlistContent.ListRow = pDlg->m_ListMessageRow;
						recvlistContent.sTimestamp = TimeStamp;
						recvlistContent.sSender = _T("<<<<ECU");
						recvlistContent.sChannelId = UINT32ToHexString(pDlg->J2534ToolDemoSdk.m_Channel[ucLoop].GetChannelId(),8);
						recvlistContent.sNetWorkId = ProtocolValueToStr(pDlg->J2534ToolDemoSdk.m_Channel[pDlg->J2534ToolDemoSdk.m_Channel[ucLoop].GetChannelId()-1].GetProtocolId());
						recvlistContent.sRxStatus = strRxStatus;
						recvlistContent.sDataBytes = strRevMessage;

						stuContentDeque.push_back(recvlistContent);

						//pDlg->SendMessage(WM_MyMessage, 0, (LPARAM)&recvlistContent);		
					}
				}
			}
		}
		//TRACE2("%s, %d\r\n",strRevMessage, uListMessageCount);
		//TRACE1("%d\r\n", pDlg->m_ListMessageRow);
		g_ThreadLock.Unlock();
		//TRACE1("%s\r\n", L"RevThread");
		//TRACE2("%.8x, %s\r\n", hRevMsgThread, L" RevThread");
	}
	return 0;
}



void CJ2534ToolDemoDlg::SetJ2534ToolDemoConfigParamEnable(BOOL bVal)
{
	UINT16_T _ArrConfigParamActiveId[8] = {IDC_ED_FILTERSTATUS, IDC_BTN_SETFILTER,
		IDC_ED_PERIODICSTATUS, IDC_BTN_SETPERIODIC,
		IDC_ED_PARAMCONFIGSTATUS, IDC_BTN_SETPARAMCONFIG,
		IDC_ED_INITSTATUS, IDC_BTN_SETINIT};
	for(UINT16_T uiLoop = 0; uiLoop < 8; uiLoop++)
		GetDlgItem(_ArrConfigParamActiveId[uiLoop])->EnableWindow(bVal);
}


void CJ2534ToolDemoDlg::SetJ2534ToolDemoIsLoadDllBtnEnable(BOOL bBtnLoad, BOOL bBtnUnload)
{
	GetDlgItem(IDC_BTN_LOADDLL)->EnableWindow(bBtnLoad);
	GetDlgItem(IDC_BTN_UNLOADDLL)->EnableWindow(bBtnUnload);
}

void CJ2534ToolDemoDlg::SetJ2534ToolDemoOpenDeviceEnable(BOOL bVal)
{
	UINT16_T _ArrOpenDeviceActiveId[7] = {IDC_ST_DEVICENAME, IDC_CB_DEVICENAME, IDC_BTN_OPENDEVICE,
		IDC_BTN_CLOSEDEVICE, IDC_ST_DEVICEINFO, IDC_ST_OPENDEVICEMSG, IDC_ST_OPENDEVICESTATUS};
	for(UINT16_T uiLoop = 0; uiLoop < 7; uiLoop++)
		GetDlgItem(_ArrOpenDeviceActiveId[uiLoop])->EnableWindow(bVal);
}

void CJ2534ToolDemoDlg::LoadInitProtocolParam()
{
	/* 添加协议类型 */
	((CComboBox*)GetDlgItem(IDC_CB_PROTOCOLNAME))->ResetContent();
	((CComboBox*)GetDlgItem(IDC_CB_PROTOCOLNAME))->InsertString(0, L"J1850PWM");
	((CComboBox*)GetDlgItem(IDC_CB_PROTOCOLNAME))->InsertString(1, L"J1850VPW");
	((CComboBox*)GetDlgItem(IDC_CB_PROTOCOLNAME))->InsertString(2, L"ISO9141");
	((CComboBox*)GetDlgItem(IDC_CB_PROTOCOLNAME))->InsertString(3, L"ISO14230");
	((CComboBox*)GetDlgItem(IDC_CB_PROTOCOLNAME))->InsertString(4, L"CAN");
	((CComboBox*)GetDlgItem(IDC_CB_PROTOCOLNAME))->InsertString(5, L"ISO15765");
	((CComboBox*)GetDlgItem(IDC_CB_PROTOCOLNAME))->InsertString(6, L"CAN_PS");
	((CComboBox*)GetDlgItem(IDC_CB_PROTOCOLNAME))->InsertString(7, L"ISO15765_PS");
	((CComboBox*)GetDlgItem(IDC_CB_PROTOCOLNAME))->InsertString(8, L"SW_CAN_PS");
	((CComboBox*)GetDlgItem(IDC_CB_PROTOCOLNAME))->InsertString(9, L"SW_ISO15765_PS");
	((CComboBox*)GetDlgItem(IDC_CB_PROTOCOLNAME))->InsertString(10, L"ISO9141_PS");
	((CComboBox*)GetDlgItem(IDC_CB_PROTOCOLNAME))->SetCurSel(0);
}


void CJ2534ToolDemoDlg::SetJ2534ToolDemoConnectDeviceEnable(BOOL bVal)
{
	UINT16_T _ArrConnectDeviceActiveId[10] = {IDC_ST_PROTOCOLID, IDC_CB_PROTOCOLNAME, IDC_ST_BAUDRATE,
		IDC_CB_BAUDRATEVALUE, IDC_ST_CONNECTFLAGS, IDC_ED_CONNECTFLAGSVALUE, IDC_ST_CHANNELID,
		IDC_CB_CHANNELIDVALUE, IDC_BTN_CONNECTPROTOCOL, IDC_BTN_DISCONNECTPROTOCOL};
	for(UINT16_T uiLoop = 0; uiLoop < 10; uiLoop++)
		GetDlgItem(_ArrConnectDeviceActiveId[uiLoop])->EnableWindow(bVal);
}

void CJ2534ToolDemoDlg::OnCbnSelchangeCbInterfacename()
{
	// 显示当前驱动的路径
	UINT16_T iInterfaceIndex = ((CComboBox*)GetDlgItem(IDC_CB_INTERFACENAME))->GetCurSel();
	((CEdit*)GetDlgItem(IDC_EDIT_PATHOFDLL))->SetWindowTextW(regDeviceInfo.regDevice[iInterfaceIndex].szRegeditPath);
}

void CJ2534ToolDemoDlg::OnCbnSelchangeCbProtocolname()
{
	CString strProtocolName;

	((CComboBox*)GetDlgItem(IDC_CB_PROTOCOLNAME))->GetLBText(((CComboBox*)GetDlgItem(IDC_CB_PROTOCOLNAME))->GetCurSel(), strProtocolName);
	SetBaudRateValue(strProtocolName);
	GetDlgItem(IDC_ED_CONNECTFLAGSVALUE)->SetWindowTextW(L"0x00000000");
}

void CJ2534ToolDemoDlg::OnCbnSelchangeCbChannelidvalue()
{
	CString strChannelName;
	((CComboBox*)GetDlgItem(IDC_CB_CHANNELIDVALUE))->GetLBText(((CComboBox*)GetDlgItem(IDC_CB_CHANNELIDVALUE))->GetCurSel(), strChannelName);
	for(UINT8_T ucLoop = 1; ucLoop <= CONNECT_MAX_COUNT; ucLoop++)
	{	
		if((L"Channel " + UINT8ToDecStirng(ucLoop)) == strChannelName)//+UINT8ToDecStirng(J2534ToolSdk.GetChannelId()
		{
			((CComboBox*)GetDlgItem(IDC_CB_PROTOCOLNAME))->SetCurSel(g_ConnectInfo[ucLoop-1].ulProtocolIndex);
			SetConnectDeviceControls(g_ConnectInfo[ucLoop-1].ulBaudRateIndex);
			GetDlgItem(IDC_ED_CONNECTFLAGSVALUE)->SetWindowTextW(UINT32ToHexString(g_ConnectInfo[ucLoop-1].ulConnectFlags,8));

			//SetJ2534ToolFiveAndFastEnable(ucLoop);
			break;
		}
	}
}

void CJ2534ToolDemoDlg::OnCbnSelchangeCbSendprotocolname()
{
	UINT32_T uInitCurrSendChannelId = 0;
	CString strProtocolName;
	/* 获取当前通道号 */
	((CComboBox*)GetDlgItem(IDC_CB_SENDPROTOCOLNAME))->GetLBText(((CComboBox*)GetDlgItem(IDC_CB_SENDPROTOCOLNAME))->GetCurSel(), strProtocolName);
	for(UINT8_T uiLoop = 0; uiLoop < CONNECT_MAX_COUNT; uiLoop++)
	{
		if(ProtocolStrToValue(strProtocolName) == J2534ToolDemoSdk.m_Channel[uiLoop].GetProtocolId())
		{	
			uInitCurrSendChannelId = J2534ToolDemoSdk.m_Channel[uiLoop].GetChannelId();
			break;
		}
	}

	GetDlgItem(IDC_ED_SENDTXFLAGS)->SetWindowText(UINT32ToHexString(saveChannelSendMsgdata[uInitCurrSendChannelId-1]._sendMessage.ulTxFlag, 8));
	GetDlgItem(IDC_ED_SENDDATATIMEOUT)->SetWindowText(UINT32ToDecString(saveChannelSendMsgdata[uInitCurrSendChannelId-1]._sendMessage.ulTimeOut));
	GetDlgItem(IDC_ED_SENDMESSAGES)->SetWindowText(L"00 00");
	GetDlgItem(IDC_ED_SENDDATARATE)->SetWindowText(L"001");
	GetDlgItem(IDC_ED_SENDDATAINTERVAL)->SetWindowText(L"100");
	GetDlgItem(IDC_ED_SENDDATAFREQUENCY)->SetWindowText(L"1");
}

void CJ2534ToolDemoDlg::SetBaudRateValue(CString strProtocolName)
{
	CString cProtocol_Num1[] = {L"10416", L"41666"};
	CString cProtocol_Num2[] = {L"41666", L"83333"};
	CString cProtocol_Num3[] = {L"4800", L"9600", L"9615", L"9800", L"10000", L"10400", L"10870", L"11905", L"12500", 
		L"13158", L"13889", L"14706", L"15625", L"19200", L"38400", L"57600", L"115200"};
	CString cProtocol_Num4[] = {L"83333", L"125000", L"250000", L"500000", L"1000000"};
	CString cProtocol_Num5[] = {L"7813", L"62500", L"125000"};
	CString cProtocol_Num6[] = {L"33300", L"83333", L"125000", L"250000", L"500000", L"1000000"};
	UINT16_T uiLoop, uiNumLen;

	((CComboBox*)GetDlgItem(IDC_CB_BAUDRATEVALUE))->ResetContent();
	if((L"J1850VPW" == strProtocolName)||(L"J1850VPW_PS" == strProtocolName)){
		uiNumLen = sizeof(cProtocol_Num1)/sizeof(cProtocol_Num1[0]);
		for (uiLoop=0;uiLoop<uiNumLen;uiLoop++){
			((CComboBox*)GetDlgItem(IDC_CB_BAUDRATEVALUE))->InsertString(uiLoop, cProtocol_Num1[uiLoop]);
		}
		((CComboBox*)GetDlgItem(IDC_CB_BAUDRATEVALUE))->SetCurSel(0);
	}
	else if((L"J1850PWM" == strProtocolName) || (L"J1850PWM_PS" == strProtocolName)){
		uiNumLen = sizeof(cProtocol_Num2)/sizeof(cProtocol_Num2[0]);
		for (uiLoop=0;uiLoop<uiNumLen;uiLoop++){
			((CComboBox*)GetDlgItem(IDC_CB_BAUDRATEVALUE))->InsertString(uiLoop, cProtocol_Num2[uiLoop]);
		}
		((CComboBox*)GetDlgItem(IDC_CB_BAUDRATEVALUE))->SetCurSel(0);
	}
	else if((L"ISO9141" == strProtocolName) || (L"ISO14230" == strProtocolName) || (L"ISO9141_PS" == strProtocolName) || (L"ISO14230_PS" == strProtocolName)){
		uiNumLen = sizeof(cProtocol_Num3)/sizeof(cProtocol_Num3[0]);
		for (uiLoop=0;uiLoop<uiNumLen;uiLoop++){
			((CComboBox*)GetDlgItem(IDC_CB_BAUDRATEVALUE))->InsertString(uiLoop, cProtocol_Num3[uiLoop]);
		}
		((CComboBox*)GetDlgItem(IDC_CB_BAUDRATEVALUE))->SetCurSel(5);
	}
	else if((L"ISO15765" == strProtocolName) || (L"CAN" == strProtocolName) ||(L"VOLVO_CAN2" == strProtocolName)){
		uiNumLen = sizeof(cProtocol_Num4)/sizeof(cProtocol_Num4[0]);
		for (uiLoop=0;uiLoop<uiNumLen;uiLoop++){
			((CComboBox*)GetDlgItem(IDC_CB_BAUDRATEVALUE))->InsertString(uiLoop, cProtocol_Num4[uiLoop]);
		}
		((CComboBox*)GetDlgItem(IDC_CB_BAUDRATEVALUE))->SetCurSel(3);
	}
	else if((L"ISO15765_PS" == strProtocolName) || (L"CAN_PS" == strProtocolName)){
		uiNumLen = sizeof(cProtocol_Num4)/sizeof(cProtocol_Num4[0]);
		for (uiLoop=0;uiLoop<uiNumLen;uiLoop++){
			((CComboBox*)GetDlgItem(IDC_CB_BAUDRATEVALUE))->InsertString(uiLoop, cProtocol_Num4[uiLoop]);
		}
		((CComboBox*)GetDlgItem(IDC_CB_BAUDRATEVALUE))->SetCurSel(1);
	}
	else if((L"SCI_A_ENGINE" == strProtocolName) || (L"SCI_A_TRANS" == strProtocolName) || (L"SCI_B_ENGINE" == strProtocolName) || (L"SCI_B_TRANS" == strProtocolName)){
		uiNumLen = sizeof(cProtocol_Num5)/sizeof(cProtocol_Num5[0]);
		for (uiLoop=0;uiLoop<uiNumLen;uiLoop++){
			((CComboBox*)GetDlgItem(IDC_CB_BAUDRATEVALUE))->InsertString(uiLoop, cProtocol_Num5[uiLoop]);
		}
		((CComboBox*)GetDlgItem(IDC_CB_BAUDRATEVALUE))->SetCurSel(2);
	}
	else if((L"SW_CAN_PS" == strProtocolName) || (L"SW_ISO15765_PS" == strProtocolName)){
		uiNumLen = sizeof(cProtocol_Num6)/sizeof(cProtocol_Num6[0]);
		for (uiLoop=0;uiLoop<uiNumLen;uiLoop++){
			((CComboBox*)GetDlgItem(IDC_CB_BAUDRATEVALUE))->InsertString(uiLoop, cProtocol_Num6[uiLoop]);
		}
		((CComboBox*)GetDlgItem(IDC_CB_BAUDRATEVALUE))->SetCurSel(3);
	}
}


void CJ2534ToolDemoDlg::LoadInitIoctlParam()
{
	/* 初始化Ioctl配置参数 */
	UINT32_T RetVal = 0, ulJ1962Pins = 0, CurrViewChannel = 0;
	CString strDataRate;

	for(UINT8_T ucLoop = 0; ucLoop < g_ChannelsMax; ucLoop++)
	{
		//if(J2534ToolDemoSdk.GetChannelStatus(ucLoop) != FALSE)
		if(J2534ToolDemoSdk.m_Channel[ucLoop].GetChannelExist() != FALSE)
		{
			CurrViewChannel = J2534ToolDemoSdk.m_Channel[ucLoop].GetChannelId();
			if(saveChannelioctldata[CurrViewChannel-1]._ioctlParam.bIsSetInit == FALSE)
			{
				saveChannelioctldata[CurrViewChannel-1].ChannelNumber = CurrViewChannel;				
				((CComboBox*)GetDlgItem(IDC_CB_BAUDRATEVALUE))->GetLBText(((CComboBox*)GetDlgItem(IDC_CB_BAUDRATEVALUE))->GetCurSel(), strDataRate);
				saveChannelioctldata[CurrViewChannel-1]._ioctlParam.ulDataRate = _tcstol(strDataRate, 0, 10);
				switch (J2534ToolDemoSdk.m_Channel[CurrViewChannel-1].GetProtocolId())
				{
				case J1850VPW:
					saveChannelioctldata[CurrViewChannel-1]._ioctlParam.bLookBack = 0;
					break;
				case J1850PWM:
					saveChannelioctldata[CurrViewChannel-1]._ioctlParam.bLookBack = 0;
					saveChannelioctldata[CurrViewChannel-1]._ioctlParam.ucNodeAddress = 0x0;
					saveChannelioctldata[CurrViewChannel-1]._ioctlParam.ucNetworkLine = 0;
					break;
				case ISO9141:
				case ISO9141_PS:
				case ISO14230:
					saveChannelioctldata[CurrViewChannel-1]._ioctlParam.bLookBack = 0;
					saveChannelioctldata[CurrViewChannel-1]._ioctlParam.uiP1Max = 40;
					saveChannelioctldata[CurrViewChannel-1]._ioctlParam.uiP3Min = 110;
					saveChannelioctldata[CurrViewChannel-1]._ioctlParam.uiP4Min = 10;
					saveChannelioctldata[CurrViewChannel-1]._ioctlParam.uiW0 = 300;
					saveChannelioctldata[CurrViewChannel-1]._ioctlParam.uiW1 = 300;
					saveChannelioctldata[CurrViewChannel-1]._ioctlParam.uiW2 = 20;
					saveChannelioctldata[CurrViewChannel-1]._ioctlParam.uiW3 = 20;
					saveChannelioctldata[CurrViewChannel-1]._ioctlParam.uiW4 = 50;
					saveChannelioctldata[CurrViewChannel-1]._ioctlParam.uiW5 = 300;
					saveChannelioctldata[CurrViewChannel-1]._ioctlParam.uiTidle = 300;
					saveChannelioctldata[CurrViewChannel-1]._ioctlParam.uiTinil = 25;
					saveChannelioctldata[CurrViewChannel-1]._ioctlParam.uiTwup = 50;
					saveChannelioctldata[CurrViewChannel-1]._ioctlParam.ucParity = 0;
					saveChannelioctldata[CurrViewChannel-1]._ioctlParam.bDataBits = 0;
					saveChannelioctldata[CurrViewChannel-1]._ioctlParam.ucFiveBaudMod = 0;
					if(J2534ToolDemoSdk.m_Channel[CurrViewChannel-1].GetProtocolId() == ISO9141_PS)
					{	
						saveChannelioctldata[CurrViewChannel-1]._ioctlParam.uiJ1962Pins = 0x0800;
						SCONFIG_LIST CfgList;
						CfgList.ulNumOfParams = 1;
						SCONFIG Cfgs[] = {{J1962_PINS, saveChannelioctldata[CurrViewChannel-1]._ioctlParam.uiJ1962Pins}};
						CfgList.pConfigPtr = Cfgs;
						g_ThreadLock.Lock();
						RetVal = _PassThruIoctl(J2534ToolDemoSdk.m_Channel[CurrViewChannel-1].GetChannelId(), SET_CONFIG, &CfgList, NULL);
						g_ThreadLock.Unlock();
						PASSTHRUMESSAGEVIEW(RetVal, L"PassThruIoctl()-OK");

						// GET_CONFIG 参数列表
						SCONFIG CfgsGet[]={
							{J1962_PINS, ulJ1962Pins}
						};
						CfgList.pConfigPtr = CfgsGet;		//array of SCONFIG
						g_ThreadLock.Lock();
						RetVal = _PassThruIoctl(J2534ToolDemoSdk.m_Channel[CurrViewChannel-1].GetChannelId(), GET_CONFIG, &CfgList, NULL);
						g_ThreadLock.Unlock();
						PASSTHRUMESSAGEVIEW(RetVal, L"PassThruIoctl_GET_CONFIG,()-OK");
					}
					break;
				case CAN:
				case CAN_PS:
				case VOLVO_CAN2:
					saveChannelioctldata[CurrViewChannel-1]._ioctlParam.bLookBack = 0;
					saveChannelioctldata[CurrViewChannel-1]._ioctlParam.ucBitSamplePoint = 80;
					saveChannelioctldata[CurrViewChannel-1]._ioctlParam.ucSyncJumpWidth = 15;
					if(J2534ToolDemoSdk.m_Channel[CurrViewChannel-1].GetProtocolId() == CAN_PS)
					{
						//saveChannelioctldata[CurrViewChannel-1]._ioctlParam.ulDataRate = 125000;
						saveChannelioctldata[CurrViewChannel-1]._ioctlParam.uiJ1962Pins = 0x030b;

						SCONFIG_LIST CfgList;
						CfgList.ulNumOfParams = 1;
						SCONFIG Cfgs[] = {{J1962_PINS, saveChannelioctldata[CurrViewChannel-1]._ioctlParam.uiJ1962Pins}};
						CfgList.pConfigPtr = Cfgs;
						g_ThreadLock.Lock();
						_PassThruIoctl(J2534ToolDemoSdk.m_Channel[CurrViewChannel-1].GetChannelId(), SET_CONFIG, &CfgList, NULL);
						g_ThreadLock.Unlock();
						PASSTHRUMESSAGEVIEW(RetVal, L"PassThruIoctl()-OK");

						// GET_CONFIG 参数列表
						SCONFIG CfgsGet[]={
							{J1962_PINS, ulJ1962Pins}
						};
						CfgList.pConfigPtr = CfgsGet;		//array of SCONFIG
						g_ThreadLock.Lock();
						RetVal = _PassThruIoctl(J2534ToolDemoSdk.m_Channel[CurrViewChannel-1].GetChannelId(), GET_CONFIG, &CfgList, NULL);
						g_ThreadLock.Unlock();
						PASSTHRUMESSAGEVIEW(RetVal, L"PassThruIoctl_GET_CONFIG,()-OK");
					}
					break;
				case ISO15765:
				case ISO15765_PS:
					saveChannelioctldata[CurrViewChannel-1]._ioctlParam.bLookBack = 0;
					saveChannelioctldata[CurrViewChannel-1]._ioctlParam.ucBitSamplePoint = 80;
					saveChannelioctldata[CurrViewChannel-1]._ioctlParam.ucSyncJumpWidth = 15;
					saveChannelioctldata[CurrViewChannel-1]._ioctlParam.ucIso15765BS = 0;
					saveChannelioctldata[CurrViewChannel-1]._ioctlParam.ucIso15765Stmin = 0;
					saveChannelioctldata[CurrViewChannel-1]._ioctlParam.uiBsTx = 0;
					saveChannelioctldata[CurrViewChannel-1]._ioctlParam.uiStminTx = 0;
					saveChannelioctldata[CurrViewChannel-1]._ioctlParam.ucIso15765WftMax = 0;
					saveChannelioctldata[CurrViewChannel-1]._ioctlParam.ulIso15765Simultaneous = 0;
					saveChannelioctldata[CurrViewChannel-1]._ioctlParam.ulDtIso15765PadByte = 0x00;
					saveChannelioctldata[CurrViewChannel-1]._ioctlParam.ulCanMixedFormat = 0;
					if(J2534ToolDemoSdk.m_Channel[CurrViewChannel-1].GetProtocolId() == ISO15765_PS)
					{
						saveChannelioctldata[CurrViewChannel-1]._ioctlParam.uiJ1962Pins = 0x030b;

						SCONFIG_LIST CfgList;
						CfgList.ulNumOfParams = 1;
						SCONFIG Cfgs[] = {{J1962_PINS, saveChannelioctldata[CurrViewChannel-1]._ioctlParam.uiJ1962Pins}};
						CfgList.pConfigPtr = Cfgs;
						g_ThreadLock.Lock();
						_PassThruIoctl(J2534ToolDemoSdk.m_Channel[CurrViewChannel-1].GetChannelId(), SET_CONFIG, &CfgList, NULL);
						g_ThreadLock.Unlock();
						PASSTHRUMESSAGEVIEW(RetVal, L"PassThruIoctl()-OK");

						// GET_CONFIG 参数列表
						SCONFIG CfgsGet[]={
							{J1962_PINS, ulJ1962Pins}
						};
						CfgList.pConfigPtr = CfgsGet;		//array of SCONFIG
						g_ThreadLock.Lock();
						RetVal = _PassThruIoctl(J2534ToolDemoSdk.m_Channel[CurrViewChannel-1].GetChannelId(), GET_CONFIG, &CfgList, NULL);
						g_ThreadLock.Unlock();
						PASSTHRUMESSAGEVIEW(RetVal, L"PassThruIoctl_GET_CONFIG,()-OK");
					}
					break;
				case SW_CAN_PS:
				case SW_ISO15765_PS:
					{
						saveChannelioctldata[CurrViewChannel-1]._ioctlParam.bLookBack = 0;
						saveChannelioctldata[CurrViewChannel-1]._ioctlParam.ucBitSamplePoint = 80;
						saveChannelioctldata[CurrViewChannel-1]._ioctlParam.ucSyncJumpWidth = 15;
						saveChannelioctldata[CurrViewChannel-1]._ioctlParam.uiJ1962Pins = 0x0100;
						saveChannelioctldata[CurrViewChannel-1]._ioctlParam.ulSwCanHsDataRate = 0;
						saveChannelioctldata[CurrViewChannel-1]._ioctlParam.ulSwCanSpdchgEnable = 0;
						saveChannelioctldata[CurrViewChannel-1]._ioctlParam.ulSwCanResSwitch = 0;
						if(J2534ToolDemoSdk.m_Channel[CurrViewChannel-1].GetProtocolId() == SW_ISO15765_PS)
						{
							saveChannelioctldata[CurrViewChannel-1]._ioctlParam.ucIso15765Stmin = 0;
							saveChannelioctldata[CurrViewChannel-1]._ioctlParam.uiBsTx = 0;
							saveChannelioctldata[CurrViewChannel-1]._ioctlParam.uiStminTx = 0;
							saveChannelioctldata[CurrViewChannel-1]._ioctlParam.ucIso15765WftMax = 0;
							saveChannelioctldata[CurrViewChannel-1]._ioctlParam.ulIso15765Simultaneous = 0;
							saveChannelioctldata[CurrViewChannel-1]._ioctlParam.ulDtIso15765PadByte = 0x00;
							saveChannelioctldata[CurrViewChannel-1]._ioctlParam.ulCanMixedFormat = 0;	
						}
						SCONFIG_LIST CfgList;
						CfgList.ulNumOfParams = 1;
						SCONFIG Cfgs[] = {{J1962_PINS, saveChannelioctldata[CurrViewChannel-1]._ioctlParam.uiJ1962Pins}};
						CfgList.pConfigPtr = Cfgs;
						g_ThreadLock.Lock();
						_PassThruIoctl(J2534ToolDemoSdk.m_Channel[CurrViewChannel-1].GetChannelId(), SET_CONFIG, &CfgList, NULL);
						g_ThreadLock.Unlock();
						PASSTHRUMESSAGEVIEW(RetVal, L"PassThruIoctl()-OK");


						// GET_CONFIG 参数列表
						SCONFIG CfgsGet[]={
							{J1962_PINS, ulJ1962Pins}
						};
						CfgList.pConfigPtr = CfgsGet;		//array of SCONFIG
						g_ThreadLock.Lock();
						RetVal = _PassThruIoctl(J2534ToolDemoSdk.m_Channel[CurrViewChannel-1].GetChannelId(), GET_CONFIG, &CfgList, NULL);
						g_ThreadLock.Unlock();
						PASSTHRUMESSAGEVIEW(RetVal, L"PassThruIoctl_GET_CONFIG,()-OK");
					}
					break;
					//case J1939_PS:
					//	break;
				default:
					break;
				}
				saveChannelioctldata[CurrViewChannel-1]._ioctlParam.bIsSetStatus = FALSE;
				saveChannelioctldata[CurrViewChannel-1]._ioctlParam.bIsSetInit = TRUE;
			}
		}
	}
}


void CJ2534ToolDemoDlg::SetJ2534ToolDemoSendMsgData()
{
	INT8_T ucTemp = 0;
	UINT32_T uInitCurrSendChannelId = 0;
	CString strProtocolName;
	/* SendProtocolId 显示 */
	((CComboBox*)GetDlgItem(IDC_CB_SENDPROTOCOLNAME))->ResetContent();
	for(UINT8_T ucLoop = 0; ucLoop < CONNECT_MAX_COUNT; ucLoop++)
	{
		if(J2534ToolDemoSdk.m_Channel[ucLoop].GetChannelExist() != FALSE)
		{
			((CComboBox*)GetDlgItem(IDC_CB_SENDPROTOCOLNAME))->InsertString(ucTemp,ProtocolValueToStr(J2534ToolDemoSdk.m_Channel[ucLoop].GetProtocolId()));
			ucTemp++;
		}
	}
	((CComboBox*)GetDlgItem(IDC_CB_SENDPROTOCOLNAME))->SetCurSel(0);

	if(g_ChannelsCount == 0)
	{
		GetDlgItem(IDC_ED_SENDTXFLAGS)->SetWindowText(L"");
		GetDlgItem(IDC_ED_SENDDATATIMEOUT)->SetWindowText(L"");
		GetDlgItem(IDC_ED_SENDMESSAGES)->SetWindowText(L"");
		GetDlgItem(IDC_ED_SENDDATARATE)->SetWindowText(L"");
		GetDlgItem(IDC_ED_SENDDATAINTERVAL)->SetWindowText(L"");
		GetDlgItem(IDC_ED_SENDDATAFREQUENCY)->SetWindowText(L"");

		SetJ2534ToolDemoConfigParamEnable(FALSE);
		SetJ2534ToolDemoSendMsgDataEnable(FALSE);
	}
	else
	{
		/* 获取当前通道号 */
		((CComboBox*)GetDlgItem(IDC_CB_SENDPROTOCOLNAME))->GetLBText(((CComboBox*)GetDlgItem(IDC_CB_SENDPROTOCOLNAME))->GetCurSel(), strProtocolName);
		for(UINT8_T uiLoop = 0; uiLoop < CONNECT_MAX_COUNT; uiLoop++)
		{
			if(ProtocolStrToValue(strProtocolName) == J2534ToolDemoSdk.m_Channel[uiLoop].GetProtocolId())
			{	
				uInitCurrSendChannelId = J2534ToolDemoSdk.m_Channel[uiLoop].GetChannelId();
				break;
			}
		}
		GetDlgItem(IDC_ED_SENDTXFLAGS)->SetWindowText(UINT32ToHexString(saveChannelSendMsgdata[uInitCurrSendChannelId-1]._sendMessage.ulTxFlag, 8));
		GetDlgItem(IDC_ED_SENDDATATIMEOUT)->SetWindowText(UINT32ToDecString(saveChannelSendMsgdata[uInitCurrSendChannelId-1]._sendMessage.ulTimeOut));
		GetDlgItem(IDC_ED_SENDMESSAGES)->SetWindowText(L"00 00");
		GetDlgItem(IDC_ED_SENDDATARATE)->SetWindowTextW(L"001");
		GetDlgItem(IDC_ED_SENDDATAINTERVAL)->SetWindowText(L"100");
		GetDlgItem(IDC_ED_SENDDATAFREQUENCY)->SetWindowText(L"1");
	}

}

void CJ2534ToolDemoDlg::SetJ2534ToolDemoSendMsgDataEnable(BOOL bVal)
{
	UINT16_T _ArrSendMsgDataActiveId[18] = {IDC_ST_SENDPROTOCOLNAME, IDC_CB_SENDPROTOCOLNAME, IDC_ST_SENDTXFLAGS, 
		IDC_ED_SENDTXFLAGS,	IDC_ST_SENDDATATIMEOUT, IDC_ED_SENDDATATIMEOUT, IDC_ST_SENDMESSAGES, 
		IDC_ED_SENDMESSAGES, IDC_ST_RECVDATATIMEOUT, IDC_ED_RECVDATATIMEOUT, IDC_BTN_SENDMESSAGE, 
		IDC_BTN_CLEARMESSAGE, IDC_BTN_RECVMESSAGE, IDC_BTN_STOPRECVMESSAGE, IDC_ST_SENDDATAINTERVAL,
		IDC_ED_SENDDATAINTERVAL, IDC_ST_SENDDATAFREQUENCY, IDC_ED_SENDDATAFREQUENCY};
	for(UINT16_T uiLoop = 0; uiLoop < 18; uiLoop++)
		GetDlgItem(_ArrSendMsgDataActiveId[uiLoop])->EnableWindow(bVal);
}

void CJ2534ToolDemoDlg::SetJ2534ToolDemoRecvChannelDisplay(UINT32_T ulchannelId, BOOL bVal)
{
#if 0 // 接收信息通道显示
	CString strCheckboxName;

	UINT16_T _ArrRecvProtocolName[CONNECT_MAX_COUNT] = {IDC_CK_RECVPROTOCOL1, IDC_CK_RECVPROTOCOL2, IDC_CK_RECVPROTOCOL3,
		IDC_CK_RECVPROTOCOL4, IDC_CK_RECVPROTOCOL5, IDC_CK_RECVPROTOCOL6, IDC_CK_RECVPROTOCOL7,
		IDC_CK_RECVPROTOCOL8, IDC_CK_RECVPROTOCOL9, IDC_CK_RECVPROTOCOL10};

	GetDlgItem(IDC_ED_RECVDATATIMEOUT)->SetWindowText(L"1");

	if(g_ChannelsCount != 0)
		GetDlgItem(IDC_ST_RECVPROTOCOL)->EnableWindow(TRUE);
	else
		GetDlgItem(IDC_ST_RECVPROTOCOL)->EnableWindow(FALSE);

	if(bVal == TRUE)// Connect bVal = TRUE;
	{
		for(UINT8_T uiLoop = 1; uiLoop <= CONNECT_MAX_COUNT; uiLoop++)
		{
			if(saveChannelRecvInfo[uiLoop-1].bStatus == TRUE)
			{
				if (BST_CHECKED != IsDlgButtonChecked(_ArrRecvProtocolName[uiLoop-1]))
				{
					GetDlgItem(_ArrRecvProtocolName[uiLoop-1])->EnableWindow(bVal);
					GetDlgItem(_ArrRecvProtocolName[uiLoop-1])->SetWindowText(ProtocolValueToStr(J2534ToolSdk.GetProtocolId(J2534ToolSdk.GetCurrChannelId())));
					((CButton *)(GetDlgItem(_ArrRecvProtocolName[uiLoop-1])))->SetCheck(bVal);
					break;
				}
			}
		} 
	}
	else if(bVal == FALSE)// DisConnect bVal = FALSE;
	{
		for(UINT8_T uiLoop = 1; uiLoop <= CONNECT_MAX_COUNT; uiLoop++)
		{
			GetDlgItem(_ArrRecvProtocolName[uiLoop-1])->GetWindowText(strCheckboxName);
			if(strCheckboxName == ProtocolValueToStr(saveChannelRecvInfo[ulchannelId-1].uProtocolValue))
			{
				GetDlgItem(_ArrRecvProtocolName[uiLoop-1])->EnableWindow(bVal);
				GetDlgItem(_ArrRecvProtocolName[uiLoop-1])->SetWindowText(L"None");
				((CButton *)(GetDlgItem(_ArrRecvProtocolName[uiLoop-1])))->SetCheck(bVal);
				break;
			}
		}
	}
#endif
}

void CJ2534ToolDemoDlg::SetJ2534ToolDemoRecvProtocolCkEnable(BOOL bVal)
{
#if 0
	UINT16_T _ArrRecvProtocolName[5] = {IDC_CK_RECVPROTOCOL1, IDC_CK_RECVPROTOCOL2, 
		IDC_CK_RECVPROTOCOL3, IDC_CK_RECVPROTOCOL4, IDC_CK_RECVPROTOCOL5};
	GetDlgItem(IDC_ST_RECVPROTOCOL)->EnableWindow(bVal);
	for(UINT16_T uiLoop = 0; uiLoop < 5; uiLoop++)
	{	
		if(bVal == FALSE)
		{
			GetDlgItem(_ArrRecvProtocolName[uiLoop])->SetWindowText(L"None");
			GetDlgItem(_ArrRecvProtocolName[uiLoop])->EnableWindow(bVal);
			((CButton*)(GetDlgItem(_ArrRecvProtocolName[uiLoop])))->SetCheck(bVal);
		}
	}
#endif
}


void CJ2534ToolDemoDlg::SetConnectDeviceControls(UINT32_T ulBaudRateIndex)
{
	CString strProtocolName = L"";
	CString cProtocol_Num1[] = {L"10416", L"41666"};
	CString cProtocol_Num2[] = {L"41666", L"83333"};
	CString cProtocol_Num3[] = {L"4800", L"9600", L"9615", L"9800", L"10000", L"10400", L"10870", L"11905", L"12500", 
		L"13158", L"13889", L"14706", L"15625", L"19200", L"38400", L"57600", L"115200"};
	CString cProtocol_Num4[] = {L"83333", L"125000", L"250000", L"500000", L"1000000"};
	CString cProtocol_Num5[] = {L"7813", L"62500", L"125000"};
	CString cProtocol_Num6[] = {L"33300", L"83333", L"125000", L"250000", L"500000", L"1000000"};
	UINT16_T uiLoop, uiNumLen;

	//GetDlgItem(IDC_CB_PROTOCOLNAME)->GetWindowText(strProtocolName);
	((CComboBox*)GetDlgItem(IDC_CB_PROTOCOLNAME))->GetLBText(((CComboBox*)GetDlgItem(IDC_CB_PROTOCOLNAME))->GetCurSel(), strProtocolName);

	((CComboBox*)GetDlgItem(IDC_CB_BAUDRATEVALUE))->ResetContent();
	if((L"J1850VPW" == strProtocolName)||(L"J1850VPW_PS" == strProtocolName)){
		uiNumLen = sizeof(cProtocol_Num1)/sizeof(cProtocol_Num1[0]);
		for (uiLoop=0;uiLoop<uiNumLen;uiLoop++){
			((CComboBox*)GetDlgItem(IDC_CB_BAUDRATEVALUE))->InsertString(uiLoop, cProtocol_Num1[uiLoop]);
		}
		((CComboBox*)GetDlgItem(IDC_CB_BAUDRATEVALUE))->SetCurSel(ulBaudRateIndex);
	}
	else if((L"J1850PWM" == strProtocolName) || (L"J1850PWM_PS" == strProtocolName)){
		uiNumLen = sizeof(cProtocol_Num2)/sizeof(cProtocol_Num2[0]);
		for (uiLoop=0;uiLoop<uiNumLen;uiLoop++){
			((CComboBox*)GetDlgItem(IDC_CB_BAUDRATEVALUE))->InsertString(uiLoop, cProtocol_Num2[uiLoop]);
		}
		((CComboBox*)GetDlgItem(IDC_CB_BAUDRATEVALUE))->SetCurSel(ulBaudRateIndex);
	}
	else if((L"ISO9141" == strProtocolName) || (L"ISO14230" == strProtocolName) || (L"ISO9141_PS" == strProtocolName) || (L"ISO14230_PS" == strProtocolName)){
		uiNumLen = sizeof(cProtocol_Num3)/sizeof(cProtocol_Num3[0]);
		for (uiLoop=0;uiLoop<uiNumLen;uiLoop++){
			((CComboBox*)GetDlgItem(IDC_CB_BAUDRATEVALUE))->InsertString(uiLoop, cProtocol_Num3[uiLoop]);
		}
		((CComboBox*)GetDlgItem(IDC_CB_BAUDRATEVALUE))->SetCurSel(ulBaudRateIndex);
	}
	else if((L"ISO15765" == strProtocolName) || (L"CAN" == strProtocolName) || (L"VOLVO_CAN2" == strProtocolName)){
		uiNumLen = sizeof(cProtocol_Num4)/sizeof(cProtocol_Num4[0]);
		for (uiLoop=0;uiLoop<uiNumLen;uiLoop++){
			((CComboBox*)GetDlgItem(IDC_CB_BAUDRATEVALUE))->InsertString(uiLoop, cProtocol_Num4[uiLoop]);
		}
		((CComboBox*)GetDlgItem(IDC_CB_BAUDRATEVALUE))->SetCurSel(ulBaudRateIndex);
	}
	else if((L"ISO15765_PS" == strProtocolName) || (L"CAN_PS" == strProtocolName)){
		uiNumLen = sizeof(cProtocol_Num4)/sizeof(cProtocol_Num4[0]);
		for (uiLoop=0;uiLoop<uiNumLen;uiLoop++){
			((CComboBox*)GetDlgItem(IDC_CB_BAUDRATEVALUE))->InsertString(uiLoop, cProtocol_Num4[uiLoop]);
		}
		((CComboBox*)GetDlgItem(IDC_CB_BAUDRATEVALUE))->SetCurSel(ulBaudRateIndex);
	}
	else if((L"SCI_A_ENGINE" == strProtocolName) || (L"SCI_A_TRANS" == strProtocolName) || (L"SCI_B_ENGINE" == strProtocolName) || (L"SCI_B_TRANS" == strProtocolName)){
		uiNumLen = sizeof(cProtocol_Num5)/sizeof(cProtocol_Num5[0]);
		for (uiLoop=0;uiLoop<uiNumLen;uiLoop++){
			((CComboBox*)GetDlgItem(IDC_CB_BAUDRATEVALUE))->InsertString(uiLoop, cProtocol_Num5[uiLoop]);
		}
		((CComboBox*)GetDlgItem(IDC_CB_BAUDRATEVALUE))->SetCurSel(ulBaudRateIndex);
	}
	else if((L"SW_CAN_PS" == strProtocolName) || (L"SW_ISO15765_PS" == strProtocolName)){
		uiNumLen = sizeof(cProtocol_Num6)/sizeof(cProtocol_Num6[0]);
		for (uiLoop=0;uiLoop<uiNumLen;uiLoop++){
			((CComboBox*)GetDlgItem(IDC_CB_BAUDRATEVALUE))->InsertString(uiLoop, cProtocol_Num6[uiLoop]);
		}
		((CComboBox*)GetDlgItem(IDC_CB_BAUDRATEVALUE))->SetCurSel(ulBaudRateIndex);
	}
}



void CJ2534ToolDemoDlg::OnItemchangedListMessageView(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMLISTVIEW pNMLV = reinterpret_cast<LPNMLISTVIEW>(pNMHDR);
	// TODO: 在此添加控件通知处理程序代码
	*pResult = 0;
}


void CJ2534ToolDemoDlg::OnRclickListMessageView(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMITEMACTIVATE pNMItemActivate = reinterpret_cast<LPNMITEMACTIVATE>(pNMHDR);

	//if(pNMItemActivate->iItem != -1)
	{
		DWORD dwPos = GetMessagePos();
		CPoint point(LOWORD(dwPos), HIWORD(dwPos));

		CMenu menu;
		VERIFY(menu.LoadMenu(IDR_MENU_LISTVIEW));
		CMenu* popup = menu.GetSubMenu(0);
		ASSERT(popup != NULL);
		popup->TrackPopupMenu(TPM_LEFTALIGN | TPM_RIGHTBUTTON, point.x, point.y, this);
	} 

	*pResult = 0;
}


void CJ2534ToolDemoDlg::SetTextColor(COLORREF color, UINT16_T ActiveXID, CString strContent)     //设置文本颜色
{
	GetDlgItem(ActiveXID)->SetWindowText(strContent);

	if (IDC_ED_FILTERSTATUS == ActiveXID)
		m_FilterForeColor = color;

	if (IDC_ED_PERIODICSTATUS == ActiveXID)
		m_PeriodicForeColor = color;

	if (IDC_ED_PARAMCONFIGSTATUS == ActiveXID)
		m_ParamConfigForeColor = color;

	if(IDC_ED_INITSTATUS == ActiveXID)
		m_InitConfigForeColor = color;

	//此处改变字体的颜色
	(CStatic*)GetDlgItem(ActiveXID)->RedrawWindow();
}

void CJ2534ToolDemoDlg::SetTextFont(UINT16_T nHeight, UINT16_T nWeight, BYTE bItalic, UINT16_T ActiveXID)//设置字体
{
	// 设置静态文本字体
	pFont = new CFont;
	pFont->CreateFontW(nHeight,
		0,
		0,
		0,
		nWeight,
		bItalic,		// Italic(斜体)
		FALSE,		// Underline(下划线)
		0,
		GB2312_CHARSET,//ANSI_CHARSET,
		OUT_DEFAULT_PRECIS,
		CLIP_DEFAULT_PRECIS,
		DEFAULT_QUALITY,
		DEFAULT_PITCH|FF_SWISS,
		_T("Arial"));

	GetDlgItem(ActiveXID)->SetFont(pFont);
}

HBRUSH CJ2534ToolDemoDlg::OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor)
{
	HBRUSH hbr = CDialogEx::OnCtlColor(pDC, pWnd, nCtlColor);
	// TODO:  在此更改 DC 的任何特性
	if(pWnd-> GetDlgCtrlID() == IDC_ED_FILTERSTATUS)
		pDC->SetTextColor(m_FilterForeColor);		//设置文字颜色
	else if(pWnd-> GetDlgCtrlID() == IDC_ED_PERIODICSTATUS)
		pDC->SetTextColor(m_PeriodicForeColor);		//设置文字颜色
	else if(pWnd-> GetDlgCtrlID() == IDC_ED_PARAMCONFIGSTATUS)
		pDC->SetTextColor(m_ParamConfigForeColor);	//设置文字颜色
	else if(pWnd-> GetDlgCtrlID() == IDC_ED_INITSTATUS)
		pDC->SetTextColor(m_InitConfigForeColor);	//设置文字颜色

	// 修改Group Box控件的背景颜色---未实现
	//if(pWnd-> GetDlgCtrlID() == IDC_STATIC)
	//	pDC->SetBkColor(RGB(0,255,0));		//设置文字颜色

	//if (nCtlColor==CTLCOLOR_STATIC)
	//{
	//	pDC->SetBkColor(RGB(255,255,255));
	//	pDC->SetTextColor(RGB(255,100,0));
	//}

	return hbr;
}

void CJ2534ToolDemoDlg::OnTimer(UINT_PTR nIDEvent)
{
	if(nIDEvent == 1)	/***显示消息队列***/
	{
#if 1	
		if(stuContentDeque.size()>0)
		{			
			for(UINT32_T uLoop = 0; uLoop < stuContentDeque.size(); uLoop++)
			{
				m_ListMessage.InsertItem(m_ListMessageRow, _T(""));
				m_ListMessage.SetItemText(m_ListMessageRow, 0, stuContentDeque[0].sTimestamp);
				m_ListMessage.SetItemText(m_ListMessageRow, 1, stuContentDeque[0].sSender);//(LPCTSTR)buffer
				m_ListMessage.SetItemText(m_ListMessageRow, 2, stuContentDeque[0].sChannelId);
				m_ListMessage.SetItemText(m_ListMessageRow, 3, stuContentDeque[0].sNetWorkId); 
				m_ListMessage.SetItemText(m_ListMessageRow, 4, stuContentDeque[0].sRxStatus); 
				m_ListMessage.SetItemText(m_ListMessageRow, 5, stuContentDeque[0].sDataBytes);
				m_ListMessageRow++;

				stuContentDeque.pop_front();	// pop弹出栈顶元素

				uListMessageCount = m_ListMessage.GetItemCount();
				if ((uListMessageCount > 0) && (uListMessageCount < 4294967295))
				{	m_ListMessage.EnsureVisible(uListMessageCount-1, FALSE);}
				else
				{
					m_ListMessageRow = 0;
					uListMessageCount = 0;
					m_ListMessage.DeleteAllItems();
				}
			}
		}
#endif
	}
	else if(nIDEvent == 9)	/***显示状态栏信息***/
	{
		CString sysTime;	/* 获取系统时间 */
		CTime tm;
		tm = CTime::GetCurrentTime();
		sysTime = tm.Format("%Y-%m-%d %X");
		m_Statusbar.SetPaneText(1, L"line:" + UINT32ToDecString(m_ListMessageRow));//
		m_Statusbar.SetPaneText(3, g_PassthruStatus);
		m_Statusbar.SetPaneText(5, sysTime);

		if ((g_GlobalStatus>>8)&0x01)		// 判断Five Baud和Fast Cmd是否设置
			SetTextColor(RGB(66,204,33), IDC_ED_INITSTATUS, L"Status Ok");
		else
			SetTextColor(RGB(255,0,0), IDC_ED_INITSTATUS, L"Status None");

		if ((g_GlobalStatus>>3)&0x01)		// 判断过滤器是否设置
			SetTextColor(RGB(66,204,33), IDC_ED_FILTERSTATUS, L"Status Ok");
		else
			SetTextColor(RGB(255,0,0), IDC_ED_FILTERSTATUS, L"Status None");

		if ((g_GlobalStatus>>4)&0x01)		// 判断定时器是否设置
			SetTextColor(RGB(66,204,33), IDC_ED_PERIODICSTATUS, L"Status Ok");
		else
			SetTextColor(RGB(255,0,0), IDC_ED_PERIODICSTATUS, L"Status None");

		if ((g_GlobalStatus>>5)&0x01)		// 判断定时器是否设置
			SetTextColor(RGB(66,204,33), IDC_ED_PARAMCONFIGSTATUS, L"Status Ok");
		else
			SetTextColor(RGB(255,0,0), IDC_ED_PARAMCONFIGSTATUS, L"Status None");
	}
	CDialogEx::OnTimer(nIDEvent);
}




void CJ2534ToolDemoDlg::OnBnClickedBtnLoaddll()
{
	UINT32_T RetVal = 0;
	// 标识设备已经加载
	SetBit(g_GlobalStatus,0);	
	// 加载当前驱动
	UINT16_T iInterfaceIndex = ((CComboBox*)GetDlgItem(IDC_CB_INTERFACENAME))->GetCurSel();
	RetVal = J2534ToolDemoSdk.LoadDll(regDeviceInfo.regDevice[iInterfaceIndex].szRegeditPath);
	if(RetVal == 0)
	{
		PASSTHRUMESSAGEVIEW(RetVal, L"PassThruLoadDll()-Error");
		SetJ2534ToolDemoIsLoadDllBtnEnable(TRUE, FALSE);
	}
	else
	{
		PASSTHRUMESSAGEVIEW(0, L"PassThruLoadDll()-OK");
		SetJ2534ToolDemoIsLoadDllBtnEnable(FALSE, TRUE);
		SetJ2534ToolDemoOpenDeviceEnable(TRUE);
	}
}

void CJ2534ToolDemoDlg::OnBnClickedBtnUnloaddll()
{
	g_ThreadLock.Lock();
	J2534ToolDemoSdk.UnLoadDll();
	g_ThreadLock.Unlock();
	PASSTHRUMESSAGEVIEW(0, L"PassThruUnLoadDll()-OK");
	SetJ2534ToolDemoIsLoadDllBtnEnable(TRUE, FALSE);
	SetJ2534ToolDemoOpenDeviceEnable(FALSE);
}

void CJ2534ToolDemoDlg::OnBnClickedBtnOpendevice()
{
	UINT32_T ulDeviceId = 0, RetVal = 0;
	// 标识设备已经打开
	SetBit(g_GlobalStatus,1);

	INT8_T FwVersion[MAX_PATH] = "";		// 返回固件本版号
	INT8_T DllVersion[MAX_PATH] = "";		// 返回DLL版本号
	INT8_T ApiVersion[MAX_PATH] = "";		// 必须为02.02或者04.04

	CString strDeviceName;
	GetDlgItem(IDC_CB_DEVICENAME)->GetWindowText(strDeviceName);
	if(strDeviceName == "")	
		strDeviceName = L"J2534 Tool " + UINT32ToDecString(g_DevicesCount+1);

	// 转换UNICODE字符串到ANSI字符串
	INT16_T WideCharLen = WideCharToMultiByte(CP_ACP, 0, strDeviceName, -1, NULL, 0, NULL, NULL);// 先取得转换后的ANSI字符串所需的长度
	INT8_T* pDeviceName = (INT8_T*)calloc(WideCharLen, sizeof(INT8_T));							// 分配缓冲区
	WideCharToMultiByte(CP_ACP, 0, strDeviceName, -1, pDeviceName, WideCharLen, NULL, NULL);	// 开始转换                                                // 输出转换后的字符串

	RetVal = J2534ToolDemoSdk.OpenDevice(pDeviceName);
	PASSTHRUMESSAGEVIEW(RetVal, L"PassThruOpen()-OK");
	g_DevicesCount++;

	free(pDeviceName);

	RetVal = J2534ToolDemoSdk.ReadVersion(FwVersion, DllVersion, ApiVersion);
	PASSTHRUMESSAGEVIEW(RetVal, L"PassThruReadVersion()-OK");

	INT8_T cTemp[MAX_PATH] = "Firmware Version = ";
	strcat_s(cTemp, FwVersion);
	strcat_s(cTemp, "\rJ2534DLL Version = ");
	strcat_s(cTemp, DllVersion);
	strcat_s(cTemp, "\rJ2534API Version = ");
	strcat_s(cTemp, ApiVersion);
	CString strTemp(cTemp);
	GetDlgItem(IDC_ST_OPENDEVICEMSG)->SetWindowTextW(strTemp);

	GetDlgItem(IDC_ST_OPENDEVICESTATUS)->SetWindowTextW(L"Device Status = Open");

	if(J2534ToolDemoSdk.GetDeviceId()!=0)
	{
		((CComboBox*)GetDlgItem(IDC_CB_DEVICENAME))->ResetContent();
		((CComboBox*)GetDlgItem(IDC_CB_DEVICENAME))->InsertString(0, strDeviceName);
		((CComboBox*)GetDlgItem(IDC_CB_DEVICENAME))->SetCurSel(0);
	}

	// Clear List-Ctl
	m_ListMessageRow = 0;
	uListMessageCount = 0;
	m_ListMessage.DeleteAllItems();

	/* 初始化添加协议类型 */	
	LoadInitProtocolParam();

	/* 设置默认配置参数 */	
	SetDefaultConfigParam();

	SetJ2534ToolDemoConnectDeviceEnable(TRUE);
	GetDlgItem(IDC_ED_CONNECTFLAGSVALUE)->SetWindowTextW(L"0x00000000");

	((CComboBox*)GetDlgItem(IDC_CB_PROTOCOLNAME))->SetCurSel(0);
	OnCbnSelchangeCbProtocolname();
}

void CJ2534ToolDemoDlg::OnBnClickedBtnClosedevice()
{
	UINT32_T RetVal = 0;
	// 标识设备已经关闭
	g_GlobalStatus = 1;			// 其他都关闭，只是LoadDll仍然打开

	g_ThreadLock.Lock();
	if(hRevMsgThread != 0)
		TerminateThread(hRevMsgThread, 0);
	hRevMsgThread = NULL;

	if(J2534ToolDemoSdk.GetDeviceId() == 0)
		return;

	RetVal = J2534ToolDemoSdk.CloseDevice();
	g_ThreadLock.Unlock();
	PASSTHRUMESSAGEVIEW(RetVal, L"PassThruClose()-OK");
	g_DevicesCount = 0;
	g_ChannelsCount = 0;
	g_ChannelsMax = 0;

	INT8_T cTemp[MAX_PATH] = "Firmware Version = 0";
	strcat_s(cTemp, "\rJ2534DLL Version = 0");
	strcat_s(cTemp, "\rJ2534API Version = 0");
	CString strTemp(cTemp);
	GetDlgItem(IDC_ST_OPENDEVICEMSG)->SetWindowTextW(strTemp);

	GetDlgItem(IDC_ST_OPENDEVICESTATUS)->SetWindowTextW(L"Device Status = Avaliable");

	((CComboBox*)GetDlgItem(IDC_CB_CHANNELIDVALUE))->ResetContent();
	((CComboBox*)GetDlgItem(IDC_CB_SENDPROTOCOLNAME))->ResetContent();
	SetDefaultConfigParam();

	SetJ2534ToolDemoConnectDeviceEnable(FALSE);
	SetJ2534ToolDemoConfigParamEnable(FALSE);
	SetJ2534ToolDemoSendMsgDataEnable(FALSE);
	SetJ2534ToolDemoRecvProtocolCkEnable(FALSE);
}

void CJ2534ToolDemoDlg::OnBnClickedBtnConnectprotocol()
{
	CString strFlags, strProtocolName, strBaudRate;
	UINT32_T RetVal = 0;
	ASSERT(J2534ToolDemoSdk.GetDeviceId() != 0);
	if(J2534ToolDemoSdk.GetDeviceId() == 0)
		return;

	// 标识设备连接成功
	SetBit(g_GlobalStatus, 2);

	GetDlgItemText(IDC_ED_CONNECTFLAGSVALUE,strFlags);
	((CComboBox*)GetDlgItem(IDC_CB_PROTOCOLNAME))->GetLBText(((CComboBox*)GetDlgItem(IDC_CB_PROTOCOLNAME))->GetCurSel(), strProtocolName);
	((CComboBox*)GetDlgItem(IDC_CB_BAUDRATEVALUE))->GetLBText(((CComboBox*)GetDlgItem(IDC_CB_BAUDRATEVALUE))->GetCurSel(), strBaudRate);

	RetVal = J2534ToolDemoSdk.Connect(ProtocolStrToValue(strProtocolName), _tcstol(strBaudRate, 0, 10), _tcstol(strFlags, 0, 16));
	PASSTHRUMESSAGEVIEW(RetVal, L"PassThruConnect()-OK");

	((CComboBox*)GetDlgItem(IDC_CB_CHANNELIDVALUE))->InsertString(J2534ToolDemoSdk.GetCurrChannelId()-1, L"Channel "+UINT8ToDecStirng(J2534ToolDemoSdk.GetCurrChannelId()));
	((CComboBox*)GetDlgItem(IDC_CB_CHANNELIDVALUE))->SetCurSel(J2534ToolDemoSdk.GetCurrChannelId()-1);

	g_ConnectInfo[J2534ToolDemoSdk.GetCurrChannelId()-1].ulProtocolIndex = ((CComboBox*)GetDlgItem(IDC_CB_PROTOCOLNAME))->GetCurSel();
	g_ConnectInfo[J2534ToolDemoSdk.GetCurrChannelId()-1].ulBaudRateIndex = ((CComboBox*)GetDlgItem(IDC_CB_BAUDRATEVALUE))->GetCurSel();
	g_ConnectInfo[J2534ToolDemoSdk.GetCurrChannelId()-1].ulConnectFlags =  _tcstol(strFlags, 0, 16);
	g_ConnectInfo[J2534ToolDemoSdk.GetCurrChannelId()-1].bExist = TRUE;

	// 用于接收消息时，协议显示
	saveChannelRecvInfo[J2534ToolDemoSdk.GetCurrChannelId()-1].ChannelNumber = J2534ToolDemoSdk.GetCurrChannelId();
	saveChannelRecvInfo[J2534ToolDemoSdk.GetCurrChannelId()-1].uProtocolValue = ProtocolStrToValue(strProtocolName);
	saveChannelRecvInfo[J2534ToolDemoSdk.GetCurrChannelId()-1].bStatus = TRUE;

	g_ChannelsCount++;
	g_ChannelsMax++;

	// 加载Ioctl配置初始化值
	LoadInitIoctlParam();

	// 使能配置参数控件
	SetJ2534ToolDemoConfigParamEnable(TRUE);

	// 设置发送消息控件
	SetJ2534ToolDemoSendMsgData();
	SetJ2534ToolDemoSendMsgDataEnable(TRUE);
	SetJ2534ToolDemoRecvChannelDisplay(J2534ToolDemoSdk.GetCurrChannelId(), TRUE);

	GetDlgItem(IDC_BTN_DISCONNECTPROTOCOL)->EnableWindow(TRUE);
}

void CJ2534ToolDemoDlg::OnBnClickedBtnDisconnectprotocol()
{
	UINT32_T RetVal = 0, uCurrChannelId = 0;;
	CString	 strChannelName = L"";

	// 没有连接通道时，不提供断开连接
	if(((CComboBox*)GetDlgItem(IDC_CB_CHANNELIDVALUE))->GetCount() == 0)
		return;

	// 标识设备断开连接
	ClearBit(g_GlobalStatus, 2);

	g_ThreadLock.Lock();

	if((hRevMsgThread != 0)&&(g_ChannelsCount == 1))
	{	
		//g_ThreadLock.Lock();
		TerminateThread(hRevMsgThread, 0);
		hRevMsgThread = NULL;
		//TerminateThread(hViewMsgThread, 0);
		//hViewMsgThread = NULL;
		//g_ThreadLock.Unlock();
	}
	
	/* 获取当前通道号 */
	((CComboBox*)GetDlgItem(IDC_CB_CHANNELIDVALUE))->GetLBText(((CComboBox*)GetDlgItem(IDC_CB_CHANNELIDVALUE))->GetCurSel(), strChannelName);
	uCurrChannelId = ChannelStrToValue(strChannelName);

	RetVal = J2534ToolDemoSdk.DisConnect(uCurrChannelId);	
	PASSTHRUMESSAGEVIEW(RetVal, L"PassThruDisConnect()-OK");
	
	g_ChannelsCount--;

	for(UINT8_T ucLoop = 0; ucLoop < CONNECT_MAX_COUNT; ucLoop++)
	{
		if(J2534ToolDemoSdk.m_Channel[ucLoop].GetChannelExist() != FALSE)
		{
			((CComboBox*)GetDlgItem(IDC_CB_PROTOCOLNAME))->SetCurSel(g_ConnectInfo[ucLoop].ulProtocolIndex);	
			SetConnectDeviceControls(g_ConnectInfo[ucLoop].ulBaudRateIndex);
			GetDlgItem(IDC_ED_CONNECTFLAGSVALUE)->SetWindowTextW(UINT32ToHexString(g_ConnectInfo[ucLoop].ulConnectFlags,8));
			break;
		}
	}

	((CComboBox*)GetDlgItem(IDC_CB_CHANNELIDVALUE))->DeleteString(((CComboBox*)GetDlgItem(IDC_CB_CHANNELIDVALUE))->GetCurSel());
	((CComboBox*)GetDlgItem(IDC_CB_CHANNELIDVALUE))->SetCurSel(0);
	((CComboBox*)GetDlgItem(IDC_CB_SENDPROTOCOLNAME))->DeleteString(((CComboBox*)GetDlgItem(IDC_CB_SENDPROTOCOLNAME))->GetCurSel());
	((CComboBox*)GetDlgItem(IDC_CB_SENDPROTOCOLNAME))->SetCurSel(0);

	SetJ2534ToolDemoSendMsgData();

	// 接收数据项CheckBox清除
	//SetJ2534ToolDemoRecvChannelDisplay(uCurrChannelId, FALSE);	// 待修改

	// 清除已设置的标记
	ClearCurrChannelConfigParam(uCurrChannelId);

	ClearBit(g_GlobalStatus, 3);
	ClearBit(g_GlobalStatus, 4);
	ClearBit(g_GlobalStatus, 5);
	ClearBit(g_GlobalStatus, 8);

	// 用于接收消息时，协议显示
	//saveChannelRecvInfo[uCurrChannelId-1].ChannelNumber = 0;
	//saveChannelRecvInfo[uCurrChannelId-1].uProtocolValue = 0;
	//saveChannelRecvInfo[uCurrChannelId-1].bStatus = FALSE;

	g_ThreadLock.Unlock();
}

void CJ2534ToolDemoDlg::OnBnClickedBtnSendmessage()
{
	ASSERT(J2534ToolDemoSdk.GetDeviceId() != 0);
	if (J2534ToolDemoSdk.GetDeviceId()==0)	return;

	TerminateThread(hSendMsgThread, 0);
	hSendMsgThread = NULL;

	if(hSendMsgThread == 0)
		hSendMsgThread = ::CreateThread(NULL, 0, ThreadSendMsgFun, this, 0, NULL);	//创建线程
}


void CJ2534ToolDemoDlg::OnBnClickedBtnRecvmessage()
{
	ASSERT(J2534ToolDemoSdk.GetDeviceId() != 0);
	if (J2534ToolDemoSdk.GetDeviceId()==0)	return;

	if(hRevMsgThread == 0)
		hRevMsgThread = ::CreateThread(NULL, 0, ThreadRevMsgFun, this, 0, NULL);	//创建线程

	SetBit(g_GlobalStatus, 7);
}

void CJ2534ToolDemoDlg::OnBnClickedBtnStoprecvmessage()
{
	ASSERT(J2534ToolDemoSdk.GetDeviceId() != 0);
	if (J2534ToolDemoSdk.GetDeviceId()==0)
		return;
	g_ThreadLock.Lock();
	if(hRevMsgThread != 0)
	{
		TerminateThread(hRevMsgThread,0);	//TerminateThread(MyThread->m_hThread , 0);
		hRevMsgThread = NULL;
	}
	ClearBit(g_GlobalStatus, 7);
	g_ThreadLock.Unlock();
}

void CJ2534ToolDemoDlg::OnBnClickedBtnClearmessage()
{
	// Clear List-Ctl
	m_ListMessageRow = 0;
	uListMessageCount = 0;
	m_ListMessage.DeleteAllItems();
}

/************************************************************
	*功		 能：调整控件大小
	*参 数 说 明：
	-nType
		Specifies the type of resizing requested. This parameter can be one of the following values:
		指定了要求的调整大小的类型。这个参数可以是下列值之一：

		SIZE_MAXIMIZED   Window has been maximized.
		SIZE_MAXIMIZED 窗口已经被最大化。 

		SIZE_MINIMIZED   Window has been minimized.
		SIZE_MINIMIZED 窗口已经被最小化 

		SIZE_RESTORED   Window has been resized, but neither SIZE_MINIMIZED nor SIZE_MAXIMIZED applies.
		SIZE_RESTORED 窗口被改变了大小，但SIZE_MINIMIZED和SIZE_ MAXIMIZED 都不适用。 

		SIZE_MAXHIDE   Message is sent to all pop-up windows when some other window is maximized.
		SIZE_MAXHIDE 当其它窗口被最大化时，消息被发送到所有的弹出窗口。 

		SIZE_MAXSHOW   Message is sent to all pop-up windows when some other window has been restored to its former size.
		SIZE_MAXSHOW 当其它窗口被恢复到原来的大小时，消息被发送到所有的弹出窗口 
	-cx
		Specifies the new width of the client area.
		指定了客户区域的新宽度
	-cy
		Specifies the new height of the client area.
		指定了客户区域的新高度

	*返  回	 值：无
	*说	     明：无
************************************************************/
void CJ2534ToolDemoDlg::OnSize(UINT nType, int cx, int cy)
{
	CDialogEx::OnSize(nType, cx, cy);

	CWnd *pCtrl=this->GetWindow(GW_CHILD);

	while(pCtrl&&nType!=1) //判断是否为空，因为对话框创建时会调用此函数，而当时控件还未创建
	{ 
		CRect rect; 

		//获取控件变化前大小
		pCtrl->GetWindowRect(&rect); 
		//得到主窗口的大小
		ScreenToClient(&rect);
		//将控件大小转换为在对话框中的区域坐标
		if (m_rect.Width() == 0) { 
			break; 
		}

		//调整控件大小
		rect.left=rect.left*cx/m_rect.Width();
		rect.right=rect.right*cx/m_rect.Width(); 
		rect.top=rect.top*cy/m_rect.Height();  
		rect.bottom=rect.bottom*cy/m_rect.Height(); 

		//设置控件大小
		pCtrl->MoveWindow(rect);

		pCtrl=pCtrl->GetNextWindow(); 
	}
	//将变化后的对话框大小设为旧大小
	GetClientRect(&m_rect);
}


void CJ2534ToolDemoDlg::OnBnClickedBtnSetinit()
{
	pInitConfig.m_InitSdk = J2534ToolDemoSdk;
	pInitConfig.DoModal();
}


void CJ2534ToolDemoDlg::OnBnClickedBtnSetfilter()
{
	pFilterConfig.m_FilterSdk = J2534ToolDemoSdk;
	pFilterConfig.DoModal();
}


void CJ2534ToolDemoDlg::OnBnClickedBtnSetperiodic()
{
	pPeriodicConfig.m_PeriodicSdk = J2534ToolDemoSdk;
	pPeriodicConfig.DoModal();
}


void CJ2534ToolDemoDlg::OnBnClickedBtnSetparamconfig()
{
	pParamConfig.m_ParamConfigSdk = J2534ToolDemoSdk;
	pParamConfig.DoModal();
}
