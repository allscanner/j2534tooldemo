/*******************************************************************************
 * FILE: FilterConfig.h
 * This file is part of ALLScanner VCX software.
 * COPYRIGHT (C) ALLScanner 2008-2014. 
 *		
 * DESC: J2534ToolDemoDlg's Defines 
 * 
 * HISTORY:
 * Date			Author		Notes	
 * 2016-07-01		my		Create
*******************************************************************************/
// J2534ToolDemoDlg.h : 头文件
//

#pragma once

#include "../J2534/PassThruSdk.h"
#include "../J2534/MakeLock.h"
#include "InitConfig.h"
#include "FilterConfig.h"
#include "PeriodicConfig.h"
#include "ParamConfig.h"
#include "Common/applications.h"
#include "Common/ErrorCode.h"
#include "Common/CString.h"

#include "afxcmn.h"
#include <winuser.h>
#include <locale.h>

#include <iostream>
#include <deque>

// CJ2534ToolDemoDlg 对话框
class CJ2534ToolDemoDlg : public CDialogEx
{
// 构造
public:
	CJ2534ToolDemoDlg(CWnd* pParent = NULL);	// 标准构造函数
	
	FilterConfig	pFilterConfig;
	PeriodicConfig	pPeriodicConfig;
	ParamConfig		pParamConfig;
	InitConfig		pInitConfig;
	PassThruSdk		J2534ToolDemoSdk;

	CListCtrl		m_ListMessage;
	CMenu		m_Menu;
	CStatusBar	m_Statusbar;

	CMFCButton	m_BtnLoadDll;
	CMFCButton	m_BtnRecvMsgs, m_BtnStopRecvMsgs, m_BtnClear;
	CMFCButton	m_BtnOpenDevice, m_BtnCloseDevice;

	CRect m_rect;

private:
	COLORREF	m_FilterForeColor, m_PeriodicForeColor, m_ParamConfigForeColor, m_InitConfigForeColor;        //文本颜色
	CFont*		pFont;
	CBrush	pBrush;

// 对话框数据
	enum { IDD = IDD_J2534TOOLDEMO_DIALOG };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV 支持


// 实现
protected:
	HICON m_hIcon;

	// 生成的消息映射函数
	virtual BOOL OnInitDialog();
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnBnClickedBtnLoaddll();
	afx_msg void OnBnClickedBtnUnloaddll();
	afx_msg void OnBnClickedBtnOpendevice();
	afx_msg void OnBnClickedBtnClosedevice();
	afx_msg void OnBnClickedBtnConnectprotocol();
	afx_msg void OnBnClickedBtnDisconnectprotocol();
	afx_msg void OnBnClickedBtnSendmessage();
	afx_msg void OnBnClickedBtnRecvmessage();
	afx_msg void OnBnClickedBtnStoprecvmessage();
	afx_msg void OnBnClickedBtnClearmessage();

	afx_msg void OnCbnSelchangeCbInterfacename();
	afx_msg void OnCbnSelchangeCbProtocolname();
	afx_msg void OnCbnSelchangeCbChannelidvalue();
	afx_msg void OnCbnSelchangeCbSendprotocolname();
	
	afx_msg void OnItemchangedListMessageView(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnRclickListMessageView(NMHDR *pNMHDR, LRESULT *pResult);

	afx_msg void OnTimer(UINT_PTR nIDEvent);

	UINT16_T m_ListMessageRow;		//表示消息列表中的行数
	UINT16_T m_ListMessageColumn;	//表示消息列表中的列数	

	HBRUSH OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor);
	void SetTextColor(COLORREF color, UINT16_T ActiveXID, CString strContent);      //设置文本颜色
	void SetTextFont(UINT16_T nHeight, UINT16_T nWeight, BYTE bItalic, UINT16_T ActiveXID);//设置字体 

	void SetJ2534ToolDemoIsLoadDllBtnEnable(BOOL btnLoad, BOOL btnUnload);
	void SetJ2534ToolDemoOpenDeviceEnable(BOOL bVal);
	void SetJ2534ToolDemoConnectDeviceEnable(BOOL bVal);
	void SetJ2534ToolDemoConfigParamEnable(BOOL bVal);
	void ClearCurrChannelConfigParam(UINT32_T ulchannelId);
	void SetDefaultConfigParam();
	void SetBaudRateValue(CString strProtocolName);
	void InitJ2345Interface();
	void LoadInitProtocolParam();
	void InitUIControls();
	void InitListMessageView();
	void InitUIStatusBar();
	void InitUIConfigView();
	void LoadInitIoctlParam();
	void LoadButtonPicture();
	void SetJ2534ToolDemoSendMsgData();
	void SetJ2534ToolDemoSendMsgDataEnable(BOOL bVal);
	void SetJ2534ToolDemoRecvChannelDisplay(UINT32_T ulchannelId, BOOL bVal);
	void SetJ2534ToolDemoRecvProtocolCkEnable(BOOL bVal);
	void SetConnectDeviceControls(UINT32_T ulBaudRateIndex);
	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg void OnBnClickedBtnSetinit();
	afx_msg void OnBnClickedBtnSetfilter();
	afx_msg void OnBnClickedBtnSetperiodic();
	afx_msg void OnBnClickedBtnSetparamconfig();
};

/************************************************************
	*功		 能：ListCtrl控件列头信息
	*参 数 说 明：无
	*返  回	 值：无
	*说	     明：无
************************************************************/
typedef struct tagListctrlContent
{
	UINT16_T ListRow;
	CString sTimestamp;
	CString sSender;
	CString	sChannelId;
	CString	sNetWorkId;
	CString	sRxStatus;
	CString sDataBytes;
}ListctrlContent;
