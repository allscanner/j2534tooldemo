﻿// ParamConfig.cpp : 实现文件
//

#include "stdafx.h"
#include "J2534ToolDemo.h"
#include "ParamConfig.h"
#include "afxdialogex.h"


// ParamConfig 对话框

IMPLEMENT_DYNAMIC(ParamConfig, CDialogEx)

ParamConfig::ParamConfig(CWnd* pParent /*=NULL*/)
	: CDialogEx(ParamConfig::IDD, pParent)
{ 
}

ParamConfig::~ParamConfig()
{
}

void ParamConfig::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(ParamConfig, CDialogEx)
	ON_BN_CLICKED(IDC_BTN_PARAMCONFIG_SET, &ParamConfig::OnBnClickedBtnParamconfigSet)
	ON_CBN_SELCHANGE(IDC_CB_PARAMCONFIG_PROTOCOL, &ParamConfig::OnCbnSelchangeCbParamconfigProtocol)
	ON_WM_SYSCOMMAND()
END_MESSAGE_MAP()

// Global Variables
ResourID g_ResId;
UINT32_T uParamconfigCurrChannelNum = 0;

extern IoctlParam saveioctldata;
extern ChannelIoctlParams saveChannelioctldata[CONNECT_MAX_COUNT];
extern UINT32_T g_GlobalStatus;
extern CErrorCode _err;
extern CString g_PassthruStatus;
extern UINT32_T g_DevicesCount;
extern Mutex g_ThreadLock;
#if 1
UINT16_T _arrayStId[PARAM_COUNT_MAX] = {IDC_ST_NAME1, IDC_ST_NAME2, IDC_ST_NAME3, IDC_ST_NAME4, IDC_ST_NAME5, 
	IDC_ST_NAME6, IDC_ST_NAME7, IDC_ST_NAME8, IDC_ST_NAME9, IDC_ST_NAME10, IDC_ST_NAME11, IDC_ST_NAME12, IDC_ST_NAME13, 
	IDC_ST_NAME14, IDC_ST_NAME15, IDC_ST_NAME16, IDC_ST_NAME17, IDC_ST_NAME18, IDC_ST_NAME19, IDC_ST_NAME20, 
	IDC_ST_NAME21, IDC_ST_NAME22, IDC_ST_NAME23, IDC_ST_NAME24, IDC_ST_NAME25, IDC_ST_NAME26, IDC_ST_NAME27, 
	IDC_ST_NAME28, IDC_ST_NAME29, IDC_ST_NAME30};

UINT16_T _arrayEdValueId[PARAM_COUNT_MAX] = {IDC_ED_VALUE1, IDC_ED_VALUE2, IDC_ED_VALUE3, IDC_ED_VALUE4, IDC_ED_VALUE5, 
	IDC_ED_VALUE6, IDC_ED_VALUE7, IDC_ED_VALUE8, IDC_ED_VALUE9, IDC_ED_VALUE10, IDC_ED_VALUE11, IDC_ED_VALUE12, 
	IDC_ED_VALUE13, IDC_ED_VALUE14, IDC_ED_VALUE15, IDC_ED_VALUE16, IDC_ED_VALUE17, IDC_ED_VALUE18, IDC_ED_VALUE19, 
	IDC_ED_VALUE20, IDC_ED_VALUE21, IDC_ED_VALUE22, IDC_ED_VALUE23, IDC_ED_VALUE24, IDC_ED_VALUE25, IDC_ED_VALUE26, 
	IDC_ED_VALUE27, IDC_ED_VALUE28, IDC_ED_VALUE29, IDC_ED_VALUE30};

UINT16_T _arrayEdModifyId[PARAM_COUNT_MAX] = {IDC_ED_MODIFY1, IDC_ED_MODIFY2, IDC_ED_MODIFY3, IDC_ED_MODIFY4, 
	IDC_ED_MODIFY5, IDC_ED_MODIFY6, IDC_ED_MODIFY7, IDC_ED_MODIFY8, IDC_ED_MODIFY9, IDC_ED_MODIFY10, IDC_ED_MODIFY11, 
	IDC_ED_MODIFY12, IDC_ED_MODIFY13, IDC_ED_MODIFY14, IDC_ED_MODIFY15, IDC_ED_MODIFY16, IDC_ED_MODIFY17, IDC_ED_MODIFY18, 
	IDC_ED_MODIFY19, IDC_ED_MODIFY20, IDC_ED_MODIFY21, IDC_ED_MODIFY22, IDC_ED_MODIFY23, IDC_ED_MODIFY24, IDC_ED_MODIFY25, 
	IDC_ED_MODIFY26, IDC_ED_MODIFY27, IDC_ED_MODIFY28, IDC_ED_MODIFY29, IDC_ED_MODIFY30};
#endif

void ParamConfig::InitStructOperate(ResourID pResId)
{
	memset(pResId.uiStIdSrc, 0, PARAM_COUNT_MAX);
	memset(pResId.uiStIdDest, 0, PARAM_COUNT_MAX);
	memset(pResId.uiEdValueIdSrc, 0, PARAM_COUNT_MAX);
	memset(pResId.uiEdValueIdDest, 0, PARAM_COUNT_MAX);
	memset(pResId.uiEdModifyIdSrc, 0, PARAM_COUNT_MAX);
	memset(pResId.uiEdModifyIdDest, 0, PARAM_COUNT_MAX);
	pResId.cParamCount = 0;
}

void ParamConfig::InitControlsDisVisible()
{
	/* 初始化将协议配置参数控件 隐藏 */
	for (UINT8_T uiLoop=0; uiLoop<PARAM_COUNT_MAX; uiLoop++)
	{
		GetDlgItem(_arrayStId[uiLoop])->ShowWindow(SW_HIDE);
		GetDlgItem(_arrayEdValueId[uiLoop])->ShowWindow(SW_HIDE);
		GetDlgItem(_arrayEdModifyId[uiLoop])->ShowWindow(SW_HIDE);
	}
}

void ParamConfig::InitUIDefaultValue()
{
	/* 配置参数显示 */
	((CEdit*)GetDlgItem(IDC_ED_VALUE_DATA_RATE))->SetWindowTextW(UInitToString(saveChannelioctldata[uParamconfigCurrChannelNum-1]._ioctlParam.ulDataRate, 10));
	((CEdit*)GetDlgItem(IDC_ED_VALUE_LOOPBACK))->SetWindowTextW(UInitToString(saveChannelioctldata[uParamconfigCurrChannelNum-1]._ioctlParam.bLookBack, 10));
	switch (m_ParamConfigSdk.m_Channel[uParamconfigCurrChannelNum-1].GetProtocolId())
	{
	case J1850VPW:
		break;
	case J1850PWM:
		((CEdit*)GetDlgItem(IDC_ED_VALUE_NODE_ADDRESS))->SetWindowTextW(UInitToString(saveChannelioctldata[uParamconfigCurrChannelNum-1]._ioctlParam.ucNodeAddress, 16));
		((CEdit*)GetDlgItem(IDC_ED_VALUE_NETWORK_LINE))->SetWindowTextW(UInitToString(saveChannelioctldata[uParamconfigCurrChannelNum-1]._ioctlParam.ucNetworkLine, 10));
		break;
	case ISO9141:
	case ISO9141_PS:
	case ISO14230:
		((CEdit*)GetDlgItem(IDC_ED_VALUE_P1_MAX))->SetWindowTextW(UInitToString(saveChannelioctldata[uParamconfigCurrChannelNum-1]._ioctlParam.uiP1Max, 10));
		((CEdit*)GetDlgItem(IDC_ED_VALUE_P3_MIN))->SetWindowTextW(UInitToString(saveChannelioctldata[uParamconfigCurrChannelNum-1]._ioctlParam.uiP3Min, 10));
		((CEdit*)GetDlgItem(IDC_ED_VALUE_P4_MIN))->SetWindowTextW(UInitToString(saveChannelioctldata[uParamconfigCurrChannelNum-1]._ioctlParam.uiP4Min, 10));
		((CEdit*)GetDlgItem(IDC_ED_VALUE_W0))->SetWindowTextW(UInitToString(saveChannelioctldata[uParamconfigCurrChannelNum-1]._ioctlParam.uiW0, 10));
		((CEdit*)GetDlgItem(IDC_ED_VALUE_W1))->SetWindowTextW(UInitToString(saveChannelioctldata[uParamconfigCurrChannelNum-1]._ioctlParam.uiW1, 10));
		((CEdit*)GetDlgItem(IDC_ED_VALUE_W2))->SetWindowTextW(UInitToString(saveChannelioctldata[uParamconfigCurrChannelNum-1]._ioctlParam.uiW2, 10));
		((CEdit*)GetDlgItem(IDC_ED_VALUE_W3))->SetWindowTextW(UInitToString(saveChannelioctldata[uParamconfigCurrChannelNum-1]._ioctlParam.uiW3, 10));
		((CEdit*)GetDlgItem(IDC_ED_VALUE_W4))->SetWindowTextW(UInitToString(saveChannelioctldata[uParamconfigCurrChannelNum-1]._ioctlParam.uiW4, 10));
		((CEdit*)GetDlgItem(IDC_ED_VALUE_W5))->SetWindowTextW(UInitToString(saveChannelioctldata[uParamconfigCurrChannelNum-1]._ioctlParam.uiW5, 10));
		((CEdit*)GetDlgItem(IDC_ED_VALUE_TIDLE))->SetWindowTextW(UInitToString(saveChannelioctldata[uParamconfigCurrChannelNum-1]._ioctlParam.uiTidle, 10));
		((CEdit*)GetDlgItem(IDC_ED_VALUE_TINIL))->SetWindowTextW(UInitToString(saveChannelioctldata[uParamconfigCurrChannelNum-1]._ioctlParam.uiTinil, 10));
		((CEdit*)GetDlgItem(IDC_ED_VALUE_TWUP))->SetWindowTextW(UInitToString(saveChannelioctldata[uParamconfigCurrChannelNum-1]._ioctlParam.uiTwup, 10));
		((CEdit*)GetDlgItem(IDC_ED_VALUE_PARITY))->SetWindowTextW(UInitToString(saveChannelioctldata[uParamconfigCurrChannelNum-1]._ioctlParam.ucParity, 10));
		((CEdit*)GetDlgItem(IDC_ED_VALUE_DATA_BITS))->SetWindowTextW(UInitToString(saveChannelioctldata[uParamconfigCurrChannelNum-1]._ioctlParam.bDataBits, 10));
		((CEdit*)GetDlgItem(IDC_ED_VALUE_FIVE_BAUD_MOD))->SetWindowTextW(UInitToString(saveChannelioctldata[uParamconfigCurrChannelNum-1]._ioctlParam.ucFiveBaudMod, 10));
		if(m_ParamConfigSdk.m_Channel[uParamconfigCurrChannelNum-1].GetProtocolId() == ISO9141_PS)
			((CEdit*)GetDlgItem(IDC_ED_VALUE_J1962_PINS))->SetWindowTextW(UInitToString(saveChannelioctldata[uParamconfigCurrChannelNum-1]._ioctlParam.uiJ1962Pins, 16, 4));//SetWindowTextW(L"0x0800");
		break;
	case CAN:
	case CAN_PS:
		if(m_ParamConfigSdk.m_Channel[uParamconfigCurrChannelNum-1].GetProtocolId() == CAN_PS)
			((CEdit*)GetDlgItem(IDC_ED_VALUE_J1962_PINS))->SetWindowTextW(UInitToString(saveChannelioctldata[uParamconfigCurrChannelNum-1]._ioctlParam.uiJ1962Pins, 16, 4));

		((CEdit*)GetDlgItem(IDC_ED_VALUE_LOOPBACK))->SetWindowTextW(UInitToString(saveChannelioctldata[uParamconfigCurrChannelNum-1]._ioctlParam.bLookBack, 10));
		((CEdit*)GetDlgItem(IDC_ED_VALUE_BIT_SAMPLE_POINT))->SetWindowTextW(UInitToString(saveChannelioctldata[uParamconfigCurrChannelNum-1]._ioctlParam.ucBitSamplePoint, 10));
		((CEdit*)GetDlgItem(IDC_ED_VALUE_SYNC_JUMP_WIDTH))->SetWindowTextW(UInitToString(saveChannelioctldata[uParamconfigCurrChannelNum-1]._ioctlParam.ucSyncJumpWidth, 10));	
		break;
	case ISO15765:
	case ISO15765_PS:
	case VOLVO_CAN2:
		if(m_ParamConfigSdk.m_Channel[uParamconfigCurrChannelNum-1].GetProtocolId() == ISO15765_PS)
			((CEdit*)GetDlgItem(IDC_ED_VALUE_J1962_PINS))->SetWindowTextW(UInitToString(saveChannelioctldata[uParamconfigCurrChannelNum-1]._ioctlParam.uiJ1962Pins, 16, 4));

		((CEdit*)GetDlgItem(IDC_ED_VALUE_BIT_SAMPLE_POINT))->SetWindowTextW(UInitToString(saveChannelioctldata[uParamconfigCurrChannelNum-1]._ioctlParam.ucBitSamplePoint, 10));
		((CEdit*)GetDlgItem(IDC_ED_VALUE_SYNC_JUMP_WIDTH))->SetWindowTextW(UInitToString(saveChannelioctldata[uParamconfigCurrChannelNum-1]._ioctlParam.ucSyncJumpWidth, 10));
		((CEdit*)GetDlgItem(IDC_ED_VALUE_ISO15765_BS))->SetWindowTextW(UInitToString(saveChannelioctldata[uParamconfigCurrChannelNum-1]._ioctlParam.ucIso15765BS, 10));
		((CEdit*)GetDlgItem(IDC_ED_VALUE_ISO15765_STMIN))->SetWindowTextW(UInitToString(saveChannelioctldata[uParamconfigCurrChannelNum-1]._ioctlParam.ucIso15765Stmin, 10));
		((CEdit*)GetDlgItem(IDC_ED_VALUE_BS_TX))->SetWindowTextW(UInitToString(saveChannelioctldata[uParamconfigCurrChannelNum-1]._ioctlParam.uiBsTx, 10));
		((CEdit*)GetDlgItem(IDC_ED_VALUE_STMIN_TX))->SetWindowTextW(UInitToString(saveChannelioctldata[uParamconfigCurrChannelNum-1]._ioctlParam.uiStminTx, 10));
		((CEdit*)GetDlgItem(IDC_ED_VALUE_ISO15765_WFT_MAX))->SetWindowTextW(UInitToString(saveChannelioctldata[uParamconfigCurrChannelNum-1]._ioctlParam.ucIso15765WftMax, 10));
		((CEdit*)GetDlgItem(IDC_ED_VALUE__ISO15765_SIMULTANEOUS))->SetWindowTextW(UInitToString(saveChannelioctldata[uParamconfigCurrChannelNum-1]._ioctlParam.ulIso15765Simultaneous, 10));
		((CEdit*)GetDlgItem(IDC_ED_VALUE_DT_ISO15765_PAD_BYTE))->SetWindowTextW(UInitToString(saveChannelioctldata[uParamconfigCurrChannelNum-1]._ioctlParam.ulDtIso15765PadByte, 16, 2));
		((CEdit*)GetDlgItem(IDC_ED_VALUE_CAN_MIXED_FORMAT))->SetWindowTextW(UInitToString(saveChannelioctldata[uParamconfigCurrChannelNum-1]._ioctlParam.ulCanMixedFormat, 10));
		break;
	case SW_CAN_PS:
	case SW_ISO15765_PS:
		((CEdit*)GetDlgItem(IDC_ED_VALUE_BIT_SAMPLE_POINT))->SetWindowTextW(UInitToString(saveChannelioctldata[uParamconfigCurrChannelNum-1]._ioctlParam.ucBitSamplePoint, 10));
		((CEdit*)GetDlgItem(IDC_ED_VALUE_SYNC_JUMP_WIDTH))->SetWindowTextW(UInitToString(saveChannelioctldata[uParamconfigCurrChannelNum-1]._ioctlParam.ucSyncJumpWidth, 10));
		((CEdit*)GetDlgItem(IDC_ED_VALUE_J1962_PINS))->SetWindowTextW(UInitToString(saveChannelioctldata[uParamconfigCurrChannelNum-1]._ioctlParam.uiJ1962Pins, 16, 4));
		((CEdit*)GetDlgItem(IDC_ED_VALUE_SW_CAN_HS_DATA_RATE))->SetWindowTextW(UInitToString(saveChannelioctldata[uParamconfigCurrChannelNum-1]._ioctlParam.ulSwCanHsDataRate, 10));
		((CEdit*)GetDlgItem(IDC_ED_VALUE_SW_CAN_SPDCHG_ENABLE))->SetWindowTextW(UInitToString(saveChannelioctldata[uParamconfigCurrChannelNum-1]._ioctlParam.ulSwCanSpdchgEnable, 10));
		((CEdit*)GetDlgItem(IDC_ED_VALUE_SW_CAN_RES_SWITCH))->SetWindowTextW(UInitToString(saveChannelioctldata[uParamconfigCurrChannelNum-1]._ioctlParam.ulSwCanResSwitch, 10));
		if(m_ParamConfigSdk.m_Channel[uParamconfigCurrChannelNum-1].GetProtocolId() == SW_ISO15765_PS)
		{
			((CEdit*)GetDlgItem(IDC_ED_VALUE_ISO15765_BS))->SetWindowTextW(UInitToString(saveChannelioctldata[uParamconfigCurrChannelNum-1]._ioctlParam.ucIso15765BS, 10));
			((CEdit*)GetDlgItem(IDC_ED_VALUE_ISO15765_STMIN))->SetWindowTextW(UInitToString(saveChannelioctldata[uParamconfigCurrChannelNum-1]._ioctlParam.ucIso15765Stmin, 10));
			((CEdit*)GetDlgItem(IDC_ED_VALUE_BS_TX))->SetWindowTextW(UInitToString(saveChannelioctldata[uParamconfigCurrChannelNum-1]._ioctlParam.uiBsTx, 10));
			((CEdit*)GetDlgItem(IDC_ED_VALUE_STMIN_TX))->SetWindowTextW(UInitToString(saveChannelioctldata[uParamconfigCurrChannelNum-1]._ioctlParam.uiStminTx, 10));
			((CEdit*)GetDlgItem(IDC_ED_VALUE_ISO15765_WFT_MAX))->SetWindowTextW(UInitToString(saveChannelioctldata[uParamconfigCurrChannelNum-1]._ioctlParam.ucIso15765WftMax, 10));
			((CEdit*)GetDlgItem(IDC_ED_VALUE__ISO15765_SIMULTANEOUS))->SetWindowTextW(UInitToString(saveChannelioctldata[uParamconfigCurrChannelNum-1]._ioctlParam.ulIso15765Simultaneous, 10));
			((CEdit*)GetDlgItem(IDC_ED_VALUE_DT_ISO15765_PAD_BYTE))->SetWindowTextW(UInitToString(saveChannelioctldata[uParamconfigCurrChannelNum-1]._ioctlParam.ulDtIso15765PadByte, 16, 2));
			((CEdit*)GetDlgItem(IDC_ED_VALUE_CAN_MIXED_FORMAT))->SetWindowTextW(UInitToString(saveChannelioctldata[uParamconfigCurrChannelNum-1]._ioctlParam.ulCanMixedFormat, 10));
		}
		break;
	default:
		break;
	}
}

// ParamConfig 消息处理程序
void ParamConfig::CoverSourControlLocal(UINT16_T uiDestControl, UINT16_T uiSourControl)
{
	CWnd *pSour, *pDest;

	pSour = GetDlgItem(uiDestControl);
	pDest = GetDlgItem(uiSourControl);

	CRect rec;
	pSour->GetWindowRect(&rec);		// 获取控件变化前的大小
	ScreenToClient(&rec);				// 将控件大小装换位在对话框中的区域坐标
	pDest->MoveWindow(rec);

	pSour->ShowWindow(SW_HIDE);
	pDest->ShowWindow(SW_SHOW);
}

void ParamConfig::ExChangeControlLocal(UINT16_T uiDestControl, UINT16_T uiSourControl)
{
	CWnd *pSour, *pDest;

	pSour = GetDlgItem(uiDestControl);
	pDest = GetDlgItem(uiSourControl);

	CRect recSour, recDest;
	pSour->GetWindowRect(&recSour);		// 获取控件变化前的大小
	ScreenToClient(&recSour);			// 将控件大小装换位在对话框中的区域坐标
	pDest->GetWindowRect(&recDest);		// 获取控件变化前的大小
	ScreenToClient(&recDest);			// 将控件大小装换位在对话框中的区域坐标

	pSour->MoveWindow(recDest);
	pDest->MoveWindow(recSour);

	pSour->ShowWindow(SW_SHOW);
	pDest->ShowWindow(SW_SHOW);
}

void ParamConfig::SortOfControlParamLocal(ResourID resId)
{
	for(UINT16_T uiLoop = 0; uiLoop < resId.cParamCount; uiLoop++)
	{	
		ExChangeControlLocal(_arrayStId[uiLoop], resId.uiStIdSrc[uiLoop]);				// St 标签置换	
		ExChangeControlLocal(_arrayEdValueId[uiLoop], resId.uiEdValueIdSrc[uiLoop]);	// Ed Value控件置换	
		ExChangeControlLocal(_arrayEdModifyId[uiLoop], resId.uiEdModifyIdSrc[uiLoop]);	// Ed Modify控件置换
	
		GetDlgItem(_arrayEdModifyId[uiLoop])->SetWindowTextW(L"");
		GetDlgItem(resId.uiEdModifyIdSrc[uiLoop])->SetWindowTextW(L"");
	}
}

void ParamConfig::RebackSortOfControlParamLocal(ResourID resId)
{
	for(UINT16_T uiLoop = 0; uiLoop < resId.cParamCount; uiLoop++)
	{	
		ExChangeControlLocal(resId.uiStIdSrc[uiLoop], _arrayStId[uiLoop]);			// St 标签置换	
		ExChangeControlLocal(resId.uiEdValueIdSrc[uiLoop], _arrayEdValueId[uiLoop]);	// Ed Value控件置换	
		ExChangeControlLocal(resId.uiEdModifyIdSrc[uiLoop], _arrayEdModifyId[uiLoop]);	// Ed Modify控件置换
	}
}

void ParamConfig::UILoadParam_J1850VPW()
{
	UINT16_T _arrayJ1850VPWStId[PARAM_COUNT_MAX] = {IDC_ST_DATA_RATE, IDC_ST_LOOPBACK};
	UINT16_T _arrayJ1850VPWEdValueId[PARAM_COUNT_MAX] = {IDC_ED_VALUE_DATA_RATE, IDC_ED_VALUE_LOOPBACK};
	UINT16_T _arrayJ1850VPWEdModifyId[PARAM_COUNT_MAX] = {IDC_ED_MODIFY_DATA_RATE, IDC_ED_MODIFY_LOOPBACK};

	InitStructOperate(g_ResId);
	g_ResId.cParamCount = 2;

	UINT16_T _ArryParamConfigColumnNameMinor[3] = {IDC_ST_PARAMETERNAME_MINOR, IDC_ST_VALUE_MINOR, IDC_ST_MODIFY_MINOR};
	if(g_ResId.cParamCount < 15)
		for(UINT8_T ucLoop = 0; ucLoop < 3; ucLoop++)
			GetDlgItem(_ArryParamConfigColumnNameMinor[ucLoop])->ShowWindow(FALSE);
	else
		for(UINT8_T ucLoop = 0; ucLoop < 3; ucLoop++)
			GetDlgItem(_ArryParamConfigColumnNameMinor[ucLoop])->ShowWindow(TRUE);

	for (UINT16_T uiLoop = 0; uiLoop < g_ResId.cParamCount; uiLoop++)
	{
		g_ResId.uiStIdSrc[uiLoop] = _arrayJ1850VPWStId[uiLoop];
		g_ResId.uiStIdDest[uiLoop] = _arrayJ1850VPWStId[uiLoop];
		g_ResId.uiEdValueIdSrc[uiLoop] = _arrayJ1850VPWEdValueId[uiLoop];
		g_ResId.uiEdValueIdDest[uiLoop] = _arrayJ1850VPWEdValueId[uiLoop];
		g_ResId.uiEdModifyIdSrc[uiLoop] = _arrayJ1850VPWEdModifyId[uiLoop];
		g_ResId.uiEdModifyIdDest[uiLoop] = _arrayJ1850VPWEdModifyId[uiLoop];
	}
}

void ParamConfig::UILoadParam_J1850PWM()
{
	UINT16_T _arrayJ1850PWMStId[PARAM_COUNT_MAX] = {IDC_ST_DATA_RATE, IDC_ST_LOOPBACK, IDC_ST_NODE_ADDRESS, IDC_ST_NETWORK_LINE};
	UINT16_T _arrayJ1850PWMEdValueId[PARAM_COUNT_MAX] = {IDC_ED_VALUE_DATA_RATE, IDC_ED_VALUE_LOOPBACK, IDC_ED_VALUE_NODE_ADDRESS, IDC_ED_VALUE_NETWORK_LINE};
	UINT16_T _arrayJ1850PWMEdModifyId[PARAM_COUNT_MAX] = {IDC_ED_MODIFY_DATA_RATE, IDC_ED_MODIFY_LOOPBACK, IDC_ED_MODIFY_NODE_ADDRESS, IDC_ED_MODIFY_NETWORK_LINE};
	InitStructOperate(g_ResId);
	g_ResId.cParamCount = 4;

	UINT16_T _ArryParamConfigColumnNameMinor[3] = {IDC_ST_PARAMETERNAME_MINOR, IDC_ST_VALUE_MINOR, IDC_ST_MODIFY_MINOR};
	if(g_ResId.cParamCount < 15)
		for(UINT8_T ucLoop = 0; ucLoop < 3; ucLoop++)
			GetDlgItem(_ArryParamConfigColumnNameMinor[ucLoop])->ShowWindow(FALSE);
	else
		for(UINT8_T ucLoop = 0; ucLoop < 3; ucLoop++)
			GetDlgItem(_ArryParamConfigColumnNameMinor[ucLoop])->ShowWindow(TRUE);

	for (UINT16_T uiLoop = 0; uiLoop < g_ResId.cParamCount; uiLoop++)
	{
		g_ResId.uiStIdSrc[uiLoop] = _arrayJ1850PWMStId[uiLoop];
		g_ResId.uiStIdDest[uiLoop] = _arrayJ1850PWMStId[uiLoop];
		g_ResId.uiEdValueIdSrc[uiLoop] = _arrayJ1850PWMEdValueId[uiLoop];
		g_ResId.uiEdValueIdDest[uiLoop] = _arrayJ1850PWMEdValueId[uiLoop];
		g_ResId.uiEdModifyIdSrc[uiLoop] = _arrayJ1850PWMEdModifyId[uiLoop];
		g_ResId.uiEdModifyIdDest[uiLoop] = _arrayJ1850PWMEdModifyId[uiLoop];
	}
}

void ParamConfig::UILoadParam_ISO9141()
{
	UINT16_T _arrayISO9141StId[PARAM_COUNT_MAX] = {IDC_ST_DATA_RATE, IDC_ST_LOOPBACK, IDC_ST_P1_MAX, IDC_ST_P3_MIN, IDC_ST_P4_MIN, 
		IDC_ST_W0, IDC_ST_W1, IDC_ST_W2, IDC_ST_W3, IDC_ST_W4, IDC_ST_W5, IDC_ST_TIDLE, IDC_ST_TINIL, IDC_ST_TWUP, IDC_ST_PARITY, 
		IDC_ST_DATA_BITS, IDC_ST_FIVE_BAUD_MOD};
	UINT16_T _arrayISO9141EdValueId[PARAM_COUNT_MAX] = {IDC_ED_VALUE_DATA_RATE, IDC_ED_VALUE_LOOPBACK, IDC_ED_VALUE_P1_MAX, IDC_ED_VALUE_P3_MIN, IDC_ED_VALUE_P4_MIN, 
		IDC_ED_VALUE_W0, IDC_ED_VALUE_W1, IDC_ED_VALUE_W2, IDC_ED_VALUE_W3, IDC_ED_VALUE_W4, IDC_ED_VALUE_W5, IDC_ED_VALUE_TIDLE, IDC_ED_VALUE_TINIL, IDC_ED_VALUE_TWUP, IDC_ED_VALUE_PARITY, 
		IDC_ED_VALUE_DATA_BITS, IDC_ED_VALUE_FIVE_BAUD_MOD};
	UINT16_T _arrayISO9141EdModifyId[PARAM_COUNT_MAX] = {IDC_ED_MODIFY_DATA_RATE, IDC_ED_MODIFY_LOOPBACK, IDC_ED_MODIFY_P1_MAX, IDC_ED_MODIFY_P3_MIN, IDC_ED_MODIFY_P4_MIN, 
		IDC_ED_MODIFY_W0, IDC_ED_MODIFY_W1, IDC_ED_MODIFY_W2, IDC_ED_MODIFY_W3, IDC_ED_MODIFY_W4, IDC_ED_MODIFY_W5, IDC_ED_MODIFY_TIDLE, IDC_ED_MODIFY_TINIL, IDC_ED_MODIFY_TWUP, IDC_ED_MODIFY_PARITY, 
		IDC_ED_MODIFY_DATA_BITS, IDC_ED_MODIFY_FIVE_BAUD_MOD};

	InitStructOperate(g_ResId);
	g_ResId.cParamCount = 17;

	UINT16_T _ArryParamConfigColumnNameMinor[3] = {IDC_ST_PARAMETERNAME_MINOR, IDC_ST_VALUE_MINOR, IDC_ST_MODIFY_MINOR};
	if(g_ResId.cParamCount < 15)
		for(UINT8_T ucLoop = 0; ucLoop < 3; ucLoop++)
			GetDlgItem(_ArryParamConfigColumnNameMinor[ucLoop])->ShowWindow(FALSE);
	else
		for(UINT8_T ucLoop = 0; ucLoop < 3; ucLoop++)
			GetDlgItem(_ArryParamConfigColumnNameMinor[ucLoop])->ShowWindow(TRUE);

	for (UINT16_T uiLoop = 0; uiLoop < g_ResId.cParamCount; uiLoop++)
	{
		g_ResId.uiStIdSrc[uiLoop] = _arrayISO9141StId[uiLoop];
		g_ResId.uiStIdDest[uiLoop] = _arrayISO9141StId[uiLoop];
		g_ResId.uiEdValueIdSrc[uiLoop] = _arrayISO9141EdValueId[uiLoop];
		g_ResId.uiEdValueIdDest[uiLoop] = _arrayISO9141EdValueId[uiLoop];
		g_ResId.uiEdModifyIdSrc[uiLoop] = _arrayISO9141EdModifyId[uiLoop];
		g_ResId.uiEdModifyIdDest[uiLoop] = _arrayISO9141EdModifyId[uiLoop];
	}
}

void ParamConfig::UILoadParam_ISO9141_PS()
{
	UINT16_T _arrayISO9141_PSStId[PARAM_COUNT_MAX] = {IDC_ST_DATA_RATE, IDC_ST_LOOPBACK, IDC_ST_P1_MAX, IDC_ST_P3_MIN, IDC_ST_P4_MIN, 
		IDC_ST_W0, IDC_ST_W1, IDC_ST_W2, IDC_ST_W3, IDC_ST_W4, IDC_ST_W5, IDC_ST_TIDLE, IDC_ST_TINIL, IDC_ST_TWUP, IDC_ST_PARITY, 
		IDC_ST_DATA_BITS, IDC_ST_FIVE_BAUD_MOD, IDC_ST_J1962_PINS};
	UINT16_T _arrayISO9141_PSEdValueId[PARAM_COUNT_MAX] = {IDC_ED_VALUE_DATA_RATE, IDC_ED_VALUE_LOOPBACK, IDC_ED_VALUE_P1_MAX, IDC_ED_VALUE_P3_MIN, IDC_ED_VALUE_P4_MIN, 
		IDC_ED_VALUE_W0, IDC_ED_VALUE_W1, IDC_ED_VALUE_W2, IDC_ED_VALUE_W3, IDC_ED_VALUE_W4, IDC_ED_VALUE_W5, IDC_ED_VALUE_TIDLE, IDC_ED_VALUE_TINIL, IDC_ED_VALUE_TWUP, IDC_ED_VALUE_PARITY, 
		IDC_ED_VALUE_DATA_BITS, IDC_ED_VALUE_FIVE_BAUD_MOD, IDC_ED_VALUE_J1962_PINS};
	UINT16_T _arrayISO9141_PSEdModifyId[PARAM_COUNT_MAX] = {IDC_ED_MODIFY_DATA_RATE, IDC_ED_MODIFY_LOOPBACK, IDC_ED_MODIFY_P1_MAX, IDC_ED_MODIFY_P3_MIN, IDC_ED_MODIFY_P4_MIN, 
		IDC_ED_MODIFY_W0, IDC_ED_MODIFY_W1, IDC_ED_MODIFY_W2, IDC_ED_MODIFY_W3, IDC_ED_MODIFY_W4, IDC_ED_MODIFY_W5, IDC_ED_MODIFY_TIDLE, IDC_ED_MODIFY_TINIL, IDC_ED_MODIFY_TWUP, IDC_ED_MODIFY_PARITY, 
		IDC_ED_MODIFY_DATA_BITS, IDC_ED_MODIFY_FIVE_BAUD_MOD, IDC_ED_MODIFY_J1962_PINS};

	InitStructOperate(g_ResId);
	g_ResId.cParamCount = 18;

	UINT16_T _ArryParamConfigColumnNameMinor[3] = {IDC_ST_PARAMETERNAME_MINOR, IDC_ST_VALUE_MINOR, IDC_ST_MODIFY_MINOR};
	if(g_ResId.cParamCount < 15)
		for(UINT8_T ucLoop = 0; ucLoop < 3; ucLoop++)
			GetDlgItem(_ArryParamConfigColumnNameMinor[ucLoop])->ShowWindow(FALSE);
	else
		for(UINT8_T ucLoop = 0; ucLoop < 3; ucLoop++)
			GetDlgItem(_ArryParamConfigColumnNameMinor[ucLoop])->ShowWindow(TRUE);

	for (UINT16_T uiLoop = 0; uiLoop < g_ResId.cParamCount; uiLoop++)
	{
		g_ResId.uiStIdSrc[uiLoop] = _arrayISO9141_PSStId[uiLoop];
		g_ResId.uiStIdDest[uiLoop] = _arrayISO9141_PSStId[uiLoop];
		g_ResId.uiEdValueIdSrc[uiLoop] = _arrayISO9141_PSEdValueId[uiLoop];
		g_ResId.uiEdValueIdDest[uiLoop] = _arrayISO9141_PSEdValueId[uiLoop];
		g_ResId.uiEdModifyIdSrc[uiLoop] = _arrayISO9141_PSEdModifyId[uiLoop];
		g_ResId.uiEdModifyIdDest[uiLoop] = _arrayISO9141_PSEdModifyId[uiLoop];
	}
}

void ParamConfig::UILoadParam_ISO14230()
{
	UINT16_T _arrayISO14230StId[PARAM_COUNT_MAX] = {IDC_ST_DATA_RATE, IDC_ST_LOOPBACK, IDC_ST_P1_MAX, IDC_ST_P3_MIN, IDC_ST_P4_MIN, 
		IDC_ST_W0, IDC_ST_W1, IDC_ST_W2, IDC_ST_W3, IDC_ST_W4, IDC_ST_W5, IDC_ST_TIDLE, IDC_ST_TINIL, IDC_ST_TWUP, IDC_ST_PARITY, 
		IDC_ST_DATA_BITS, IDC_ST_FIVE_BAUD_MOD};
	UINT16_T _arrayISO14230EdValueId[PARAM_COUNT_MAX] = {IDC_ED_VALUE_DATA_RATE, IDC_ED_VALUE_LOOPBACK, IDC_ED_VALUE_P1_MAX, IDC_ED_VALUE_P3_MIN, IDC_ED_VALUE_P4_MIN, 
		IDC_ED_VALUE_W0, IDC_ED_VALUE_W1, IDC_ED_VALUE_W2, IDC_ED_VALUE_W3, IDC_ED_VALUE_W4, IDC_ED_VALUE_W5, IDC_ED_VALUE_TIDLE, IDC_ED_VALUE_TINIL, IDC_ED_VALUE_TWUP, IDC_ED_VALUE_PARITY, 
		IDC_ED_VALUE_DATA_BITS, IDC_ED_VALUE_FIVE_BAUD_MOD};
	UINT16_T _arrayISO14230EdModifyId[PARAM_COUNT_MAX] = {IDC_ED_MODIFY_DATA_RATE, IDC_ED_MODIFY_LOOPBACK, IDC_ED_MODIFY_P1_MAX, IDC_ED_MODIFY_P3_MIN, IDC_ED_MODIFY_P4_MIN, 
		IDC_ED_MODIFY_W0, IDC_ED_MODIFY_W1, IDC_ED_MODIFY_W2, IDC_ED_MODIFY_W3, IDC_ED_MODIFY_W4, IDC_ED_MODIFY_W5, IDC_ED_MODIFY_TIDLE, IDC_ED_MODIFY_TINIL, IDC_ED_MODIFY_TWUP, IDC_ED_MODIFY_PARITY, 
		IDC_ED_MODIFY_DATA_BITS, IDC_ED_MODIFY_FIVE_BAUD_MOD};

	InitStructOperate(g_ResId);
	g_ResId.cParamCount = 17;

	UINT16_T _ArryParamConfigColumnNameMinor[3] = {IDC_ST_PARAMETERNAME_MINOR, IDC_ST_VALUE_MINOR, IDC_ST_MODIFY_MINOR};
	if(g_ResId.cParamCount < 15)
		for(UINT8_T ucLoop = 0; ucLoop < 3; ucLoop++)
			GetDlgItem(_ArryParamConfigColumnNameMinor[ucLoop])->ShowWindow(FALSE);
	else
		for(UINT8_T ucLoop = 0; ucLoop < 3; ucLoop++)
			GetDlgItem(_ArryParamConfigColumnNameMinor[ucLoop])->ShowWindow(TRUE);

	for (UINT16_T uiLoop = 0; uiLoop < g_ResId.cParamCount; uiLoop++)
	{
		g_ResId.uiStIdSrc[uiLoop] = _arrayISO14230StId[uiLoop];
		g_ResId.uiStIdDest[uiLoop] = _arrayISO14230StId[uiLoop];
		g_ResId.uiEdValueIdSrc[uiLoop] = _arrayISO14230EdValueId[uiLoop];
		g_ResId.uiEdValueIdDest[uiLoop] = _arrayISO14230EdValueId[uiLoop];
		g_ResId.uiEdModifyIdSrc[uiLoop] = _arrayISO14230EdModifyId[uiLoop];
		g_ResId.uiEdModifyIdDest[uiLoop] = _arrayISO14230EdModifyId[uiLoop];
	}
}

void ParamConfig::UILoadParam_CAN()
{
	UINT16_T _arrayCANStId[PARAM_COUNT_MAX] = {IDC_ST_DATA_RATE, IDC_ST_LOOPBACK, IDC_ST_BIT_SAMPLE_POINT, IDC_ST_SYNC_JUMP_WIDTH};
	UINT16_T _arrayCANEdValueId[PARAM_COUNT_MAX] = {IDC_ED_VALUE_DATA_RATE, IDC_ED_VALUE_LOOPBACK, IDC_ED_VALUE_BIT_SAMPLE_POINT, IDC_ED_VALUE_SYNC_JUMP_WIDTH};
	UINT16_T _arrayCANEdModifyId[PARAM_COUNT_MAX] = {IDC_ED_MODIFY_DATA_RATE, IDC_ED_MODIFY_LOOPBACK, IDC_ED_MODIFY_BIT_SAMPLE_POINT, IDC_ED_MODIFY_SYNC_JUMP_WIDTH};
	
	InitStructOperate(g_ResId);
	g_ResId.cParamCount = 4;

	UINT16_T _ArryParamConfigColumnNameMinor[3] = {IDC_ST_PARAMETERNAME_MINOR, IDC_ST_VALUE_MINOR, IDC_ST_MODIFY_MINOR};
	if(g_ResId.cParamCount < 15)
		for(UINT8_T ucLoop = 0; ucLoop < 3; ucLoop++)
			GetDlgItem(_ArryParamConfigColumnNameMinor[ucLoop])->ShowWindow(FALSE);
	else
		for(UINT8_T ucLoop = 0; ucLoop < 3; ucLoop++)
			GetDlgItem(_ArryParamConfigColumnNameMinor[ucLoop])->ShowWindow(TRUE);

	for (UINT16_T uiLoop = 0; uiLoop < g_ResId.cParamCount; uiLoop++)
	{
		g_ResId.uiStIdSrc[uiLoop] = _arrayCANStId[uiLoop];
		g_ResId.uiStIdDest[uiLoop] = _arrayCANStId[uiLoop];
		g_ResId.uiEdValueIdSrc[uiLoop] = _arrayCANEdValueId[uiLoop];
		g_ResId.uiEdValueIdDest[uiLoop] = _arrayCANEdValueId[uiLoop];
		g_ResId.uiEdModifyIdSrc[uiLoop] = _arrayCANEdModifyId[uiLoop];
		g_ResId.uiEdModifyIdDest[uiLoop] = _arrayCANEdModifyId[uiLoop];
	}
}

void ParamConfig::UILoadParam_CAN_PS()
{
	UINT16_T _arrayCAN_PSStId[PARAM_COUNT_MAX] = {IDC_ST_DATA_RATE, IDC_ST_LOOPBACK, IDC_ST_BIT_SAMPLE_POINT, IDC_ST_SYNC_JUMP_WIDTH,
		IDC_ST_J1962_PINS};
	UINT16_T _arrayCAN_PSEdValueId[PARAM_COUNT_MAX] = {IDC_ED_VALUE_DATA_RATE, IDC_ED_VALUE_LOOPBACK, IDC_ED_VALUE_BIT_SAMPLE_POINT, IDC_ED_VALUE_SYNC_JUMP_WIDTH,
		IDC_ED_VALUE_J1962_PINS};
	UINT16_T _arrayCAN_PSEdModifyId[PARAM_COUNT_MAX] = {IDC_ED_MODIFY_DATA_RATE, IDC_ED_MODIFY_LOOPBACK, IDC_ED_MODIFY_BIT_SAMPLE_POINT, IDC_ED_MODIFY_SYNC_JUMP_WIDTH,
		IDC_ED_MODIFY_J1962_PINS};

	InitStructOperate(g_ResId);
	g_ResId.cParamCount = 5;

	UINT16_T _ArryParamConfigColumnNameMinor[3] = {IDC_ST_PARAMETERNAME_MINOR, IDC_ST_VALUE_MINOR, IDC_ST_MODIFY_MINOR};
	if(g_ResId.cParamCount < 15)
		for(UINT8_T ucLoop = 0; ucLoop < 3; ucLoop++)
			GetDlgItem(_ArryParamConfigColumnNameMinor[ucLoop])->ShowWindow(FALSE);
	else
		for(UINT8_T ucLoop = 0; ucLoop < 3; ucLoop++)
			GetDlgItem(_ArryParamConfigColumnNameMinor[ucLoop])->ShowWindow(TRUE);

	for (UINT16_T uiLoop = 0; uiLoop < g_ResId.cParamCount; uiLoop++)
	{
		g_ResId.uiStIdSrc[uiLoop] = _arrayCAN_PSStId[uiLoop];
		g_ResId.uiStIdDest[uiLoop] = _arrayCAN_PSStId[uiLoop];
		g_ResId.uiEdValueIdSrc[uiLoop] = _arrayCAN_PSEdValueId[uiLoop];
		g_ResId.uiEdValueIdDest[uiLoop] = _arrayCAN_PSEdValueId[uiLoop];
		g_ResId.uiEdModifyIdSrc[uiLoop] = _arrayCAN_PSEdModifyId[uiLoop];
		g_ResId.uiEdModifyIdDest[uiLoop] = _arrayCAN_PSEdModifyId[uiLoop];
	}
}

void ParamConfig::UILoadParam_ISO15765()
{
	UINT16_T _arrayISO15765StId[PARAM_COUNT_MAX] = {IDC_ST_DATA_RATE, IDC_ST_LOOPBACK, IDC_ST_BIT_SAMPLE_POINT, IDC_ST_SYNC_JUMP_WIDTH, 
		IDC_ST_ISO15765_BS, IDC_ST_ISO15765_STMIN, IDC_ST_BS_TX, IDC_ST_STMIN_TX, IDC_ST_ISO15765_WFT_MAX, IDC_ST__ISO15765_SIMULTANEOUS, 
		IDC_ST_DT_ISO15765_PAD_BYTE, IDC_ST_CAN_MIXED_FORMAT};
	UINT16_T _arrayISO15765EdValueId[PARAM_COUNT_MAX] = {IDC_ED_VALUE_DATA_RATE, IDC_ED_VALUE_LOOPBACK, IDC_ED_VALUE_BIT_SAMPLE_POINT, IDC_ED_VALUE_SYNC_JUMP_WIDTH, 
		IDC_ED_VALUE_ISO15765_BS, IDC_ED_VALUE_ISO15765_STMIN, IDC_ED_VALUE_BS_TX, IDC_ED_VALUE_STMIN_TX, IDC_ED_VALUE_ISO15765_WFT_MAX, IDC_ED_VALUE__ISO15765_SIMULTANEOUS, 
		IDC_ED_VALUE_DT_ISO15765_PAD_BYTE, IDC_ED_VALUE_CAN_MIXED_FORMAT};
	UINT16_T _arrayISO15765EdModifyId[PARAM_COUNT_MAX] = {IDC_ED_MODIFY_DATA_RATE, IDC_ED_MODIFY_LOOPBACK, IDC_ED_MODIFY_BIT_SAMPLE_POINT, IDC_ED_MODIFY_SYNC_JUMP_WIDTH, 
		IDC_ED_MODIFY_ISO15765_BS, IDC_ED_MODIFY_ISO15765_STMIN, IDC_ED_MODIFY_BS_TX, IDC_ED_MODIFY_STMIN_TX, IDC_ED_MODIFY_ISO15765_WFT_MAX, IDC_ED_MODIFY__ISO15765_SIMULTANEOUS, 
		IDC_ED_MODIFY_DT_ISO15765_PAD_BYTE, IDC_ED_MODIFY_CAN_MIXED_FORMAT};

	InitStructOperate(g_ResId);
	g_ResId.cParamCount = 12;

	UINT16_T _ArryParamConfigColumnNameMinor[3] = {IDC_ST_PARAMETERNAME_MINOR, IDC_ST_VALUE_MINOR, IDC_ST_MODIFY_MINOR};
	if(g_ResId.cParamCount < 15)
		for(UINT8_T ucLoop = 0; ucLoop < 3; ucLoop++)
			GetDlgItem(_ArryParamConfigColumnNameMinor[ucLoop])->ShowWindow(FALSE);
	else
		for(UINT8_T ucLoop = 0; ucLoop < 3; ucLoop++)
			GetDlgItem(_ArryParamConfigColumnNameMinor[ucLoop])->ShowWindow(TRUE);

	for (UINT16_T uiLoop = 0; uiLoop < g_ResId.cParamCount; uiLoop++)
	{
		g_ResId.uiStIdSrc[uiLoop] = _arrayISO15765StId[uiLoop];
		g_ResId.uiStIdDest[uiLoop] = _arrayISO15765StId[uiLoop];
		g_ResId.uiEdValueIdSrc[uiLoop] = _arrayISO15765EdValueId[uiLoop];
		g_ResId.uiEdValueIdDest[uiLoop] = _arrayISO15765EdValueId[uiLoop];
		g_ResId.uiEdModifyIdSrc[uiLoop] = _arrayISO15765EdModifyId[uiLoop];
		g_ResId.uiEdModifyIdDest[uiLoop] = _arrayISO15765EdModifyId[uiLoop];
	}
}

void ParamConfig::UILoadParam_ISO15765_PS()
{
	UINT16_T _arrayISO15765_PSStId[PARAM_COUNT_MAX] = {IDC_ST_DATA_RATE, IDC_ST_LOOPBACK, IDC_ST_BIT_SAMPLE_POINT, IDC_ST_SYNC_JUMP_WIDTH, 
		IDC_ST_ISO15765_BS, IDC_ST_ISO15765_STMIN, IDC_ST_BS_TX, IDC_ST_STMIN_TX, IDC_ST_ISO15765_WFT_MAX, IDC_ST__ISO15765_SIMULTANEOUS, 
		IDC_ST_DT_ISO15765_PAD_BYTE, IDC_ST_CAN_MIXED_FORMAT, IDC_ST_J1962_PINS};
	UINT16_T _arrayISO15765_PSEdValueId[PARAM_COUNT_MAX] = {IDC_ED_VALUE_DATA_RATE, IDC_ED_VALUE_LOOPBACK, IDC_ED_VALUE_BIT_SAMPLE_POINT, IDC_ED_VALUE_SYNC_JUMP_WIDTH, 
		IDC_ED_VALUE_ISO15765_BS, IDC_ED_VALUE_ISO15765_STMIN, IDC_ED_VALUE_BS_TX, IDC_ED_VALUE_STMIN_TX, IDC_ED_VALUE_ISO15765_WFT_MAX, IDC_ED_VALUE__ISO15765_SIMULTANEOUS, 
		IDC_ED_VALUE_DT_ISO15765_PAD_BYTE, IDC_ED_VALUE_CAN_MIXED_FORMAT, IDC_ED_VALUE_J1962_PINS};
	UINT16_T _arrayISO15765_PSEdModifyId[PARAM_COUNT_MAX] = {IDC_ED_MODIFY_DATA_RATE, IDC_ED_MODIFY_LOOPBACK, IDC_ED_MODIFY_BIT_SAMPLE_POINT, IDC_ED_MODIFY_SYNC_JUMP_WIDTH, 
		IDC_ED_MODIFY_ISO15765_BS, IDC_ED_MODIFY_ISO15765_STMIN, IDC_ED_MODIFY_BS_TX, IDC_ED_MODIFY_STMIN_TX, IDC_ED_MODIFY_ISO15765_WFT_MAX, IDC_ED_MODIFY__ISO15765_SIMULTANEOUS, 
		IDC_ED_MODIFY_DT_ISO15765_PAD_BYTE, IDC_ED_MODIFY_CAN_MIXED_FORMAT, IDC_ED_MODIFY_J1962_PINS};

	InitStructOperate(g_ResId);
	g_ResId.cParamCount = 13;

	UINT16_T _ArryParamConfigColumnNameMinor[3] = {IDC_ST_PARAMETERNAME_MINOR, IDC_ST_VALUE_MINOR, IDC_ST_MODIFY_MINOR};
	if(g_ResId.cParamCount < 15)
		for(UINT8_T ucLoop = 0; ucLoop < 3; ucLoop++)
			GetDlgItem(_ArryParamConfigColumnNameMinor[ucLoop])->ShowWindow(FALSE);
	else
		for(UINT8_T ucLoop = 0; ucLoop < 3; ucLoop++)
			GetDlgItem(_ArryParamConfigColumnNameMinor[ucLoop])->ShowWindow(TRUE);

	for (UINT16_T uiLoop = 0; uiLoop < g_ResId.cParamCount; uiLoop++)
	{
		g_ResId.uiStIdSrc[uiLoop] = _arrayISO15765_PSStId[uiLoop];
		g_ResId.uiStIdDest[uiLoop] = _arrayISO15765_PSStId[uiLoop];
		g_ResId.uiEdValueIdSrc[uiLoop] = _arrayISO15765_PSEdValueId[uiLoop];
		g_ResId.uiEdValueIdDest[uiLoop] = _arrayISO15765_PSEdValueId[uiLoop];
		g_ResId.uiEdModifyIdSrc[uiLoop] = _arrayISO15765_PSEdModifyId[uiLoop];
		g_ResId.uiEdModifyIdDest[uiLoop] = _arrayISO15765_PSEdModifyId[uiLoop];
	}
}

void ParamConfig::UILoadParam_SW_CAN_PS()
{
	UINT16_T _arraySW_CAN_PSStId[PARAM_COUNT_MAX] = {IDC_ST_DATA_RATE, IDC_ST_LOOPBACK, IDC_ST_BIT_SAMPLE_POINT, IDC_ST_SYNC_JUMP_WIDTH, 
		IDC_ST_SW_CAN_HS_DATA_RATE, IDC_ST_SW_CAN_SPDCHG_ENABLE, IDC_ST_SW_CAN_RES_SWITCH, IDC_ST_J1962_PINS};
	UINT16_T _arraySW_CAN_PSEdValueId[PARAM_COUNT_MAX] = {IDC_ED_VALUE_DATA_RATE, IDC_ED_VALUE_LOOPBACK, IDC_ED_VALUE_BIT_SAMPLE_POINT, IDC_ED_VALUE_SYNC_JUMP_WIDTH, 
		IDC_ED_VALUE_SW_CAN_HS_DATA_RATE, IDC_ED_VALUE_SW_CAN_SPDCHG_ENABLE, IDC_ED_VALUE_SW_CAN_RES_SWITCH, IDC_ED_VALUE_J1962_PINS};
	UINT16_T _arraySW_CAN_PSEdModifyId[PARAM_COUNT_MAX] = {IDC_ED_MODIFY_DATA_RATE, IDC_ED_MODIFY_LOOPBACK, IDC_ED_MODIFY_BIT_SAMPLE_POINT, IDC_ED_MODIFY_SYNC_JUMP_WIDTH, 
		IDC_ED_MODIFY_SW_CAN_HS_DATA_RATE, IDC_ED_MODIFY_SW_CAN_SPDCHG_ENABLE, IDC_ED_MODIFY_SW_CAN_RES_SWITCH, IDC_ED_MODIFY_J1962_PINS};

	InitStructOperate(g_ResId);
	g_ResId.cParamCount = 8;

	UINT16_T _ArryParamConfigColumnNameMinor[3] = {IDC_ST_PARAMETERNAME_MINOR, IDC_ST_VALUE_MINOR, IDC_ST_MODIFY_MINOR};
	if(g_ResId.cParamCount < 15)
		for(UINT8_T ucLoop = 0; ucLoop < 3; ucLoop++)
			GetDlgItem(_ArryParamConfigColumnNameMinor[ucLoop])->ShowWindow(FALSE);
	else
		for(UINT8_T ucLoop = 0; ucLoop < 3; ucLoop++)
			GetDlgItem(_ArryParamConfigColumnNameMinor[ucLoop])->ShowWindow(TRUE);

	for (UINT16_T uiLoop = 0; uiLoop < g_ResId.cParamCount; uiLoop++)
	{
		g_ResId.uiStIdSrc[uiLoop] = _arraySW_CAN_PSStId[uiLoop];
		g_ResId.uiStIdDest[uiLoop] = _arraySW_CAN_PSStId[uiLoop];
		g_ResId.uiEdValueIdSrc[uiLoop] = _arraySW_CAN_PSEdValueId[uiLoop];
		g_ResId.uiEdValueIdDest[uiLoop] = _arraySW_CAN_PSEdValueId[uiLoop];
		g_ResId.uiEdModifyIdSrc[uiLoop] = _arraySW_CAN_PSEdModifyId[uiLoop];
		g_ResId.uiEdModifyIdDest[uiLoop] = _arraySW_CAN_PSEdModifyId[uiLoop];
	}
}

void ParamConfig::UILoadParam_SW_ISO15765_PS()
{
	UINT16_T _arraySW_ISO15765_PSStId[PARAM_COUNT_MAX] = {IDC_ST_DATA_RATE, IDC_ST_LOOPBACK, IDC_ST_BIT_SAMPLE_POINT, IDC_ST_SYNC_JUMP_WIDTH, 
		IDC_ST_ISO15765_BS, IDC_ST_ISO15765_STMIN, IDC_ST_BS_TX, IDC_ST_STMIN_TX, IDC_ST_ISO15765_WFT_MAX, IDC_ST__ISO15765_SIMULTANEOUS, 
		IDC_ST_DT_ISO15765_PAD_BYTE, IDC_ST_CAN_MIXED_FORMAT, IDC_ST_SW_CAN_HS_DATA_RATE, IDC_ST_SW_CAN_SPDCHG_ENABLE, IDC_ST_SW_CAN_RES_SWITCH, 
		IDC_ST_J1962_PINS};
	UINT16_T _arraySW_ISO15765_PSEdValueId[PARAM_COUNT_MAX] = {IDC_ED_VALUE_DATA_RATE, IDC_ED_VALUE_LOOPBACK, IDC_ED_VALUE_BIT_SAMPLE_POINT, IDC_ED_VALUE_SYNC_JUMP_WIDTH, 
		IDC_ED_VALUE_ISO15765_BS, IDC_ED_VALUE_ISO15765_STMIN, IDC_ED_VALUE_BS_TX, IDC_ED_VALUE_STMIN_TX, IDC_ED_VALUE_ISO15765_WFT_MAX, IDC_ED_VALUE__ISO15765_SIMULTANEOUS, 
		IDC_ED_VALUE_DT_ISO15765_PAD_BYTE, IDC_ED_VALUE_CAN_MIXED_FORMAT, IDC_ED_VALUE_SW_CAN_HS_DATA_RATE, IDC_ED_VALUE_SW_CAN_SPDCHG_ENABLE, IDC_ED_VALUE_SW_CAN_RES_SWITCH, 
		IDC_ED_VALUE_J1962_PINS};
	UINT16_T _arraySW_ISO15765_PSEdModifyId[PARAM_COUNT_MAX] = {IDC_ED_MODIFY_DATA_RATE, IDC_ED_MODIFY_LOOPBACK, IDC_ED_MODIFY_BIT_SAMPLE_POINT, IDC_ED_MODIFY_SYNC_JUMP_WIDTH, 
		IDC_ED_MODIFY_ISO15765_BS, IDC_ED_MODIFY_ISO15765_STMIN, IDC_ED_MODIFY_BS_TX, IDC_ED_MODIFY_STMIN_TX, IDC_ED_MODIFY_ISO15765_WFT_MAX, IDC_ED_MODIFY__ISO15765_SIMULTANEOUS, 
		IDC_ED_MODIFY_DT_ISO15765_PAD_BYTE, IDC_ED_MODIFY_CAN_MIXED_FORMAT, IDC_ED_MODIFY_SW_CAN_HS_DATA_RATE, IDC_ED_MODIFY_SW_CAN_SPDCHG_ENABLE, IDC_ED_MODIFY_SW_CAN_RES_SWITCH, 
		IDC_ED_MODIFY_J1962_PINS};

	InitStructOperate(g_ResId);
	g_ResId.cParamCount = 16;

	UINT16_T _ArryParamConfigColumnNameMinor[3] = {IDC_ST_PARAMETERNAME_MINOR, IDC_ST_VALUE_MINOR, IDC_ST_MODIFY_MINOR};
	if(g_ResId.cParamCount < 15)
		for(UINT8_T ucLoop = 0; ucLoop < 3; ucLoop++)
			GetDlgItem(_ArryParamConfigColumnNameMinor[ucLoop])->ShowWindow(FALSE);
	else
		for(UINT8_T ucLoop = 0; ucLoop < 3; ucLoop++)
			GetDlgItem(_ArryParamConfigColumnNameMinor[ucLoop])->ShowWindow(TRUE);

	for (UINT16_T uiLoop = 0; uiLoop < g_ResId.cParamCount; uiLoop++)
	{
		g_ResId.uiStIdSrc[uiLoop] = _arraySW_ISO15765_PSStId[uiLoop];
		g_ResId.uiStIdDest[uiLoop] = _arraySW_ISO15765_PSStId[uiLoop];
		g_ResId.uiEdValueIdSrc[uiLoop] = _arraySW_ISO15765_PSEdValueId[uiLoop];
		g_ResId.uiEdValueIdDest[uiLoop] = _arraySW_ISO15765_PSEdValueId[uiLoop];
		g_ResId.uiEdModifyIdSrc[uiLoop] = _arraySW_ISO15765_PSEdModifyId[uiLoop];
		g_ResId.uiEdModifyIdDest[uiLoop] = _arraySW_ISO15765_PSEdModifyId[uiLoop];
	}
}

void ParamConfig::UILoadParam_J1939_PS()
{

}

void ParamConfig::UILoadParam_VOLVO_CAN2()
{
	UINT16_T _arrayCANStId[PARAM_COUNT_MAX] = {IDC_ST_DATA_RATE, IDC_ST_LOOPBACK, IDC_ST_BIT_SAMPLE_POINT, IDC_ST_SYNC_JUMP_WIDTH};
	UINT16_T _arrayCANEdValueId[PARAM_COUNT_MAX] = {IDC_ED_VALUE_DATA_RATE, IDC_ED_VALUE_LOOPBACK, IDC_ED_VALUE_BIT_SAMPLE_POINT, IDC_ED_VALUE_SYNC_JUMP_WIDTH};
	UINT16_T _arrayCANEdModifyId[PARAM_COUNT_MAX] = {IDC_ED_MODIFY_DATA_RATE, IDC_ED_MODIFY_LOOPBACK, IDC_ED_MODIFY_BIT_SAMPLE_POINT, IDC_ED_MODIFY_SYNC_JUMP_WIDTH};

	InitStructOperate(g_ResId);
	g_ResId.cParamCount = 4;

	UINT16_T _ArryParamConfigColumnNameMinor[3] = {IDC_ST_PARAMETERNAME_MINOR, IDC_ST_VALUE_MINOR, IDC_ST_MODIFY_MINOR};
	if(g_ResId.cParamCount < 15)
		for(UINT8_T ucLoop = 0; ucLoop < 3; ucLoop++)
			GetDlgItem(_ArryParamConfigColumnNameMinor[ucLoop])->ShowWindow(FALSE);
	else
		for(UINT8_T ucLoop = 0; ucLoop < 3; ucLoop++)
			GetDlgItem(_ArryParamConfigColumnNameMinor[ucLoop])->ShowWindow(TRUE);

	for (UINT16_T uiLoop = 0; uiLoop < g_ResId.cParamCount; uiLoop++)
	{
		g_ResId.uiStIdSrc[uiLoop] = _arrayCANStId[uiLoop];
		g_ResId.uiStIdDest[uiLoop] = _arrayCANStId[uiLoop];
		g_ResId.uiEdValueIdSrc[uiLoop] = _arrayCANEdValueId[uiLoop];
		g_ResId.uiEdValueIdDest[uiLoop] = _arrayCANEdValueId[uiLoop];
		g_ResId.uiEdModifyIdSrc[uiLoop] = _arrayCANEdModifyId[uiLoop];
		g_ResId.uiEdModifyIdDest[uiLoop] = _arrayCANEdModifyId[uiLoop];
	}
}

void ParamConfig::UILoadProtocolParam(UINT32_T ulProtocolType)
{
	switch (ulProtocolType)
	{
		case J1850VPW:		UILoadParam_J1850VPW();			break;
		case J1850PWM:		UILoadParam_J1850PWM();			break;
		case ISO9141:		UILoadParam_ISO9141();			break;
		case ISO9141_PS:	UILoadParam_ISO9141_PS();		break;
		case ISO14230:		UILoadParam_ISO14230();			break;
		case CAN:			UILoadParam_CAN();				break;
		case CAN_PS:		UILoadParam_CAN_PS();			break;
		case ISO15765:		UILoadParam_ISO15765();			break;
		case ISO15765_PS:	UILoadParam_ISO15765_PS();		break;
		case SW_CAN_PS:		UILoadParam_SW_CAN_PS();		break;
		case SW_ISO15765_PS:UILoadParam_SW_ISO15765_PS();	break;
		default:											break;
	}
}

BOOL ParamConfig::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	UILoadParamConfigSdkParam();

	CString strProtocolName;
	((CComboBox*)GetDlgItem(IDC_CB_PARAMCONFIG_PROTOCOL))->GetLBText(((CComboBox*)GetDlgItem(IDC_CB_PARAMCONFIG_PROTOCOL))->GetCurSel(), strProtocolName);
	for(UINT8_T uiLoop = 0; uiLoop < CONNECT_MAX_COUNT; uiLoop++)
	{
		if(ProtocolStrToValue(strProtocolName) == m_ParamConfigSdk.m_Channel[uiLoop].GetProtocolId())
		{	
			uParamconfigCurrChannelNum = m_ParamConfigSdk.m_Channel[uiLoop].GetChannelId();
			break;
		}
	}

	InitControlsDisVisible();
	InitUIDefaultValue();
	UILoadProtocolParam(m_ParamConfigSdk.m_Channel[uParamconfigCurrChannelNum-1].GetProtocolId());
	SortOfControlParamLocal(g_ResId);
	UIParamConfigDisplay();
	return TRUE;  // return TRUE unless you set the focus to a control
	// 异常: OCX 属性页应返回 FALSE
}

/*------------------------------------------------------------------------
*功		 能：初始化加载Device，Protocol控件的显示
*参 数 说 明：无
*返  回	 值：无
*说	     明：无
------------------------------------------------------------------------*/
void ParamConfig::UILoadParamConfigSdkParam()
{
	/* DeviceId 显示 */
	CString	strParamConfigDevice = L"J2534 Tool ", strProtocolName;
	INT8_T	ucTemp = 0;;
	strParamConfigDevice += UINT32ToDecString(g_DevicesCount);
	GetDlgItem(IDC_ED_PARAMCONFIG_DEVICE)->SetWindowTextW(strParamConfigDevice);

	/* ProtocolId 显示 */
	((CComboBox*)GetDlgItem(IDC_CB_PARAMCONFIG_PROTOCOL))->ResetContent();
	for(UINT8_T ucLoop = 0; ucLoop < CONNECT_MAX_COUNT; ucLoop++)
	{
		if(m_ParamConfigSdk.m_Channel[ucLoop].GetChannelExist() != FALSE)
		{
			((CComboBox*)GetDlgItem(IDC_CB_PARAMCONFIG_PROTOCOL))->InsertString(ucTemp,ProtocolValueToStr(m_ParamConfigSdk.m_Channel[ucLoop].GetProtocolId()));
			ucTemp++;
		}
	}
	((CComboBox*)GetDlgItem(IDC_CB_PARAMCONFIG_PROTOCOL))->SetCurSel(0);

	/* 获取当前通道号 */
	//((CComboBox*)GetDlgItem(IDC_CB_PARAMCONFIG_PROTOCOL))->GetLBText(((CComboBox*)GetDlgItem(IDC_CB_PARAMCONFIG_PROTOCOL))->GetCurSel(), strProtocolName);
	//for(UINT8_T uiLoop = 0; uiLoop < CONNECT_MAX_COUNT; uiLoop++)
	//{
	//	if(ProtocolStrToValue(strProtocolName) == m_ParamConfigSdk.GetProtocolId(uiLoop))
	//	{	
	//		uParamconfigCurrChannelNum = m_ParamConfigSdk.GetChannelId(uiLoop);
	//		break;
	//	}
	//}
}

void ParamConfig::UIParamConfigDisplay()
{
	CRect recWnd;
	GetWindowRect(&recWnd);			// 获取控件变化前的大小
	MoveWindow(recWnd.left, recWnd.top, recWnd.Width()/2, recWnd.Height());
}

UINT32_T ParamConfig::GetParamConfigValue(UINT16_T uiTemp)
{
	UINT32_T ulRetValue;
	CString strTemp; 
	UINT16_T uiDefaultAry[33] = {IDC_ED_VALUE_DATA_RATE, IDC_ED_VALUE_LOOPBACK, IDC_ED_VALUE_NODE_ADDRESS, IDC_ED_VALUE_NETWORK_LINE, IDC_ED_VALUE_P1_MAX, 
		IDC_ED_VALUE_P3_MIN, IDC_ED_VALUE_P4_MIN, IDC_ED_VALUE_W0, IDC_ED_VALUE_W1, IDC_ED_VALUE_W2, IDC_ED_VALUE_W3, IDC_ED_VALUE_W4, 
		IDC_ED_VALUE_W5, IDC_ED_VALUE_TIDLE, IDC_ED_VALUE_TINIL, IDC_ED_VALUE_TWUP, IDC_ED_VALUE_PARITY, IDC_ED_VALUE_DATA_BITS, 
		IDC_ED_VALUE_FIVE_BAUD_MOD, IDC_ED_VALUE_BIT_SAMPLE_POINT, IDC_ED_VALUE_SYNC_JUMP_WIDTH, IDC_ED_VALUE_ISO15765_BS, 
		IDC_ED_VALUE_ISO15765_STMIN, IDC_ED_VALUE_BS_TX, IDC_ED_VALUE_STMIN_TX, IDC_ED_VALUE_ISO15765_WFT_MAX, IDC_ED_VALUE__ISO15765_SIMULTANEOUS, 
		IDC_ED_VALUE_DT_ISO15765_PAD_BYTE, IDC_ED_VALUE_CAN_MIXED_FORMAT, IDC_ED_VALUE_SW_CAN_HS_DATA_RATE, IDC_ED_VALUE_SW_CAN_SPDCHG_ENABLE, 
		IDC_ED_VALUE_SW_CAN_RES_SWITCH, IDC_ED_VALUE_J1962_PINS};

	UINT16_T uiModifyAry[33] = {IDC_ED_MODIFY_DATA_RATE, IDC_ED_MODIFY_LOOPBACK, IDC_ED_MODIFY_NODE_ADDRESS, IDC_ED_MODIFY_NETWORK_LINE, 
		IDC_ED_MODIFY_P1_MAX, IDC_ED_MODIFY_P3_MIN, IDC_ED_MODIFY_P4_MIN, IDC_ED_MODIFY_W0, IDC_ED_MODIFY_W1, 
		IDC_ED_MODIFY_W2, IDC_ED_MODIFY_W3, IDC_ED_MODIFY_W4, IDC_ED_MODIFY_W5, IDC_ED_MODIFY_TIDLE, IDC_ED_MODIFY_TINIL, 
		IDC_ED_MODIFY_TWUP, IDC_ED_MODIFY_PARITY, IDC_ED_MODIFY_DATA_BITS, IDC_ED_MODIFY_FIVE_BAUD_MOD, 
		IDC_ED_MODIFY_BIT_SAMPLE_POINT, IDC_ED_MODIFY_SYNC_JUMP_WIDTH, IDC_ED_MODIFY_ISO15765_BS, IDC_ED_MODIFY_ISO15765_STMIN, 
		IDC_ED_MODIFY_BS_TX, IDC_ED_MODIFY_STMIN_TX, IDC_ED_MODIFY_ISO15765_WFT_MAX, IDC_ED_MODIFY__ISO15765_SIMULTANEOUS, 
		IDC_ED_MODIFY_DT_ISO15765_PAD_BYTE, IDC_ED_MODIFY_CAN_MIXED_FORMAT, IDC_ED_MODIFY_SW_CAN_HS_DATA_RATE, 
		IDC_ED_MODIFY_SW_CAN_SPDCHG_ENABLE, IDC_ED_MODIFY_SW_CAN_RES_SWITCH, IDC_ED_MODIFY_J1962_PINS};

	UINT16_T uiIoctlAry[33] = {DATA_RATE, LOOPBACK, NODE_ADDRESS, NETWORK_LINE, P1_MAX, P3_MIN, P4_MIN, W0, W1, W2, W3, W4, W5, 
		TIDLE, TINIL, TWUP, PARITY, DATA_BITS, FIVE_BAUD_MOD, BIT_SAMPLE_POINT, SYNC_JUMP_WIDTH, ISO15765_BS, 
		ISO15765_STMIN, BS_TX, STMIN_TX, ISO15765_WFT_MAX, _ISO15765_SIMULTANEOUS, DT_ISO15765_PAD_BYTE, 
		CAN_MIXED_FORMAT, SW_CAN_HS_DATA_RATE, SW_CAN_SPDCHG_ENABLE, SW_CAN_RES_SWITCH, J1962_PINS};

	for(UINT16_T uiLoop = 0; uiLoop < 33; uiLoop++)
	{
		if (uiTemp != uiIoctlAry[uiLoop])
		{
			continue;
		}
		GetDlgItem(uiModifyAry[uiLoop])->GetWindowTextW(strTemp);
		if("" == strTemp)					//没有修改默认值
		{	
			GetDlgItem(uiDefaultAry[uiLoop])->GetWindowTextW(strTemp);
			if ("0x" == strTemp.Left(2))
				ulRetValue = _tcstol(strTemp, 0, 16);
			else
				ulRetValue = _tcstol(strTemp, 0, 10);
		}
		else	// 获取Default参数值
		{
			GetDlgItem(uiDefaultAry[uiLoop])->GetWindowTextW(strTemp);
			if ("0x" == strTemp.Left(2))
				ulRetValue = _tcstol(strTemp, 0, 16);
			else
				ulRetValue = _tcstol(strTemp, 0, 10);
		}
	}
	return ulRetValue;

}

UINT8_T ParamConfig::IsEmptyModifyPC(UINT16_T* uiModifyAry, UINT16_T* uiDefaultAry, UINT16_T uiCount)
{
	CString strTemp;
	UINT8_T ucRes = 0;
	for(UINT16_T uiLoop = 0; uiLoop < uiCount; uiLoop++)
	{
		GetDlgItem(uiModifyAry[uiLoop])->GetWindowTextW(strTemp);	
		if("" == strTemp)	// 没有修改默认值	
			ucRes++;	
		else
			GetDlgItem(uiDefaultAry[uiLoop])->SetWindowTextW(strTemp);
	}
	return ucRes;
}

BOOL ParamConfig::IsEmptyModifyPC_J1850VPW()
{
	UINT8_T ucResults = 0;
	UINT16_T uiDefaultAry[2] = {IDC_ED_VALUE_DATA_RATE, IDC_ED_VALUE_LOOPBACK};
	UINT16_T uiModifyAry[2] = {IDC_ED_MODIFY_DATA_RATE, IDC_ED_MODIFY_LOOPBACK};

	ucResults = IsEmptyModifyPC(uiModifyAry, uiDefaultAry, 2);
	if (ucResults == 2)
		return TRUE;
	else
		return FALSE;
}

void ParamConfig::SetIoctl_J1850VPW()
{
	UINT32_T ulDataRateValue=0, ulLoopBackValue=0;
	UINT32_T RetVal;

	SCONFIG_LIST CfgList;
	CfgList.ulNumOfParams = 2;		//number of SCONFIG elements

	if(!IsEmptyModifyPC_J1850VPW())
	{
		// SET_CONFIG 参数列表
		SCONFIG Cfgs[] = {
			{DATA_RATE, GetParamConfigValue(DATA_RATE)},
			{LOOPBACK, GetParamConfigValue(LOOPBACK)}
		};

		saveChannelioctldata[uParamconfigCurrChannelNum-1]._ioctlParam.ulDataRate = GetParamConfigValue(DATA_RATE);
		saveChannelioctldata[uParamconfigCurrChannelNum-1]._ioctlParam.bLookBack = GetParamConfigValue(LOOPBACK);
		saveChannelioctldata[uParamconfigCurrChannelNum-1]._ioctlParam.bIsSetStatus = TRUE;

		CfgList.pConfigPtr = Cfgs;

		g_ThreadLock.Lock();
		RetVal = _PassThruIoctl(m_ParamConfigSdk.m_Channel[uParamconfigCurrChannelNum-1].GetChannelId(), SET_CONFIG, &CfgList, NULL);
		g_ThreadLock.Unlock();
		PASSTHRUMESSAGEVIEW(RetVal, L"PassThruIoctl_SET_CONFIG,()-OK");
	}

	// GET_CONFIG 参数列表
	SCONFIG CfgsGet[]={
		{DATA_RATE, ulDataRateValue},
		{LOOPBACK, ulLoopBackValue}
	};

	CfgList.pConfigPtr = CfgsGet;		//array of SCONFIG

	g_ThreadLock.Lock();
	RetVal = _PassThruIoctl(m_ParamConfigSdk.m_Channel[uParamconfigCurrChannelNum-1].GetChannelId(), GET_CONFIG, &CfgList, NULL);
	g_ThreadLock.Unlock();
	PASSTHRUMESSAGEVIEW(RetVal, L"PassThruIoctl_GET_CONFIG,()-OK");
	
}

BOOL ParamConfig::IsEmptyModifyPC_J1850PWM()
{
	UINT8_T ucResults = 0;
	UINT16_T uiDefaultAry[4] = {IDC_ED_VALUE_DATA_RATE, IDC_ED_VALUE_LOOPBACK, IDC_ED_VALUE_NODE_ADDRESS, IDC_ED_VALUE_NETWORK_LINE};
	UINT16_T uiModifyAry[4] = {IDC_ED_MODIFY_DATA_RATE, IDC_ED_MODIFY_LOOPBACK, IDC_ED_MODIFY_NODE_ADDRESS, IDC_ED_MODIFY_NETWORK_LINE};

	ucResults = IsEmptyModifyPC(uiModifyAry, uiDefaultAry, 4);
	if (ucResults == 4)
		return TRUE;
	else
		return FALSE;
}

void ParamConfig::SetIoctl_J1850PWM()
{
	UINT32_T ulDataRateValue=0, ulLoopBackValue=0, ulNodeAddress=0, ulNetworkLine=0;
	UINT32_T RetVal;

	SCONFIG_LIST CfgList;
	CfgList.ulNumOfParams = 4;		//number of SCONFIG elements

	BOOL bBool = IsEmptyModifyPC_J1850PWM();
	if(!bBool)
	{
		// SET_CONFIG 参数列表
		SCONFIG Cfgs[] = {
			{DATA_RATE, GetParamConfigValue(DATA_RATE)},
			{LOOPBACK, GetParamConfigValue(LOOPBACK)},
			{NODE_ADDRESS, GetParamConfigValue(NODE_ADDRESS)	},
			{NETWORK_LINE, GetParamConfigValue(NETWORK_LINE)}
		};
		
		saveChannelioctldata[uParamconfigCurrChannelNum-1]._ioctlParam.ulDataRate = GetParamConfigValue(DATA_RATE);
		saveChannelioctldata[uParamconfigCurrChannelNum-1]._ioctlParam.bLookBack = GetParamConfigValue(LOOPBACK);
		saveChannelioctldata[uParamconfigCurrChannelNum-1]._ioctlParam.ucNodeAddress = GetParamConfigValue(NODE_ADDRESS);
		saveChannelioctldata[uParamconfigCurrChannelNum-1]._ioctlParam.ucNetworkLine = GetParamConfigValue(NETWORK_LINE);
		saveChannelioctldata[uParamconfigCurrChannelNum-1]._ioctlParam.bIsSetStatus = TRUE;

		CfgList.pConfigPtr = Cfgs;
		g_ThreadLock.Lock();
		RetVal = _PassThruIoctl(m_ParamConfigSdk.m_Channel[uParamconfigCurrChannelNum-1].GetChannelId(), SET_CONFIG, &CfgList, NULL);
		g_ThreadLock.Unlock();
		PASSTHRUMESSAGEVIEW(RetVal, L"PassThruIoctl_SET_CONFIG,()-OK");
	}

	// GET_CONFIG 参数列表
	SCONFIG CfgsGet[]={
		{DATA_RATE, ulDataRateValue},
		{LOOPBACK, ulLoopBackValue},
		{NODE_ADDRESS, ulNodeAddress},
		{NETWORK_LINE, ulNetworkLine}
	};
	CfgList.pConfigPtr = CfgsGet;		//array of SCONFIG
	g_ThreadLock.Lock();
	RetVal = _PassThruIoctl(m_ParamConfigSdk.m_Channel[uParamconfigCurrChannelNum-1].GetChannelId(), GET_CONFIG, &CfgList, NULL);
	g_ThreadLock.Unlock();
	PASSTHRUMESSAGEVIEW(RetVal, L"PassThruIoctl_GET_CONFIG,()-OK");
}

BOOL ParamConfig::IsEmptyModifyPC_ISO9141()
{
	UINT8_T ucResults = 0;
	UINT16_T uiDefaultAry[17] = {IDC_ED_VALUE_DATA_RATE, IDC_ED_VALUE_LOOPBACK, IDC_ED_VALUE_P1_MAX, IDC_ED_VALUE_P3_MIN, 
		IDC_ED_VALUE_P4_MIN, IDC_ED_VALUE_W0, IDC_ED_VALUE_W1, IDC_ED_VALUE_W2, IDC_ED_VALUE_W3, IDC_ED_VALUE_W4, IDC_ED_VALUE_W5, 
		IDC_ED_VALUE_TIDLE, IDC_ED_VALUE_TINIL, IDC_ED_VALUE_TWUP, IDC_ED_VALUE_PARITY, IDC_ED_VALUE_DATA_BITS, IDC_ED_VALUE_FIVE_BAUD_MOD};
	UINT16_T uiModifyAry[17] = {IDC_ED_MODIFY_DATA_RATE, IDC_ED_MODIFY_LOOPBACK, IDC_ED_MODIFY_P1_MAX, IDC_ED_MODIFY_P3_MIN, 
		IDC_ED_MODIFY_P4_MIN, IDC_ED_MODIFY_W0, IDC_ED_MODIFY_W1, IDC_ED_MODIFY_W2, IDC_ED_MODIFY_W3, IDC_ED_MODIFY_W4, IDC_ED_MODIFY_W5, 
		IDC_ED_MODIFY_TIDLE, IDC_ED_MODIFY_TINIL, IDC_ED_MODIFY_TWUP, IDC_ED_MODIFY_PARITY, IDC_ED_MODIFY_DATA_BITS, IDC_ED_MODIFY_FIVE_BAUD_MOD};
	ucResults = IsEmptyModifyPC(uiModifyAry, uiDefaultAry, 17);
	if (ucResults == 17)
		return TRUE;
	else
		return FALSE;
}

void ParamConfig::SetIoctl_ISO9141()
{
	UINT32_T ulDataRate=0, ulLoopBack=0, ulP1Max=0, ulP3Min=0, ulP4Min=0, ulW0=0, ulW1=0, ulW2=0, ulW3=0, ulW4=0, ulW5=0;
	UINT32_T ulTidle=0, ulTinil=0, ulTwup=0, ulParity=0, ulDataBits=0, ulFiveBaudMod=0;
	UINT32_T RetVal;

	SCONFIG_LIST CfgList;
	CfgList.ulNumOfParams = 17;		//number of SCONFIG elements

	if(!IsEmptyModifyPC_ISO9141())
	{
		// SET_CONFIG 参数列表
		SCONFIG Cfgs[] = {
			{DATA_RATE, GetParamConfigValue(DATA_RATE)},
			{LOOPBACK, GetParamConfigValue(LOOPBACK)},
			{P1_MAX, GetParamConfigValue(P1_MAX)},
			{P3_MIN, GetParamConfigValue(P3_MIN)},
			{P4_MIN, GetParamConfigValue(P4_MIN)},
			{W0, GetParamConfigValue(W0)},
			{W1, GetParamConfigValue(W1)},
			{W2, GetParamConfigValue(W2)},
			{W3, GetParamConfigValue(W3)},
			{W4, GetParamConfigValue(W4)},
			{W5, GetParamConfigValue(W5)},
			{TIDLE, GetParamConfigValue(TIDLE)},
			{TINIL, GetParamConfigValue(TINIL)},
			{TWUP, GetParamConfigValue(TWUP)},
			{PARITY, GetParamConfigValue(PARITY)},
			{DATA_BITS, GetParamConfigValue(DATA_BITS )},
			{FIVE_BAUD_MOD, GetParamConfigValue(FIVE_BAUD_MOD)}
		};

		saveChannelioctldata[uParamconfigCurrChannelNum-1]._ioctlParam.ulDataRate = GetParamConfigValue(DATA_RATE);
		saveChannelioctldata[uParamconfigCurrChannelNum-1]._ioctlParam.bLookBack = GetParamConfigValue(LOOPBACK);
		saveChannelioctldata[uParamconfigCurrChannelNum-1]._ioctlParam.uiP1Max = GetParamConfigValue(P1_MAX);
		saveChannelioctldata[uParamconfigCurrChannelNum-1]._ioctlParam.uiP3Min = GetParamConfigValue(P3_MIN);
		saveChannelioctldata[uParamconfigCurrChannelNum-1]._ioctlParam.uiP4Min = GetParamConfigValue(P4_MIN);
		saveChannelioctldata[uParamconfigCurrChannelNum-1]._ioctlParam.uiW0 = GetParamConfigValue(W0);
		saveChannelioctldata[uParamconfigCurrChannelNum-1]._ioctlParam.uiW1 = GetParamConfigValue(W1);
		saveChannelioctldata[uParamconfigCurrChannelNum-1]._ioctlParam.uiW2 = GetParamConfigValue(W2);
		saveChannelioctldata[uParamconfigCurrChannelNum-1]._ioctlParam.uiW3 = GetParamConfigValue(W3);
		saveChannelioctldata[uParamconfigCurrChannelNum-1]._ioctlParam.uiW4 = GetParamConfigValue(W4);
		saveChannelioctldata[uParamconfigCurrChannelNum-1]._ioctlParam.uiW5 = GetParamConfigValue(W5);
		saveChannelioctldata[uParamconfigCurrChannelNum-1]._ioctlParam.uiTidle = GetParamConfigValue(TIDLE);
		saveChannelioctldata[uParamconfigCurrChannelNum-1]._ioctlParam.uiTinil = GetParamConfigValue(TINIL);
		saveChannelioctldata[uParamconfigCurrChannelNum-1]._ioctlParam.uiTwup = GetParamConfigValue(TWUP);
		saveChannelioctldata[uParamconfigCurrChannelNum-1]._ioctlParam.ucParity = GetParamConfigValue(PARITY);
		saveChannelioctldata[uParamconfigCurrChannelNum-1]._ioctlParam.bDataBits = GetParamConfigValue(DATA_BITS);
		saveChannelioctldata[uParamconfigCurrChannelNum-1]._ioctlParam.ucFiveBaudMod = GetParamConfigValue(FIVE_BAUD_MOD);
		saveChannelioctldata[uParamconfigCurrChannelNum-1]._ioctlParam.bIsSetStatus = TRUE;

		CfgList.pConfigPtr = Cfgs;
		g_ThreadLock.Lock();
		RetVal = _PassThruIoctl(m_ParamConfigSdk.m_Channel[uParamconfigCurrChannelNum-1].GetChannelId(), SET_CONFIG, &CfgList, NULL);
		g_ThreadLock.Unlock();
		PASSTHRUMESSAGEVIEW(RetVal, L"PassThruIoctl_SET_CONFIG,()-OK");
	}

	// GET_CONFIG 参数列表
	SCONFIG CfgsGet[]={
		{DATA_RATE, ulDataRate},
		{LOOPBACK, ulLoopBack},
		{P1_MAX, ulP1Max},
		{P3_MIN, ulP3Min},
		{P4_MIN, ulP4Min},
		{W0, ulW0},
		{W1, ulW1},
		{W2, ulW2},
		{W3, ulW3},
		{W4, ulW4},
		{W5, ulW5},
		{TIDLE, ulTidle},
		{TINIL, ulTinil},
		{TWUP, ulTwup},
		{PARITY, ulParity},
		{DATA_BITS, ulDataBits},
		{FIVE_BAUD_MOD, ulFiveBaudMod}
	};
	CfgList.pConfigPtr = CfgsGet;		//array of SCONFIG
	g_ThreadLock.Lock();
	RetVal = _PassThruIoctl(m_ParamConfigSdk.m_Channel[uParamconfigCurrChannelNum-1].GetChannelId(), GET_CONFIG, &CfgList, NULL);
	g_ThreadLock.Unlock();
	PASSTHRUMESSAGEVIEW(RetVal, L"PassThruIoctl_GET_CONFIG,()-OK");
}

BOOL ParamConfig::IsEmptyModifyPC_ISO9141_PS()
{
	UINT8_T ucResults = 0;
	UINT16_T uiDefaultAry[18] = {IDC_ED_VALUE_DATA_RATE, IDC_ED_VALUE_LOOPBACK, IDC_ED_VALUE_P1_MAX, IDC_ED_VALUE_P3_MIN, 
		IDC_ED_VALUE_P4_MIN, IDC_ED_VALUE_W0, IDC_ED_VALUE_W1, IDC_ED_VALUE_W2, IDC_ED_VALUE_W3, IDC_ED_VALUE_W4, IDC_ED_VALUE_W5, 
		IDC_ED_VALUE_TIDLE, IDC_ED_VALUE_TINIL, IDC_ED_VALUE_TWUP, IDC_ED_VALUE_PARITY, IDC_ED_VALUE_DATA_BITS, 
		IDC_ED_VALUE_FIVE_BAUD_MOD, IDC_ED_VALUE_J1962_PINS};
	UINT16_T uiModifyAry[18] = {IDC_ED_MODIFY_DATA_RATE, IDC_ED_MODIFY_LOOPBACK, IDC_ED_MODIFY_P1_MAX, IDC_ED_MODIFY_P3_MIN, 
		IDC_ED_MODIFY_P4_MIN, IDC_ED_MODIFY_W0, IDC_ED_MODIFY_W1, IDC_ED_MODIFY_W2, IDC_ED_MODIFY_W3, IDC_ED_MODIFY_W4, IDC_ED_MODIFY_W5, 
		IDC_ED_MODIFY_TIDLE, IDC_ED_MODIFY_TINIL, IDC_ED_MODIFY_TWUP, IDC_ED_MODIFY_PARITY, IDC_ED_MODIFY_DATA_BITS, 
		IDC_ED_MODIFY_FIVE_BAUD_MOD, IDC_ED_MODIFY_J1962_PINS};
	ucResults = IsEmptyModifyPC(uiModifyAry, uiDefaultAry, 18);
	if (ucResults == 18)
		return TRUE;
	else
		return FALSE;
}

void ParamConfig::SetIoctl_ISO9141_PS()
{
	UINT32_T ulDataRate=0, ulLoopBack=0, ulP1Max=0, ulP3Min=0, ulP4Min=0, ulW0=0, ulW1=0, ulW2=0, ulW3=0, ulW4=0, ulW5=0;
	UINT32_T ulTidle=0, ulTinil=0, ulTwup=0, ulParity=0, ulDataBits=0, ulFiveBaudMod=0, ulJ1962Pins=0;
	UINT32_T RetVal;

	SCONFIG_LIST CfgList;
	CfgList.ulNumOfParams = 18;		//number of SCONFIG elements

	if(!IsEmptyModifyPC_ISO9141_PS())
	{
		// SET_CONFIG 参数列表
		SCONFIG Cfgs[] = {
			{DATA_RATE, GetParamConfigValue(DATA_RATE)},
			{LOOPBACK, GetParamConfigValue(LOOPBACK)},
			{P1_MAX, GetParamConfigValue(P1_MAX)},
			{P3_MIN, GetParamConfigValue(P3_MIN)},
			{P4_MIN, GetParamConfigValue(P4_MIN)},
			{W0, GetParamConfigValue(W0)},
			{W1, GetParamConfigValue(W1)},
			{W2, GetParamConfigValue(W2)},
			{W3, GetParamConfigValue(W3)},
			{W4, GetParamConfigValue(W4)},
			{W5, GetParamConfigValue(W5)},
			{TIDLE, GetParamConfigValue(TIDLE)},
			{TINIL, GetParamConfigValue(TINIL)},
			{TWUP, GetParamConfigValue(TWUP)},
			{PARITY, GetParamConfigValue(PARITY)},
			{DATA_BITS, GetParamConfigValue(DATA_BITS )},
			{FIVE_BAUD_MOD, GetParamConfigValue(FIVE_BAUD_MOD)},
			{J1962_PINS, GetParamConfigValue(J1962_PINS)}
		};

		saveChannelioctldata[uParamconfigCurrChannelNum-1]._ioctlParam.ulDataRate = GetParamConfigValue(DATA_RATE);
		saveChannelioctldata[uParamconfigCurrChannelNum-1]._ioctlParam.bLookBack = GetParamConfigValue(LOOPBACK);
		saveChannelioctldata[uParamconfigCurrChannelNum-1]._ioctlParam.uiP1Max = GetParamConfigValue(P1_MAX);
		saveChannelioctldata[uParamconfigCurrChannelNum-1]._ioctlParam.uiP3Min = GetParamConfigValue(P3_MIN);
		saveChannelioctldata[uParamconfigCurrChannelNum-1]._ioctlParam.uiP4Min = GetParamConfigValue(P4_MIN);
		saveChannelioctldata[uParamconfigCurrChannelNum-1]._ioctlParam.uiW0 = GetParamConfigValue(W0);
		saveChannelioctldata[uParamconfigCurrChannelNum-1]._ioctlParam.uiW1 = GetParamConfigValue(W1);
		saveChannelioctldata[uParamconfigCurrChannelNum-1]._ioctlParam.uiW2 = GetParamConfigValue(W2);
		saveChannelioctldata[uParamconfigCurrChannelNum-1]._ioctlParam.uiW3 = GetParamConfigValue(W3);
		saveChannelioctldata[uParamconfigCurrChannelNum-1]._ioctlParam.uiW4 = GetParamConfigValue(W4);
		saveChannelioctldata[uParamconfigCurrChannelNum-1]._ioctlParam.uiW5 = GetParamConfigValue(W5);
		saveChannelioctldata[uParamconfigCurrChannelNum-1]._ioctlParam.uiTidle = GetParamConfigValue(TIDLE);
		saveChannelioctldata[uParamconfigCurrChannelNum-1]._ioctlParam.uiTinil = GetParamConfigValue(TINIL);
		saveChannelioctldata[uParamconfigCurrChannelNum-1]._ioctlParam.uiTwup = GetParamConfigValue(TWUP);
		saveChannelioctldata[uParamconfigCurrChannelNum-1]._ioctlParam.ucParity = GetParamConfigValue(PARITY);
		saveChannelioctldata[uParamconfigCurrChannelNum-1]._ioctlParam.bDataBits = GetParamConfigValue(DATA_BITS);
		saveChannelioctldata[uParamconfigCurrChannelNum-1]._ioctlParam.ucFiveBaudMod = GetParamConfigValue(FIVE_BAUD_MOD);
		saveChannelioctldata[uParamconfigCurrChannelNum-1]._ioctlParam.uiJ1962Pins = GetParamConfigValue(J1962_PINS);
		saveChannelioctldata[uParamconfigCurrChannelNum-1]._ioctlParam.bIsSetStatus = TRUE;

		CfgList.pConfigPtr = Cfgs;
		g_ThreadLock.Lock();
		RetVal = _PassThruIoctl(m_ParamConfigSdk.m_Channel[uParamconfigCurrChannelNum-1].GetChannelId(), SET_CONFIG, &CfgList, NULL);
		g_ThreadLock.Unlock();
		PASSTHRUMESSAGEVIEW(RetVal, L"PassThruIoctl_SET_CONFIG,()-OK");
	}

	// GET_CONFIG 参数列表
	SCONFIG CfgsGet[]={
		{DATA_RATE, ulDataRate},
		{LOOPBACK, ulLoopBack},
		{P1_MAX, ulP1Max},
		{P3_MIN, ulP3Min},
		{P4_MIN, ulP4Min},
		{W0, ulW0},
		{W1, ulW1},
		{W2, ulW2},
		{W3, ulW3},
		{W4, ulW4},
		{W5, ulW5},
		{TIDLE, ulTidle},
		{TINIL, ulTinil},
		{TWUP, ulTwup},
		{PARITY, ulParity},
		{DATA_BITS, ulDataBits},
		{FIVE_BAUD_MOD, ulFiveBaudMod},
		{J1962_PINS, ulJ1962Pins}
	};
	CfgList.pConfigPtr = CfgsGet;		//array of SCONFIG
	g_ThreadLock.Lock();
	RetVal = _PassThruIoctl(m_ParamConfigSdk.m_Channel[uParamconfigCurrChannelNum-1].GetChannelId(), GET_CONFIG, &CfgList, NULL);
	g_ThreadLock.Unlock();
	PASSTHRUMESSAGEVIEW(RetVal, L"PassThruIoctl_GET_CONFIG,()-OK");
}

BOOL ParamConfig::IsEmptyModifyPC_ISO14230()
{
	UINT8_T ucResults = 0;
	UINT16_T uiDefaultAry[17] = {IDC_ED_VALUE_DATA_RATE, IDC_ED_VALUE_LOOPBACK, IDC_ED_VALUE_P1_MAX, IDC_ED_VALUE_P3_MIN, 
		IDC_ED_VALUE_P4_MIN, IDC_ED_VALUE_W0, IDC_ED_VALUE_W1, IDC_ED_VALUE_W2, IDC_ED_VALUE_W3, IDC_ED_VALUE_W4, IDC_ED_VALUE_W5, 
		IDC_ED_VALUE_TIDLE, IDC_ED_VALUE_TINIL, IDC_ED_VALUE_TWUP, IDC_ED_VALUE_PARITY, IDC_ED_VALUE_DATA_BITS, IDC_ED_VALUE_FIVE_BAUD_MOD};
	UINT16_T uiModifyAry[17] = {IDC_ED_MODIFY_DATA_RATE, IDC_ED_MODIFY_LOOPBACK, IDC_ED_MODIFY_P1_MAX, IDC_ED_MODIFY_P3_MIN, 
		IDC_ED_MODIFY_P4_MIN, IDC_ED_MODIFY_W0, IDC_ED_MODIFY_W1, IDC_ED_MODIFY_W2, IDC_ED_MODIFY_W3, IDC_ED_MODIFY_W4, IDC_ED_MODIFY_W5, 
		IDC_ED_MODIFY_TIDLE, IDC_ED_MODIFY_TINIL, IDC_ED_MODIFY_TWUP, IDC_ED_MODIFY_PARITY, IDC_ED_MODIFY_DATA_BITS, IDC_ED_MODIFY_FIVE_BAUD_MOD};
	ucResults = IsEmptyModifyPC(uiModifyAry, uiDefaultAry, 17);
	if (ucResults == 17)
		return TRUE;
	else
		return FALSE;
}

void ParamConfig::SetIoctl_ISO14230()
{
	UINT32_T ulDataRate=0, ulLoopBack=0, ulP1Max=0, ulP3Min=0, ulP4Min=0, ulW0=0, ulW1=0, ulW2=0, ulW3=0, ulW4=0, ulW5=0;
	UINT32_T ulTidle=0, ulTinil=0, ulTwup=0, ulParity=0, ulDataBits=0, ulFiveBaudMod=0;
	UINT32_T RetVal;

	SCONFIG_LIST CfgList;
	CfgList.ulNumOfParams = 17;		//number of SCONFIG elements

	if(!IsEmptyModifyPC_ISO14230())
	{
		// SET_CONFIG 参数列表
		SCONFIG Cfgs[] = {
			{DATA_RATE, GetParamConfigValue(DATA_RATE)},
			{LOOPBACK, GetParamConfigValue(LOOPBACK)},
			{P1_MAX, GetParamConfigValue(P1_MAX)},
			{P3_MIN, GetParamConfigValue(P3_MIN)},
			{P4_MIN, GetParamConfigValue(P4_MIN)},
			{W0, GetParamConfigValue(W0)},
			{W1, GetParamConfigValue(W1)},
			{W2, GetParamConfigValue(W2)},
			{W3, GetParamConfigValue(W3)},
			{W4, GetParamConfigValue(W4)},
			{W5, GetParamConfigValue(W5)},
			{TIDLE, GetParamConfigValue(TIDLE)},
			{TINIL, GetParamConfigValue(TINIL)},
			{TWUP, GetParamConfigValue(TWUP)},
			{PARITY, GetParamConfigValue(PARITY)},
			{DATA_BITS, GetParamConfigValue(DATA_BITS )},
			{FIVE_BAUD_MOD, GetParamConfigValue(FIVE_BAUD_MOD)}
		};

		saveChannelioctldata[uParamconfigCurrChannelNum-1]._ioctlParam.ulDataRate = GetParamConfigValue(DATA_RATE);
		saveChannelioctldata[uParamconfigCurrChannelNum-1]._ioctlParam.bLookBack = GetParamConfigValue(LOOPBACK);
		saveChannelioctldata[uParamconfigCurrChannelNum-1]._ioctlParam.uiP1Max = GetParamConfigValue(P1_MAX);
		saveChannelioctldata[uParamconfigCurrChannelNum-1]._ioctlParam.uiP3Min = GetParamConfigValue(P3_MIN);
		saveChannelioctldata[uParamconfigCurrChannelNum-1]._ioctlParam.uiP4Min = GetParamConfigValue(P4_MIN);
		saveChannelioctldata[uParamconfigCurrChannelNum-1]._ioctlParam.uiW0 = GetParamConfigValue(W0);
		saveChannelioctldata[uParamconfigCurrChannelNum-1]._ioctlParam.uiW1 = GetParamConfigValue(W1);
		saveChannelioctldata[uParamconfigCurrChannelNum-1]._ioctlParam.uiW2 = GetParamConfigValue(W2);
		saveChannelioctldata[uParamconfigCurrChannelNum-1]._ioctlParam.uiW3 = GetParamConfigValue(W3);
		saveChannelioctldata[uParamconfigCurrChannelNum-1]._ioctlParam.uiW4 = GetParamConfigValue(W4);
		saveChannelioctldata[uParamconfigCurrChannelNum-1]._ioctlParam.uiW5 = GetParamConfigValue(W5);
		saveChannelioctldata[uParamconfigCurrChannelNum-1]._ioctlParam.uiTidle = GetParamConfigValue(TIDLE);
		saveChannelioctldata[uParamconfigCurrChannelNum-1]._ioctlParam.uiTinil = GetParamConfigValue(TINIL);
		saveChannelioctldata[uParamconfigCurrChannelNum-1]._ioctlParam.uiTwup = GetParamConfigValue(TWUP);
		saveChannelioctldata[uParamconfigCurrChannelNum-1]._ioctlParam.ucParity = GetParamConfigValue(PARITY);
		saveChannelioctldata[uParamconfigCurrChannelNum-1]._ioctlParam.bDataBits = GetParamConfigValue(DATA_BITS);
		saveChannelioctldata[uParamconfigCurrChannelNum-1]._ioctlParam.ucFiveBaudMod = GetParamConfigValue(FIVE_BAUD_MOD);
		saveChannelioctldata[uParamconfigCurrChannelNum-1]._ioctlParam.bIsSetStatus = TRUE;

		CfgList.pConfigPtr = Cfgs;
		g_ThreadLock.Lock();
		RetVal = _PassThruIoctl(m_ParamConfigSdk.m_Channel[uParamconfigCurrChannelNum-1].GetChannelId(), SET_CONFIG, &CfgList, NULL);
		g_ThreadLock.Unlock();
		PASSTHRUMESSAGEVIEW(RetVal, L"PassThruIoctl_SET_CONFIG,()-OK");
	}

	// GET_CONFIG 参数列表
	SCONFIG CfgsGet[]={
		{DATA_RATE, ulDataRate},
		{LOOPBACK, ulLoopBack},
		{P1_MAX, ulP1Max},
		{P3_MIN, ulP3Min},
		{P4_MIN, ulP4Min},
		{W0, ulW0},
		{W1, ulW1},
		{W2, ulW2},
		{W3, ulW3},
		{W4, ulW4},
		{W5, ulW5},
		{TIDLE, ulTidle},
		{TINIL, ulTinil},
		{TWUP, ulTwup},
		{PARITY, ulParity},
		{DATA_BITS, ulDataBits},
		{FIVE_BAUD_MOD, ulFiveBaudMod}
	};
	CfgList.pConfigPtr = CfgsGet;		//array of SCONFIG
	g_ThreadLock.Lock();
	RetVal = _PassThruIoctl(m_ParamConfigSdk.m_Channel[uParamconfigCurrChannelNum-1].GetChannelId(), GET_CONFIG, &CfgList, NULL);
	g_ThreadLock.Unlock();
	PASSTHRUMESSAGEVIEW(RetVal, L"PassThruIoctl_GET_CONFIG,()-OK");
}

BOOL ParamConfig::IsEmptyModifyPC_CAN()
{
	UINT8_T ucResults = 0;
	UINT16_T uiDefaultAry[4] = {IDC_ED_VALUE_DATA_RATE, IDC_ED_VALUE_LOOPBACK, IDC_ED_VALUE_BIT_SAMPLE_POINT, IDC_ED_VALUE_SYNC_JUMP_WIDTH};
	UINT16_T uiModifyAry[4] = {IDC_ED_MODIFY_DATA_RATE, IDC_ED_MODIFY_LOOPBACK, IDC_ED_MODIFY_BIT_SAMPLE_POINT, IDC_ED_MODIFY_SYNC_JUMP_WIDTH};

	ucResults = IsEmptyModifyPC(uiModifyAry, uiDefaultAry, 4);
	if (ucResults == 4)
		return TRUE;
	else
		return FALSE;
}

void ParamConfig::SetIoctl_CAN()
{
	UINT32_T ulDataRateValue=0, ulLoopBackValue=0, ulBitSamplePoint=0, ulSyncJumpPoint=0;
	UINT32_T RetVal;

	SCONFIG_LIST CfgList;
	CfgList.ulNumOfParams = 4;		//number of SCONFIG elements

	if(!IsEmptyModifyPC_CAN())
	{
		// SET_CONFIG 参数列表
		SCONFIG Cfgs[] = {
			{DATA_RATE, GetParamConfigValue(DATA_RATE)},
			{LOOPBACK, GetParamConfigValue(LOOPBACK)},
			{BIT_SAMPLE_POINT, GetParamConfigValue(BIT_SAMPLE_POINT)	},
			{SYNC_JUMP_WIDTH, GetParamConfigValue(SYNC_JUMP_WIDTH)}
		};

		saveChannelioctldata[uParamconfigCurrChannelNum-1]._ioctlParam.ulDataRate = GetParamConfigValue(DATA_RATE);
		saveChannelioctldata[uParamconfigCurrChannelNum-1]._ioctlParam.bLookBack = GetParamConfigValue(LOOPBACK);
		saveChannelioctldata[uParamconfigCurrChannelNum-1]._ioctlParam.ucBitSamplePoint = GetParamConfigValue(BIT_SAMPLE_POINT);
		saveChannelioctldata[uParamconfigCurrChannelNum-1]._ioctlParam.ucSyncJumpWidth = GetParamConfigValue(SYNC_JUMP_WIDTH);
		saveChannelioctldata[uParamconfigCurrChannelNum-1]._ioctlParam.bIsSetStatus = TRUE;

		CfgList.pConfigPtr = Cfgs;
		g_ThreadLock.Lock();
		RetVal = _PassThruIoctl(m_ParamConfigSdk.m_Channel[uParamconfigCurrChannelNum-1].GetChannelId(), SET_CONFIG, &CfgList, NULL);
		g_ThreadLock.Unlock();
		PASSTHRUMESSAGEVIEW(RetVal, L"PassThruIoctl_SET_CONFIG,()-OK");
	}

	// GET_CONFIG 参数列表
	SCONFIG CfgsGet[]={
		{DATA_RATE, ulDataRateValue},
		{LOOPBACK, ulLoopBackValue},
		{BIT_SAMPLE_POINT, ulBitSamplePoint},
		{SYNC_JUMP_WIDTH, ulSyncJumpPoint}
	};
	CfgList.pConfigPtr = CfgsGet;		//array of SCONFIG
	g_ThreadLock.Lock();
	RetVal = _PassThruIoctl(m_ParamConfigSdk.m_Channel[uParamconfigCurrChannelNum-1].GetChannelId(), GET_CONFIG, &CfgList, NULL);
	g_ThreadLock.Unlock();
	PASSTHRUMESSAGEVIEW(RetVal, L"PassThruIoctl_GET_CONFIG,()-OK");
}

BOOL ParamConfig::IsEmptyModifyPC_CAN_PS()
{
	UINT8_T ucResults = 0;
	UINT16_T uiDefaultAry[5] = {IDC_ED_VALUE_DATA_RATE, IDC_ED_VALUE_LOOPBACK, IDC_ED_VALUE_BIT_SAMPLE_POINT, IDC_ED_VALUE_SYNC_JUMP_WIDTH, IDC_ED_VALUE_J1962_PINS};
	UINT16_T uiModifyAry[5] = {IDC_ED_MODIFY_DATA_RATE, IDC_ED_MODIFY_LOOPBACK, IDC_ED_MODIFY_BIT_SAMPLE_POINT, IDC_ED_MODIFY_SYNC_JUMP_WIDTH, IDC_ED_MODIFY_J1962_PINS};

	ucResults = IsEmptyModifyPC(uiModifyAry, uiDefaultAry, 5);
	if (ucResults == 5)
		return TRUE;
	else
		return FALSE;
}

void ParamConfig::SetIoctl_CAN_PS()
{
	UINT32_T ulDataRateValue=0, ulLoopBackValue=0, ulBitSamplePoint=0, ulSyncJumpPoint=0, ulJ1962Pins=0;
	UINT32_T RetVal;

	SCONFIG_LIST CfgList;
	CfgList.ulNumOfParams = 5;		//number of SCONFIG elements

	if(!IsEmptyModifyPC_CAN_PS())
	{
		// SET_CONFIG 参数列表
		SCONFIG Cfgs[] = {
			{DATA_RATE, GetParamConfigValue(DATA_RATE)},
			{LOOPBACK, GetParamConfigValue(LOOPBACK)},
			{BIT_SAMPLE_POINT, GetParamConfigValue(BIT_SAMPLE_POINT)},
			{SYNC_JUMP_WIDTH, GetParamConfigValue(SYNC_JUMP_WIDTH)},
			{J1962_PINS, GetParamConfigValue(J1962_PINS)}
		};

		saveChannelioctldata[uParamconfigCurrChannelNum-1]._ioctlParam.ulDataRate = GetParamConfigValue(DATA_RATE);
		saveChannelioctldata[uParamconfigCurrChannelNum-1]._ioctlParam.bLookBack = GetParamConfigValue(LOOPBACK);
		saveChannelioctldata[uParamconfigCurrChannelNum-1]._ioctlParam.ucBitSamplePoint = GetParamConfigValue(BIT_SAMPLE_POINT);
		saveChannelioctldata[uParamconfigCurrChannelNum-1]._ioctlParam.ucSyncJumpWidth = GetParamConfigValue(SYNC_JUMP_WIDTH);
		saveChannelioctldata[uParamconfigCurrChannelNum-1]._ioctlParam.uiJ1962Pins = GetParamConfigValue(J1962_PINS);
		saveChannelioctldata[uParamconfigCurrChannelNum-1]._ioctlParam.bIsSetStatus = TRUE;

		CfgList.pConfigPtr = Cfgs;
		g_ThreadLock.Lock();
		RetVal = _PassThruIoctl(m_ParamConfigSdk.m_Channel[uParamconfigCurrChannelNum-1].GetChannelId(), SET_CONFIG, &CfgList, NULL);
		g_ThreadLock.Unlock();
		PASSTHRUMESSAGEVIEW(RetVal, L"PassThruIoctl_SET_CONFIG,()-OK");
	}

	// GET_CONFIG 参数列表
	SCONFIG CfgsGet[]={
		{DATA_RATE, ulDataRateValue},
		{LOOPBACK, ulLoopBackValue},
		{BIT_SAMPLE_POINT, ulBitSamplePoint},
		{SYNC_JUMP_WIDTH, ulSyncJumpPoint},
		{J1962_PINS, ulJ1962Pins}
	};
	CfgList.pConfigPtr = CfgsGet;		//array of SCONFIG
	g_ThreadLock.Lock();
	RetVal = _PassThruIoctl(m_ParamConfigSdk.m_Channel[uParamconfigCurrChannelNum-1].GetChannelId(), GET_CONFIG, &CfgList, NULL);
	g_ThreadLock.Unlock();
	PASSTHRUMESSAGEVIEW(RetVal, L"PassThruIoctl_GET_CONFIG,()-OK");
}

BOOL ParamConfig::IsEmptyModifyPC_ISO15765()
{
	UINT8_T ucResults = 0;	
	UINT16_T uiDefaultAry[12] = {IDC_ED_VALUE_DATA_RATE, IDC_ED_VALUE_LOOPBACK, IDC_ED_VALUE_BIT_SAMPLE_POINT, 
		IDC_ED_VALUE_SYNC_JUMP_WIDTH, IDC_ED_VALUE_ISO15765_BS, IDC_ED_VALUE_ISO15765_STMIN, IDC_ED_VALUE_BS_TX, IDC_ED_VALUE_STMIN_TX, 
		IDC_ED_VALUE_ISO15765_WFT_MAX, IDC_ED_VALUE__ISO15765_SIMULTANEOUS, IDC_ED_VALUE_DT_ISO15765_PAD_BYTE, IDC_ED_VALUE_CAN_MIXED_FORMAT};
	UINT16_T uiModifyAry[12] = {IDC_ED_MODIFY_DATA_RATE, IDC_ED_MODIFY_LOOPBACK, IDC_ED_MODIFY_BIT_SAMPLE_POINT, 
		IDC_ED_MODIFY_SYNC_JUMP_WIDTH, IDC_ED_MODIFY_ISO15765_BS, IDC_ED_MODIFY_ISO15765_STMIN, IDC_ED_MODIFY_BS_TX, IDC_ED_MODIFY_STMIN_TX, 
		IDC_ED_MODIFY_ISO15765_WFT_MAX, IDC_ED_MODIFY__ISO15765_SIMULTANEOUS, 	IDC_ED_MODIFY_DT_ISO15765_PAD_BYTE, IDC_ED_MODIFY_CAN_MIXED_FORMAT};
	ucResults = IsEmptyModifyPC(uiModifyAry, uiDefaultAry, 12);
	if (ucResults == 12)
		return TRUE;
	else
		return FALSE;
}

void ParamConfig::SetIoctl_ISO15765()
{
	UINT32_T ulDataRate=0, ulLoopBack=0, ulBitSamplePoint=0, ulSyncJumpPoint=0, ulIso15765Bs=0, ulIso15765Stmin=0, ulBsTx=0, ulStminTx=0;
	UINT32_T ulIso15765WftMax=0, ulIso15765Simultaneous=0, ulDtIso15765PadType=0,ulCanMixedFormat=0;
	UINT32_T RetVal;

	SCONFIG_LIST CfgList;
	CfgList.ulNumOfParams = 12;		//number of SCONFIG elements

	if(!IsEmptyModifyPC_ISO15765())
	{
		// SET_CONFIG 参数列表
		SCONFIG Cfgs[] = {
			{DATA_RATE, GetParamConfigValue(DATA_RATE)},
			{LOOPBACK, GetParamConfigValue(LOOPBACK)},
			{BIT_SAMPLE_POINT, GetParamConfigValue(BIT_SAMPLE_POINT)},
			{SYNC_JUMP_WIDTH, GetParamConfigValue(SYNC_JUMP_WIDTH)},
			{ISO15765_BS, GetParamConfigValue(ISO15765_BS)},
			{ISO15765_STMIN, GetParamConfigValue(ISO15765_STMIN)},
			{BS_TX, GetParamConfigValue(BS_TX)},
			{STMIN_TX, GetParamConfigValue(STMIN_TX)},
			{ISO15765_WFT_MAX, GetParamConfigValue(ISO15765_WFT_MAX)},
			{_ISO15765_SIMULTANEOUS, GetParamConfigValue(_ISO15765_SIMULTANEOUS)},
			{DT_ISO15765_PAD_BYTE, GetParamConfigValue(DT_ISO15765_PAD_BYTE)},
			{CAN_MIXED_FORMAT, GetParamConfigValue(CAN_MIXED_FORMAT)}
		};

		saveChannelioctldata[uParamconfigCurrChannelNum-1]._ioctlParam.ulDataRate = GetParamConfigValue(DATA_RATE);
		saveChannelioctldata[uParamconfigCurrChannelNum-1]._ioctlParam.bLookBack = GetParamConfigValue(LOOPBACK);
		saveChannelioctldata[uParamconfigCurrChannelNum-1]._ioctlParam.ucBitSamplePoint = GetParamConfigValue(BIT_SAMPLE_POINT);
		saveChannelioctldata[uParamconfigCurrChannelNum-1]._ioctlParam.ucSyncJumpWidth = GetParamConfigValue(SYNC_JUMP_WIDTH);
		saveChannelioctldata[uParamconfigCurrChannelNum-1]._ioctlParam.ucIso15765BS = GetParamConfigValue(ISO15765_BS);
		saveChannelioctldata[uParamconfigCurrChannelNum-1]._ioctlParam.ucIso15765Stmin = GetParamConfigValue(ISO15765_STMIN);
		saveChannelioctldata[uParamconfigCurrChannelNum-1]._ioctlParam.uiBsTx = GetParamConfigValue(BS_TX);
		saveChannelioctldata[uParamconfigCurrChannelNum-1]._ioctlParam.uiStminTx = GetParamConfigValue(STMIN_TX);
		saveChannelioctldata[uParamconfigCurrChannelNum-1]._ioctlParam.ucIso15765WftMax = GetParamConfigValue(ISO15765_WFT_MAX);
		saveChannelioctldata[uParamconfigCurrChannelNum-1]._ioctlParam.ulIso15765Simultaneous = GetParamConfigValue(_ISO15765_SIMULTANEOUS);
		saveChannelioctldata[uParamconfigCurrChannelNum-1]._ioctlParam.ulDtIso15765PadByte = GetParamConfigValue(DT_ISO15765_PAD_BYTE);
		saveChannelioctldata[uParamconfigCurrChannelNum-1]._ioctlParam.ulCanMixedFormat = GetParamConfigValue(CAN_MIXED_FORMAT);
		saveChannelioctldata[uParamconfigCurrChannelNum-1]._ioctlParam.bIsSetStatus = TRUE;

		CfgList.pConfigPtr = Cfgs;
		g_ThreadLock.Lock();
		RetVal = _PassThruIoctl(m_ParamConfigSdk.m_Channel[uParamconfigCurrChannelNum-1].GetChannelId(), SET_CONFIG, &CfgList, NULL);
		g_ThreadLock.Unlock();
		PASSTHRUMESSAGEVIEW(RetVal, L"PassThruIoctl_SET_CONFIG,()-OK");
	}

	// GET_CONFIG 参数列表
	SCONFIG CfgsGet[]={
		{DATA_RATE, ulDataRate},
		{LOOPBACK, ulLoopBack},
		{BIT_SAMPLE_POINT, ulBitSamplePoint},
		{SYNC_JUMP_WIDTH, ulSyncJumpPoint},
		{ISO15765_BS, ulIso15765Bs},
		{ISO15765_STMIN, ulIso15765Stmin},
		{BS_TX, ulBsTx},
		{STMIN_TX, ulStminTx},
		{ISO15765_WFT_MAX, ulIso15765WftMax},
		{_ISO15765_SIMULTANEOUS, ulIso15765Simultaneous},
		{DT_ISO15765_PAD_BYTE, ulDtIso15765PadType},
		{CAN_MIXED_FORMAT, ulCanMixedFormat}
	};
	CfgList.pConfigPtr = CfgsGet;		//array of SCONFIG
	g_ThreadLock.Lock();
	RetVal = _PassThruIoctl(m_ParamConfigSdk.m_Channel[uParamconfigCurrChannelNum-1].GetChannelId(), GET_CONFIG, &CfgList, NULL);
	g_ThreadLock.Unlock();
	PASSTHRUMESSAGEVIEW(RetVal, L"PassThruIoctl_GET_CONFIG,()-OK");
}

BOOL ParamConfig::IsEmptyModifyPC_ISO15765_PS()
{
	UINT8_T ucResults = 0;	
	UINT16_T uiDefaultAry[13] = {IDC_ED_VALUE_DATA_RATE, IDC_ED_VALUE_LOOPBACK, IDC_ED_VALUE_BIT_SAMPLE_POINT, 
		IDC_ED_VALUE_SYNC_JUMP_WIDTH, IDC_ED_VALUE_ISO15765_BS, IDC_ED_VALUE_ISO15765_STMIN, IDC_ED_VALUE_BS_TX, IDC_ED_VALUE_STMIN_TX, 
		IDC_ED_VALUE_ISO15765_WFT_MAX, IDC_ED_VALUE__ISO15765_SIMULTANEOUS, IDC_ED_VALUE_DT_ISO15765_PAD_BYTE, IDC_ED_VALUE_CAN_MIXED_FORMAT,
		IDC_ED_VALUE_J1962_PINS};
	UINT16_T uiModifyAry[13] = {IDC_ED_MODIFY_DATA_RATE, IDC_ED_MODIFY_LOOPBACK, IDC_ED_MODIFY_BIT_SAMPLE_POINT, 
		IDC_ED_MODIFY_SYNC_JUMP_WIDTH, IDC_ED_MODIFY_ISO15765_BS, IDC_ED_MODIFY_ISO15765_STMIN, IDC_ED_MODIFY_BS_TX, IDC_ED_MODIFY_STMIN_TX, 
		IDC_ED_MODIFY_ISO15765_WFT_MAX, IDC_ED_MODIFY__ISO15765_SIMULTANEOUS, 	IDC_ED_MODIFY_DT_ISO15765_PAD_BYTE, IDC_ED_MODIFY_CAN_MIXED_FORMAT,
		IDC_ED_MODIFY_J1962_PINS};
	ucResults = IsEmptyModifyPC(uiModifyAry, uiDefaultAry, 13);
	if (ucResults == 13)
		return TRUE;
	else
		return FALSE;
}

void ParamConfig::SetIoctl_ISO15765_PS()
{
	UINT32_T ulDataRate=0, ulLoopBack=0, ulBitSamplePoint=0, ulSyncJumpPoint=0, ulIso15765Bs=0, ulIso15765Stmin=0, ulBsTx=0, ulStminTx=0;
	UINT32_T ulIso15765WftMax=0, ulIso15765Simultaneous=0, ulDtIso15765PadType=0, ulCanMixedFormat=0, ulJ1962Pins=0;
	UINT32_T RetVal;

	SCONFIG_LIST CfgList;
	CfgList.ulNumOfParams = 13;		//number of SCONFIG elements

	if(!IsEmptyModifyPC_ISO15765_PS())
	{
		// SET_CONFIG 参数列表
		SCONFIG Cfgs[] = {
			{DATA_RATE, GetParamConfigValue(DATA_RATE)},
			{LOOPBACK, GetParamConfigValue(LOOPBACK)},
			{BIT_SAMPLE_POINT, GetParamConfigValue(BIT_SAMPLE_POINT)},
			{SYNC_JUMP_WIDTH, GetParamConfigValue(SYNC_JUMP_WIDTH)},
			{ISO15765_BS, GetParamConfigValue(ISO15765_BS)},
			{ISO15765_STMIN, GetParamConfigValue(ISO15765_STMIN)},
			{BS_TX, GetParamConfigValue(BS_TX)},
			{STMIN_TX, GetParamConfigValue(STMIN_TX)},
			{ISO15765_WFT_MAX, GetParamConfigValue(ISO15765_WFT_MAX)},
			{_ISO15765_SIMULTANEOUS, GetParamConfigValue(_ISO15765_SIMULTANEOUS)},
			{DT_ISO15765_PAD_BYTE, GetParamConfigValue(DT_ISO15765_PAD_BYTE)},
			{CAN_MIXED_FORMAT, GetParamConfigValue(CAN_MIXED_FORMAT)},
			{J1962_PINS, GetParamConfigValue(J1962_PINS)}
		};

		saveChannelioctldata[uParamconfigCurrChannelNum-1]._ioctlParam.ulDataRate = GetParamConfigValue(DATA_RATE);
		saveChannelioctldata[uParamconfigCurrChannelNum-1]._ioctlParam.bLookBack = GetParamConfigValue(LOOPBACK);
		saveChannelioctldata[uParamconfigCurrChannelNum-1]._ioctlParam.ucBitSamplePoint = GetParamConfigValue(BIT_SAMPLE_POINT);
		saveChannelioctldata[uParamconfigCurrChannelNum-1]._ioctlParam.ucSyncJumpWidth = GetParamConfigValue(SYNC_JUMP_WIDTH);
		saveChannelioctldata[uParamconfigCurrChannelNum-1]._ioctlParam.ucIso15765BS = GetParamConfigValue(ISO15765_BS);
		saveChannelioctldata[uParamconfigCurrChannelNum-1]._ioctlParam.ucIso15765Stmin = GetParamConfigValue(ISO15765_STMIN);
		saveChannelioctldata[uParamconfigCurrChannelNum-1]._ioctlParam.uiBsTx = GetParamConfigValue(BS_TX);
		saveChannelioctldata[uParamconfigCurrChannelNum-1]._ioctlParam.uiStminTx = GetParamConfigValue(STMIN_TX);
		saveChannelioctldata[uParamconfigCurrChannelNum-1]._ioctlParam.ucIso15765WftMax = GetParamConfigValue(ISO15765_WFT_MAX);
		saveChannelioctldata[uParamconfigCurrChannelNum-1]._ioctlParam.ulIso15765Simultaneous = GetParamConfigValue(_ISO15765_SIMULTANEOUS);
		saveChannelioctldata[uParamconfigCurrChannelNum-1]._ioctlParam.ulDtIso15765PadByte = GetParamConfigValue(DT_ISO15765_PAD_BYTE);
		saveChannelioctldata[uParamconfigCurrChannelNum-1]._ioctlParam.ulCanMixedFormat = GetParamConfigValue(CAN_MIXED_FORMAT);
		saveChannelioctldata[uParamconfigCurrChannelNum-1]._ioctlParam.uiJ1962Pins = GetParamConfigValue(J1962_PINS);
		saveChannelioctldata[uParamconfigCurrChannelNum-1]._ioctlParam.bIsSetStatus = TRUE;

		CfgList.pConfigPtr = Cfgs;
		g_ThreadLock.Lock();
		RetVal = _PassThruIoctl(m_ParamConfigSdk.m_Channel[uParamconfigCurrChannelNum-1].GetChannelId(), SET_CONFIG, &CfgList, NULL);
		g_ThreadLock.Unlock();
		PASSTHRUMESSAGEVIEW(RetVal, L"PassThruIoctl_SET_CONFIG,()-OK");
	}

	// GET_CONFIG 参数列表
	SCONFIG CfgsGet[]={
		{DATA_RATE, ulDataRate},
		{LOOPBACK, ulLoopBack},
		{BIT_SAMPLE_POINT, ulBitSamplePoint},
		{SYNC_JUMP_WIDTH, ulSyncJumpPoint},
		{ISO15765_BS, ulIso15765Bs},
		{ISO15765_STMIN, ulIso15765Stmin},
		{BS_TX, ulBsTx},
		{STMIN_TX, ulStminTx},
		{ISO15765_WFT_MAX, ulIso15765WftMax},
		{_ISO15765_SIMULTANEOUS, ulIso15765Simultaneous},
		{DT_ISO15765_PAD_BYTE, ulDtIso15765PadType},
		{CAN_MIXED_FORMAT, ulCanMixedFormat},
		{J1962_PINS, ulJ1962Pins}
	};
	CfgList.pConfigPtr = CfgsGet;		//array of SCONFIG
	g_ThreadLock.Lock();
	RetVal = _PassThruIoctl(m_ParamConfigSdk.m_Channel[uParamconfigCurrChannelNum-1].GetChannelId(), GET_CONFIG, &CfgList, NULL);
	g_ThreadLock.Unlock();
	PASSTHRUMESSAGEVIEW(RetVal, L"PassThruIoctl_GET_CONFIG,()-OK");
}

BOOL ParamConfig::IsEmptyModifyPC_SW_CAN_PS()
{
	UINT8_T ucResults = 0;	
	UINT16_T uiDefaultAry[8] = {IDC_ED_VALUE_DATA_RATE, IDC_ED_VALUE_LOOPBACK, IDC_ED_VALUE_BIT_SAMPLE_POINT, IDC_ED_VALUE_SYNC_JUMP_WIDTH, 
		IDC_ED_VALUE_SW_CAN_HS_DATA_RATE, IDC_ED_VALUE_SW_CAN_SPDCHG_ENABLE, IDC_ED_VALUE_SW_CAN_RES_SWITCH, IDC_ED_VALUE_J1962_PINS};
	UINT16_T uiModifyAry[8] = {IDC_ED_MODIFY_DATA_RATE, IDC_ED_MODIFY_LOOPBACK, IDC_ED_MODIFY_BIT_SAMPLE_POINT, IDC_ED_MODIFY_SYNC_JUMP_WIDTH, 
		IDC_ED_MODIFY_SW_CAN_HS_DATA_RATE, IDC_ED_MODIFY_SW_CAN_SPDCHG_ENABLE, IDC_ED_MODIFY_SW_CAN_RES_SWITCH, IDC_ED_MODIFY_J1962_PINS};
	ucResults = IsEmptyModifyPC(uiModifyAry, uiDefaultAry, 8);
	if (ucResults == 8)
		return TRUE;
	else
		return FALSE;
}

void ParamConfig::SetIoctl_SW_CAN_PS()
{
	UINT32_T ulDataRate=0, ulLoopBack=0, ulBitSamplePoint=0, ulSyncJumpPoint=0, ulSwCanHsDataRate=0, ulSwCanSpdchgEnable=0;
	UINT32_T ulSwCanResSwitch=0, ulJ1962Pins=0;
	UINT32_T RetVal;

	SCONFIG_LIST CfgList;
	CfgList.ulNumOfParams = 8;		//number of SCONFIG elements

	if(!IsEmptyModifyPC_SW_CAN_PS())
	{
		// SET_CONFIG 参数列表
		SCONFIG Cfgs[] = {
			{DATA_RATE, GetParamConfigValue(DATA_RATE)},
			{LOOPBACK, GetParamConfigValue(LOOPBACK)},
			{BIT_SAMPLE_POINT, GetParamConfigValue(BIT_SAMPLE_POINT)},
			{SYNC_JUMP_WIDTH, GetParamConfigValue(SYNC_JUMP_WIDTH)},
			{SW_CAN_HS_DATA_RATE, GetParamConfigValue(SW_CAN_HS_DATA_RATE)},
			{SW_CAN_SPDCHG_ENABLE, GetParamConfigValue(SW_CAN_SPDCHG_ENABLE)},
			{SW_CAN_RES_SWITCH, GetParamConfigValue(SW_CAN_RES_SWITCH)},
			{J1962_PINS, GetParamConfigValue(J1962_PINS)}
		};

		saveChannelioctldata[uParamconfigCurrChannelNum-1]._ioctlParam.ulDataRate = GetParamConfigValue(DATA_RATE);
		saveChannelioctldata[uParamconfigCurrChannelNum-1]._ioctlParam.bLookBack = GetParamConfigValue(LOOPBACK);
		saveChannelioctldata[uParamconfigCurrChannelNum-1]._ioctlParam.ucBitSamplePoint = GetParamConfigValue(BIT_SAMPLE_POINT);
		saveChannelioctldata[uParamconfigCurrChannelNum-1]._ioctlParam.ucSyncJumpWidth = GetParamConfigValue(SYNC_JUMP_WIDTH);
		saveChannelioctldata[uParamconfigCurrChannelNum-1]._ioctlParam.ulSwCanHsDataRate = GetParamConfigValue(SW_CAN_HS_DATA_RATE);
		saveChannelioctldata[uParamconfigCurrChannelNum-1]._ioctlParam.ulSwCanSpdchgEnable = GetParamConfigValue(SW_CAN_SPDCHG_ENABLE);
		saveChannelioctldata[uParamconfigCurrChannelNum-1]._ioctlParam.ulSwCanResSwitch = GetParamConfigValue(SW_CAN_RES_SWITCH);
		saveChannelioctldata[uParamconfigCurrChannelNum-1]._ioctlParam.uiJ1962Pins = GetParamConfigValue(J1962_PINS);
		saveChannelioctldata[uParamconfigCurrChannelNum-1]._ioctlParam.bIsSetStatus = TRUE;

		CfgList.pConfigPtr = Cfgs;
		g_ThreadLock.Lock();
		RetVal = _PassThruIoctl(m_ParamConfigSdk.m_Channel[uParamconfigCurrChannelNum-1].GetChannelId(), SET_CONFIG, &CfgList, NULL);
		g_ThreadLock.Unlock();
		PASSTHRUMESSAGEVIEW(RetVal, L"PassThruIoctl_SET_CONFIG,()-OK");
	}

	// GET_CONFIG 参数列表
	SCONFIG CfgsGet[]={
		{DATA_RATE, ulDataRate},
		{LOOPBACK, ulLoopBack},
		{BIT_SAMPLE_POINT, ulBitSamplePoint},
		{SYNC_JUMP_WIDTH, ulSyncJumpPoint},
		{SW_CAN_HS_DATA_RATE, ulSwCanHsDataRate},
		{SW_CAN_SPDCHG_ENABLE, ulSwCanSpdchgEnable},
		{SW_CAN_RES_SWITCH, ulSwCanResSwitch},
		{J1962_PINS, ulJ1962Pins}
	};
	CfgList.pConfigPtr = CfgsGet;		//array of SCONFIG
	g_ThreadLock.Lock();
	RetVal = _PassThruIoctl(m_ParamConfigSdk.m_Channel[uParamconfigCurrChannelNum-1].GetChannelId(), GET_CONFIG, &CfgList, NULL);
	g_ThreadLock.Unlock();
	PASSTHRUMESSAGEVIEW(RetVal, L"PassThruIoctl_GET_CONFIG,()-OK");
}

BOOL ParamConfig::IsEmptyModifyPC_SW_ISO15765_PS()
{
	UINT8_T ucResults = 0;
	UINT16_T uiDefaultAry[16] = {IDC_ED_VALUE_DATA_RATE, IDC_ED_VALUE_LOOPBACK, IDC_ED_VALUE_BIT_SAMPLE_POINT, IDC_ED_VALUE_SYNC_JUMP_WIDTH, 
		IDC_ED_VALUE_ISO15765_BS, IDC_ED_VALUE_ISO15765_STMIN, IDC_ED_VALUE_BS_TX, IDC_ED_VALUE_STMIN_TX, IDC_ED_VALUE_ISO15765_WFT_MAX,
		IDC_ED_VALUE__ISO15765_SIMULTANEOUS, IDC_ED_VALUE_DT_ISO15765_PAD_BYTE, IDC_ED_VALUE_CAN_MIXED_FORMAT, IDC_ED_VALUE_SW_CAN_HS_DATA_RATE,
		IDC_ED_VALUE_SW_CAN_SPDCHG_ENABLE, IDC_ED_VALUE_SW_CAN_RES_SWITCH, IDC_ED_VALUE_J1962_PINS};
	UINT16_T uiModifyAry[16] = {IDC_ED_MODIFY_DATA_RATE, IDC_ED_MODIFY_LOOPBACK, IDC_ED_MODIFY_BIT_SAMPLE_POINT, IDC_ED_MODIFY_SYNC_JUMP_WIDTH, 
		IDC_ED_MODIFY_ISO15765_BS, IDC_ED_MODIFY_ISO15765_STMIN, IDC_ED_MODIFY_BS_TX, IDC_ED_MODIFY_STMIN_TX, IDC_ED_MODIFY_ISO15765_WFT_MAX, 
		IDC_ED_MODIFY__ISO15765_SIMULTANEOUS, IDC_ED_MODIFY_DT_ISO15765_PAD_BYTE, IDC_ED_MODIFY_CAN_MIXED_FORMAT, 
		IDC_ED_MODIFY_SW_CAN_HS_DATA_RATE, IDC_ED_MODIFY_SW_CAN_SPDCHG_ENABLE, IDC_ED_MODIFY_SW_CAN_RES_SWITCH, IDC_ED_MODIFY_J1962_PINS};
	ucResults = IsEmptyModifyPC(uiModifyAry, uiDefaultAry, 16);
	if (ucResults == 16)
		return TRUE;
	else
		return FALSE;
}

void ParamConfig::SetIoctl_SW_ISO15765_PS()
{
	UINT32_T ulDataRate=0, ulLoopBack=0, ulBitSamplePoint=0, ulSyncJumpPoint=0, ulIso15765Bs=0, ulIso15765Stmin=0, ulBsTx=0, ulStminTx=0;
	UINT32_T ulIso15765WftMax=0, ulIso15765Simultaneous=0, ulDtIso15765PadType=0, ulCanMixedFormat=0, ulSwCanHsDataRate=0, ulSwCanSpdchgEnable=0;
	UINT32_T ulSwCanResSwitch=0, ulJ1962Pins=0;
	UINT32_T RetVal;

	SCONFIG_LIST CfgList;
	CfgList.ulNumOfParams = 16;		//number of SCONFIG elements

	if(!IsEmptyModifyPC_SW_ISO15765_PS())
	{
		// SET_CONFIG 参数列表
		SCONFIG Cfgs[] = {
			{DATA_RATE, GetParamConfigValue(DATA_RATE)},
			{LOOPBACK, GetParamConfigValue(LOOPBACK)},
			{BIT_SAMPLE_POINT, GetParamConfigValue(BIT_SAMPLE_POINT)},
			{SYNC_JUMP_WIDTH, GetParamConfigValue(SYNC_JUMP_WIDTH)},
			{ISO15765_BS, GetParamConfigValue(ISO15765_BS)},
			{ISO15765_STMIN, GetParamConfigValue(ISO15765_STMIN)},
			{BS_TX, GetParamConfigValue(BS_TX)},
			{STMIN_TX, GetParamConfigValue(STMIN_TX)},
			{ISO15765_WFT_MAX, GetParamConfigValue(ISO15765_WFT_MAX)},
			{_ISO15765_SIMULTANEOUS, GetParamConfigValue(_ISO15765_SIMULTANEOUS)},
			{DT_ISO15765_PAD_BYTE, GetParamConfigValue(DT_ISO15765_PAD_BYTE)},
			{CAN_MIXED_FORMAT, GetParamConfigValue(CAN_MIXED_FORMAT)},
			{SW_CAN_HS_DATA_RATE, GetParamConfigValue(SW_CAN_HS_DATA_RATE)},
			{SW_CAN_SPDCHG_ENABLE, GetParamConfigValue(SW_CAN_SPDCHG_ENABLE)},
			{SW_CAN_RES_SWITCH, GetParamConfigValue(SW_CAN_RES_SWITCH)},
			{J1962_PINS, GetParamConfigValue(J1962_PINS)}
		};

		saveChannelioctldata[uParamconfigCurrChannelNum-1]._ioctlParam.ulDataRate = GetParamConfigValue(DATA_RATE);
		saveChannelioctldata[uParamconfigCurrChannelNum-1]._ioctlParam.bLookBack = GetParamConfigValue(LOOPBACK);
		saveChannelioctldata[uParamconfigCurrChannelNum-1]._ioctlParam.ucBitSamplePoint = GetParamConfigValue(BIT_SAMPLE_POINT);
		saveChannelioctldata[uParamconfigCurrChannelNum-1]._ioctlParam.ucSyncJumpWidth = GetParamConfigValue(SYNC_JUMP_WIDTH);
		saveChannelioctldata[uParamconfigCurrChannelNum-1]._ioctlParam.ucIso15765BS = GetParamConfigValue(ISO15765_BS);
		saveChannelioctldata[uParamconfigCurrChannelNum-1]._ioctlParam.ucIso15765Stmin = GetParamConfigValue(ISO15765_STMIN);
		saveChannelioctldata[uParamconfigCurrChannelNum-1]._ioctlParam.uiBsTx = GetParamConfigValue(BS_TX);
		saveChannelioctldata[uParamconfigCurrChannelNum-1]._ioctlParam.uiStminTx = GetParamConfigValue(STMIN_TX);
		saveChannelioctldata[uParamconfigCurrChannelNum-1]._ioctlParam.ucIso15765WftMax = GetParamConfigValue(ISO15765_WFT_MAX);
		saveChannelioctldata[uParamconfigCurrChannelNum-1]._ioctlParam.ulIso15765Simultaneous = GetParamConfigValue(_ISO15765_SIMULTANEOUS);
		saveChannelioctldata[uParamconfigCurrChannelNum-1]._ioctlParam.ulDtIso15765PadByte = GetParamConfigValue(DT_ISO15765_PAD_BYTE);
		saveChannelioctldata[uParamconfigCurrChannelNum-1]._ioctlParam.ulCanMixedFormat = GetParamConfigValue(CAN_MIXED_FORMAT);
		saveChannelioctldata[uParamconfigCurrChannelNum-1]._ioctlParam.ulSwCanHsDataRate = GetParamConfigValue(SW_CAN_HS_DATA_RATE);
		saveChannelioctldata[uParamconfigCurrChannelNum-1]._ioctlParam.ulSwCanSpdchgEnable = GetParamConfigValue(SW_CAN_SPDCHG_ENABLE);
		saveChannelioctldata[uParamconfigCurrChannelNum-1]._ioctlParam.ulSwCanResSwitch = GetParamConfigValue(SW_CAN_RES_SWITCH);
		saveChannelioctldata[uParamconfigCurrChannelNum-1]._ioctlParam.uiJ1962Pins = GetParamConfigValue(J1962_PINS);
		saveChannelioctldata[uParamconfigCurrChannelNum-1]._ioctlParam.bIsSetStatus = TRUE;

		CfgList.pConfigPtr = Cfgs;
		g_ThreadLock.Lock();
		RetVal = _PassThruIoctl(m_ParamConfigSdk.m_Channel[uParamconfigCurrChannelNum-1].GetChannelId(), SET_CONFIG, &CfgList, NULL);
		g_ThreadLock.Unlock();
		PASSTHRUMESSAGEVIEW(RetVal, L"PassThruIoctl_SET_CONFIG,()-OK");
	}

	// GET_CONFIG 参数列表
	SCONFIG CfgsGet[]={
		{DATA_RATE, ulDataRate},
		{LOOPBACK, ulLoopBack},
		{BIT_SAMPLE_POINT, ulBitSamplePoint},
		{SYNC_JUMP_WIDTH, ulSyncJumpPoint},
		{ISO15765_BS, ulIso15765Bs},
		{ISO15765_STMIN, ulIso15765Stmin},
		{BS_TX, ulBsTx},
		{STMIN_TX, ulStminTx},
		{ISO15765_WFT_MAX, ulIso15765WftMax},
		{_ISO15765_SIMULTANEOUS, ulIso15765Simultaneous},
		{DT_ISO15765_PAD_BYTE, ulDtIso15765PadType},
		{CAN_MIXED_FORMAT, ulCanMixedFormat},
		{SW_CAN_HS_DATA_RATE, ulSwCanHsDataRate},
		{SW_CAN_SPDCHG_ENABLE, ulSwCanSpdchgEnable},
		{SW_CAN_RES_SWITCH, ulSwCanResSwitch},
		{J1962_PINS, ulJ1962Pins}
	};
	CfgList.pConfigPtr = CfgsGet;		//array of SCONFIG
	g_ThreadLock.Lock();
	RetVal = _PassThruIoctl(m_ParamConfigSdk.m_Channel[uParamconfigCurrChannelNum-1].GetChannelId(), GET_CONFIG, &CfgList, NULL);
	g_ThreadLock.Unlock();
	PASSTHRUMESSAGEVIEW(RetVal, L"PassThruIoctl_GET_CONFIG,()-OK");
}

BOOL ParamConfig::IsEmptyModifyPC_J1939_PS()
{
	return TRUE;
}

void ParamConfig::SetIoctl_J1939_PS()
{

}


BOOL ParamConfig::IsEmptyModifyPC_VOLVO_CAN2()
{
	UINT8_T ucResults = 0;
	UINT16_T uiDefaultAry[4] = {IDC_ED_VALUE_DATA_RATE, IDC_ED_VALUE_LOOPBACK, IDC_ED_VALUE_BIT_SAMPLE_POINT, IDC_ED_VALUE_SYNC_JUMP_WIDTH};
	UINT16_T uiModifyAry[4] = {IDC_ED_MODIFY_DATA_RATE, IDC_ED_MODIFY_LOOPBACK, IDC_ED_MODIFY_BIT_SAMPLE_POINT, IDC_ED_MODIFY_SYNC_JUMP_WIDTH};

	ucResults = IsEmptyModifyPC(uiModifyAry, uiDefaultAry, 4);
	if (ucResults == 4)
		return TRUE;
	else
		return FALSE;
}


void ParamConfig::SetIoctl_VOLVO_CAN2()
{
	UINT32_T ulDataRateValue=0, ulLoopBackValue=0, ulBitSamplePoint=0, ulSyncJumpPoint=0;
	UINT32_T RetVal;

	SCONFIG_LIST CfgList;
	CfgList.ulNumOfParams = 4;		//number of SCONFIG elements

	if(!IsEmptyModifyPC_VOLVO_CAN2())
	{
		// SET_CONFIG 参数列表
		SCONFIG Cfgs[] = {
			{DATA_RATE, GetParamConfigValue(DATA_RATE)},
			{LOOPBACK, GetParamConfigValue(LOOPBACK)},
			{BIT_SAMPLE_POINT, GetParamConfigValue(BIT_SAMPLE_POINT)	},
			{SYNC_JUMP_WIDTH, GetParamConfigValue(SYNC_JUMP_WIDTH)}
		};

		saveChannelioctldata[uParamconfigCurrChannelNum-1]._ioctlParam.ulDataRate = GetParamConfigValue(DATA_RATE);
		saveChannelioctldata[uParamconfigCurrChannelNum-1]._ioctlParam.bLookBack = GetParamConfigValue(LOOPBACK);
		saveChannelioctldata[uParamconfigCurrChannelNum-1]._ioctlParam.ucBitSamplePoint = GetParamConfigValue(BIT_SAMPLE_POINT);
		saveChannelioctldata[uParamconfigCurrChannelNum-1]._ioctlParam.ucSyncJumpWidth = GetParamConfigValue(SYNC_JUMP_WIDTH);
		saveChannelioctldata[uParamconfigCurrChannelNum-1]._ioctlParam.bIsSetStatus = TRUE;

		CfgList.pConfigPtr = Cfgs;
		g_ThreadLock.Lock();
		RetVal = _PassThruIoctl(m_ParamConfigSdk.m_Channel[uParamconfigCurrChannelNum-1].GetChannelId(), SET_CONFIG, &CfgList, NULL);
		g_ThreadLock.Unlock();
		PASSTHRUMESSAGEVIEW(RetVal, L"PassThruIoctl_SET_CONFIG,()-OK");
	}

	// GET_CONFIG 参数列表
	SCONFIG CfgsGet[]={
		{DATA_RATE, ulDataRateValue},
		{LOOPBACK, ulLoopBackValue},
		{BIT_SAMPLE_POINT, ulBitSamplePoint},
		{SYNC_JUMP_WIDTH, ulSyncJumpPoint}
	};
	CfgList.pConfigPtr = CfgsGet;		//array of SCONFIG
	g_ThreadLock.Lock();
	RetVal = _PassThruIoctl(m_ParamConfigSdk.m_Channel[uParamconfigCurrChannelNum-1].GetChannelId(), GET_CONFIG, &CfgList, NULL);
	g_ThreadLock.Unlock();
	PASSTHRUMESSAGEVIEW(RetVal, L"PassThruIoctl_GET_CONFIG,()-OK");
}

void ParamConfig::OnBnClickedBtnParamconfigSet()
{
	switch(m_ParamConfigSdk.m_Channel[uParamconfigCurrChannelNum-1].GetProtocolId())
	{
	case J1850VPW:		SetIoctl_J1850VPW();		break;
	case J1850PWM:		SetIoctl_J1850PWM();		break;
	case ISO9141:		SetIoctl_ISO9141();			break;
	case ISO9141_PS:	SetIoctl_ISO9141_PS();		break;
	case ISO14230:		SetIoctl_ISO14230();		break;
	case CAN:			SetIoctl_CAN();				break;
	case CAN_PS:		SetIoctl_CAN_PS();			break;
	case ISO15765:		SetIoctl_ISO15765();		break;
	case ISO15765_PS:	SetIoctl_ISO15765_PS();		break;
	case SW_CAN_PS:		SetIoctl_SW_CAN_PS();		break;
	case SW_ISO15765_PS:SetIoctl_SW_ISO15765_PS();	break;
	case VOLVO_CAN2:	SetIoctl_VOLVO_CAN2();		break;
	default:
		break;
	}
}

void ParamConfig::OnSysCommand(UINT nID, LPARAM lParam)
{
	// TODO: 在此添加消息处理程序代码和/或调用默认值
	if ((nID & 0xFFF0) == SC_CLOSE) 
	{
		for(UINT8_T ucLoop = 0; ucLoop < CONNECT_MAX_COUNT; ucLoop++)
		{
			if(saveChannelioctldata[ucLoop]._ioctlParam.bIsSetStatus == TRUE)
			{
				SetBit(g_GlobalStatus, 5);
				break;
			}
		}
		SendMessage(WM_CLOSE, 0, 0);
	} 
	else 
	{ 
		CDialog::OnSysCommand(nID, lParam); 
	} 
}

BOOL ParamConfig::PreTranslateMessage(MSG* pMsg)
{
	if((pMsg->message == WM_KEYDOWN) && (pMsg->wParam == VK_ESCAPE))
		return TRUE;  
	else if((pMsg->message == WM_KEYDOWN) && (pMsg->wParam == VK_RETURN))
		return TRUE;
	else
		return CDialogEx::PreTranslateMessage(pMsg);
}

void ParamConfig::SetDefaultParamconfig(UINT32_T ChannelNum, UINT32_T ProtocolId)
{
	UINT32_T RetVal = 0;
	switch (ProtocolId)
	{
	case J1850VPW:
		saveChannelioctldata[ChannelNum-1]._ioctlParam.ulDataRate = 10416;
		saveChannelioctldata[ChannelNum-1]._ioctlParam.bLookBack = 0;
		break;
	case J1850PWM:
		saveChannelioctldata[ChannelNum-1]._ioctlParam.ulDataRate = 41666;
		saveChannelioctldata[ChannelNum-1]._ioctlParam.bLookBack = 0;
		saveChannelioctldata[ChannelNum-1]._ioctlParam.ucNodeAddress = 0x0;
		saveChannelioctldata[ChannelNum-1]._ioctlParam.ucNetworkLine = 0;
		break;
	case ISO9141:
	case ISO9141_PS:
	case ISO14230:
		saveChannelioctldata[ChannelNum-1]._ioctlParam.ulDataRate = 10400;
		saveChannelioctldata[ChannelNum-1]._ioctlParam.bLookBack = 0;
		saveChannelioctldata[ChannelNum-1]._ioctlParam.uiP1Max = 40;
		saveChannelioctldata[ChannelNum-1]._ioctlParam.uiP3Min = 110;
		saveChannelioctldata[ChannelNum-1]._ioctlParam.uiP4Min = 10;
		saveChannelioctldata[ChannelNum-1]._ioctlParam.uiW0 = 300;
		saveChannelioctldata[ChannelNum-1]._ioctlParam.uiW1 = 300;
		saveChannelioctldata[ChannelNum-1]._ioctlParam.uiW2 = 20;
		saveChannelioctldata[ChannelNum-1]._ioctlParam.uiW3 = 20;
		saveChannelioctldata[ChannelNum-1]._ioctlParam.uiW4 = 50;
		saveChannelioctldata[ChannelNum-1]._ioctlParam.uiW5 = 300;
		saveChannelioctldata[ChannelNum-1]._ioctlParam.uiTidle = 300;
		saveChannelioctldata[ChannelNum-1]._ioctlParam.uiTinil = 25;
		saveChannelioctldata[ChannelNum-1]._ioctlParam.uiTwup = 50;
		saveChannelioctldata[ChannelNum-1]._ioctlParam.ucParity = 0;
		saveChannelioctldata[ChannelNum-1]._ioctlParam.bDataBits = 0;
		saveChannelioctldata[ChannelNum-1]._ioctlParam.ucFiveBaudMod = 0;
		if(ProtocolId == ISO9141_PS)
		{	
			saveChannelioctldata[ChannelNum-1]._ioctlParam.uiJ1962Pins = 0x0800;
			SCONFIG_LIST CfgList;
			CfgList.ulNumOfParams = 1;
			SCONFIG Cfgs[] = {{J1962_PINS, saveChannelioctldata[ChannelNum-1]._ioctlParam.uiJ1962Pins}};
			CfgList.pConfigPtr = Cfgs;
			RetVal = _PassThruIoctl(ChannelNum, SET_CONFIG, &CfgList, NULL);
			PASSTHRUMESSAGEVIEW(RetVal, L"PassThruIoctl()-OK");
		}
		break;
	case CAN:
	case CAN_PS:
	case VOLVO_CAN2:
		saveChannelioctldata[ChannelNum-1]._ioctlParam.ulDataRate = 500000;	
		saveChannelioctldata[ChannelNum-1]._ioctlParam.bLookBack = 0;
		saveChannelioctldata[ChannelNum-1]._ioctlParam.ucBitSamplePoint = 80;
		saveChannelioctldata[ChannelNum-1]._ioctlParam.ucSyncJumpWidth = 15;
		if(ProtocolId == CAN_PS)
		{
			saveChannelioctldata[ChannelNum-1]._ioctlParam.ulDataRate = 125000;
			saveChannelioctldata[ChannelNum-1]._ioctlParam.uiJ1962Pins = 0x030b;

			SCONFIG_LIST CfgList;
			CfgList.ulNumOfParams = 1;
			SCONFIG Cfgs[] = {{J1962_PINS, saveChannelioctldata[ChannelNum-1]._ioctlParam.uiJ1962Pins}};
			CfgList.pConfigPtr = Cfgs;
			RetVal = _PassThruIoctl(ChannelNum, SET_CONFIG, &CfgList, NULL);
			PASSTHRUMESSAGEVIEW(RetVal, L"PassThruIoctl()-OK");
		}
		break;
	case ISO15765:
	case ISO15765_PS:
		saveChannelioctldata[ChannelNum-1]._ioctlParam.ulDataRate = 500000;
		saveChannelioctldata[ChannelNum-1]._ioctlParam.bLookBack = 0;
		saveChannelioctldata[ChannelNum-1]._ioctlParam.ucBitSamplePoint = 80;
		saveChannelioctldata[ChannelNum-1]._ioctlParam.ucSyncJumpWidth = 15;
		saveChannelioctldata[ChannelNum-1]._ioctlParam.ucIso15765BS = 0;
		saveChannelioctldata[ChannelNum-1]._ioctlParam.ucIso15765Stmin = 0;
		saveChannelioctldata[ChannelNum-1]._ioctlParam.uiBsTx = 0;
		saveChannelioctldata[ChannelNum-1]._ioctlParam.uiStminTx = 0;
		saveChannelioctldata[ChannelNum-1]._ioctlParam.ucIso15765WftMax = 0;
		saveChannelioctldata[ChannelNum-1]._ioctlParam.ulIso15765Simultaneous = 0;
		saveChannelioctldata[ChannelNum-1]._ioctlParam.ulDtIso15765PadByte = 0x00;
		saveChannelioctldata[ChannelNum-1]._ioctlParam.ulCanMixedFormat = 0;
		if(ProtocolId == ISO15765_PS)
		{
			saveChannelioctldata[ChannelNum-1]._ioctlParam.ulDataRate = 125000;
			saveChannelioctldata[ChannelNum-1]._ioctlParam.uiJ1962Pins = 0x030b;

			SCONFIG_LIST CfgList;
			CfgList.ulNumOfParams = 1;
			SCONFIG Cfgs[] = {{J1962_PINS, saveChannelioctldata[ChannelNum-1]._ioctlParam.uiJ1962Pins}};
			CfgList.pConfigPtr = Cfgs;
			RetVal = _PassThruIoctl(ChannelNum, SET_CONFIG, &CfgList, NULL);
			PASSTHRUMESSAGEVIEW(RetVal, L"PassThruIoctl()-OK");
		}
		break;
	case SW_CAN_PS:
	case SW_ISO15765_PS:
		saveChannelioctldata[ChannelNum-1]._ioctlParam.ulDataRate = 250000;
		saveChannelioctldata[ChannelNum-1]._ioctlParam.bLookBack = 0;
		saveChannelioctldata[ChannelNum-1]._ioctlParam.ucBitSamplePoint = 80;
		saveChannelioctldata[ChannelNum-1]._ioctlParam.ucSyncJumpWidth = 15;
		saveChannelioctldata[ChannelNum-1]._ioctlParam.uiJ1962Pins = 0x0100;
		saveChannelioctldata[ChannelNum-1]._ioctlParam.ulSwCanHsDataRate = 0;
		saveChannelioctldata[ChannelNum-1]._ioctlParam.ulSwCanSpdchgEnable = 0;
		saveChannelioctldata[ChannelNum-1]._ioctlParam.ulSwCanResSwitch = 0;
		if(ProtocolId == SW_ISO15765_PS)
		{
			saveChannelioctldata[ChannelNum-1]._ioctlParam.ucIso15765BS = 0;
			saveChannelioctldata[ChannelNum-1]._ioctlParam.ucIso15765Stmin = 0;
			saveChannelioctldata[ChannelNum-1]._ioctlParam.uiBsTx = 0;
			saveChannelioctldata[ChannelNum-1]._ioctlParam.uiStminTx = 0;
			saveChannelioctldata[ChannelNum-1]._ioctlParam.ucIso15765WftMax = 0;
			saveChannelioctldata[ChannelNum-1]._ioctlParam.ulIso15765Simultaneous = 0;
			saveChannelioctldata[ChannelNum-1]._ioctlParam.ulDtIso15765PadByte = 0x00;
			saveChannelioctldata[ChannelNum-1]._ioctlParam.ulCanMixedFormat = 0;

			SCONFIG_LIST CfgList;
			CfgList.ulNumOfParams = 1;
			SCONFIG Cfgs[] = {{J1962_PINS, saveChannelioctldata[ChannelNum-1]._ioctlParam.uiJ1962Pins}};
			CfgList.pConfigPtr = Cfgs;
			RetVal = _PassThruIoctl(ChannelNum, SET_CONFIG, &CfgList, NULL);
			PASSTHRUMESSAGEVIEW(RetVal, L"PassThruIoctl()-OK");
		}
		break;
	default:
		break;
	}
	saveChannelioctldata[ChannelNum-1]._ioctlParam.bIsSetStatus = FALSE;
}

void ParamConfig::OnCbnSelchangeCbParamconfigProtocol()
{
	CString strProtocolName;
	RebackSortOfControlParamLocal(g_ResId);

	/* 获取当前通道号 */
	((CComboBox*)GetDlgItem(IDC_CB_PARAMCONFIG_PROTOCOL))->GetLBText(((CComboBox*)GetDlgItem(IDC_CB_PARAMCONFIG_PROTOCOL))->GetCurSel(), strProtocolName);
	for(UINT8_T uiLoop = 0; uiLoop < CONNECT_MAX_COUNT; uiLoop++)
	{
		if(ProtocolStrToValue(strProtocolName) == m_ParamConfigSdk.m_Channel[uiLoop].GetProtocolId())
		{	
			uParamconfigCurrChannelNum = m_ParamConfigSdk.m_Channel[uiLoop].GetChannelId();
			break;
		}
	}

	InitControlsDisVisible();
	//SetDefaultParamconfig(uParamconfigCurrChannelNum, m_ParamConfigSdk.GetProtocolId(uParamconfigCurrChannelNum));
	InitUIDefaultValue();
	UILoadProtocolParam(m_ParamConfigSdk.m_Channel[uParamconfigCurrChannelNum-1].GetProtocolId());	
	SortOfControlParamLocal(g_ResId);
	//UIParamConfigDisplay();
}

