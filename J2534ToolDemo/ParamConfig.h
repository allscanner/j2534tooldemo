﻿#pragma once
#include "../J2534/J2534Dll.h"
#include "../J2534/J2534.h"
#include "../J2534/J2534Log.h"
#include "../J2534/PassThruSdk.h"
#include "../J2534/MakeLock.h"
#include "Common/ErrorCode.h"

// ParamConfig 对话框

#define PARAM_COUNT_MAX 30

typedef struct tagIdName
{
	UINT16_T uiStIdSrc[PARAM_COUNT_MAX];
	UINT16_T uiStIdDest[PARAM_COUNT_MAX];
	UINT16_T uiEdValueIdSrc[PARAM_COUNT_MAX];
	UINT16_T uiEdValueIdDest[PARAM_COUNT_MAX];
	UINT16_T uiEdModifyIdSrc[PARAM_COUNT_MAX];
	UINT16_T uiEdModifyIdDest[PARAM_COUNT_MAX];
	INT8_T cParamCount;
}ResourID;

typedef struct tagIoctlConfig
{
	UINT32_T ulIoctlId;
	UINT8_T ucFastCmd[MAX_PATH];
	UINT8_T ucECUAddr;
	UINT8_T ucKeyWord1;
	UINT8_T ucKeyWord2;
}IoctlContent;

class ParamConfig : public CDialogEx
{
	DECLARE_DYNAMIC(ParamConfig)

public:
	ParamConfig(CWnd* pParent = NULL);   // 标准构造函数
	virtual ~ParamConfig();

	PassThruSdk m_ParamConfigSdk;
// 对话框数据
	enum { IDD = IDD_PARAMCONFIG_DIALOG };
protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 支持

	DECLARE_MESSAGE_MAP()
public:
	virtual BOOL OnInitDialog();
	afx_msg void OnBnClickedBtnParamconfigSet();
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);

	void InitStructOperate(ResourID resId);
	void InitUIDefaultValue();
	void InitControlsDisVisible();
	void UILoadParam_J1850VPW();
	void UILoadParam_J1850PWM();
	void UILoadParam_ISO9141();
	void UILoadParam_ISO9141_PS();
	void UILoadParam_ISO14230();
	void UILoadParam_CAN();
	void UILoadParam_CAN_PS();
	void UILoadParam_ISO15765();
	void UILoadParam_ISO15765_PS();
	void UILoadParam_SW_CAN_PS();
	void UILoadParam_SW_ISO15765_PS();
	void UILoadParam_J1939_PS();
	void UILoadParam_VOLVO_CAN2();
	void UILoadProtocolParam(UINT32_T ulProtocolType);
	void UIParamConfigDisplay();
	void UILoadParamConfigSdkParam();
	void CoverSourControlLocal(UINT16_T uiDestControl, UINT16_T uiSourControl);
	void ExChangeControlLocal(UINT16_T uiDestControl, UINT16_T uiSourControl);
	void SortOfControlParamLocal(ResourID resId);
	void RebackSortOfControlParamLocal(ResourID resId);
	void IoctlConfigIsEnabled(UINT16_T uiIndex);
	void SetIoctl_J1850VPW();
	void SetIoctl_J1850PWM();
	void SetIoctl_ISO9141();
	void SetIoctl_ISO9141_PS();
	void SetIoctl_ISO14230();
	void SetIoctl_CAN();
	void SetIoctl_CAN_PS();
	void SetIoctl_ISO15765();
	void SetIoctl_ISO15765_PS();
	void SetIoctl_SW_CAN_PS();
	void SetIoctl_SW_ISO15765_PS();
	void SetIoctl_J1939_PS();
	void SetIoctl_VOLVO_CAN2();
	void SetDefaultParamconfig(UINT32_T ChannelNum, UINT32_T ProtocolId);
	UINT32_T GetParamConfigValue(UINT16_T uiTemp);
	UINT8_T IsEmptyModifyPC(UINT16_T* uiModifyAry, UINT16_T* uiDefaultAry, UINT16_T uiCount);
	BOOL IsEmptyModifyPC_J1850VPW();
	BOOL IsEmptyModifyPC_J1850PWM();
	BOOL IsEmptyModifyPC_ISO9141();
	BOOL IsEmptyModifyPC_ISO9141_PS();
	BOOL IsEmptyModifyPC_ISO14230();
	BOOL IsEmptyModifyPC_CAN();
	BOOL IsEmptyModifyPC_CAN_PS();
	BOOL IsEmptyModifyPC_ISO15765();
	BOOL IsEmptyModifyPC_ISO15765_PS();
	BOOL IsEmptyModifyPC_SW_CAN_PS();
	BOOL IsEmptyModifyPC_SW_ISO15765_PS();
	BOOL IsEmptyModifyPC_J1939_PS();
	BOOL IsEmptyModifyPC_VOLVO_CAN2();
	virtual BOOL PreTranslateMessage(MSG* pMsg);
	afx_msg void OnCbnSelchangeCbParamconfigProtocol();
};
