﻿/*******************************************************************************
 * FILE: FilterConfig.cpp
 * This file is part of ALLScanner VCX software.
 * COPYRIGHT (C) ALLScanner 2008-2014. 
 *		
 * DESC: J2534Tool Periodic Config Parameters's declaration 
 * 
 * HISTORY:
 * Date			Author		Notes	
 * 2016-01-01	my			Create
*******************************************************************************/

#include "stdafx.h"
#include "J2534ToolDemo.h"
#include "PeriodicConfig.h"
#include "afxdialogex.h"


// PeriodicConfig 对话框

IMPLEMENT_DYNAMIC(PeriodicConfig, CDialogEx)

PeriodicConfig::PeriodicConfig(CWnd* pParent /*=NULL*/)
	: CDialogEx(PeriodicConfig::IDD, pParent)
{

}

PeriodicConfig::~PeriodicConfig()
{
}

void PeriodicConfig::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);

}


BEGIN_MESSAGE_MAP(PeriodicConfig, CDialogEx)
	ON_BN_CLICKED(IDC_BTN_PERIODIC_CLEARALL, &PeriodicConfig::OnBnClickedBtnPeriodicClearall)
	ON_BN_CLICKED(IDC_BTN_PERIODIC_CANCEL, &PeriodicConfig::OnBnClickedBtnPeriodicCancel)
	ON_BN_CLICKED(IDC_BTN_PERIODIC_APPLY, &PeriodicConfig::OnBnClickedBtnPeriodicApply)
	ON_EN_CHANGE(IDC_ED_PERIODICCONFIG_MESSAGEDATA0, &PeriodicConfig::OnChangeEdPeriodicconfigMessagedata0)
	ON_EN_CHANGE(IDC_ED_PERIODICCONFIG_MESSAGEDATA1, &PeriodicConfig::OnChangeEdPeriodicconfigMessagedata1)
	ON_EN_CHANGE(IDC_ED_PERIODICCONFIG_MESSAGEDATA2, &PeriodicConfig::OnChangeEdPeriodicconfigMessagedata2)
	ON_EN_CHANGE(IDC_ED_PERIODICCONFIG_MESSAGEDATA3, &PeriodicConfig::OnChangeEdPeriodicconfigMessagedata3)
	ON_EN_CHANGE(IDC_ED_PERIODICCONFIG_MESSAGEDATA4, &PeriodicConfig::OnChangeEdPeriodicconfigMessagedata4)
	ON_EN_CHANGE(IDC_ED_PERIODICCONFIG_MESSAGEDATA5, &PeriodicConfig::OnChangeEdPeriodicconfigMessagedata5)
	ON_EN_CHANGE(IDC_ED_PERIODICCONFIG_MESSAGEDATA6, &PeriodicConfig::OnChangeEdPeriodicconfigMessagedata6)
	ON_EN_CHANGE(IDC_ED_PERIODICCONFIG_MESSAGEDATA7, &PeriodicConfig::OnChangeEdPeriodicconfigMessagedata7)
	ON_EN_CHANGE(IDC_ED_PERIODICCONFIG_MESSAGEDATA8, &PeriodicConfig::OnChangeEdPeriodicconfigMessagedata8)
	ON_EN_CHANGE(IDC_ED_PERIODICCONFIG_MESSAGEDATA9, &PeriodicConfig::OnChangeEdPeriodicconfigMessagedata9)
	//ON_WM_CHAR()
	ON_WM_SYSCOMMAND()
	ON_CBN_SELCHANGE(IDC_CB_PERIODICCONFIG_PROTOCOL, &PeriodicConfig::OnCbnSelchangeCbPeriodicconfigProtocol)
END_MESSAGE_MAP()


// PeriodicConfig 消息处理程序


// Global Variables
UINT32_T uPeriodicCurrChannelNum = 0;
static BinaryData bMessages[PERIODIC_MAX_COUNT];
static UINT32_T uTxFlag[PERIODIC_MAX_COUNT], uInterval[PERIODIC_MAX_COUNT];
static UINT32_T uOutPeriodicId[PERIODIC_MAX_COUNT];

extern CErrorCode _err;
extern CString g_PassthruStatus;
extern UINT32_T g_GlobalStatus;
extern UINT32_T g_DevicesCount;
//extern PeriodicParam saveperiodicdata[PERIODIC_MAX_COUNT];
extern ChannelPeriodics saveChannelperiodicdata[CONNECT_MAX_COUNT];
extern Mutex g_ThreadLock;

#if 1
static int _arryInterval[PERIODIC_MAX_COUNT] = {IDC_ED_PERIODICCONFIG_INTERVAL0, IDC_ED_PERIODICCONFIG_INTERVAL1, IDC_ED_PERIODICCONFIG_INTERVAL2,
	IDC_ED_PERIODICCONFIG_INTERVAL3, IDC_ED_PERIODICCONFIG_INTERVAL4, IDC_ED_PERIODICCONFIG_INTERVAL5, IDC_ED_PERIODICCONFIG_INTERVAL6,
	IDC_ED_PERIODICCONFIG_INTERVAL7, IDC_ED_PERIODICCONFIG_INTERVAL8,	IDC_ED_PERIODICCONFIG_INTERVAL9};
static int _arryMessagedata[PERIODIC_MAX_COUNT] = {IDC_ED_PERIODICCONFIG_MESSAGEDATA0, IDC_ED_PERIODICCONFIG_MESSAGEDATA1, IDC_ED_PERIODICCONFIG_MESSAGEDATA2,
	IDC_ED_PERIODICCONFIG_MESSAGEDATA3, IDC_ED_PERIODICCONFIG_MESSAGEDATA4, IDC_ED_PERIODICCONFIG_MESSAGEDATA5, IDC_ED_PERIODICCONFIG_MESSAGEDATA6,
	IDC_ED_PERIODICCONFIG_MESSAGEDATA7, IDC_ED_PERIODICCONFIG_MESSAGEDATA8, IDC_ED_PERIODICCONFIG_MESSAGEDATA9};
static unsigned int _arryTxflags[PERIODIC_MAX_COUNT] = {IDC_ED_PERIODICCONFIG_TXFLAGS0, IDC_ED_PERIODICCONFIG_TXFLAGS1, IDC_ED_PERIODICCONFIG_TXFLAGS2,
	IDC_ED_PERIODICCONFIG_TXFLAGS3, IDC_ED_PERIODICCONFIG_TXFLAGS4, IDC_ED_PERIODICCONFIG_TXFLAGS5, IDC_ED_PERIODICCONFIG_TXFLAGS6,
	IDC_ED_PERIODICCONFIG_TXFLAGS7, IDC_ED_PERIODICCONFIG_TXFLAGS8, IDC_ED_PERIODICCONFIG_TXFLAGS9};
static unsigned int _arryEnable[PERIODIC_MAX_COUNT] = {IDC_CK_PERIODICCONFIG_ENABLE0, IDC_CK_PERIODICCONFIG_ENABLE1, IDC_CK_PERIODICCONFIG_ENABLE2,
	IDC_CK_PERIODICCONFIG_ENABLE3, IDC_CK_PERIODICCONFIG_ENABLE4, IDC_CK_PERIODICCONFIG_ENABLE5, IDC_CK_PERIODICCONFIG_ENABLE6,
	IDC_CK_PERIODICCONFIG_ENABLE7, IDC_CK_PERIODICCONFIG_ENABLE8, IDC_CK_PERIODICCONFIG_ENABLE9};
static unsigned long _arryNumber[PERIODIC_MAX_COUNT] = {IDC_ST_PERIODICCONFIG_NUM0, IDC_ST_PERIODICCONFIG_NUM1, IDC_ST_PERIODICCONFIG_NUM2, IDC_ST_PERIODICCONFIG_NUM3,
	IDC_ST_PERIODICCONFIG_NUM4, IDC_ST_PERIODICCONFIG_NUM5, IDC_ST_PERIODICCONFIG_NUM6, IDC_ST_PERIODICCONFIG_NUM7, IDC_ST_PERIODICCONFIG_NUM8,
	IDC_ST_PERIODICCONFIG_NUM9};
#endif

#define ARR_MESSAGE_COUNT 10

BOOL PeriodicConfig::OnInitDialog()
{
	CDialogEx::OnInitDialog();
	
	/* 界面显示 */
	if (m_PeriodicSdk.GetDeviceId() == 0)
		UIDisplayPeriodicConfig(FALSE);
	else
		UIDisplayPeriodicConfig(TRUE);


	return TRUE;  // return TRUE unless you set the focus to a control
	// 异常: OCX 属性页应返回 FALSE
}

void PeriodicConfig::InitGlobalPeriodicParam()
{
	for(UINT8_T uiLoop = 0; uiLoop < PERIODIC_MAX_COUNT; uiLoop++)
	{
		memset(&bMessages[uiLoop], 0, sizeof(bMessages[uiLoop]));
		uTxFlag[uiLoop] = 0;
		uInterval[uiLoop] = 100;
		uOutPeriodicId[uiLoop] = 0;
	}
}

/*------------------------------------------------------------------------
*功		 能：界面获取备份过的定时器消息（如果为初次，则为初始化消息内容）
*参 数 说 明：无
*返  回	 值：无
*说	     明：无
------------------------------------------------------------------------*/
void PeriodicConfig::UILoadPeriodicSavedParam()
{
	CString strMsgs, strTxFlag, strInterval;

	for(UINT8_T uiLoop = 0; uiLoop < PERIODIC_MAX_COUNT; uiLoop++)
	{
		strTxFlag = L"";
		strMsgs = HexToString(saveChannelperiodicdata[uPeriodicCurrChannelNum-1]._periodicParam[uiLoop].pMessage.ucConvertData, saveChannelperiodicdata[uPeriodicCurrChannelNum-1]._periodicParam[uiLoop].pMessage.ulResLen);
		GetDlgItem(_arryMessagedata[uiLoop])->SetWindowTextW(strMsgs);

		strTxFlag.Format(L"0x%.8x", (saveChannelperiodicdata[uPeriodicCurrChannelNum-1]._periodicParam[uiLoop].ulTxFlags));
		GetDlgItem(_arryTxflags[uiLoop])->SetWindowTextW(strTxFlag);

		strInterval.Format(L"%d", (saveChannelperiodicdata[uPeriodicCurrChannelNum-1]._periodicParam[uiLoop].ulIntervel));
		GetDlgItem(_arryInterval[uiLoop])->SetWindowTextW(strInterval);

		((CButton*)GetDlgItem(_arryEnable[uiLoop]))->SetCheck(saveChannelperiodicdata[uPeriodicCurrChannelNum-1]._periodicParam[uiLoop].bCheckStatus);
	}
}

/*------------------------------------------------------------------------
*功		 能：界面显示定时器消息
*参 数 说 明：bRes - 表示是否显示有效的定时器消息内容
*返  回	 值：无
*说	     明：如果主窗口为配置好协议，则无法设置定时器消息
------------------------------------------------------------------------*/
void PeriodicConfig::UIDisplayPeriodicConfig(BOOL bRes)
{
	InitGlobalPeriodicParam();
	UILoadPeriodicSdkParam();
	if(bRes == TRUE)
	{				
		InitPeriodicParamInterval(L"100");
		InitPeriodicParamTxflags(L"0x00000000");
		UILoadPeriodicSavedParam();
	}
	else
	{	
		InitPeriodicParamInterval(L"");		
		InitPeriodicParamTxflags(L"");
	}
	//InitPeriodicParamCheckBox();	
	SetPeriodicAllCtlsIsEnable(bRes);
	//SetPeriodicBtnIsEnable(bRes, FALSE, FALSE);
}

/*------------------------------------------------------------------------
*功		 能：初始化加载Device，Protocol控件的现实
*参 数 说 明：无
*返  回	 值：无
*说	     明：无
------------------------------------------------------------------------*/
void PeriodicConfig::UILoadPeriodicSdkParam()
{
	/* DeviceId 显示 */
	CString	strFilterDevice = L"J2534 Tool ", strProtocolName;
	INT8_T	ucTemp = 0;
	strFilterDevice += UINT32ToDecString(g_DevicesCount);
	GetDlgItem(IDC_ED_PERIODICCONFIG_DEVICE)->SetWindowTextW(strFilterDevice);

	/* ProtocolId 显示 */
	((CComboBox*)GetDlgItem(IDC_CB_PERIODICCONFIG_PROTOCOL))->ResetContent();
	for(UINT8_T ucLoop = 0; ucLoop < CONNECT_MAX_COUNT; ucLoop++)
	{
		if(m_PeriodicSdk.m_Channel[ucLoop].GetChannelId() != FALSE)
		{
			((CComboBox*)GetDlgItem(IDC_CB_PERIODICCONFIG_PROTOCOL))->InsertString(ucTemp,ProtocolValueToStr(m_PeriodicSdk.m_Channel[ucLoop].GetProtocolId()));
			ucTemp++;
		}
	}
	((CComboBox*)GetDlgItem(IDC_CB_PERIODICCONFIG_PROTOCOL))->SetCurSel(0);

	/* 获取当前通道号 */
	((CComboBox*)GetDlgItem(IDC_CB_PERIODICCONFIG_PROTOCOL))->GetLBText(((CComboBox*)GetDlgItem(IDC_CB_PERIODICCONFIG_PROTOCOL))->GetCurSel(), strProtocolName);
	for(UINT8_T uiLoop = 0; uiLoop < CONNECT_MAX_COUNT; uiLoop++)
	{
		if(ProtocolStrToValue(strProtocolName) == m_PeriodicSdk.m_Channel[uiLoop].GetProtocolId())
		{	
			uPeriodicCurrChannelNum = m_PeriodicSdk.m_Channel[uiLoop].GetChannelId();
			break;
		}
	}
}

/*------------------------------------------------------------------------
*功		 能：设置定时器配置参数是否使能
*参 数 说 明：bRes：TRUE-表示显示使能参数控件；反之，则否
*返  回	 值：无
*说	     明：无
------------------------------------------------------------------------*/
void PeriodicConfig::SetPeriodicAllCtlsIsEnable(BOOL bRes)
{
	UINT16_T uiLoop;
	((CStatic*)GetDlgItem(IDC_ST_PERIODICCONFIG_MESSAGESDATA))->EnableWindow(bRes);
	for(uiLoop=0; uiLoop<ARR_MESSAGE_COUNT; uiLoop++)
		((CEdit*)GetDlgItem(_arryMessagedata[uiLoop]))->EnableWindow(bRes);

	((CStatic*)GetDlgItem(IDC_ST_PERIODICCONFIG_TXFLAGS))->EnableWindow(bRes);
	for(uiLoop=0; uiLoop<ARR_MESSAGE_COUNT; uiLoop++)
		((CEdit*)GetDlgItem(_arryTxflags[uiLoop]))->EnableWindow(bRes);

	((CStatic*)GetDlgItem(IDC_ST_PERIODICCONFIG_INTERVAL))->EnableWindow(bRes);
	for(uiLoop=0; uiLoop<ARR_MESSAGE_COUNT; uiLoop++)
		((CEdit*)GetDlgItem(_arryInterval[uiLoop]))->EnableWindow(bRes);

	((CStatic*)GetDlgItem(IDC_ST_PERIODICCONFIG_ENABLE))->EnableWindow(bRes);
	for(uiLoop=0; uiLoop<ARR_MESSAGE_COUNT; uiLoop++)
		((CEdit*)GetDlgItem(_arryEnable[uiLoop]))->EnableWindow(bRes);

	for(uiLoop=0; uiLoop<ARR_MESSAGE_COUNT; uiLoop++)
		GetDlgItem(_arryNumber[uiLoop])->EnableWindow(bRes);
}

/*------------------------------------------------------------------------
*功		 能：初始化定时器消息内容
*参 数 说 明：无
*返  回	 值：无
*说	     明：无
------------------------------------------------------------------------*/
void PeriodicConfig::InitPeriodicParamMessagedata()
{
	for(UINT8_T uiLoop=0; uiLoop<ARR_MESSAGE_COUNT; uiLoop++)
		GetDlgItem(_arryMessagedata[uiLoop])->SetWindowTextW(_T(""));
}

/*------------------------------------------------------------------------
*功		 能：初始化定时器消息发送标记
*参 数 说 明：strTemp-发送TxFlag的字符串
*返  回	 值：无
*说	     明：无
------------------------------------------------------------------------*/
void PeriodicConfig::InitPeriodicParamTxflags(CString strTemp)
{
	for(UINT8_T uiLoop=0; uiLoop<ARR_MESSAGE_COUNT; uiLoop++)
		GetDlgItem(_arryTxflags[uiLoop])->SetWindowTextW(strTemp);
}

/*------------------------------------------------------------------------
*功		 能：初始化定时器消息超时时间
*参 数 说 明：strTemp-Timeout的字符串
*返  回	 值：无
*说	     明：无
------------------------------------------------------------------------*/
void PeriodicConfig::InitPeriodicParamInterval(CString strTemp)
{
	for(UINT8_T uiLoop=0; uiLoop<ARR_MESSAGE_COUNT; uiLoop++)
		GetDlgItem(_arryInterval[uiLoop])->SetWindowTextW(strTemp);
}

/*------------------------------------------------------------------------
*功		 能：初始化定时器消息默认为空
*参 数 说 明：无
*返  回	 值：无
*说	     明：无
------------------------------------------------------------------------*/
void PeriodicConfig::InitPeriodicParamCheckBox()
{
	for(UINT8_T uiLoop=0; uiLoop<ARR_MESSAGE_COUNT; uiLoop++)
		((CButton*)GetDlgItem(_arryEnable[uiLoop]))->SetCheck(FALSE);
}

/*------------------------------------------------------------------------
*功		 能：清除单条定时器消息的配置数据
*参 数 说 明：uIndex-被清除定时器消息的索引
*返  回	 值：无
*说	     明：无
------------------------------------------------------------------------*/
void PeriodicConfig::SetPeriodicSingleMsgIsEmpty(UINT8_T uIndex)
{
	GetDlgItem(_arryMessagedata[uIndex])->SetWindowTextW(L"");
	GetDlgItem(_arryTxflags[uIndex])->SetWindowTextW(L"0x00000000");
	GetDlgItem(_arryInterval[uIndex])->SetWindowTextW(L"100");
	((CButton*)GetDlgItem(_arryEnable[uIndex]))->SetCheck(FALSE);
}

/*------------------------------------------------------------------------
*功		 能：设置按钮使能操作
*参 数 说 明：bBtn1，bBtn2， bBtn3-表示对应的按钮是否使能
*返  回	 值：无
*说	     明：无
------------------------------------------------------------------------*/
void PeriodicConfig::SetPeriodicBtnIsEnable(BOOL bBtn1, BOOL bBtn2, BOOL bBtn3)
{
	GetDlgItem(IDC_BTN_PERIODIC_CLEARALL)->EnableWindow(bBtn1);
	GetDlgItem(IDC_BTN_PERIODIC_CANCEL)->EnableWindow(bBtn2);
	GetDlgItem(IDC_BTN_PERIODIC_APPLY)->EnableWindow(bBtn3);
}

/*------------------------------------------------------------------------
*功		 能：比较当前定时器消息和上次配置的定时器消息是否完全相同
*参 数 说 明：periodicCur - 当前定时器消息；periodicLast - 之前定时器消息
*返  回	 值：如果完全相同，则返回TRUE；反之，返回FALSE
*说	     明：无
------------------------------------------------------------------------*/
BOOL PeriodicConfig::PeriodicParamCurIsEqualLast(PeriodicParam periodicCur, PeriodicParam periodicLast)
{
	if(!CompareBinaryData(periodicCur.pMessage, periodicLast.pMessage))
		return FALSE;

	if(periodicCur.ulTxFlags != periodicLast.ulTxFlags)
		return FALSE;

	if(periodicCur.ulIntervel != periodicLast.ulIntervel)
		return FALSE;
	return TRUE;
}

/*------------------------------------------------------------------------
*功		 能：获取当前索引的定时器消息配置内容
*参 数 说 明：uIndex - 表示当前索引
*返  回	 值：获取的定时器消息
*说	     明：无
------------------------------------------------------------------------*/
PeriodicParam PeriodicConfig::GetPeriodicValue(UINT8_T uIndex)
{
	PeriodicParam periodic;
	periodic.pMessage.ulResLen = bMessages[uIndex].ulResLen;
	memcpy(periodic.pMessage.ucConvertData, bMessages[uIndex].ucConvertData, bMessages[uIndex].ulResLen);

	periodic.ulTxFlags = uTxFlag[uIndex];
	periodic.ulIntervel = uInterval[uIndex];
	return periodic;
}

/*------------------------------------------------------------------------
*功		 能：判断当前定时器是否被使用
*参 数 说 明：pFilterId表示当前过滤器id
*返  回	 值：如果过滤器被使用，则返回TRUE; 否则返回FALSE
*说	     明：无
------------------------------------------------------------------------*/
BOOL PeriodicConfig::GetPeriodicUseStatus(UINT32_T pPeriodicId)
{
	if(pPeriodicId != 0)	// 此过滤器未被设置
		return TRUE;
	return FALSE;
}

/*------------------------------------------------------------------------
*功		 能：ClearAll 响应动作
*参 数 说 明：无
*返  回	 值：无
*说	     明：无
------------------------------------------------------------------------*/
void PeriodicConfig::OnBnClickedBtnPeriodicClearall()
{
	UINT32_T RetVal = 0;
	CString strProtocolName;
	InitPeriodicParamInterval(L"100");
	InitPeriodicParamMessagedata();
	InitPeriodicParamTxflags(L"0x00000000");
	InitPeriodicParamCheckBox();

	/* 获取当前通道号 */
	((CComboBox*)GetDlgItem(IDC_CB_PERIODICCONFIG_PROTOCOL))->GetLBText(((CComboBox*)GetDlgItem(IDC_CB_PERIODICCONFIG_PROTOCOL))->GetCurSel(), strProtocolName);
	for(UINT8_T uiLoop = 0; uiLoop < CONNECT_MAX_COUNT; uiLoop++)
	{
		if(ProtocolStrToValue(strProtocolName) == m_PeriodicSdk.m_Channel[uiLoop].GetProtocolId())
		{	
			uPeriodicCurrChannelNum = m_PeriodicSdk.m_Channel[uiLoop].GetChannelId();
			break;
		}
	}

	g_ThreadLock.Lock();
	RetVal = _PassThruIoctl(m_PeriodicSdk.m_Channel[uPeriodicCurrChannelNum].GetChannelId(), CLEAR_PERIODIC_MSGS, NULL, NULL);
	g_ThreadLock.Unlock();
	if (RetVal != J2534_STATUS_NOERROR)
		::MessageBoxW(NULL, _err.GetErrInfo(RetVal), L"Warnning!", MB_ICONINFORMATION);

	for(UINT8_T uiLoop = 0; uiLoop < CONNECT_MAX_COUNT; uiLoop++)
	{
		saveChannelperiodicdata[uiLoop].ChannelNumber = 0;
		for(UINT8_T ujLoop = 0; ujLoop < PERIODIC_MAX_COUNT; ujLoop++)
		{
			memset(&saveChannelperiodicdata[uiLoop]._periodicParam[ujLoop].pMessage, 0, sizeof(saveChannelperiodicdata[uiLoop]._periodicParam[ujLoop].pMessage));
			saveChannelperiodicdata[uiLoop]._periodicParam[ujLoop].ulTxFlags = 0;
			saveChannelperiodicdata[uiLoop]._periodicParam[ujLoop].ulIntervel = 100;
			saveChannelperiodicdata[uiLoop]._periodicParam[ujLoop].ulMsgId = 0;
			saveChannelperiodicdata[uiLoop]._periodicParam[ujLoop].bCheckStatus = FALSE;
		}
	}
	//SetPeriodicBtnIsEnable(TRUE, FALSE, FALSE);
}

/*------------------------------------------------------------------------
*功		 能：Apply 响应动作
*参 数 说 明：无
*返  回	 值：无
*说	     明：无
------------------------------------------------------------------------*/
void PeriodicConfig::OnBnClickedBtnPeriodicApply()
{
	PeriodicParam pPeriodicCurrent[PERIODIC_MAX_COUNT];
	UINT32_T RetVal = 0;
	CString strMessages, strTxFlags, strInterval;
	BOOL bEqualStatus[PERIODIC_MAX_COUNT];				// 标记当前定时器是否发生修改：0-表示修改，1-表示未修改

	memset(bEqualStatus, 1, PERIODIC_MAX_COUNT);

	for(UINT8_T uiLoop = 0; uiLoop < PERIODIC_MAX_COUNT; uiLoop++)
	{
		memset(&pPeriodicCurrent[uiLoop], 0, sizeof(pPeriodicCurrent[uiLoop]));

		if(BST_CHECKED == ((CButton*)GetDlgItem(_arryEnable[uiLoop]))->GetCheck())
		{
			GetDlgItem(_arryMessagedata[uiLoop])->GetWindowTextW(strMessages);	
			bMessages[uiLoop] = StringToHex(strMessages);

			GetDlgItem(_arryTxflags[uiLoop])->GetWindowTextW(strTxFlags);
			uTxFlag[uiLoop] = _tcstol(strTxFlags, 0, 16);

			GetDlgItem(_arryInterval[uiLoop])->GetWindowTextW(strInterval);
			uInterval[uiLoop] = _tcstol(strInterval, 0, 10);

			pPeriodicCurrent[uiLoop] = GetPeriodicValue(uiLoop);	// 获取当前的Filter配置

			if(PeriodicParamCurIsEqualLast(pPeriodicCurrent[uiLoop], saveChannelperiodicdata[uPeriodicCurrChannelNum-1]._periodicParam[uiLoop]))//pFilterLast
				bEqualStatus[uiLoop] = 1;						// 表示数据未被修改
			else
				bEqualStatus[uiLoop] = 0;

			if(bEqualStatus[uiLoop] == 0)						// 修改过滤器之前，关闭之前过滤器
			{
				if(GetPeriodicUseStatus(saveChannelperiodicdata[uPeriodicCurrChannelNum-1]._periodicParam[uiLoop].ulMsgId))
				{
					g_ThreadLock.Lock();
					RetVal = m_PeriodicSdk.StopPeriodicMessage(uPeriodicCurrChannelNum, uOutPeriodicId[uiLoop]);
					g_ThreadLock.Unlock();
					PASSTHRUMESSAGEVIEW(RetVal, L"PassThruStopPeriodic()-OK");
				}
				g_ThreadLock.Lock();
				RetVal = m_PeriodicSdk.StartPeriodicMessage(uPeriodicCurrChannelNum, bMessages[uiLoop], uTxFlag[uiLoop], uInterval[uiLoop], &uOutPeriodicId[uiLoop]);
				g_ThreadLock.Unlock();
				PASSTHRUMESSAGEVIEW(RetVal, L"PassThruStartPeriodic()-OK");
			}	

			/* 进行定时器数据备份 */

			saveChannelperiodicdata[uPeriodicCurrChannelNum-1]._periodicParam[uiLoop].pMessage = CopyBinaryData(bMessages[uiLoop]);
			saveChannelperiodicdata[uPeriodicCurrChannelNum-1]._periodicParam[uiLoop].ulTxFlags = uTxFlag[uiLoop];
			saveChannelperiodicdata[uPeriodicCurrChannelNum-1]._periodicParam[uiLoop].ulIntervel = uInterval[uiLoop];
			saveChannelperiodicdata[uPeriodicCurrChannelNum-1]._periodicParam[uiLoop].ulMsgId = uOutPeriodicId[uiLoop];
			saveChannelperiodicdata[uPeriodicCurrChannelNum-1]._periodicParam[uiLoop].bCheckStatus = TRUE;
		}
		else
		{
			if (saveChannelperiodicdata[uPeriodicCurrChannelNum-1]._periodicParam[uiLoop].ulMsgId != 0)
			{
				g_ThreadLock.Lock();
				RetVal = m_PeriodicSdk.StopPeriodicMessage(uPeriodicCurrChannelNum, uOutPeriodicId[uiLoop]);
				g_ThreadLock.Unlock();
				PASSTHRUMESSAGEVIEW(RetVal, L"PassThruStopPeriodic()-OK");
			}
			/* 进行定时器数据备份 */
			BinaryData bBinary;
			bBinary.ucConvertData[0] = 0;
			bBinary.ulResLen = 0;
			saveChannelperiodicdata[uPeriodicCurrChannelNum-1]._periodicParam[uiLoop].pMessage = CopyBinaryData(bBinary);
			saveChannelperiodicdata[uPeriodicCurrChannelNum-1]._periodicParam[uiLoop].ulTxFlags = uTxFlag[uiLoop];
			saveChannelperiodicdata[uPeriodicCurrChannelNum-1]._periodicParam[uiLoop].ulIntervel = uInterval[uiLoop];
			saveChannelperiodicdata[uPeriodicCurrChannelNum-1]._periodicParam[uiLoop].ulMsgId = uOutPeriodicId[uiLoop];
			saveChannelperiodicdata[uPeriodicCurrChannelNum-1]._periodicParam[uiLoop].bCheckStatus = FALSE;
			saveChannelperiodicdata[uPeriodicCurrChannelNum-1]._periodicParam[uiLoop].ulMsgId = 0;
			SetPeriodicSingleMsgIsEmpty(uiLoop);
		}
	}
	SetPeriodicBtnIsEnable(TRUE, TRUE, TRUE);
}

/*------------------------------------------------------------------------
*功		 能：Cancel 响应动作
*参 数 说 明：无
*返  回	 值：无
*说	     明：无
------------------------------------------------------------------------*/
void PeriodicConfig::OnBnClickedBtnPeriodicCancel()
{
	InitPeriodicParamInterval(L"100");
	InitPeriodicParamMessagedata();
	InitPeriodicParamTxflags(L"0x00000000");
	InitPeriodicParamCheckBox();
	//SetPeriodicBtnIsEnable(TRUE, FALSE, FALSE);
}

/*------------------------------------------------------------------------
*功		 能：Periodic配置参数只接收16进制字符串
*参 数 说 明：strTemp - Edit的输入字符串；
		    uCtlId - EditId
*返  回	 值：无
*说	     明：无
------------------------------------------------------------------------*/
void PeriodicConfig::SetPeriodicParamInputLegal(CString strTemp, UINT32_T uCtlId)
{
	INT8_T	nChar;
	BOOL	bIsLegal = 0;		// 0-表示不合法； 1-表示合法
	
	for(UINT8_T uiLoop = 0; uiLoop < strTemp.GetLength(); uiLoop++)
	{
		nChar = strTemp.GetAt(uiLoop);
		if((nChar >= '0' && nChar <= '9')||(nChar >= 'A' && nChar <= 'F')||(nChar >= 'a' && nChar <= 'f'))
			bIsLegal = 1;
		if(!bIsLegal)
		{
			strTemp = strTemp.Left(uiLoop);
			GetDlgItem(uCtlId)->SetWindowText(strTemp);
			((CEdit*)GetDlgItem(uCtlId))->SetSel(uiLoop,uiLoop,TRUE);
		}
	}
}


/*------------------------------------------------------------------------
*功		 能：Periodic配置参数只接收16进制字符串; 
			CEdit控件触发改变内容使之设为有效，无改变则为无效
*参 数 说 明：uCtlId - EditId
*返  回	 值：无
*说	     明：无
------------------------------------------------------------------------*/
void PeriodicConfig::HandlePeriodicMessagedata(UINT32_T uEdCtlId, UINT32_T uCkCtlId)
{
	CString	strMsgData;
	INT8_T	nChar;

	GetDlgItem(uEdCtlId)->GetWindowTextW(strMsgData);
	if(strMsgData.GetLength()==1)
	{
		nChar = strMsgData.GetAt(0);
		if(!((nChar >= '0' && nChar <= '9')||(nChar >= 'A' && nChar <= 'F')||(nChar >= 'a' && nChar <= 'f')))
			strMsgData="";
		if(strMsgData == "")
		{
			((CButton*)GetDlgItem(uCkCtlId))->SetCheck(FALSE);
			//SetPeriodicBtnIsEnable(TRUE, FALSE, FALSE);
		}
		else
		{
			((CButton*)GetDlgItem(uCkCtlId))->SetCheck(TRUE);
			//SetPeriodicBtnIsEnable(TRUE, TRUE, TRUE);
		}
	}

}

void PeriodicConfig::OnChangeEdPeriodicconfigMessagedata0()
{
	CString	strMsgData0;

	GetDlgItem(IDC_ED_PERIODICCONFIG_MESSAGEDATA0)->GetWindowTextW(strMsgData0);

	if(strMsgData0 == L"")
	{
		((CButton*)GetDlgItem(IDC_CK_PERIODICCONFIG_ENABLE0))->SetCheck(FALSE);
		//SetPeriodicBtnIsEnable(TRUE, FALSE, FALSE);
	}
	else
	{
		((CButton*)GetDlgItem(IDC_CK_PERIODICCONFIG_ENABLE0))->SetCheck(TRUE);
		//SetPeriodicBtnIsEnable(TRUE, TRUE, TRUE);
	}
}

void PeriodicConfig::OnChangeEdPeriodicconfigMessagedata1()
{
	CString strMsgData1;
	((CButton*)GetDlgItem(IDC_CK_PERIODICCONFIG_ENABLE1))->SetCheck(TRUE);
	//SetPeriodicBtnIsEnable(TRUE, TRUE, TRUE);
	GetDlgItem(IDC_ED_PERIODICCONFIG_MESSAGEDATA1)->GetWindowTextW(strMsgData1);
	if(strMsgData1 == L"")
	{
		((CButton*)GetDlgItem(IDC_CK_PERIODICCONFIG_ENABLE1))->SetCheck(FALSE);
		//SetPeriodicBtnIsEnable(TRUE, FALSE, FALSE);
	}
}

void PeriodicConfig::OnChangeEdPeriodicconfigMessagedata2()
{
	CString strMsgData2;
	((CButton*)GetDlgItem(IDC_CK_PERIODICCONFIG_ENABLE2))->SetCheck(TRUE);
	//SetPeriodicBtnIsEnable(TRUE, TRUE, TRUE);
	GetDlgItem(IDC_ED_PERIODICCONFIG_MESSAGEDATA2)->GetWindowTextW(strMsgData2);
	if(strMsgData2 == L"")
	{
		((CButton*)GetDlgItem(IDC_CK_PERIODICCONFIG_ENABLE2))->SetCheck(FALSE);
		//SetPeriodicBtnIsEnable(TRUE, FALSE, FALSE);
	}
}

void PeriodicConfig::OnChangeEdPeriodicconfigMessagedata3()
{
	CString strMsgData3;

	GetDlgItem(IDC_ED_PERIODICCONFIG_MESSAGEDATA3)->GetWindowTextW(strMsgData3);

	if(strMsgData3 == L"")
	{
		((CButton*)GetDlgItem(IDC_CK_PERIODICCONFIG_ENABLE3))->SetCheck(FALSE);
		//SetPeriodicBtnIsEnable(TRUE, FALSE, FALSE);
	}
	else
	{
		((CButton*)GetDlgItem(IDC_CK_PERIODICCONFIG_ENABLE3))->SetCheck(TRUE);
		//SetPeriodicBtnIsEnable(TRUE, TRUE, TRUE);
	}
}

void PeriodicConfig::OnChangeEdPeriodicconfigMessagedata4()
{
	CString strMsgData4;

	GetDlgItem(IDC_ED_PERIODICCONFIG_MESSAGEDATA4)->GetWindowTextW(strMsgData4);

	if(strMsgData4 == L"")
	{
		((CButton*)GetDlgItem(IDC_CK_PERIODICCONFIG_ENABLE4))->SetCheck(FALSE);
		//SetPeriodicBtnIsEnable(TRUE, FALSE, FALSE);
	}
	else
	{
		((CButton*)GetDlgItem(IDC_CK_PERIODICCONFIG_ENABLE4))->SetCheck(TRUE);
		//SetPeriodicBtnIsEnable(TRUE, TRUE, TRUE);
	}
}

void PeriodicConfig::OnChangeEdPeriodicconfigMessagedata5()
{
	CString strMsgData5;

	GetDlgItem(IDC_ED_PERIODICCONFIG_MESSAGEDATA5)->GetWindowTextW(strMsgData5);

	if(strMsgData5 == L"")
	{
		((CButton*)GetDlgItem(IDC_CK_PERIODICCONFIG_ENABLE5))->SetCheck(FALSE);
		//SetPeriodicBtnIsEnable(TRUE, FALSE, FALSE);
	}
	else
	{
		((CButton*)GetDlgItem(IDC_CK_PERIODICCONFIG_ENABLE5))->SetCheck(TRUE);
		//SetPeriodicBtnIsEnable(TRUE, TRUE, TRUE);
	}
}

void PeriodicConfig::OnChangeEdPeriodicconfigMessagedata6()
{
	CString strMsgData6;

	GetDlgItem(IDC_ED_PERIODICCONFIG_MESSAGEDATA6)->GetWindowTextW(strMsgData6);

	if(strMsgData6 == L"")
	{
		((CButton*)GetDlgItem(IDC_CK_PERIODICCONFIG_ENABLE6))->SetCheck(FALSE);
		//SetPeriodicBtnIsEnable(TRUE, FALSE, FALSE);
	}
	else
	{
		((CButton*)GetDlgItem(IDC_CK_PERIODICCONFIG_ENABLE6))->SetCheck(TRUE);
		//SetPeriodicBtnIsEnable(TRUE, TRUE, TRUE);
	}
}

void PeriodicConfig::OnChangeEdPeriodicconfigMessagedata7()
{
	CString strMsgData7;

	GetDlgItem(IDC_ED_PERIODICCONFIG_MESSAGEDATA7)->GetWindowTextW(strMsgData7);

	if(strMsgData7 == L"")
	{
		((CButton*)GetDlgItem(IDC_CK_PERIODICCONFIG_ENABLE7))->SetCheck(FALSE);
		//SetPeriodicBtnIsEnable(TRUE, FALSE, FALSE);
	}
	else
	{
		((CButton*)GetDlgItem(IDC_CK_PERIODICCONFIG_ENABLE7))->SetCheck(TRUE);
		//SetPeriodicBtnIsEnable(TRUE, TRUE, TRUE);
	}
}

void PeriodicConfig::OnChangeEdPeriodicconfigMessagedata8()
{
	CString strMsgData8;

	GetDlgItem(IDC_ED_PERIODICCONFIG_MESSAGEDATA8)->GetWindowTextW(strMsgData8);

	if(strMsgData8 == L"")
	{
		((CButton*)GetDlgItem(IDC_CK_PERIODICCONFIG_ENABLE8))->SetCheck(FALSE);
		//SetPeriodicBtnIsEnable(TRUE, FALSE, FALSE);
	}
	else
	{
		((CButton*)GetDlgItem(IDC_CK_PERIODICCONFIG_ENABLE8))->SetCheck(TRUE);
		//SetPeriodicBtnIsEnable(TRUE, TRUE, TRUE);
	}
}

void PeriodicConfig::OnChangeEdPeriodicconfigMessagedata9()
{
	CString strMsgData9;

	GetDlgItem(IDC_ED_PERIODICCONFIG_MESSAGEDATA9)->GetWindowTextW(strMsgData9);

	if(strMsgData9 == L"")
	{
		((CButton*)GetDlgItem(IDC_CK_PERIODICCONFIG_ENABLE9))->SetCheck(FALSE);
		//SetPeriodicBtnIsEnable(TRUE, FALSE, FALSE);
	}
	else
	{
		((CButton*)GetDlgItem(IDC_CK_PERIODICCONFIG_ENABLE9))->SetCheck(TRUE);
		//SetPeriodicBtnIsEnable(TRUE, TRUE, TRUE);
	}
}

BOOL PeriodicConfig::PreTranslateMessage(MSG* pMsg)
{
	if (pMsg->message == WM_CHAR)
	{
		if(!((IS_HEX(pMsg->wParam))||(pMsg->wParam == 32)||(pMsg->wParam == 8)))//if(!((pMsg->wParam >= '0' && pMsg->wParam <= '9')||(pMsg->wParam >= 'A' && pMsg->wParam <= 'F')||(pMsg->wParam >= 'a' && pMsg->wParam <= 'f')||(pMsg->wParam == 32)||(pMsg->wParam == 8)))
			return TRUE;
	}

	if((pMsg->message == WM_KEYDOWN) && (pMsg->wParam == VK_ESCAPE))
		return TRUE;  
	else if((pMsg->message == WM_KEYDOWN) && (pMsg->wParam == VK_RETURN))
		return TRUE;
	else
		return CDialogEx::PreTranslateMessage(pMsg);
}


void PeriodicConfig::OnSysCommand(UINT nID, LPARAM lParam)
{
	// TODO: 在此添加消息处理程序代码和/或调用默认值
	if ((nID & 0xFFF0) == SC_CLOSE) 
	{
		ClearBit(g_GlobalStatus, 4);
		for(UINT8_T ujLoop = 1; ujLoop <= CONNECT_MAX_COUNT; ujLoop++)
		{
			for(UINT8_T uiLoop = 0; uiLoop < PERIODIC_MAX_COUNT; uiLoop++)
			{
				if (saveChannelperiodicdata[ujLoop-1]._periodicParam[uiLoop].bCheckStatus == TRUE)
				{
					SetBit(g_GlobalStatus,4);
					break;
				}
			}
		}
		//CDialog::OnSysCommand(nID, lParam);
		SendMessage(WM_CLOSE, 0, 0);
	} 
	else 
	{ 
		CDialog::OnSysCommand(nID, lParam); 
	} 
	CDialogEx::OnSysCommand(nID, lParam);
}


void PeriodicConfig::OnCbnSelchangeCbPeriodicconfigProtocol()
{
	CString strProtocolName;
	/* 获取当前通道号 */
	((CComboBox*)GetDlgItem(IDC_CB_PERIODICCONFIG_PROTOCOL))->GetLBText(((CComboBox*)GetDlgItem(IDC_CB_PERIODICCONFIG_PROTOCOL))->GetCurSel(), strProtocolName);
	for(UINT8_T uiLoop = 0; uiLoop < CONNECT_MAX_COUNT; uiLoop++)
	{
		if(ProtocolStrToValue(strProtocolName) == m_PeriodicSdk.m_Channel[uiLoop].GetProtocolId())
		{	
			uPeriodicCurrChannelNum = m_PeriodicSdk.m_Channel[uiLoop].GetChannelId();
			break;
		}
	}

	InitPeriodicParamInterval(L"100");
	InitPeriodicParamTxflags(L"0x00000000");
	UILoadPeriodicSavedParam();

	//UILoadPeriodicSdkParam();	
	SetPeriodicAllCtlsIsEnable(TRUE);
}
