﻿/*******************************************************************************
 * FILE: FilterConfig.cpp
 * This file is part of ALLScanner VCX software.
 * COPYRIGHT (C) ALLScanner 2008-2014. 
 *		
 * DESC: J2534Tool Periodic Config Parameters's Defines  
 * 
 * HISTORY:
 * Date			Author		Notes	
 * 2016-01-01	my			Create
*******************************************************************************/

#pragma once
#include "afxwin.h"
#include "../J2534/J2534Dll.h"
#include "../J2534/J2534.h"
#include "../J2534/J2534Log.h"
#include "../J2534/PassThruSdk.h"
#include "../J2534/MakeLock.h"
#include "../J2534/common.h"
#include "Common/ErrorCode.h"

// PeriodicConfig 对话框

class PeriodicConfig : public CDialogEx
{
	DECLARE_DYNAMIC(PeriodicConfig)

public:
	PeriodicConfig(CWnd* pParent = NULL);   // 标准构造函数
	virtual ~PeriodicConfig();
	PassThruSdk m_PeriodicSdk;
// 对话框数据
	enum { IDD = IDD_PERIODICCONFIG_DIALOG };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 支持

	DECLARE_MESSAGE_MAP()
public:
	virtual BOOL OnInitDialog();
	virtual BOOL PreTranslateMessage(MSG* pMsg);
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);

	afx_msg void OnBnClickedBtnPeriodicClearall();
	afx_msg void OnBnClickedBtnPeriodicCancel();
	afx_msg void OnBnClickedBtnPeriodicApply();

	afx_msg void OnChangeEdPeriodicconfigMessagedata0();
	afx_msg void OnChangeEdPeriodicconfigMessagedata1();
	afx_msg void OnChangeEdPeriodicconfigMessagedata2();
	afx_msg void OnChangeEdPeriodicconfigMessagedata3();
	afx_msg void OnChangeEdPeriodicconfigMessagedata4();
	afx_msg void OnChangeEdPeriodicconfigMessagedata5();
	afx_msg void OnChangeEdPeriodicconfigMessagedata6();
	afx_msg void OnChangeEdPeriodicconfigMessagedata7();
	afx_msg void OnChangeEdPeriodicconfigMessagedata8();
	afx_msg void OnChangeEdPeriodicconfigMessagedata9();
	
	void InitGlobalPeriodicParam();
	void InitPeriodicParamMessagedata();
	void InitPeriodicParamTxflags(CString strTemp);
	void InitPeriodicParamInterval(CString strTemp);
	void InitPeriodicParamCheckBox();
	void SetPeriodicAllCtlsIsEnable(BOOL bRes);
	void SetPeriodicSingleMsgIsEmpty(UINT8_T uIndex);
	void UILoadPeriodicSdkParam();
	void UILoadPeriodicSavedParam();
	void UIDisplayPeriodicConfig(BOOL bRes);

	void SetPeriodicBtnIsEnable(BOOL bBtn1, BOOL bBtn2, BOOL bBtn3);
	void SetPeriodicParamInputLegal(CString strTemp, UINT32_T uCtlName);
	void HandlePeriodicMessagedata(UINT32_T uEdCtlId, UINT32_T uCkCtlId);

	BOOL PeriodicParamCurIsEqualLast(PeriodicParam periodicCur, PeriodicParam periodicLast);
	BOOL GetPeriodicUseStatus(UINT32_T pPeriodicId);
	PeriodicParam GetPeriodicValue(UINT8_T uiCount);
	afx_msg void OnCbnSelchangeCbPeriodicconfigProtocol();
};


